<?php
/**
 * comp_db_table
 *
 * @package zendframeworkLibrary
 * @version $Id: table.php,v 1.11 2014/04/07 15:50:05 faust Exp $
 * @copyright (c) QuestBack http://www.questback.com
 * @author $Author: faust $
 */

class comp_db_table extends Zend_Db_Table
{

  // the error code the statement adapter throws when
  // a table does not exist
  const ERR_TABLE_NOT_EXIST = 1146;


  /**
   * __call
   *
   * @param string $name
   * @param array $args
   * @return array
   */
  public function __call($name, $args)
  {
    if (method_exists($this->_db, $name))
      return $this->_db->$name($args);
    return array();
  }

  /**
   * Drop the tables from $this->>definitions
   */
  protected function dropTable()
  {
    foreach ($this->_definitions as $table => $fields)
      $this->_db->query('DROP TABLE IF EXISTS ' . $table);
  }

  /**
   * Creates the table according to the _definition but
   * only if the table does not exist yet.
   */
  protected function createTable()
  {
    foreach ($this->_definitions as $table => $fields)
    {
      $sql = array();

      foreach ($fields as $def)
      {
        if (array_key_exists('extra', $def))
          $sql[] = $def['name'] . ' ' . $def['type'] . ' ' . $def['extra'];
        else
          $sql[] = $def['name'] . ' ' . $def['type'];
      }

      $this->_db->query('CREATE TABLE IF NOT EXISTS ' . $table . '(' . implode($sql, ',') . ')');
    }
  }

  /**
   * Catch statement errors
   *
   * @param string $sql
   * @param array $para
   * @param bool $tryCreate
   * @throws Exception
   * @return \Zend_Db_Statement_Interface
   */
  protected function query($sql, $para = array(), $tryCreate = true)
  {
    try
    {
      return $this->_db->query($sql, $para);
    }
    catch (Zend_Db_Statement_Exception $e)
    {
      // when table does not exist yet try to create it
      if ($tryCreate == true && $e->getCode() == self::ERR_TABLE_NOT_EXIST)
      {
        //$this -> dropTable();
        $this->createTable();
        return $this->query($sql, $para, false);
      }
      else
      {
        throw new Exception($e->getMessage(), $e->getCode(), $e);
      }
    }
  }
}