<?php
  /**
   * efs_models_abort
   *
   * @package   zendframeworkLibrary
   * @version   $Id: abort.php,v 1.11 2013/10/16 09:27:49 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   * @deprecated
   * @notused
   * @tested
   */

  class efs_models_abort extends Exception
  {
    public $code = 0;

    /**
     * __construct
     *
     * @param     $msg
     * @param int $code
     */
    public function __construct($msg, $code = 0)
    {
      $this->code = $code;
      parent::__construct($msg, $code);
    }

    /**
     * __toString
     *
     * (non-PHPdoc)
     *
     * @see Exception::__toString()
     */
    public function __toString()
    {
      /** @noinspection PhpDeprecationInspection */
      return self::get_error(parent::getMessage(), parent::getCode(), parent::getFile(), parent::getLine());
    }

    /**
     * get_error
     *
     * @param string $msg
     * @param int    $code
     * @param string $script
     * @param string $line
     *
     * @return string
     */
    public function get_error($msg, $code, $script = NULL, $line = NULL)
    {
      return "<div style='color: red; font-size:23px; text-align: center;'>" . htmlspecialchars($msg)
             . (($code != 0) ? ("<br>Code: " . $code) : (""))
             . (($script != NULL) ? ("<br>Script: " . htmlspecialchars($script)) : (""))
             . (($line != NULL) ? ("<br>Line: " . htmlspecialchars($line)) : (""))
             . "</div>";
    }

    /**
     * print_error
     *
     * @param string $msg
     * @param int    $code
     * @param string $script
     * @param int    $line
     */
    public function print_error($msg, $code = 0, $script = null, $line = null)
    {
      /** @noinspection PhpDeprecationInspection */
      print self::get_error($msg, $code, $script, $line);
    }

    /**
     * check_and_print_error
     *
     * @param $arr_msg
     *
     * @return boolean
     */
    public function check_and_print_error($arr_msg)
    {
      if (!is_array($arr_msg))
        return false;
      if (count($arr_msg) > 0)
      {
        foreach ($arr_msg as $msg)
        {
          /** @noinspection PhpDeprecationInspection */
          self::print_error($msg);
        }
        return true;
      }
      return false;
    }

    /**
     * errorPreviewNotSupported
     */
    public function errorPreviewNotSupported()
    {
    }
  }