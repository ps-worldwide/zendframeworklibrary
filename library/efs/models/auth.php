<?php
  /**
   * efs_models_auth
   *
   * @tested
   * @package efs_models_session
   * @package efs_models_init
   * @uses    Pct_Efs_Version
   * @author  umschlag
   * @todo    test if really needed by any project
   */

  class efs_models_auth extends Zend_Auth_Adapter_DbTable
  {
    const DB_TABLE      = 'users';
    const DB_ACCOUNT    = 'u_account';
    const DB_CREDENTIAL = 'u_passwd';
    const DB_AUTH_METH  = 'MD5(?)';

    protected $efsVersion;

    /**
     * __construct
     *
     * @param mixed $id
     * @param mixed $cred
     * @param mixed $db
     * @param null  $efsVersion
     *
     * @return \efs_models_auth
     */
    public function __construct($id, $cred, $db, $efsVersion = null)
    {
      $this->setIdentity   = $id;
      $this->setCredential = $cred;
      if ($db !== NULL)
      {
        parent::__construct($db, self::DB_TABLE, self::DB_ACCOUNT, self::DB_CREDENTIAL, self::DB_AUTH_METH);
        $this->db = $db;
      }

      // Get EFS version if not passed
      if ($efsVersion == null)
        $efsVersion = Pct_Efs_Version::getVersion();

      $this->efsVersion = $efsVersion;
    }
  }
