<?php
  /**
   * efs_models_multibyte
   * multi-byte string handling
   * copied from [8.2]/modules/efs/components/multibyte.inc.php to be used in PCT-projects
   *
   * @abstract
   * @copyright (c) 1999-2012 QuestBack AG http://www.questback.de/
   * @namespace Efs
   * @globalsfree
   */

  abstract class efs_models_multibyte
  {
    const WORDWRAP_CHUNK_SIZE = 50;

    /**
     * @param $string
     *
     * @return array
     */
    public static function split2chars($string)
    {
      $chars = array();
      $pos   = 0;
      $end   = strlen($string);
      while ($pos < $end)
      {
        $i = ord($string[$pos]);

        if ($i < 0x80)
        {
          $chars[] = $string[$pos++];
        }
        elseif (($i&0xE0) == 0xC0)
        {
          $chars[] = substr($string, $pos, 2);
          $pos += 2;
        }
        elseif (($i&0xF0) == 0xE0)
        {
          $chars[] = substr($string, $pos, 3);
          $pos += 3;
        }
        elseif (($i&0xF8) == 0xF0)
        {
          $chars[] = substr($string, $pos, 4);
          $pos += 4;
        }
        elseif (($i&0xFC) == 0xF8)
        {
          $chars[] = substr($string, $pos, 5);
          $pos += 5;
        }
        elseif (($i&0xFE) == 0xFC)
        {
          $chars[] = substr($string, $pos, 6);
          $pos += 6;
        }
      }
      return $chars;
    }

    // Achtung, ungetestet
    /**
     * @param $string
     *
     * @return string
     */
    public static function strtoupper($string)
    {
      return mb_strtoupper($string);
    }

    // Achtung, ungetestet
    /**
     * @param     $haystack
     * @param     $needle
     * @param int $offset
     *
     * @return int
     */
    public static function strpos($haystack, $needle, $offset = 0)
    {
      return mb_strpos($haystack, $needle, $offset);
    }

    // Achtung, ungetestet
    // todo: $offset gibt es in mb_strrpos nicht!!!!
    /**
     * @param     $haystack
     * @param     $needle
     * @param int $offset
     *
     * @return int
     */
    public static function strrpos($haystack, $needle, $offset = 0)
    {
      return mb_strrpos($haystack, $needle, $offset);
    }

    // Achtung, ungetestet
    /**
     * @param     $haystack
     * @param     $needle
     * @param int $offset
     *
     * @return int
     */
    public static function stripos($haystack, $needle, $offset = 0)
    {
      return mb_strpos(mb_strtolower($haystack), mb_strtolower($needle), $offset);
    }

    // Achtung, ungetestet
    /**
     * @param $search
     * @param $replace
     * @param $string
     *
     * @return mixed
     */
    public static function str_replace($search, $replace, $string)
    {
      return str_replace($search, $replace, $string);
    }

    // Achtung, ungetestet
    /**
     * @param $search
     * @param $replace
     * @param $string
     *
     * @return mixed
     */
    public static function str_ireplace($search, $replace, $string)
    {
      if (is_array($search) and is_array($replace))
      {
        $search_regex = array();
        foreach ($search as $key => $value)
          $search_regex[$key] = "/" . preg_quote($value, "/") . "/iu";
      }
      else
        $search_regex = "/" . preg_quote($search, "/") . "/iu";

      return preg_replace($search_regex, $replace, $string);
    }

    /**
     * @param $haystack
     * @param $needle
     *
     * @return int
     */
    public static function substr_count($haystack, $needle)
    {
      return mb_substr_count($haystack, $needle);
    }

    // Achtung, ungetestet
    /**
     * @param      $string
     * @param      $start
     * @param null $length
     *
     * @return string
     */
    public static function substr($string, $start, $length = NULL)
    {
      // stj: HASS: mb_substr kann nicht $start>0 and $length===NULL
      if ($length === NULL)
        return mb_substr($string, $start);

      return mb_substr($string, $start, $length);
    }

    /**
     * mb_strlen wrapper
     *
     * @param $string
     *
     * @return int
     */
    public static function strlen($string)
    {
      return mb_strlen($string);
    }

    /**
     * gibt die Ordinalzahl eines Zeichens zurueck
     *
     * @param $char
     *
     * @return bool|int
     */
    public static function ord($char)
    {
      $first = ord($char[0]);
      if ($first <= 127)
        return $first;
      $second = ord($char[1]);
      if ($first >= 192 and $first <= 223)
        return ($first - 192) * 64 + $second - 128;
      $third = ord($char[2]);
      if ($first >= 224 and $first <= 239)
        return ($first - 224) * 4096 + ($second - 128) * 64 + $third - 128;
      $fourth = ord($char[3]);
      if ($first >= 240 and $first <= 247)
        return ($first - 240) * 262144 + ($second - 128) * 4096 + ($third - 128) * 64 + $fourth - 128;
      $fifth = ord($char[4]);
      if ($first >= 248 and $first <= 251)
        return ($first - 248) * 16777216 + ($second - 128) * 262144 + ($third - 128) * 4096 + ($fourth - 128) * 64 + $fifth - 128;
      $sixth = ord($char[5]);
      if ($first >= 252 and $first <= 253)
        return ($first - 252) * 1073741824 + ($second - 128) * 16777216 + ($third - 128) * 262144 + ($fourth - 128) * 4096 + ($fifth - 128) * 64 + $sixth - 128;
      return false;
    }

    /**
     * gibt die Ordinalzahlen aller Zeichen eines Strings als Array zurueck
     *
     * @param $char
     *
     * @return array
     */
    public static function get_ords($char)
    {
      $ords = array();

      $len = strlen($char);

      for ($i = 0; $i < $len; $i++)
      {
        $first = ord($char[$i]);
        if ($first <= 127)
          $ords[] = $first;
        else
        {
          $i++;
          $second = ord($char[$i]);
          if ($first >= 192 and $first <= 223)
            $ords[] = ($first - 192) * 64 + $second - 128;
          else
          {
            $i++;
            $third = ord($char[$i]);
            if ($first >= 224 and $first <= 239)
              $ords[] = ($first - 224) * 4096 + ($second - 128) * 64 + $third - 128;
            else
            {
              $i++;
              $fourth = ord($char[$i]);
              if ($first >= 240 and $first <= 247)
                $ords[] = ($first - 240) * 262144 + ($second - 128) * 4096 + ($third - 128) * 64 + $fourth - 128;
              else
              {
                $i++;
                $fifth = ord($char[$i]);
                if ($first >= 248 and $first <= 251)
                  $ords[] = ($first - 248) * 16777216 + ($second - 128) * 262144 + ($third - 128) * 4096 + ($fourth - 128) * 64 + $fifth - 128;
                else
                {
                  $i++;
                  $sixth = ord($char[$i]);
                  if ($first >= 252 and $first <= 253)
                    $ords[] = ($first - 252) * 1073741824 + ($second - 128) * 16777216 + ($third - 128) * 262144 + ($fourth - 128) * 4096 + ($fifth - 128) * 64 + $sixth - 128;
                }
              }
            }
          }
        }
      }
      return $ords;
    }

    /**
     * ist eins der Zeichen im String kein ASCII-Zeichen?
     *
     * @param $string
     *
     * @return bool
     */
    public static function is_ascii($string)
    {
      $len = strlen($string);
      for ($i = 0; $i < $len; $i++)
        if (ord($string[$i]) > 127)
          return false;

      return true;
    }

    /**
     * wieviel bytes verbraucht ein zeichen?
     *
     * @param $char
     *
     * @return int
     */
    public static function get_size($char)
    {
      $n = 0;
      $i = ord($char[0]);

      if ($i < 0x80)
        $n = 1; # 0bbbbbbb
      elseif (($i&0xE0) == 0xC0)
        $n = 2; # 110bbbbb
      elseif (($i&0xF0) == 0xE0)
        $n = 3; # 1110bbbb
      elseif (($i&0xF8) == 0xF0)
        $n = 4; # 11110bbb
      elseif (($i&0xFC) == 0xF8)
        $n = 5; # 111110bb
      elseif (($i&0xFE) == 0xFC)
        $n = 6; # 1111110b

      return $n;
    }

    /**
     * @param      $string
     * @param bool $encoding
     *
     * @return string
     */
    public static function strtolower($string, $encoding = false)
    {
      if (!$encoding)
      {
        return mb_strtolower($string);
      }
      else
      {
        return mb_strtolower($string, $encoding);
      }
    }

    /**
     * @param $string
     *
     * @return string
     */
    public static function ucfirst($string)
    {
      return self::strtoupper(self::substr($string, 0, 1)) . self::substr($string, 1);
    }

    /**
     * Nachbau von similar_text fuer UTF-8
     *
     * @param      $s1
     * @param      $s2
     * @param null $percent
     *
     * @internal param $char
     * @return int
     */
    public static function similar_text($s1, $s2, &$percent = NULL)
    {
      $table = array();

      $ords1 = self::get_ords($s1);
      $ords2 = self::get_ords($s2);

      $m = count($ords1);
      $n = count($ords2);

      if ($m + $n == 0)
      {
        $percent = 0.0;
        return 0;
      }

      for ($i = 0; $i < $m; $i++)
      {
        for ($j = 0; $j < $n; $j++)
        {
          if ($ords1[$i] == $ords2[$j])
            $table[$i + 1][$j + 1] = @$table[$i][$j] + 1;
          elseif (@$table[$i][$j + 1] >= @$table[$i + 1][$j])
            $table[$i + 1][$j + 1] = @$table[$i][$j + 1];
          else
            $table[$i + 1][$j + 1] = @$table[$i + 1][$j];
        }
      }

      $percent = (float)$table[$m][$n] * 200 / ($m + $n);
      return (int)$table[$m][$n];
    }

    /**
     * @param $string
     * @param $len
     * @param $end
     *
     * @return string
     */
    public static function chunk_split($string, $len, $end)
    {
      if (self::strlen($string) < $len)
        return $string;

      $chunk_string = "";

      for ($i = 0; $i < self::strlen($string); $i += $len)
        $chunk_string .= self::substr($string, $i, $len) . $end;

      return $chunk_string;
    }

    /**
     * @param        $string
     * @param        $length
     * @param string $pad_string
     * @param int    $pad_type
     *
     * @return string
     */
    public static function str_pad($string, $length, $pad_string = " ", $pad_type = STR_PAD_RIGHT)
    {
      /** @noinspection PhpUndefinedClassInspection */
      return str_pad($string, $length + strlen($string) - efs_multibyte::strlen($string), $pad_string, $pad_type);
    }

    /**
     * @param $string
     * @param $index
     *
     * @return string
     */
    public static function char_at($string, $index)
    {
      return self::substr($string, $index, 1);
    }

    /**
     * @param $string
     *
     * @return string
     */
    public static function str_shuffle($string)
    {
      if (self::strlen($string) == strlen($string))
        return str_shuffle($string);

      $positions = array();
      for ($i = 0; $i < self::strlen($string); $i++)
        $positions[] = $i;

      $new = "";
      shuffle($positions);
      for ($i = 0; $i < sizeof($positions); $i++)
        $new .= self::substr($string, $positions[$i], 1);

      return $new;
    }

    /**
     * unicode nummer zu einem utf-8 zeichen herausfinden
     *
     * @param $string
     *
     * @return int
     */
    public static function uninumber($string)
    {
      $c = self::char_at($string, 0);

      if (ord($c[0]) < 128)
        return ord($c[0]);

      $size = self::get_bytecount($string);

      $dec = 0;
      for ($i = 0; $i < $size; $i++)
      {
        if ($i == 0)
        {
          $xor = 0;
          for ($count = 0, $j = 128; $count < $size; $j /= 2, $count++)
            $xor += $j;

          $dec = ($dec<<6) + (ord($string[$i])^$xor);
        }
        else
          $dec = ($dec<<6) + (ord($string[$i])^128);
      }
      return (int)$dec;
    }

    /**
     * @param $dec
     *
     * @return string
     */
    public static function from_uninumber($dec)
    {
      if ($dec <= 127)
        return chr($dec);

      $decoded     = "";
      $count       = 1;
      $decoded_bin = "";
      while ($dec > 0)
      {
        $part = $dec&0x3F;
        $dec  = $dec>>6;
        if ($dec == 0)
        {
          $last_bits = 0;
          for ($i = 0, $j = 128; $i < $count; $j /= 2, $i++)
            $last_bits += $j;
          $decoded_int = $part|$last_bits;
        }
        else
          $decoded_int = $part|bindec("10000000");

        $decoded_bin = decbin($decoded_int) . " " . $decoded_bin;
        $decoded     = chr($decoded_int) . $decoded;
        $count++;
      }

      return $decoded;
    }

    /**
     * wieviel bytes verbraucht das erste unicode zeichen?
     *
     * @param $string
     *
     * @return int
     */
    public static function get_bytecount($string)
    {
      return self::get_size($string);
    }

    /**
     * @param        $string
     * @param int    $width
     * @param string $break
     * @param bool   $cut
     *
     * @return string
     */
    public static function wordwrap($string, $width = 75, $break = "\n", $cut = false)
    {
      if ($cut)
        return self::chunk_split($string, $width, $break);

      $break_len = mb_strlen($break);
      if ($break_len == 0 || $width == 0)
        return $string;

      $parts    = explode($break, $string);
      $partsize = sizeof($parts) - 1;

      $out = "";
      foreach ($parts as $key => $rest)
      {
        if ($key != $partsize)
          $rest .= $break;
        $strlen = mb_strlen($rest);
        while ($strlen)
        {
          if ($strlen <= $width)
          {
            $out .= $rest;
            continue(2);
          }

          $part = mb_substr($rest, 0, $width + $break_len);
          $pos  = mb_strpos($part, $break);

          if ($pos !== false)
          {
            $peter = $pos + $break_len;
            $out .= mb_substr($part, 0, $peter);
            $rest = mb_substr($rest, $peter);
            $strlen -= $peter;
            continue;
          }
          // mop: part nochens auf die laenge verkuerz0ren...sonst koennten wir theoretisch falsch breaken..es darf hoechstens das zeichen $width+1..nicht mehr...da oben $break_len verwendet wird, muss das so..
          $part = mb_substr($part, 0, $width + 1);
          $pos  = mb_strrpos($part, " ");

          if ($pos === false)
          {
            $pos = mb_strpos($rest, " ", $width);
            if ($pos === false)
            {
              $out .= $rest;
              continue(2);
            }
          }

          $out .= mb_substr($rest, 0, $pos) . $break;
          $rest = mb_substr($rest, $pos + 1);
          $strlen -= $pos + 1;
        }
      }
      return $out;
    }

    /**
     * verkuerzt einen String auf eine bestimmte Laenge, haengt ggf. ... dahinter. Zerschneidet nicht mitten in HTML-Entities
     *
     * @param        $string
     * @param int    $length
     * @param string $append
     * @param bool   $remove_whitespace
     *
     * @return mixed|string
     */
    public static function smart_trim($string, $length = 25, $append = "...", $remove_whitespace = true)
    {
      if ($remove_whitespace)
      {
        $string = trim($string);
        $string = preg_replace("/(\r\n|\r|\n)/", " ", $string);
        $string = preg_replace("/\s{2,}/", " ", $string);
      }
      /** @noinspection PhpUndefinedClassInspection */
      if (efs_multibyte::strlen($string) > $length)
      {
        /** @noinspection PhpUndefinedClassInspection */
        if (preg_match("/&[^;]*$/", efs_multibyte::substr($string, 0, $length)))
        {
          for ($i = $length; $i < $length + 10; $i++)
          {
            if ($string[$i] != ";")
              continue;
            else
              break;
          }
        }

        /** @noinspection PhpUndefinedClassInspection */
        return efs_multibyte::substr($string, 0, $length) . $append;
      }
      else
        return $string;
    }

    /**
     * @param        $value
     * @param        $length
     * @param string $delimiter
     *
     * @return string
     */
    public static function truncate($value, $length, $delimiter = "...")
    {
      // stj: schoen ist das nicht, aber hierdurch verhindern wir das Abschneiden in der Mitte von html-sonderzeichen (z. B. "hinz &a;... kunz" wird zu "hinz &... kunz")
      $must_escape   = false;
      $decoded_value = html_entity_decode($value, ENT_QUOTES, "UTF-8");
      if ($decoded_value != $value)
      {
        $value       = $decoded_value;
        $must_escape = true;
      }

      /** @noinspection PhpUndefinedClassInspection */
      if ($length && efs_multibyte::strlen($value) > $length)
      {
        /** @noinspection PhpUndefinedClassInspection */
        $value = efs_multibyte::substr($value, 0, (int)($length - 10 - efs_multibyte::strlen($delimiter))) . $delimiter . efs_multibyte::substr($value, -10);
      }

      if ($must_escape)
        $value = htmlspecialchars($value, ENT_QUOTES);

      return $value;
    }
  }

