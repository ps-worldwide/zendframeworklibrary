<?php
  /**
   * efs_models_config
   * Creates private zend-config object holding the efs-config settings (gp_conf)
   *
   * @uses      efs_apis_config
   * @package   zendframeworkLibrary
   * @version   $Id: config.php,v 1.14 2013/10/16 09:27:49 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class efs_models_config extends efs_apis_config
  {
    private $conf;

    /**
     * __construct
     *
     * @return \efs_models_config
     */
    public function __construct()
    {
      efs_apis_config::load_config(true);
      $this->conf = new Zend_Config(efs_apis_config::getConfig());
    }

    /**
     * config
     *
     */
    public function config()
    {
      return $this->conf;
    }

    /**
     * Returns the given configuration flag in Zend-Config manner
     *
     * @param $flag
     * @param $default
     *
     * @return mixed|null
     */
    public function get($flag, $default = null)
    {
      return $this->conf->get($flag, $default);
    }

    /**
     * Returns the path and binary-name to zip executable program
     */
    public function getZipBinary()
    {
      return $this->conf->get('zip_path') . 'zip';
    }

    /**
     * Returns the path and binary-name to the unzip executable program
     */
    public function getUnzipBinary()
    {
      return $this->conf->get('zip_path') . 'unzip';
    }

    /**
     *
     */
    public function isMaintenance()
    {
      return $this->conf->get('maintenance');
    }

    /**
     * checks wheather a given module is activated in EFS
     *
     * @param string $module
     *
     * @return bool $isActive
     */
    public function isModuleActive($module)
    {
      $activeModules = $this->conf->get('modules')->toArray();
      return (in_array($module, $activeModules));
    }
  }
