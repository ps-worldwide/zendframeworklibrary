<?php
  /**
   * efs_models_questionnaire
   * An EFS-questionnaire
   *
   * @package   Csv2Questionnaire
   * @version   $Id: questionnaire.php,v 1.10 2013/10/16 09:27:49 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   * @deprecated
   * @notused
   */
  class efs_models_questionnaire
  {
    /* DB-connection */
    private $db;

    /* The EFS - API */
    private $api;

    /* New project-ID of the questionnaire */
    private $pid;
    private $projectInfo;

    /**
     * __construct
     *
     * @param Zend_Db_Adapter_Abstract $db
     *
     * @return \efs_models_questionnaire
     */
    public function __construct(Zend_Db_Adapter_Abstract $db)
    {
      $this->db  = $db;
      $this->api = new efs_models_api($db);
      $this->reset();
    }

    /**
     * reset
     *
     * @return void
     */
    public function reset()
    {
      $this->pid         = 0;
      $this->projectInfo = array();
    }

    /**
     * getProperty
     *
     * @param mixed $prop
     *
     * @return null
     * @return null
     */
    public function getProperty($prop = NULL)
    {
      if ($prop !== NULL && array_key_exists($prop, $this->projectInfo))
        return $this->projectInfo[$prop];
      elseif ($prop !== NULL)
        return NULL;
      else
        return $this->projectInfo;
    }

    /**
     * init
     *
     * @param mixed $pid
     *
     * @return void
     */
    public function init($pid)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      $this->projectInfo = $this->api->getProjectInfo($pid);
      $this->pid         = $pid;
    }

    /**
     * initFromCopy
     *
     * @param mixed $srcpid
     *
     * @throws Exception
     */
    public function initFromCopy($srcpid)
    {
      /* Get information from the original project */
      /** @noinspection PhpUndefinedMethodInspection */
      $info = $this->api->getProjectInfo($srcpid);
      if ($info["project_id"] < 1)
        throw new Exception('Cannot initialise questionnaire from copy due' . ' to wrong source pid!');

      /* Copy the questionnaire */
      /** @noinspection PhpUndefinedMethodInspection */
      $this->pid = $this->api->copyProject($info['project_id'], $info['project_type'], $info['project_title'] . '-' . gmdate('Y-m-d_H:i:s'));

      /* Load project information of the new questionnaire */
      /** @noinspection PhpUndefinedMethodInspection */
      $this->projectInfo = $this->api->getProjectInfo($this->pid);
      return $this->pid;
    }

    /**
     * delete
     *
     * @internal param mixed $pid
     */
    public function delete()
    {
      /** @noinspection PhpUndefinedMethodInspection */
      $res = $this->api->deleteProject($this->pid);
      if ($res)
      {
        $this->pid         = NULL;
        $this->projectInfo = NULL;
        $this->reset();
      }
      return $res;
    }

    /**
     * addNewPage
     *
     * @param $title
     * @param $desc
     * @param $type
     *
     * @internal param mixed $prm
     */
    public function addNewPage($title, $desc, $type)
    {
      $param            = array();
      $param['pid']     = $this->pid;
      $param['pgtitle'] = $title;
      $param['pgdesc']  = $desc;
      $param['pgtype']  = $type;

      /** @noinspection PhpUndefinedMethodInspection */
      return $this->api->addNewPage($param);
    }

    /**
     * addTriggerRecode
     *
     * @param mixed $pgid
     */
    public function addTriggerRecode($pgid)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->api->addNewTriggerRecode($this->pid, $pgid);
    }

    // Wildcards / Placeholder

    /**
     * changeTranslation
     *
     * @param mixed $lang
     * @param mixed $type
     * @param mixed $id
     * @param mixed $text
     */
    public function changeTranslation($lang, $type, $id, $text)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->api->changeTranslation($this->pid, $lang, $id, $text);
    }
  }