<?php
  /**
   * efs_models_csvFile
   *
   * @package   zendframeworkLibrary
   * @version   $Id: csvFile.php,v 1.14 2013/10/16 09:27:49 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   * @notused
   * @deprecated
   */

  class efs_models_csvFile
  {
    private $filename = NULL;
    private $filepointer = NULL;

    private $act_file_pointer_pos = NULL;

    /* Parameter fuer die PHP fgetcsv() - Funktion */
    private $max_line_length = NULL;
    private $delimiter = NULL;
    private $enclosure = NULL;

    /* hat die CSV Datei Ueberschriften (0 || 1) */
    private $has_headline = 1;

    /* die aktuelle Zeilenummer, die eingelesen wurde */
    private $act_line_no = 0;

    /* der zuletzt eingelesene Inhalt */
    private $act_line_content = NULL;

    /* file cache damit man nicht nochmal alles neu einlesen muss */
    private $line_pointer_cache = NULL;

    /* Datenbank */
    private $db = NULL;

    /* table in which the csv file is stored */
    private $tablename = NULL;

    /* Error Messages */
    private $error = array();

    /**
     * __construct
     *
     * @param string $filename
     * @param int    $length
     * @param string $delim
     * @param string $encl
     * @param int    $headline
     * @param        $db
     * @param string $tablename
     */
    public function __construct($filename, $length = 1024, $delim = ",", $encl = "\"", $headline = 1, $db = NULL, $tablename = NULL)
    {
      $this->filename        = $filename;
      $this->max_line_length = $length;
      $this->delimiter       = $delim;
      $this->enclosure       = $encl;
      $this->has_headline    = $headline;

      $this->db        = $db;
      $this->tablename = $tablename;

      if (!efs_models_filefunx::fileExists($this->filename))
        $this->error[] = "No such file '" . $this->filename . "'";
      elseif (!efs_models_filefunx::isReadable($this->filename))
        $this->error[] = "File not readable";

      /* The first line starts at position 0 */
      $this->line_pointer_cache[1] = 0;
    }

    /**
     * __destruct
     */
    public function __destruct()
    {
    }

    /**
     * open
     *
     * @param string $mode
     *
     * @return boolean
     */
    public function open($mode = "r")
    {
      $this->filepointer = fopen($this->filename, $mode);

      /* Check utf-8 ByteOrderMark and remove it ... */
      $line = (string)fgets($this->filepointer, 4);
      if ($line == pack("CCC", 0xef, 0xbb, 0xbf))
        $this->line_pointer_cache[1] = 3;

      return true;
    }

    /**
     * close
     *
     * @return boolean
     */
    public function close()
    {
      return fclose($this->filepointer);
    }

    /**
     * get_error_msg
     *
     * @return array
     */
    public function get_error_msg()
    {
      return $this->error;
    }

    /**
     * get_line
     *
     * @param int $line_no
     *
     * @return NULL
     */
    public function get_line($line_no)
    {
      if ($this->act_line_no == $line_no)
        return $this->act_line_content;
      else
        $this->move_pointer_to_line($line_no);

      $this->read_from_filepointer();
      return $this->act_line_content;
    }

    /**
     * get_next_line
     *
     * @return NULL
     */
    public function get_next_line()
    {
      if ($this->file_end())
        return null;
      return $this->get_line(($this->act_line_no + 1));
    }

    /* is the filepointer at the end of the file? */
    /**
     * @return bool
     */
    public function file_end()
    {
      return feof($this->filepointer);
    }

    /*********************************************************************
     * Private
     ********************************************************************/
    /**
     * read_from_filepointer
     *
     * @return boolean
     */
    private function read_from_filepointer()
    {
      $this->act_line_content = fgetcsv($this->filepointer,
                                        $this->max_line_length,
                                        $this->delimiter,
                                        $this->enclosure);

      /* To what position in the file do act recently point to? */
      $this->act_file_pointer_pos = ftell($this->filepointer);

      /* As we read the line we know where the next line starts */
      $this->line_pointer_cache[($this->act_line_no + 1)] = $this->act_file_pointer_pos;
      return true;
    }

    /**
     * move_pointer_to_line
     *
     * @param int $line_no
     */
    private function move_pointer_to_line($line_no)
    {
      if ($this->line_pointer_cache[$line_no] !== NULL)
      {
        if ($this->act_file_pointer_pos !== $this->line_pointer_cache[$line_no])
        {
          fseek($this->filepointer, $this->line_pointer_cache[$line_no], SEEK_SET);
          $this->act_line_no = $line_no;
        }
      }
      else
      {
        die("Do not know where line " . $line_no . " starts!");
      }
    }

    /*********************************************************************
     * Database stuff
     ********************************************************************/

    /* get the youngest change date of the table */
    public function get_latest_updatetime($table, $timestamp = true)
    {
      /* Zeitzonenunterschied zwischen SQL-Server und Webserver beruecksichtigen */
      /** @noinspection PhpUndefinedMethodInspection */
      $sth = $this->db->execute("SHOW VARIABLES LIKE 'time_zone'");
      /** @noinspection PhpUndefinedMethodInspection */
      $data         = $this->db->fetch_assoc($sth);
      $old_timezone = $data["Value"];
      /** @noinspection PhpUndefinedMethodInspection */
      $this->db->execute("SET time_zone='" . addslashes(date_default_timezone_get()) . "'");

      /** @noinspection PhpUndefinedMethodInspection */
      $sth = $this->db->execute("SHOW TABLE STATUS LIKE '" . addslashes($table) . "'");
      if (!$sth)
        return 0;
      /** @noinspection PhpUndefinedMethodInspection */
      $data = $this->db->fetch_assoc($sth);

      /** @noinspection PhpUndefinedMethodInspection */
      $this->db->execute("SET time_zone='" . addslashes($old_timezone) . "'");

      if (!is_array($data) || @count($data) < 1)
        return 0;
      if ($timestamp)
        return (int)strtotime($data["Update_time"]);
      return $data["Update_time"];
    }

    /**
     * create_table_from_headline
     *
     * @param string $headline
     * @param string $table
     *
     * @return boolean
     */
    public function create_table_from_headline($headline, $table)
    {
      if (!is_array($headline) || @count($headline) < 1)
        return false;
      $table = str_replace("`", "\\`", $table);

      $query = array();

      /* prepare query to create table */
      foreach ($headline as $colname)
      {
        $colname = str_replace("`", "\\`", $colname);
        $query[] = "`" . $colname . "` TEXT NULL";
      }
      /** @noinspection PhpUndefinedMethodInspection */
      $sth = $this->db->execute("CREATE TABLE `" . $table . "` (" . implode(", ", $query) . ")");
      if (!$sth)
      {
        /** @noinspection PhpUndefinedMethodInspection */
        $this->error[] = $this->db->errstr($sth);
      }
      return $sth;
    }

    /**
     * copy_csv_to_temp
     *
     */
    public function copy_csv_to_temp()
    {
      /* copy file to /tmp to let mysql do the import */
      $temp_name = efs_models_filefunx::getTempFilename("", "tmp_", ".csv");
      if (!efs_models_filefunx::copyFile($this->filename, $temp_name))
        return NULL;
      return $temp_name;
    }

    /**
     * get_warnings
     *
     * @return boolean
     */
    public function get_warnings()
    {
      /** @noinspection PhpUndefinedMethodInspection */
      $sth = $this->db->execute("SHOW WARNINGS");
      /** @noinspection PhpUndefinedMethodInspection */
      if ($this->db->num_rows($sth) > 0)
      {
        /** @noinspection PhpUndefinedMethodInspection */
        while (($data = $this->db->fetch_assoc($sth)) != false) //Level, Code, Message
        $this->error[] = $data["Message"];
        return true;
      }
      return false;
    }

    /**
     * load_data_into_table
     *
     * @param $file
     * @param $table
     * @param $delimiter
     * @param $enclosure
     *
     * @return
     */
    public function load_data_into_table($file, $table, $delimiter, $enclosure)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      $sth = $this->db->execute("LOAD DATA LOCAL INFILE '" . addslashes($file) . "' INTO TABLE `" . addslashes($table) . "` FIELDS TERMINATED BY '" . addslashes($delimiter) . "' OPTIONALLY ENCLOSED BY '" . addslashes($enclosure) . "'");
      if (!$sth)
      {
        /** @noinspection PhpUndefinedMethodInspection */
        $this->error[] = $this->db->errstr($sth);
      }
      return $sth;
    }

    /* prueft ob die Datei erneuert wurde und pflegt sie dann ggf. (neu) in die DB ein */
    /**
     * @return bool
     */
    function update_csv_file_in_db()
    {
      /* open csv file */
      $this->open();

      /* Backup old table */
      $this->backup_table($this->tablename);

      /* read headline to create table */
      $headline = $this->get_next_line();

      /* Query to create table from headline */
      if (!$this->create_table_from_headline($headline, $this->tablename))
        return !$this->restore_table($this->tablename);

      /* insert data from csv into table */
      set_time_limit(5 * 60); //It might take a while to import the csv file

      $temp_name = $this->copy_csv_to_temp();
      if (!efs_models_filefunx::fileExists($temp_name))
        return !$this->restore_table($this->tablename, $temp_name);

      if (!$this->load_data_into_table($temp_name, $this->tablename, $this->delimiter, $this->enclosure))
        return !$this->restore_table($this->tablename, $temp_name);

      efs_models_filefunx::removeFile($temp_name);

      if ($this->get_warnings())
        return !$this->restore_table($this->tablename);

      /* delete headline */
      /** @noinspection PhpUndefinedMethodInspection */
      $sth = $this->db->execute("DELETE FROM `" . addslashes($this->tablename) . "` LIMIT 1");
      if (!$sth)
      {
        /** @noinspection PhpUndefinedMethodInspection */
        $this->error[] = $this->db->errstr();
        return !$this->restore_table($this->tablename);
      }
      /** @noinspection PhpUndefinedMethodInspection */
      if (!$this->db->execute("DROP TABLE IF EXISTS `" . $this->tablename . "_backup`"))
        $this->error[] = "ERROR::Unable to delete backup!";
      return true;
    }

    /**
     * backup_table
     *
     * @param $table
     *
     * @return boolean
     */
    function backup_table($table)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      $sth = $this->db->execute("SELECT * FROM `" . addslashes($table) . "` LIMIT 1");
      if ($sth)
      {
        /** @noinspection PhpUndefinedMethodInspection */
        $sth = $this->db->execute("RENAME TABLE `" . addslashes($table) . "` TO `" . addslashes($table) . "_backup`");
        if (!$sth)
          return false;
      }
      return true;
    }

    /**
     * restore_table
     *
     * @param $table
     * @param $file
     *
     * @return boolean
     */
    function restore_table($table, $file = NULL)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      $this->db->execute("DROP TABLE IF EXISTS `" . addslashes($table) . "`");
      /** @noinspection PhpUndefinedMethodInspection */
      $this->db->execute("RENAME TABLE `" . addslashes($table) . "_backup` TO `" . addslashes($table) . "`");
      if ($file != NULL)
        efs_models_filefunx::removeFile($file);
      return true;
    }
  }