<?php
/**
 * efs_models_filefunx
 *
 * names of dynamic methods use underscores in names, static ones use camelCase
 *
 * Better approach would have been:
 * @see http://de2.php.net/manual/de/language.oop5.overloading.php#object.callstatic
 *
 * public static function __callStatic($name, $arguments)
 * {
 *      $emf = new efs_models_filefunx();
 *      return call_user_method_array($name, $emf, $arguments);
 * }
 *
 * @package   zendframeworkLibrary
 * @version   $Id: filefunx.php,v 1.47 2021/05/19 13:58:24 fiedler Exp $
 * @copyright (c) QuestBack http://www.questback.com
 * @author    $Author: fiedler $
 */

/** @noinspection PhpUndefinedClassInspection */
class efs_models_filefunx //extends efs_apis_filefunx
{
    const DEFAULT_TEMP_DIR = "/opt/pdp/tmp";

    const FILE_CSV_MAX_ROWSIZE = 4194304;
    const FILE_INSPECT_SIZE = 128000;

    protected static $healself_logger;

    /**
     * @return string
     */
    public function get_separator()
    {
        if (DIRECTORY_SEPARATOR) {
            return DIRECTORY_SEPARATOR;
        } else {
            return "/";
        }
    }

    /**
     * @deprecated typo in name
     * @return string
     */
    public static function getSeperator()
    {
        return self::getSeparator();
    }

    /**
     * @deprecated typo in name
     * @return string
     */
    public function get_seperator()
    {
        return $this->get_separator();
    }

    /**
     * @return string
     */
    public static function getSeparator()
    {
        $emf = new efs_models_filefunx();

        return $emf->get_separator();
    }

    /**
     * @param      $filename
     * @param null $seperator
     *
     * @return string
     */
    public function append_slash($filename, $seperator = null)
    {
        if ($seperator === null) {
            $seperator = $this->get_separator();
        }

        if (substr($filename, -1) != $seperator) {
            $filename .= $seperator;
        }

        return $filename;
    }

    /**
     * @param $filename
     * @param $seperator
     *
     * @return string
     */
    public static function appendSlash($filename, $seperator = null)
    {
        $emf = new efs_models_filefunx();

        return $emf->append_slash($filename, $seperator);
    }

    /**
     * @param      $filename
     * @param bool $tail_only
     *
     * @return string
     */
    public function strip_slash($filename, $tail_only = true)
    {
        if (substr($filename, -1) == $this->get_separator()) {
            $filename = substr($filename, 0, -1);
        }

        if (!$tail_only) {
            if (substr($filename, 0, 1) == $this->get_separator()) {
                $filename = substr($filename, 1);
            }
        }

        return $filename;
    }

    /**
     * @param      $filename
     * @param bool $tail_only
     *
     * @return string
     */
    public static function stripSlash($filename, $tail_only = true)
    {
        $emf = new efs_models_filefunx();

        return $emf->strip_slash($filename, $tail_only);
    }

    /**
     * @param $filename
     * @param $extension
     *
     * @return string
     */
    public function ensure_extension($filename, $extension)
    {
        $extension = strtolower($extension);
        if (substr($extension, 0, 1) == ".") {
            $extension = substr($extension, 1);
        }
        $file_extension = strtolower($this->get_extension($filename));
        if ($file_extension != $extension) {
            return $filename . "." . $extension;
        } else {
            return $filename;
        }
    }

    /**
     * Entfernt komische Zeichen aus einem Dateinamen
     *
     * @param $filename
     *
     * @return mixed|string
     */
    public function make_nice_filename($filename)
    {
        $filename = strtr(
            $filename,
            array(
                "ä" => "a",
                "ü" => "u",
                "ö" => "o",
                "Ä" => "A",
                "Ü" => "U",
                "Ö" => "O",
                "ß" => "s",
                "(" => "_",
                ")" => "_"
            )
        );

        $filename = preg_replace("/[^a-zA-Z0-9_\-\.\(\)]/", "_", $filename);
        $filename = preg_replace("/_+/", "_", $filename);
        $filename = preg_replace("/(^_+|_+$)/", "", $filename);

        if (strlen($filename) == 0) {
            $filename = "file";
        }

        return $filename;
    }

    /**
     * Entfernt komische Zeichen aus einem Dateinamen
     *
     * @param $filename
     *
     * @return mixed|string
     */
    public static function makeNiceFilename($filename)
    {
        $emf = new efs_models_filefunx();

        return $emf->make_nice_filename($filename);
    }

    /**
     * @param $filename
     *
     * @return bool
     */
    public function is_htxxx_file($filename)
    {
        return (bool)preg_match('/^\.ht/', $this->get_filename($filename));
    }

    /**
     * @param $filename
     *
     * @return bool
     */
    public function is_evil_name($filename)
    {
        return !$this->is_filename($filename);
    }

    /**
     * @param $filename
     *
     * @return bool
     */
    public function is_filename($filename)
    {
        return !preg_match(
            '/(\.\.|%2e%2e|' . preg_quote($this->get_seperator(), "/") . "|%" . preg_quote(
                dechex(ord($this->get_seperator())),
                "/"
            ) . ")/i",
            $filename
        );
    }

    /**
     * @param $filename
     *
     * @return bool
     */
    public static function isFilename($filename)
    {
        $emf = new efs_models_filefunx();

        return $emf->is_filename($filename);
    }

    /**
     * Prueft, ob ein bestimmtes Pattern (PREG-Ausdruck) in einer Datei vorkommt
     *
     * @param $filename
     * @param $pattern
     *
     * @return int
     */
    public function file_contains_pattern($filename, $pattern)
    {
        return preg_match($pattern, $this->get_content($filename));
    }

    /**
     * @param $filename
     * @param $pattern
     *
     * @return int
     */
    public static function fileContainsPattern($filename, $pattern)
    {
        $emf = new efs_models_filefunx();

        return $emf->file_contains_pattern($filename, $pattern);
    }

    /**
     * Prueft, ob eine Datei einen bestimmten String enthaelt
     *
     * @param      $filename
     * @param      $string
     * @param bool $case_sensitive
     *
     * @return int
     */
    public function file_contains_string($filename, $string, $case_sensitive = true)
    {
        return $this->file_contains_pattern(
            $filename,
            "/" . preg_quote($string, "/") . "/" . ($case_sensitive ? "" : "i")
        );
    }

    /**
     * Schmeisst einen definierbaren Basispfad aus einem Dateinamen, z. B. macht strip_basepath("/tmp/foo/bar","/tmp"): foo/bar
     *
     * @param $filename
     * @param $basepath
     *
     * @return array|string
     */
    public function strip_basepath($filename, $basepath)
    {
        $basepath = $this->strip_slash($basepath);

        if (is_array($filename)) {
            $files = array();
            foreach ($filename as $file) {
                $file    = $this->strip_slash($file);
                $baselen = strlen($basepath);
                if (substr($file, 0, $baselen) == $basepath) {
                    $files[] = @substr($file, $baselen, strlen($file) - $baselen);
                }
            }

            return $files;
        } else {
            $filename = $this->strip_slash($filename);
            $baselen  = strlen($basepath);
            if (substr($filename, 0, $baselen) == $basepath) {
                $filename = @substr($filename, $baselen, strlen($filename) - $baselen);
            }

            return $this->strip_slash($filename, false);
        }
    }

    /**
     * @param $filename
     * @param $basepath
     *
     * @return array|string
     */
    public static function stripBasepath($filename, $basepath)
    {
        $emf = new efs_models_filefunx();

        return $emf->strip_basepath($filename, $basepath);
    }

    /**
     * @param $start_dir
     * @param $target_dir
     *
     * @return string
     */
    public static function make_relative_path($start_dir, $target_dir)
    {
        $real_path1 = self::stripSlash($start_dir); //self::get_realpath($path1);
        $real_path2 = self::stripSlash($target_dir); //self::get_realpath($path2);

        $count = 0;
        while ($real_path1 != substr($real_path2, 0, strlen($real_path1)) && strlen($real_path1) > 0) {
            $real_path1 = self::getDirname($real_path1);
            $count++;
        }

        return str_repeat("../", $count) . self::stripBasepath($real_path2, $real_path1);
    }

    /**
     * @param $filename
     *
     * @return string
     */
    public function strip_quotes($filename)
    {
        $delim_start = substr($filename, 0, 1);
        $delim_end   = substr($filename, -1);
        if (($delim_start == "\"" and $delim_end == "\"") or ($delim_start == "'" and $delim_end == "'")) {
            return substr($filename, 1, -1);
        } else {
            return $filename;
        }
    }

    /**
     * Haengt zwei Dateinamen so aneinander, dass es keine Probleme mit / gibt
     *
     * @return string
     */
    public function combine_path_names()
    {
        $result = "";
        foreach (func_get_args() as $path) { // func_get_args() not affected by changes by PHP7.x
            if ($path != "") {
                if ($result != "") {
                    $result .= $this->get_separator();
                    $result .= $this->strip_slash($path, false);
                } else {
                    $result .= $this->strip_slash($path, true);
                }
            }
        }

        return $result;
    }

    /**
     * @param $zipfile
     * @param $dest_path
     *
     * @return bool
     */
    public static function unzipFile($zipfile, $dest_path)
    {
        $emf = new efs_models_filefunx();

        return $emf->unzip_file($zipfile, $dest_path);
    }

    /**
     * @param $zipfile
     * @param $dest_path
     *
     * @return bool
     */
    public function unzip_file($zipfile, $dest_path)
    {
        if (!$this->is_file($zipfile)) {
            return false;
        }

        if (!$this->is_dir($dest_path)) {
            $this->create_dir_complete($dest_path);
        }

        if (!$this->is_dir($dest_path)) {
            return false;
        }

        $old_dir = $this->get_current_dir();
        $this->change_dir($dest_path);

        @exec("unzip -o " . escapeshellarg($zipfile));

        $this->change_dir($old_dir);

        return true;
    }

    /**
     * @param $file
     *
     * @return bool
     */
    public function is_in_temp_path($file)
    {
        $temp_path = $this->get_temp_path();
        if (substr(strtolower($file), 0, strlen($temp_path)) == strtolower($temp_path)) {
            return true;
        }

        return false;
    }

    /**
     * @param $file
     *
     * @return bool
     */
    public function is_in_export_path($file)
    {
        $temp_path = $this->get_export_path();
        if (substr(strtolower($file), 0, strlen($temp_path)) == strtolower($temp_path)) {
            return true;
        }

        return false;
    }

    /**
     * Gibt den temporaeren Pfad zurueck (aus config.inc.php3)
     *
     * @return mixed|null|string
     */
    public static function getTempPath()
    {
        $emf = new efs_models_filefunx();

        return $emf->get_temp_path();
    }

    /**
     * Gibt den temporaeren Pfad fuer Exporte zurueck (aus config.inc.php3)
     *
     * @return mixed|null|string
     */
    public static function getExportPath()
    {
        $emf = new efs_models_filefunx();

        return $emf->get_export_path();
    }

    /**
     * Gibt den temporaeren Pfad zurueck (aus config.inc.php3)
     *
     * @return mixed|null|string
     */
    public function get_temp_path()
    {
        $efsconf = new efs_models_config();

        if ($this->is_dir($efsconf->get("tmp_path")) and $this->is_writable(
                $efsconf->get("tmp_path")
            )
        ) // 2. Versuch: Konfigurationsvariable tmp_path
        {
            return $efsconf->get("tmp_path");
        }
    }

    /**
     * Gibt den temporaeren Pfad fuer Exporte zurueck (aus config.inc.php3)
     *
     * @return mixed|null|string
     */
    public function get_export_path()
    {
        $efsconf = new efs_models_config();

        if ($this->is_dir($efsconf->get("tmp_path_export")) and $this->is_writable(
                $efsconf->get("tmp_path_export")
            )
        ) // 2. Versuch: Konfigurationsvariable tmp_path_export
        {
            return $efsconf->get("tmp_path_export");
        }
    }

    /**
     * Legt ein neues temporaeres Verzeichnis an, Das Temp-Verzeichnis bekommt die Rechte 0777
     *
     * @param string $basedir
     * @param string $prefix
     *
     * @return bool|string
     */
    public function create_temp_dir($basedir = "", $prefix = "tmp_")
    {
        if ($basedir == "") {
            $basedir = $this->get_temp_path();
        }

        $base  = $this->append_slash($basedir) . $prefix;
        $tries = 0;
        while (++$tries < 20) {
            $name = md5(uniqid("", true));
            if (!$this->is_dir($base . $name)) {
                // Ins Temp-Verzeichnis darf jeder
                if (@$this->create_dir_complete($base . $name, 0777)) {
                    return $base . $name;
                }
            }
        }

        return false;
    }

    /**
     * Legt ein neues temporaeres Verzeichnis fuer Exporte an, Das Temp-Verzeichnis bekommt die Rechte 0777
     *
     * @param string $basedir
     * @param string $prefix
     *
     * @return bool|string
     */
    public function create_export_dir($basedir = "", $prefix = "tmp_")
    {
        if ($basedir == "") {
            $basedir = $this->get_export_path();
        }

        $base  = $this->append_slash($basedir) . $prefix;
        $tries = 0;
        while (++$tries < 20) {
            $name = md5(uniqid("", true));
            if (!$this->is_dir($base . $name)) {
                // Ins Temp-Verzeichnis darf jeder
                if (@$this->create_dir_complete($base . $name, 0777)) {
                    return $base . $name;
                }
            }
        }

        return false;
    }

    /**
     * Legt ein neues temporaeres Verzeichnis an, Das Temp-Verzeichnis bekommt die Rechte 0777
     *
     * @param string $basedir
     * @param string $prefix
     *
     * @return bool|string
     */
    public static function createTempDir($basedir = "", $prefix = "tmp_")
    {
        $emf = new efs_models_filefunx();

        return $emf->create_temp_dir($basedir, $prefix);
    }

    /**
     * Legt ein neues temporaeres Verzeichnis fuer Exporte an, Das Temp-Verzeichnis bekommt die Rechte 0777
     *
     * @param string $basedir
     * @param string $prefix
     *
     * @return bool|string
     */
    public static function createExportDir($basedir = "", $prefix = "tmp_")
    {
        $emf = new efs_models_filefunx();

        return $emf->create_export_dir($basedir, $prefix);
    }

    /**
     * Gibt den Namen fuer eine temporaere Datei zurueck
     * Die Datei wird durch die Funktion NICHT angelegt
     * wird kein $basedir angegeben, wird das Temp-Verzeichnis verwendet
     *
     * @param string $basedir
     * @param string $prefix
     * @param string $suffix
     *
     * @return bool|string
     */
    public function get_temp_filename($basedir = "", $prefix = "tmp_", $suffix = "")
    {
        if ($basedir == "") {
            $basedir = $this->get_export_path();
        }

        $base  = $this->append_slash($basedir) . $prefix;
        $tries = 0;
        while (++$tries < 20) {
            $name = sha1(uniqid("", true) . uniqid("", true)) . $suffix;
            if (!$this->is_file($base . $name)) {
                return $base . $name;
            }
        }

        return false;
    }

    /**
     * Gibt den Namen fuer eine temporaere Datei zurueck
     * Die Datei wird durch die Funktion NICHT angelegt
     * wird kein $basedir angegeben, wird das Temp-Verzeichnis verwendet
     *
     * @param string $basedir
     * @param string $prefix
     * @param string $suffix
     *
     * @return bool|string
     */
    public static function getTempFilename($basedir = "", $prefix = "tmp_", $suffix = "")
    {
        $emf = new efs_models_filefunx();

        return $emf->get_temp_filename($basedir, $prefix, $suffix);
    }

    /**
     * Gibt den Namen fuer eine temporaere Export-Datei zurueck
     * Die Datei wird durch die Funktion NICHT angelegt
     * wird kein $basedir angegeben, wird das Temp-Export-Verzeichnis verwendet
     *
     * @param string $basedir
     * @param string $prefix
     * @param string $suffix
     *
     * @return bool|string
     */
    public function get_export_filename($basedir = "", $prefix = "tmp_", $suffix = "")
    {
        if ($basedir == "") {
            $basedir = $this->get_export_path();
        }

        $base  = $this->append_slash($basedir) . $prefix;
        $tries = 0;
        while (++$tries < 20) {
            $name = sha1(uniqid("", true) . uniqid("", true)) . $suffix;
            if (!$this->is_file($base . $name)) {
                return $base . $name;
            }
        }

        return false;
    }

    /**
     * Gibt den Namen fuer eine temporaere Export-Datei zurueck
     * Die Datei wird durch die Funktion NICHT angelegt
     * wird kein $basedir angegeben, wird das Temp-Export-Verzeichnis verwendet
     *
     * @param string $basedir
     * @param string $prefix
     * @param string $suffix
     *
     * @return bool|string
     */
    public static function getExportFilename($basedir = "", $prefix = "tmp_", $suffix = "")
    {
        $emf = new efs_models_filefunx();

        return $emf->get_temp_filename($basedir, $prefix, $suffix);
    }

    /**
     * Listet alle Dateien im angegebenen Verzeichnis auf
     * Ist recurse true, steigt die Funktion rekursiv in alle Unterordner ab
     * Es kann auch ein Pattern (PREG-Ausdruck) angegeben werden, dem die Dateinamen entsprechen muessen
     *
     * @param        $dir
     * @param bool $recurse
     * @param string $pattern
     * @param bool $with_cvs
     *
     * @return array
     */
    public function enum_files($dir, $recurse = true, $pattern = "/.*/", $with_cvs = true)
    {
        $files = array();

        $dh = @opendir($dir);
        if ($dh) {
            $match_all = ($pattern == "/.*/");
            $base      = $this->append_slash($dir);
            while (($entry = @readdir($dh)) != false) {
                if ($entry != "." and $entry != "..") {
                    $entrypath = $base . $entry;
                    $is_dir    = is_dir($entrypath);
                    if ($recurse and $is_dir and ($with_cvs or (!$with_cvs and $entry != "CVS"))) {
                        $files = array_merge($files, $this->enum_files($entrypath, $recurse, $pattern, $with_cvs));
                    } elseif (!$is_dir and ($match_all or preg_match($pattern, $entry))) {
                        $files[] = $entrypath;
                    }
                }
            }
            @closedir($dh);
        }

        return $files;
    }

    /**
     * @param        $dir
     * @param bool $recurse
     * @param string $pattern
     * @param bool $with_cvs
     *
     * @return array
     */
    public static function enumFiles($dir, $recurse = true, $pattern = "/.*/", $with_cvs = true)
    {
        $emf = new efs_models_filefunx();

        return $emf->enum_files($dir, $recurse, $pattern, $with_cvs);
    }

    /**
     * Listet alle Verzeichnisse unterhalb des angegebenen Verzeichnisses auf
     * Ist recurse true, steigt die Funktion rekursiv in alle Unterordner ab
     * Es kann auch ein Pattern (PREG-Ausdruck) angegeben werden, dem die Dateinamen entsprechen muessen
     *
     * @param        $dir
     * @param bool $recurse
     * @param string $pattern
     * @param bool $with_cvs
     *
     * @return array
     */
    public function enum_dirs($dir, $recurse = true, $pattern = "/.*/", $with_cvs = true)
    {
        $dirs = array();

        $dh = @opendir($dir);
        if ($dh) {
            while (($entry = @readdir($dh)) != false) {
                if ($entry != "." and $entry != "..") {
                    $entrypath = $this->append_slash($dir) . $entry;
                    if (preg_match($pattern, $entry) and $this->is_dir(
                            $entrypath
                        ) and ($with_cvs or (!$with_cvs and $entry != "CVS"))
                    ) {
                        $dirs[] = $entrypath;
                    }

                    if ($this->is_dir($entrypath) && $recurse) {
                        $dirs = array_merge(
                            (array)$dirs,
                            (array)$this->enum_dirs($entrypath, $recurse, $pattern, $with_cvs)
                        );
                    }
                }
            }
            @closedir($dh);
        }

        return $dirs;
    }

    /**
     * @param        $dir
     * @param bool $recurse
     * @param string $pattern
     * @param bool $with_cvs
     *
     * @return array
     */
    public static function enumDirs($dir, $recurse = true, $pattern = "/.*/", $with_cvs = true)
    {
        $emf = new efs_models_filefunx();

        return $emf->enum_dirs($dir, $recurse, $pattern, $with_cvs);
    }

    /**
     * Loescht ein Verzeichnis mit allen Dateien und Unterverzeichnissen (rekursiv)
     *
     * @param       $dir
     * @param array $exclude
     *
     * @return bool
     */
    public function remove_dir($dir, $exclude = array())
    {
        $keep_dir = false;

        $dh = @opendir($dir);
        if ($dh) {
            while (false !== ($entry = @readdir($dh))) {
                if ($entry != "." and $entry != "..") {
                    $entrypath = $this->append_slash($dir) . $entry;
                    if (in_array($entry, $exclude)) {
                        $keep_dir = true;
                        continue;
                    }
                    if ($this->is_dir($entrypath)) {
                        $this->remove_dir($entrypath, array());
                    } else {
                        $this->remove_file($entrypath);
                    }
                }
            }
            @closedir($dh);
        }

        if ($keep_dir) {
            return true;
        }

        return @rmdir($dir);
    }

    /**
     * Loescht ein Verzeichnis mit allen Dateien und Unterverzeichnissen (rekursiv)
     *
     * @param       $dir
     * @param array $exclude
     *
     * @return bool
     */
    public static function removeDir($dir, $exclude = array())
    {
        $emf = new efs_models_filefunx();

        return $emf->remove_dir($dir, $exclude);
    }

    /**
     * Loescht eine Datei
     *
     * @param $filename
     *
     * @return bool
     */
    public function remove_file($filename)
    {
        if (!is_file($filename)) {
            return false;
        }

        return @unlink($filename);
    }

    /**
     * Alias fuer remove_file
     *
     * @param $filename
     *
     * @return bool
     */
    public static function removeFile($filename)
    {
        $emf = new efs_models_filefunx();

        return $emf->remove_file($filename);
    }

    /**
     * Alias fuer remove_file
     *
     * @deprecated
     * @see deleteFile()
     * @param $filename
     *
     * @return bool
     */
    public static function delete_file($filename)
    {
        return self::removeFile($filename);
    }

    /**
     * Alias fuer remove_file
     *
     * @param $filename
     *
     * @return bool
     */
    public static function deleteFile($filename)
    {
        return self::removeFile($filename);
    }

    /**
     * Delete multiple files in a directory
     * @param $dir
     * @param string $pattern
     * @return bool
     */
    public static function deleteMultipleFiles($dir, $pattern = '/*.*/')
    {
        $filesToBackup = self::enumFiles($dir, false, $pattern);
        if (count($filesToBackup) > 0) {
            foreach ($filesToBackup as $file) {
                self::removeFile($file);
            }
        }
    }

    /**
     * Alias fuer remove_file
     *
     * @param $filename
     *
     * @return bool
     */
    public function unlink($filename)
    {
        return self::removeFile($filename);
    }

    /**
     * Ermittelt die Groesse aller Dateien in einem Verzeichnis (rekursiv)
     *
     * @param $dir
     *
     * @return bool|int
     */
    public function get_dir_size($dir)
    {
        if (self::is_dir($dir)) {
            $size  = 0;
            $files = self::enumFiles($dir);
            foreach ($files as $file) {
                $size += self::getFilesize($file);
            }

            return $size;
        } else {
            return false;
        }
    }

    /**
     * Ermittelt die Groesse aller Dateien in einem Verzeichnis (rekursiv)
     *
     * @param $file1
     * @param $file2
     *
     * @return bool
     */
    public function compare_files($file1, $file2)
    {
        return self::getMd5($file1) === self::getMd5($file2);
    }

    /**
     * @param $file1
     * @param $file2
     *
     * @return bool
     */
    public static function compareFiles($file1, $file2)
    {
        $emf = new efs_models_filefunx();

        return $emf->compare_files($file1, $file2);
    }

    /**
     * @param $filename
     *
     * @return string
     */
    public function guess_archive_type($filename)
    {
        $head = $this->get_first_bytes($filename, 2);
        if ($head == "PK") {
            return "zip";
        } elseif ($head == chr(31) . chr(139)) {
            return "tgz";
        } else // Fallback
        {
            return "";
        }
    }

    /**
     * @param $filename
     *
     * @return string
     */
    public static function guessArchiveType($filename)
    {
        $emf = new efs_models_filefunx();

        return $emf->guess_archive_type($filename);
    }

    /**
     * @param       $zipfile
     * @param array $files
     *
     * @return bool|string
     */
    public static function extract_zip($zipfile, $files = array())
    {
        $dir = self::createExportDir();
        $old = self::getCurrentDir();

        self::changeDir($dir);

        $exec = "unzip -o " . escapeshellarg($zipfile) . " " . escapeshellarg(implode(" ", $files));
        @exec($exec);

        self::changeDir($old);

        return $dir;
    }

    /**
     * @param      $archive_filename
     * @param      $dest_dir
     * @param bool $create_dest_dir
     * @param bool $forced_archive_type
     * @param bool $check_archive_size
     *
     * @return bool
     */
    public static function extract_archive(
        $archive_filename,
        $dest_dir,
        $create_dest_dir = true,
        $forced_archive_type = false,
        $check_archive_size = true
    ) {
        $old_dir = self::getCurrentDir();
        if ($create_dest_dir) {
            self::createDirComplete($dest_dir);
        }

        if (!self::isDir($dest_dir)) {
            return false;
        }

        self::changeDir($dest_dir);

        if ($forced_archive_type) {
            $archive_type = $forced_archive_type;
        } else {
            $archive_type = strtolower(self::getExtension($archive_filename));
            if (!in_array($archive_type, array( "zip", "gz", "tgz" ))) {
                $archive_type = self::guessArchiveType($archive_filename);
            }
        }

        if ($check_archive_size) {
            $efsconf = new efs_models_config();

            //ub 01 Oct 2007: Größe des Archivinhalts ermitteln und mit Grenzwert aus der config vergleichen - Abbruch, wenn zu groß
            $max_archive_size_mb = $efsconf->get("max_archive_size_mb");
            if (empty($max_archive_size_mb)) {
                $max_archive_size_mb = 100;
            } //fallback in MB
            $archive_info = self::getArchiveInfo($archive_filename, $archive_type);

            //ub: Wenn bei der Größenermittlung etwas schief läuft (!isset) oder der Maximalwert überschritten würde, dann Abbruch
            if (!isset($archive_info["sum_bytes"]) or (int)$archive_info["sum_bytes"] > $max_archive_size_mb * 1024 * 1024) {
                return false;
            }
        }

        switch ($archive_type) {
            case "gz":
            case "tgz":
                exec("tar xfz " . escapeshellarg($archive_filename));
                break;
            case "zip":
                exec("unzip -o " . escapeshellarg($archive_filename), $retval, $out);
                break;
            default:
                return false;
        }

        self::change_dir($old_dir);

        return true;
    }

    /**
     * @param $archive1
     * @param $archive2
     *
     * @return bool
     */
    public function compare_archives($archive1, $archive2)
    {
        $result    = true;
        $checked   = array();
        $temp_dir1 = $this->create_export_dir();
        $temp_dir2 = $this->create_export_dir();

        $this->extract_archive($archive1, $temp_dir1);
        $this->extract_archive($archive2, $temp_dir2);

        foreach ($this->enum_files($temp_dir1, true) as $filename) {
            $rel_name  = $this->strip_basepath($filename, $temp_dir1);
            $checked[] = $rel_name;
            if (!$this->compare_files($filename, $this->combine_path_names($temp_dir2, $rel_name))) {
                $result = false;
                break;
            }
        }

        if ($result) {
            foreach ($this->enum_files($temp_dir2, true) as $filename) {
                $rel_name = $this->strip_basepath($filename, $temp_dir2);
                if (!in_array($rel_name, $checked)) {
                    $result = false;
                    break;
                }
            }
        }

        $this->remove_dir($temp_dir1);
        $this->remove_dir($temp_dir2);

        return $result;
    }

    /**
     * Erstellt via tar oder zip ein komprimiertes Archiv aus Quelldateien.
     *
     * Achtung: tar schnallt es anscheinend nicht, wenn man absolute Pfade angibt,
     * also muss man vorher in das Verzeichnis mit den Dateien wechseln und die relativen Dateinamen verwenden
     * $auto_add_extension haengt bei Bedarf noch die entsprechende Dateiendung hinten dran,
     * falls sie nicht schon im Dateinamen vorkommt
     *
     * @param        $destfile
     * @param array $source_files
     * @param string $type
     * @param bool $auto_add_extension
     *
     * @return string
     */
    public static function create_archive($destfile, $source_files = array(), $type = "zip", $auto_add_extension = true)
    {
        $dir = self::createExportDir();

        if (is_array($source_files)) {
            foreach ($source_files as $key => $source_file) {
                self::copyFile($source_file, self::appendSlash($dir) . self::getFilename($source_file));
                $source_files[$key] = self::getFilename($source_file);
            }
        } else {
            self::copyFile($source_files, self::appendSlash($dir) . self::getFilename($source_files));
            $source_files = array( self::getFilename($source_files) );
        }

        $destfile = self::createRealArchive($destfile, $dir, $source_files, $type, $auto_add_extension);
        self::removeDir($dir);

        return $destfile;
    }

    /**
     * @param        $destfile
     * @param array $source_files
     * @param string $type
     * @param bool $auto_add_extension
     *
     * @return string
     */
    public static function createArchive($destfile, $source_files = array(), $type = "zip", $auto_add_extension = true)
    {
        return self::create_archive($destfile, $source_files, $type, $auto_add_extension);
    }

    /**
     * @param        $destfile
     * @param        $dir
     * @param array $source_files
     * @param string $type
     * @param bool $auto_add_extension
     *
     * @return string
     */
    public static function createRealArchive(
        $destfile,
        $dir,
        $source_files = array(),
        $type = "zip",
        $auto_add_extension = true
    ) {
        $emf = new efs_models_filefunx();

        return $emf->create_real_archive($destfile, $dir, $source_files, $type, $auto_add_extension);
    }

    /**
     * @param        $destfile
     * @param        $dir
     * @param array $source_files
     * @param string $type
     * @param bool $auto_add_extension
     *
     * @return string
     */
    public function create_real_archive(
        $destfile,
        $dir,
        $source_files = array(),
        $type = "zip",
        $auto_add_extension = true
    ) {
        $conf = new efs_models_config();

        $old_dir = self::getCurrentDir();
        self::change_dir($dir);

        $archive_filename = self::getFilename($destfile);

        $esc_source_files = array();
        foreach ($source_files as $file) {
            $esc_source_files[] = escapeshellarg($file);
        }

        switch (strtolower($type)) {
            case "tar":
                if ($auto_add_extension and !preg_match('/\.(tar\.gz|tgz)$/i', $archive_filename)) {
                    $archive_filename .= ".tar.gz";
                }
                @exec("tar cvfz " . $archive_filename . " " . implode(" ", $esc_source_files), $out, $retval);
                break;

            case "bz":
            case "bz2":
                if ($auto_add_extension and !preg_match('/\.tar\.bz2?$/i', $archive_filename)) {
                    $archive_filename .= ".tar.bz2";
                }
                @exec("tar cvfj " . $archive_filename . " " . implode(" ", $esc_source_files), $out, $retval);
                break;

            case "zip":
            default:
                if ($auto_add_extension and !preg_match('/\.zip$/i', $archive_filename)) {
                    $archive_filename .= ".zip";
                }

                @exec(
                    $conf->getZipBinary() . " -9 -r " . escapeshellarg($archive_filename) . " " . implode(
                        " ",
                        $esc_source_files
                    ),
                    $out,
                    $retval
                );
                break;
        }

        $destfile = self::appendSlash(self::getDirname($destfile)) . $archive_filename;

        if (self::appendSlash($dir) . $archive_filename != $destfile) {
            self::copyFile(self::appendSlash($dir) . $archive_filename, $destfile);
        }
        if (self::appendSlash($dir) . $archive_filename != $destfile) {
            self::removeFile(self::appendSlash($dir) . $archive_filename);
        }
        self::changeDir($old_dir);

        return $destfile;
    }

    /**
     * @param        $destfile
     * @param        $srcdir
     * @param string $type
     * @param bool $auto_add_extension
     * @param string $pattern
     * @param bool $with_cvs
     *
     * @return string
     */
    public static function create_dir_archive(
        $destfile,
        $srcdir,
        $type = "zip",
        $auto_add_extension = true,
        $pattern = "/.*/",
        $with_cvs = false
    ) {
        $files = self::enumFiles($srcdir, true, $pattern, $with_cvs);

        $real_files = array();

        foreach ($files as $key => $file) {
            $real_files[$key] = self::stripBasepath($file, $srcdir);
        }

        return self::createRealArchive($destfile, $srcdir, $real_files, $type, $auto_add_extension);
    }

    /**
     * Gibt den Inhalt eines ZIP-Archivs zurück.
     *
     * Lässt via unzip -l den Inhalt des Archivs nach stdout ausgeben,
     * wertet die Ausgabe aus und packt die Details in ein Array.
     *
     * @param string $filename Dateiname des ZIP-Archivs.
     * @deprecated
     *
     * @return array
     */
    protected static function get_zip_content_info($filename)
    {
        return self::getZipContentInfo($filename);
    }

    /**
     * Gibt den Inhalt eines ZIP-Archivs zurück.
     *
     * Lässt via unzip -l den Inhalt des Archivs nach stdout ausgeben,
     * wertet die Ausgabe aus und packt die Details in ein Array.
     *
     * @param string $filename Dateiname des ZIP-Archivs.
     *
     * @return array
     */
    protected static function getZipContentInfo($filename)
    {
        $conf = new efs_models_config();

        $cmd    = $conf->get_unzip_binary() . " -l " . escapeshellarg($filename);
        $out    = array();
        $retval = false;
        @exec($cmd, $out, $retval);
        if ($retval !== 0) {
            return false;
        }

        $regex = '/\s*Archive:\s+' . preg_quote($filename, "/") . "/";
        if (!preg_match($regex, array_shift($out))) {
            return false;
        }

        $sum_bytes = 0;
        $files     = array();
        foreach ($out as $line) {
            $pieces = preg_split('/\s+/', trim($line));
            //nur Zeilen auswerten, in denen das Datum erwartungsgemäß formatiert und an der richtige Stelle ist
            if (preg_match('/\d{2}-\d{2}-\d{2}/', $pieces[1])) {
                $files[] = array(
                    "count_bytes" => $pieces[0],
                    "date"        => $pieces[1],
                    "time"        => $pieces[2],
                    "filename"    => $pieces[3]
                );
                $sum_bytes += (int)$pieces[0];
            }
        }

        $result = array( "files" => $files, "sum_bytes" => $sum_bytes );

        return $result;
    }

    /**
     * @param $filename
     * @deprecated
     *
     * @return array|bool
     */
    protected static function get_tar_content_info($filename)
    {
        return self::getTarContentInfo($filename);
    }

    /**
     * @param $filename
     *
     * @return array|bool
     */
    protected static function getTarContentInfo($filename)
    {
        $cmd    = "tar tvfz " . escapeshellarg($filename);
        $out    = array();
        $retval = false;
        @exec($cmd, $out, $retval);
        if ($retval !== 0) {
            return false;
        }

        $sum_bytes = 0;
        $files     = array();
        foreach ($out as $line) {
            $pieces = preg_split('/\s+/', trim($line));
            if (preg_match('/\d{4}-\d{2}-\d{2}/', $pieces[3])) {
                $record  = array(
                    "rights"      => array_shift($pieces),
                    "user"        => array_shift($pieces),
                    "count_bytes" => array_shift($pieces),
                    "date"        => array_shift($pieces),
                    "time"        => array_shift($pieces),
                    "filename"    => implode(" ", $pieces)
                );
                $files[] = $record;

                //ub: im Fall von Ordnern wird die Anzahl Bytes mit 0 angegeben, deshalb kann es einfach mitgezählt werden
                $sum_bytes += (int)$record["count_bytes"];
            }
        }

        $result = array( "files" => $files, "sum_bytes" => $sum_bytes );

        return $result;
    }

    /**
     * @param      $filename
     * @param bool $type
     * @deprecated wrong name for method
     *
     * @return array|bool
     */
    public static function get_archive_info($filename, $type = false)
    {
        return self::getArchiveInfo($filename, $type);
    }

    /**
     * @param      $filename
     * @param bool $type
     *
     * @return array|bool
     */
    public static function getArchiveInfo($filename, $type = false)
    {
        if (!$type) {
            $type = self::getExtension($filename);
        }

        switch ($type) {
            case "zip":
                return self::getZipContentInfo($filename);
            case "tgz":
            case "bz":
            case "bz2":
            case "tar.gz":
            case "gz":
                return self::getTarContentInfo($filename);
            default:
                return false;
        }
    }

    /**
     * @param $filename
     */
    public function touch($filename)
    {
        @touch($filename);
    }

    /**
     * @param $filename
     */
    public static function touchFile($filename)
    {
        $emf = new efs_models_filefunx();
        $emf->touch($filename);
    }

    // Legt ein Verzeichnis an
    // $mode muss als Oktalzahl angegeben werden!
    /**
     * @param     $dirname
     * @param int $mode
     *
     * @return bool
     */
    public function create_dir($dirname, $mode = 0777)
    {
        if (is_dir($dirname)) {
            return true;
        }
        $result = @mkdir($dirname, $mode);
        if ($result) {
            self::setRights($dirname, $mode);
        }

        return $result;
    }

    /**
     * @param     $dirname
     * @param int $mode
     *
     * @return bool
     */
    public static function createDir($dirname, $mode = 0777)
    {
        $emf = new efs_models_filefunx();

        return $emf->create_dir($dirname, $mode);
    }

    /**
     * Legt den kompletten Pfad zu einem Verzeichnis an, d. h. create_dir_complete("/foo/bar")
     * legt die Verzeichnisse "/foo" und "/foo/bar" an
     * $mode muss als Oktalzahl angegeben werden
     *
     * @param     $dirname
     * @param int $mode
     *
     * @return bool
     */
    public function create_dir_complete($dirname, $mode = 0777)
    {
        $parts = explode($this->get_separator(), $dirname);
        $path  = '';
        foreach ($parts as $part) {
            $path .= $this->get_separator() . $part;
            if (!$this->is_dir($path)) {
                $this->create_dir($path, $mode);
            }
        }

        return is_dir($dirname);
    }

    /**
     * @param     $dirname
     * @param int $mode
     *
     * @return bool
     */
    public static function createDirComplete($dirname, $mode = 0777)
    {
        $emf = new efs_models_filefunx();

        return $emf->create_dir_complete($dirname, $mode);
    }

    /**
     * @param       $source
     * @param       $dest
     * @param array $options
     *
     * @return bool
     */
    public function copy_file($source, $dest, $options = array())
    {
        $result = @copy($source, $dest);
        if (sizeof($options) == 0) {
            return $result;
        }

        if (in_array("p", $options)) {
            $this->set_rights($dest, $this->get_rights($source));
        }

        return $result;
    }

    /**
     * @param       $source
     * @param       $dest
     * @param array $options
     *
     * @return bool
     */
    public static function copyFile($source, $dest, $options = array())
    {
        $emf = new efs_models_filefunx();

        return $emf->copy_file($source, $dest, $options);
    }

    /**
     * Kopiert ein Verzeichnis mit seinen Unterverzeichnissen (rekursiv)
     * Das Zielverzeichnis wird automatisch erstellt, falls es nicht vorhanden ist
     * $mode muss als Oktalzahl angegeben werden
     *
     * @param      $sourcedir
     * @param      $destdir
     * @param int $mode
     * @param bool $with_cvs
     *
     * @return bool
     */
    public function copy_dir($sourcedir, $destdir, $mode = 0777, $with_cvs = true)
    {
        if (!$this->is_dir($sourcedir)) {
            return false;
        }

        $destdir = $this->append_slash($destdir);
        $files   = $this->enum_files($sourcedir, true, "/.*/", $with_cvs);

        foreach ($files as $file) {
            //$realname = $this->get_realpath($file);
            $relname = $this->strip_basepath($file, $sourcedir);

            $destfile = $this->append_slash($destdir) . $relname;
            if (!$this->is_dir($this->get_dirname($destfile))) {
                if (!$this->create_dir_complete($this->get_dirname($destfile), $mode)) {
                    return false;
                }
            }
            $this->copy_file($file, $destfile);
            $this->set_rights($destfile, $mode);
        }

        return true;
    }

    /**
     * Kopiert ein Verzeichnis mit seinen Unterverzeichnissen (rekursiv)
     * Das Zielverzeichnis wird automatisch erstellt, falls es nicht vorhanden ist
     * $mode muss als Oktalzahl angegeben werden
     *
     * @param      $sourcedir
     * @param      $destdir
     * @param int $mode
     * @param bool $with_cvs
     *
     * @return bool
     */
    public static function copyDir($sourcedir, $destdir, $mode = 0777, $with_cvs = true)
    {
        $emf = new efs_models_filefunx();

        return $emf->copy_dir($sourcedir, $destdir, $mode, $with_cvs);
    }

    /**
     * @param $source
     * @param $dest
     *
     * @return bool
     */
    public function rename_file($source, $dest)
    {
        if ($source != $dest) {
            return @rename($source, $dest);
        }

        return true;
    }

    /**
     * @param $source
     * @param $dest
     *
     * @return bool
     */
    public static function renameFile($source, $dest)
    {
        $emf = new efs_models_filefunx();

        return $emf->rename_file($source, $dest);
    }

    /**
     * @param     $filename
     * @param int $mode
     *
     * @return bool
     */
    public function set_rights($filename, $mode = 0777)
    {
        return @chmod($filename, $mode);
    }

    /**
     * @param     $filename
     * @param int $mode
     *
     * @return bool
     */
    public static function setRights($filename, $mode = 0777)
    {
        $emf = new efs_models_filefunx();

        return $emf->set_rights($filename, $mode);
    }

    /**
     * @param $filename
     *
     * @return int
     */
    public function get_rights($filename)
    {
        $this->stat_file($filename);

        return @fileperms($filename);
    }

    /**
     * @param     $filename
     * @param int $mode
     *
     * @return bool
     */
    public function chmod($filename, $mode = 0777)
    {
        return $this->set_rights($filename, $mode);
    }

    /**
     * @param     $dirname
     * @param int $mode
     *
     * @return bool
     */
    public function set_rights_r($dirname, $mode = 0777)
    {
        if (!is_dir($dirname)) {
            return $this->set_rights($dirname, $mode);
        }

        $ret = true;
        $dh  = @opendir($dirname);
        while (($file = @readdir($dh)) && $ret) {
            if ($file != "." && $file != "..") {
                $fullpath = $dirname . "/" . $file;
                $ret      = ($ret && $this->set_rights_r($fullpath, $mode));
            }
        }
        @closedir($dh);

        return $ret;
    }

    /**
     * @param     $dirname
     * @param int $mode
     *
     * @return bool
     */
    public static function setRightsR($dirname, $mode = 0777)
    {
        $emf = new efs_models_filefunx();

        return $emf->set_rights_r($dirname, $mode);
    }

    /**
     * @param $filename
     *
     * @return array
     */
    public function get_lines($filename)
    {
        return @file($filename);
    }

    /**
     * @param $filename
     *
     * @return string
     */
    public function get_content($filename)
    {
        return @file_get_contents($filename);
    }

    /**
     * @param $filename
     *
     * @return string
     */
    public static function getContent($filename)
    {
        $emf = new efs_models_filefunx();

        return $emf->get_content($filename);
    }

    /**
     * @param $filename
     */
    function print_content($filename)
    {
        @readfile($filename);
    }

    /**
     * @param $filename
     */
    public static function printContent($filename)
    {
        $emf = new efs_models_filefunx();
        $emf->print_content($filename);
    }

    /**
     * @param $filename
     * @param $content
     *
     * @return bool
     */
    public static function atomic_save($filename, $content)
    {
        $appendix = "." . md5(uniqid("", true));
        // stj: Datei zuerst unter temp-Namen erzeugen...
        if (!self::saveFile($filename . $appendix, $content)) {
            return false;
        }

        // stj: dann umbenennen, rename in derselben Partition ist atomar
        if (@rename($filename . $appendix, $filename)) {
            return true;
        }

        // stj: aufrauemen
        self::removeFile($filename . $appendix);

        return false;
    }

    /**
     * @param $filename
     * @param $content
     *
     * @return bool
     */
    function save($filename, $content)
    {
        $result = @file_put_contents($filename, $content);
        if ($result === false) {
            self::log("COULDN'T WRITE TO " . $filename);

            return false;
        }

        return true;
    }

    /**
     * @param $filename
     * @param $content
     *
     * @return bool
     */
    public static function saveFile($filename, $content)
    {
        $emf = new efs_models_filefunx();

        return $emf->save($filename, $content);
    }

    /**
     * @param $filename
     * @param $content
     *
     * @return bool
     */
    public function append($filename, $content)
    {
        return (boolean)file_put_contents($filename, $content, FILE_APPEND);
    }

    /**
     * @param $filename
     * @param $content
     *
     * @return bool
     */
    public static function appendToFile($filename, $content)
    {
        $emf = new efs_models_filefunx();

        return $emf->append($filename, $content);
    }

    /**
     * @param $source_files
     * @param $destfile
     *
     * @return bool
     */
    public function concat_files($source_files, $destfile)
    {
        if (is_array($source_files)) {
            foreach ($source_files as $source_file) {
                if (!$this->append($destfile, self::get_content($source_file))) {
                    return false;
                }
            }

            return true;
        } else {
            return $this->copy_file($source_files, $destfile);
        }
    }

    /**
     * @param $filename
     * @param $content
     *
     * @return bool
     */
    public static function saveGz($filename, $content)
    {
        $emf = new efs_models_filefunx();

        return $emf->save_gz($filename, $content);
    }

    /**
     * @param $filename
     * @param $content
     *
     * @return bool
     */
    public function save_gz($filename, $content)
    {
        $fp = @gzopen($filename, "w");
        if ($fp) {
            @fwrite($fp, $content);
            @fclose($fp);

            return true;
        }

        return false;
    }

    /**
     * @param $filename
     *
     * @return string
     */
    public function read_gz($filename)
    {
        $content = '';
        $fp      = @gzopen($filename, "r");
        if ($fp) {
            $content = @gzread($fp, $this->get_filesize($filename));
            @gzclose($fp);
        }

        return $content;
    }

    /**
     * @param      $content
     * @param null $dir
     *
     * @return bool|string
     */
    public function save_temp_file($content, $dir = null)
    {
        if ($dir === null) {
            $dir = $this->get_export_path();
        }

        $filename = $this->get_temp_filename($dir);
        if ($this->save($filename, $content)) {
            return $filename;
        }

        return false;
    }

    /**
     * Startet einen Datei-Download
     *
     * @param        $filename
     * @param null $send_filename
     * @param bool $die
     * @param string $contentType
     *
     * @return bool
     */
    public function send_file(
        $filename,
        $send_filename = null,
        $die = false,
        $contentType = "application/x-file-download"
    ) {
        if (headers_sent()) {
            return false;
        }

        ob_end_clean();

        if ($send_filename === null) {
            $send_filename = $this->get_filename($filename);
        }

        header("Cache-control: must-revalidate", true);
        header("Pragma: x-file-download", true);
        header("Accept-Ranges: bytes", true);
        header("Content-Length: " . $this->get_filesize($filename), true);
        header("Content-Transfer-Encoding: binary", true);
        header("Content-Type: " . $contentType);
        header("Content-Disposition: filename=\"" . $send_filename . "\"\r\n");

        $this->print_content($filename);

        if ($die) {
            die();
        }

        return true;
    }

    /**
     * @param $string
     *
     * @return string
     */
    public function get_gz($string)
    {
        $tempname = $this->get_temp_filename("", "gz");
        $this->save_gz($tempname, $string);
        $value = $this->get_content($tempname);
        $this->remove_file($tempname);

        return $value;
    }

    /**
     * @param $dir
     *
     * @return bool
     */
    public function change_dir($dir)
    {
        return @chdir($dir);
    }

    /**
     * @param $dir
     *
     * @return bool
     */
    public static function changeDir($dir)
    {
        $emf = new efs_models_filefunx();

        return $emf->change_dir($dir);
    }

    /**
     * @return string
     */
    public function get_current_dir()
    {
        return getcwd();
    }

    /**
     * @return string
     */
    public static function getCurrentDir()
    {
        $emf = new efs_models_filefunx();

        return $emf->get_current_dir();
    }

    /**
     * @param $filename
     *
     * @return bool|string
     */
    public function get_md5($filename)
    {
        if ($this->file_exists($filename)) {
            return @md5_file($filename);
        } else {
            return false;
        }
    }

    /**
     * @param $filename
     *
     * @return bool|string
     */
    public static function getMd5($filename)
    {
        $emf = new efs_models_filefunx();

        return $emf->get_md5($filename);
    }

    /**
     * @param $filename
     *
     * @return bool|string
     */
    public function get_sha($filename)
    {
        if ($this->file_exists($filename)) {
            return @sha1_file($filename);
        } else {
            return false;
        }
    }

    /**
     * @param $filename
     *
     * @return bool|int
     */
    public function get_crc($filename)
    {
        if ($this->file_exists($filename)) {
            $content = $this->get_content($filename);

            return @crc32($content);
        } else {
            return false;
        }
    }

    /**
     * @param $filename
     *
     * @return bool
     */
    public function file_exists($filename)
    {
        return file_exists($filename);
    }

    /**
     * @param $filename
     *
     * @return bool
     */
    public static function fileExists($filename)
    {
        $emf = new efs_models_filefunx();

        return $emf->file_exists($filename);
    }

    /**
     * @param $filename
     *
     * @return bool
     */
    public function is_file($filename)
    {
        return is_file($filename);
    }

    /**
     * @param $filename
     *
     * @return bool
     */
    public static function isFile($filename)
    {
        $emf = new efs_models_filefunx();

        return $emf->is_file($filename);
    }

    /**
     * @param $filename
     *
     * @return bool
     */
    public function is_uploaded_file($filename)
    {
        return is_uploaded_file($filename);
    }

    /**
     * Liefert globale lang_id des Uploadfehlers wenn self::is_uploaded_file() false ist.
     *
     * @param string $input_name - Name des file Inputs im Formular: Bsp -> <input type="file" name=[$input_name]>
     *
     * @return string
     */
    public static function get_upload_file_error($input_name)
    {
        switch ($_FILES[$input_name]["error"]) {
            case UPLOAD_ERR_INI_SIZE:
                return "gl_UPLOAD_ERR_INI_SIZE";
            case UPLOAD_ERR_FORM_SIZE:
                return "gl_UPLOAD_ERR_FORM_SIZE";
            case UPLOAD_ERR_PARTIAL:
                return "gl_UPLOAD_ERR_PARTIAL";
            case UPLOAD_ERR_NO_FILE:
                return "gl_UPLOAD_ERR_NO_FILE";
            default:
                return "gl_UPLOAD_ERR_OK";
        }
    }

    /**
     * @param $dirname
     *
     * @return bool
     */
    public function is_dir($dirname)
    {
        return is_dir($dirname);
    }

    /**
     * @param $dirname
     *
     * @return bool
     */
    public static function isDir($dirname)
    {
        $emf = new efs_models_filefunx();

        return $emf->is_dir($dirname);
    }

    /**
     * Prueft, ob Dateien in einem Verzeichnis vorhanden sind
     *
     * @param $dirname
     *
     * @return bool
     */
    public function is_dir_empty($dirname)
    {
        $files = $this->enum_files($dirname, true);

        return count($files) == 0;
    }

    /**
     * @param $filename
     *
     * @return bool
     */
    public function is_readable($filename)
    {
        $this->stat_file($filename);

        return is_readable($filename);
    }

    /**
     * @param $filename
     *
     * @return bool
     */
    public static function isReadable($filename)
    {
        $emf = new efs_models_filefunx();

        return $emf->is_readable($filename);
    }

    /**
     * @param $filename
     *
     * @return bool
     */
    public function is_writable($filename)
    {
        $this->stat_file($filename);

        return is_writable($filename);
    }

    /**
     * @param $filename
     *
     * @return bool
     */
    public function is_writeable($filename)
    {
        return $this->is_writable($filename);
    }

    /**
     * @param $filename
     *
     * @return bool
     */
    public static function isWriteable($filename)
    {
        $emf = new efs_models_filefunx();

        return $emf->is_writable($filename);
    }

    /**
     * @param $filename
     *
     * @return bool
     */
    public function is_executable($filename)
    {
        $this->stat_file($filename);

        return is_executable($filename);
    }

    /**
     * @param      $filename
     * @param bool $suppress_dots
     *
     * @return string
     */
    public function get_dirname($filename, $suppress_dots = false)
    {
        $dirname = dirname($filename);
        if ($suppress_dots and $dirname == ".") {
            $dirname = "";
        }

        return $dirname;
    }

    /**
     * @param      $filename
     * @param bool $suppress_dots
     *
     * @return string
     */
    public static function getDirname($filename, $suppress_dots = false)
    {
        $emf = new efs_models_filefunx();

        return $emf->get_dirname($filename, $suppress_dots);
    }

    /**
     * @param $filename
     *
     * @return string
     */
    public function get_filename($filename)
    {
        return basename($filename);
    }

    /**
     * @param $filename
     *
     * @return string
     */
    public static function getFilename($filename)
    {
        $emf = new efs_models_filefunx();

        return $emf->get_filename($filename);
    }

    /**
     * @param $filename
     *
     * @return mixed
     */
    public function get_extension($filename)
    {
        $parts = $this->get_filename_parts($filename);

        return $parts["extension"];
    }

    /**
     * @param $filename
     *
     * @return mixed
     */
    public static function getExtension($filename)
    {
        $emf = new efs_models_filefunx();

        return $emf->get_extension($filename);
    }

    /**
     * @param $filename
     *
     * @return mixed
     */
    public function get_filename_parts($filename)
    {
        return @pathinfo($filename);
    }

    /**
     * @param $filename
     *
     * @return mixed
     */
    public static function getFilenameParts($filename)
    {
        $emf = new efs_models_filefunx();

        return $emf->get_filename_parts($filename);
    }

    /**
     * @param $filename
     *
     * @return string
     */
    public function get_realpath($filename)
    {
        return @realpath($filename);
    }

    /**
     * @param $filename
     *
     * @return string
     */
    public static function getRealpath($filename)
    {
        $emf = new efs_models_filefunx();

        return $emf->get_realpath($filename);
    }

    /**
     * @param $filename
     *
     * @return int
     */
    public function get_file_modification_time($filename)
    {
        $this->stat_file($filename);

        return @filemtime($filename);
    }

    /**
     * @param $filename
     *
     * @return int
     */
    public static function getFileModificationTime($filename)
    {
        $emf = new efs_models_filefunx();

        return $emf->get_file_modification_time($filename);
    }

    /**
     * @param $filename
     *
     * @return int
     */
    public function get_file_creation_time($filename)
    {
        $this->stat_file($filename);

        return @filectime($filename);
    }

    /**
     * @param $volume
     *
     * @return float
     */
    function get_free_space($volume)
    {
        return @disk_free_space($volume);
    }

    /**
     * @param $filename
     *
     * @return int
     */
    public function get_filesize($filename)
    {
        clearstatcache();
        $this->stat_file($filename);

        return @filesize($filename);
    }

    /**
     * @param $filename
     *
     * @return int
     */
    public static function getFilesize($filename)
    {
        $emf = new efs_models_filefunx();

        return $emf->get_filesize($filename);
    }

    /**
     * Gibt einen formatierten String zur uebergebenen Dateigroesse zurueck (z. B. "3.455 KB")
     *
     * @param        $size
     * @param int $decimals
     * @param string $decimal_seperator
     * @param string $thousand_seperator
     *
     * @return string
     */
    function get_size_string($size, $decimals = 1, $decimal_seperator = ",", $thousand_seperator = ".")
    {
        $units = array( "B", "KB", "MB", "GB" );
        $count = count($units) - 1;

        $i = 0;
        while ($i++ < $count and $size >= 1024) {
            $size /= 1024;
        }

        $i--;
        if ($i == 0) {
            $decimals = 0;
        }

        return number_format($size, $decimals, $decimal_seperator, $thousand_seperator) . " " . $units[$i];
    }

    /**
     * @param      $names
     * @param bool $ignore_readable
     * @param bool $accept_empty
     *
     * @return bool
     */
    public function check_files($names, $ignore_readable = false, $accept_empty = false)
    {
        if (!is_array($names)) {
            $names = array( $names );
        }

        foreach ($names as $name) {
            if (!$this->is_file($name)) {
                return false;
            }

            if (!$ignore_readable and !$this->is_readable($name)) {
                return false;
            }

            if (!$accept_empty and $this->get_filesize($name) == 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param     $filename
     * @param int $length
     *
     * @return bool|string
     */
    public function get_first_bytes($filename, $length = 4096)
    {
        if (!$this->file_exists($filename) or !$this->is_readable($filename)) {
            return false;
        }

        $fp = @fopen($filename, "r");
        if ($fp) {
            $result = @fread($fp, $length);
            @fclose($fp);

            return $result;
        }

        return false;
    }

    /**
     * @param     $filename
     * @param int $length
     *
     * @return bool|string
     */
    public static function getFirstBytes($filename, $length = 4096)
    {
        $emf = new efs_models_filefunx();

        return $emf->get_first_bytes($filename, $length);
    }

    /**
     * @param        $filename
     * @param null $delimiter
     * @param string $enclosure
     * @param null $max_rows
     * @param bool $skip_first_row
     *
     * @return array|bool
     */
    public static function getCsvValues(
        $filename,
        $delimiter = null,
        $enclosure = "\"",
        $max_rows = null,
        $skip_first_row = false
    ) {
        $emf = new efs_models_filefunx();

        return $emf->get_csv_values($filename, $delimiter, $enclosure, $max_rows, $skip_first_row);
    }

    /**
     * @param        $filename
     * @param null $delimiter
     * @param string $enclosure
     * @param null $max_rows
     * @param bool $skip_first_row
     *
     * @return array|bool
     */
    public function get_csv_values(
        $filename,
        $delimiter = null,
        $enclosure = "\"",
        $max_rows = null,
        $skip_first_row = false
    ) {
        if (!$this->file_exists($filename) or !$this->is_readable($filename)) {
            return false;
        }

        // stj: locale auf festen Wert setzen, denn fgetcsv ist davon abhaengig!
        /** @noinspection PhpUndefinedClassInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        efs_apis_locale::reset();

        if ($delimiter === null or $delimiter == "test") {
            $delimiter = $this->guess_delimiter($filename, array( ";", "\t", "," ), $enclosure);
        }

        if ($delimiter === null) {
            $delimiter = ",";
        }

        $old_error_level = error_reporting();
        error_reporting($old_error_level - E_WARNING);

        $fp = fopen($filename, "r");
        if ($fp) {
            $rows = array();

            if ($max_rows <= 0) {
                while (($row = fgetcsv($fp, self::FILE_CSV_MAX_ROWSIZE, $delimiter, $enclosure)) != false) {
                    $rows[] = $row;
                }
            } else {
                $current = 0;
                while (($row = fgetcsv($fp, self::FILE_CSV_MAX_ROWSIZE, $delimiter, $enclosure)) != false) {
                    $rows[] = $row;
                    if (++$current >= $max_rows) {
                        break;
                    }
                }
            }

            fclose($fp);
            error_reporting($old_error_level);

            if ($skip_first_row) {
                unset($rows[0]);
            }

            /** @noinspection PhpUndefinedClassInspection */
            /** @noinspection PhpUndefinedMethodInspection */
            efs_apis_locale::restore();

            return $rows;
        }

        /** @noinspection PhpUndefinedClassInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        efs_apis_locale::restore();

        return false;
    }

    /**
     * @param        $filename
     * @param array $test_delimiters
     * @param string $enclosure
     * @param int $test_rows
     *
     * @return bool|mixed
     */
    public static function guessDelimiter(
        $filename,
        $test_delimiters = array( ";", "\t", "," ),
        $enclosure = "\"",
        $test_rows = 10
    ) {
        $emf = new efs_models_filefunx();

        return $emf->guess_delimiter($filename, $test_delimiters, $enclosure, $test_rows);
    }

    /**
     * @param        $filename
     * @param array $test_delimiters
     * @param string $enclosure
     * @param int $test_rows
     *
     * @return bool|mixed
     */
    public function guess_delimiter(
        $filename,
        $test_delimiters = array( ";", "\t", "," ),
        $enclosure = "\"",
        $test_rows = 10
    ) {
        if (!$this->file_exists($filename) or !$this->is_readable($filename)) {
            return false;
        }

        if (!is_array($test_delimiters)) {
            return false;
        }

        $old_error_level = error_reporting();
        error_reporting($old_error_level - E_WARNING);

        $quoted_enclosure = preg_quote($enclosure, "/");
        $fp               = fopen($filename, "r");
        if ($fp) {
            // count every $test_delimiter
            $entries = array_flip($test_delimiters);
            foreach ($test_delimiters as $delimiter) {
                $quoted_delimiter = preg_quote($delimiter, "/");
                fseek($fp, 0);
                $tries  = 0;
                $buffer = fread($fp, self::FILE_INSPECT_SIZE);
                while ($tries++ < $test_rows and strlen($buffer)) {
                    while ($tries < $test_rows and strlen($buffer)) {
                        if (preg_match(
                            "/^(" . $quoted_enclosure . "[^" . $quoted_enclosure . "]*" . $quoted_enclosure . "|[^" . $quoted_delimiter . $quoted_enclosure . "]+)/",
                            $buffer,
                            $match
                        )) {
                            $buffer = substr($buffer, strlen($match[0]));
                        }
                        if (preg_match("/^" . $quoted_delimiter . "/", $buffer)) {
                            $entries[$delimiter] = (array_key_exists(
                                $delimiter,
                                $entries
                            )) ? $entries[$delimiter] + 1 : 1;
                            $buffer              = substr($buffer, 1);
                        } elseif (preg_match("/^(\r\n|\r|\n)/", $buffer, $match)) {
                            $buffer = substr($buffer, strlen($match[0]));
                            $tries++;
                        } elseif (preg_match("/(\r\n|\r|\n)/", $buffer, $match, PREG_OFFSET_CAPTURE)) {
                            $tries++;
                            if (is_array($match[1])) {
                                $buffer = substr($buffer, $match[1][1] + strlen($match[1][0]));
                            }
                        } else {
                            break(2);
                        }
                    }
                }
            }
            fclose($fp);
            error_reporting($old_error_level);

            @arsort($entries);
            @reset($entries);

            return @key($entries);
        }

        return false;
    }

    /**
     * @param        $filename
     * @param        $rows
     * @param string $separator
     * @param string $enclosure
     * @param string $line_terminator
     *
     * @return bool
     */
    function save_csv_values($filename, $rows, $separator = ";", $enclosure = "\"", $line_terminator = "\r\n")
    {
        if (!is_array($rows)) {
            return false;
        }

        $old_error_level = error_reporting();
        error_reporting($old_error_level - E_WARNING);
        $fp = fopen($filename, "w");
        if ($fp) {
            if ($enclosure == "\"") {
                $replacement = "\"\"";
            } //RFC-4189
            else {
                $replacement = "\\" . $enclosure;
            }
            $append   = $enclosure . $separator . $enclosure;
            $line_end = $enclosure . $line_terminator;
            $len      = strlen($separator . $enclosure);
            foreach ($rows as $row) {
                if (is_array($row)) {
                    $line = "";
                    foreach ($row as $col) {
                        $line .= $append . str_replace($enclosure, $replacement, $col);
                    }
                    $line = substr($line, $len) . $line_end;
                    fwrite($fp, $line);
                }
            }

            fclose($fp);
            error_reporting($old_error_level);

            return true;
        }

        return false;
    }

    /**
     * @param $filename
     *
     * @return string
     */
    public function harmonize_newlines($filename)
    {
        return system(
            "sed --in-place -e 's/" . chr(13) . "$//g' -e 's/" . chr(13) . "/\\n/g' " . escapeshellarg($filename)
        );
    }

    /**
     * @param     $filename
     * @param int $test_length
     *
     * @return mixed
     */
    public function get_line_ending($filename, $test_length = 32768)
    {
        $counter = array();

        $bytes = $this->get_first_bytes($filename, $test_length);
        preg_match_all("/(\r\n|\r|\n)/", $bytes, $matches);
        foreach ($matches[1] as $match) {
            $counter[$match]++;
        }

        arsort($counter);
        $keys = array_keys($counter);

        return $keys[0];
    }

    /**
     * @param healself_logger $logger
     */
    public static function attach_healself_logger(
        /** @noinspection PhpUndefinedClassInspection */
        healself_logger $logger
    ) {
        self::$healself_logger = $logger;
    }

    /**
     *
     */
    public static function detach_healself_logger()
    {
        self::$healself_logger = null;
    }

    /**
     * @param $logtext
     */
    public static function log($logtext)
    {
        if (self::$healself_logger) {
            /** @noinspection PhpUndefinedMethodInspection */
            self::$healself_logger->log(__CLASS__, $logtext);
        }
    }

    /**
     * @param $source
     * @param $target
     *
     * @return bool
     */
    public function move_file($source, $target)
    {
        return $this->rename_file($source, $target);
    }

    /**
     * @param $source
     * @param $target
     *
     * @return bool
     */
    public static function moveFile($source, $target)
    {
        $emf = new efs_models_filefunx();

        return $emf->rename_file($source, $target);
    }

    /**
     * @param $sourcedir
     * @param $destdir
     *
     * @return bool
     */
    public static function move_dir($sourcedir, $destdir)
    {
        if (!self::copyDir($sourcedir, $destdir)) {
            return false;
        }

        return self::removeDir($sourcedir, $destdir);
    }

    /**
     * @param $filename
     * @param $mode
     *
     * @return resource
     */
    public static function fopen($filename, $mode)
    {
        return @fopen($filename, $mode);
    }

    /**
     * @param $pointer
     */
    public static function fclose($pointer)
    {
        if (is_resource($pointer)) {
            @fclose($pointer);
        }
    }

    /**
     * @param      $pointer
     * @param      $data
     * @param null $length
     *
     * @return int
     */
    public static function fwrite($pointer, $data, $length = null)
    {
        if ($length !== null) {
            return fwrite($pointer, $data, (int)$length);
        } else {
            return fwrite($pointer, $data);
        }
    }

    /**
     * @param $pointer
     * @param $length
     *
     * @return string
     */
    public static function fread($pointer, $length)
    {
        return @fread($pointer, $length);
    }

    /**
     * @param $pointer
     *
     * @return bool
     */
    public static function feof($pointer)
    {
        return @feof($pointer);
    }

    /**
     * @param      $pointer
     * @param null $length
     *
     * @return string
     */
    public static function fgets($pointer, $length = null)
    {
        if ($length !== null) {
            return @fgets($pointer, (int)$length);
        } else {
            return @fgets($pointer);
        }
    }

    /**
     * @param $filename
     */
    public function stat_file($filename)
    {
        // stj: hier kann bei Bedarf GSA2-Zirkus hinzugefuegt werden
        if (($fp = @fopen($filename, "r")) != false) {
            fclose($fp);
        }
    }

    /**
     * @param $filename
     */
    public static function statFile($filename)
    {
        $emf = new efs_models_filefunx();
        $emf->stat_file($filename);
    }

    /**
     * @param $fileHandle
     * @param int $lockOption
     * @return bool
     */
    public static function flock($fileHandle, $lockOption = LOCK_SH)
    {
        return @flock($fileHandle, $lockOption);
    }

    /**
     * Moves files from a given directory to a backup directory
     * @param $sourceDirPath
     * @param $targetDirPath
     * @param bool $recurse Scan dirs recursively?
     * @param string $pattern Pattern to match filenames against
     */
    public static function backup_files($sourceDirPath, $targetDirPath, $recurse = false, $pattern = '*.*')
    {
        $filesToBackup = self::enumFiles($sourceDirPath, $recurse, $pattern);
        if (count($filesToBackup) > 0) {
            foreach ($filesToBackup as $file) {
                self::moveFile($file, $targetDirPath);
            }
        }
    }
}
