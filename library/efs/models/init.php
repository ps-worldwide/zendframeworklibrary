<?php
  /**
   * efs_models_init
   * Initiates the "connection" to EFS (auth, conf, ...)
   *
   * @package   Csv2Questionnaire
   * @version   $Id: init.php,v 1.14 2016/07/27 08:07:04 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   * @deprecated
   * @notused
   */

  class efs_models_init
  {
    private static $instance = null;
    private $confObj;
    private $authObj;
    private $apiObj;

    /**
     *
     */
    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public function __destruct()
    {
    }

    /**
     * getInstance
     *
     * @return efs_models_init
     */
    public static function getInstance()
    {
      /** @noinspection PhpDeprecationInspection */
      if (self::$instance === null)
      {
        /** @noinspection PhpDeprecationInspection */
        self::init();
      }

      /** @noinspection PhpDeprecationInspection */
      return self::$instance;
    }

    /**
     * init
     *
     * @return void
     */
    static private function init()
    {
      /** @noinspection PhpDeprecationInspection */
      self::$instance = new self();

      /** @noinspection PhpDeprecationInspection */
      self::$instance->setConf();
      /** @noinspection PhpDeprecationInspection */
      self::$instance->setApi();
      /** @noinspection PhpDeprecationInspection */
      self::$instance->setAuth();
    }

    /**
     * setConf
     *
     * @return void
     */
    private function setConf()
    {
      $this->confObj = new efs_models_config();
      $this->confObj = $this->confObj->config();
    }

    /**
     * conf
     *
     */
    public function conf()
    {
      return $this->confObj;
    }

    /**
     * setAuth
     *
     * @return void
     */
    private function setAuth()
    {
      $this->authObj = new efs_models_auth('', '', Zend_Registry::get('storage'));
    }

    /**
     * auth
     *
     */
    public function auth()
    {
      return $this->authObj;
    }

    /**
     * setApi
     *
     * @return void
     */
    private function setApi()
    {
      $this->apiObj = new efs_models_api();
    }

    /**
     * api
     *
     */
    public function api()
    {
      return $this->apiObj;
    }
  }