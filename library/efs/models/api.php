<?php
  /**
   * efs_models_api
   * Combines the API and Webservice methods to one API object
   *
   * @package    zendframeworkLibrary
   * @version    $Id: api.php,v 1.34 2013/10/16 09:27:49 funke Exp $
   * @copyright  (c) QuestBack http://www.questback.com
   * @author     $Author: funke $
   * @deprecated use Pct_Efs_Api instead
   */

  class efs_models_api
  {
    // The api model
    private $api = null;

    // The EFS Version the model talks to
    private $efsVersion = NULL;

    // The Zend-Config instance of the application
    private $conf = NULL;

    /**
     * __construct
     *
     * @param Zend_Db_Adapter_Abstract $db
     * @param null                     $efsVersion
     *
     * @throws Exception
     * @return \efs_models_api
     */
    public function __construct(Zend_Db_Adapter_Abstract $db = null, $efsVersion = null)
    {
      // Stay backward compatible
      $this->setConf(Zend_Registry::get('conf'));

      // Shouldn't that be done before?
      if (!isset($this->getConf()->enableApi))
        throw new Exception('Api not enabled via application.ini');

      // Ensure old code still works
      if ($efsVersion == null)
        $efsVersion = Pct_Efs_Version::getVersion();

      $this->efsVersion = $efsVersion;

      // Get instance of API for the requestion EFS version
      /** @noinspection PhpDynamicAsStaticMethodCallInspection */
      /** @noinspection PhpParamsInspection */
      $this->api = Pct_Efs_Api::getInstance($this->efsVersion, $db, $this->getConf());
    }

    /**
     * Setter for the Zend-Config instance
     *
     * @param Zend_Config $conf
     *
     * @return $this
     */
    public function setConf(Zend_Config $conf)
    {
      $this->conf = $conf;

      return $this;
    }

    /**
     * Returns the Zend-Config instance
     *
     * @return Zend_Config $this->conf
     */
    public function getConf()
    {
      return $this->conf;
    }

    /**
     * call
     *
     * @param mixed $method
     * @param mixed $param
     *
     * @return mixed
     */
    public function __call($method, $param)
    {
      // Pass the event to the "new" API
      return call_user_func_array(array($this->api, $method), $param);
    }
  }
