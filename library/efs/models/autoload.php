<?php
  /**
   * efs_models_autoload
   * This class shall provide an autoloader that combines the application
   * specific classes (in Zend-Framework - style) as well as the framework itself
   * and the EFS classes (aka Objectmaster and ModuleManager).
   *
   * @package   zendframeworkLibrary
   * @version   $Id: autoload.php,v 1.34 2013/10/16 09:27:49 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class efs_models_autoload implements Zend_Loader_Autoloader_Interface
  {

    private $efsVersion;

    // Singelton instance
    private static $instance = NULL;

    // Namespaces used in PCT projects
    private $pctNamespaces = array('Pct_', 'pct_', 'comp_', 'efs_', 'models_');

    /**
     * Older projects directly instantiate this class so the constructor must
     * be public.
     */
    public function __construct()
    {
      self::printDeprecated(__METHOD__, 'Pct_Efs_Autoloader::getInstance(EFS_VERSION)');

      // Register namespaces used by PCT projects
      $this->registerPctNamespaces();

      // Initiate the AutoLoader for the currect EFS - version
      $this->efsVersion = Pct_Efs_Version::getVersion();
      if ($this->efsVersion == null)
        throw new Zend_Loader_Exception('Cannot determine EFS version');
      Zend_Loader::loadClass('Pct_Efs_Autoloader', null, true);

      // Push the autoloader to load efs classes (with and without "namespace")
      $autoloader = Pct_Efs_Autoloader::getInstance($this->efsVersion);

      // TODO: test if every project is fine with this as fallbackAutoloader
      Zend_Loader_Autoloader::getInstance()->setFallbackAutoloader(true);

      Zend_Loader_Autoloader::getInstance()->pushAutoloader($autoloader);
      Zend_Loader_Autoloader::getInstance()->pushAutoloader($autoloader, 'efs_');
    }

    /**
     * Returns (singleton) instance
     */
    public static function getInstance()
    {
      if (self::$instance == NULL)
        self::$instance = new self();

      return self::$instance;
    }

    /**
     * The autoloader function. It checks if the requested class is known in EFS
     * itself. If it is the class-file is included. If it is not known in EFS
     * it is left to the Zend-Loader to include the according file. Usually this
     * means the class comes from the module within this zend-framework-project.
     */
    public function autoload($class)
    {
      self::printDeprecated(__METHOD__, 'Pct_Efs_Autoloader::getInstance(EFS_VERSION)');

      /** @noinspection PhpDynamicAsStaticMethodCallInspection */

      return Pct_Efs_Autoloader::autoload($class);
    }

    /**
     * Enabled once the loader from EFS
     *
     * @deprecated
     */
    public function enableEfsAutoload()
    {
      self::printDeprecated(__METHOD__, ' getInstance()');
    }

    /**
     * enableEfs
     */
    public function enableEfs()
    {
    }

    /**
     * Prints a deprecated message to stout
     *
     * @param string $method
     * @param string $instead
     */
    protected function printDeprecated($method, $instead)
    {
      /*if (defined('APPLICATION_ENV') && APPLICATION_ENV == 'development') {
        print "\n". $method . ' is deprecated. Instead use: ' .
                 $instead;
      }*/
    }

    /**
     * Registers the namespaces used by the Product Customizing Team for the
     * Zend Loader if namespaces are not registered yet. Usually these should /
     * could be registered in the Bootstrap.php.
     */
    protected function registerPctNamespaces()
    {
      $zendAutoLoader = Zend_Loader_Autoloader::getInstance();

      $zendNs = $zendAutoLoader->getRegisteredNamespaces();
      $zendNs = array_flip($zendNs);

      foreach ($this->pctNamespaces as $namespace)
      {
        if (false == array_key_exists($namespace, $zendNs))
          $zendAutoLoader->registerNamespace($namespace);
      }
    }
  }
