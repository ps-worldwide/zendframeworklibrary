<?php
  /**
   * efs_models_wildcard
   *
   * @package   zendframeworkLibrary
   * @version   $Id: wildcard.php,v 1.13 2013/10/16 09:27:49 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   * @usedby    cvs:/projekte/GlobalparkUK
   * @usedby    cvs:/projekte/ProductCustomizingTeam/intervista
   */

  /** @noinspection PhpUndefinedClassInspection */
  class efs_models_wildcard
  {
    //DB-Handler
    private $db;

    //Cachen
    private $cache;
    private $cache_prefix;

    //>= EFS 6.1
    private $six_1;

    /* Table changed in 6.1 */
    const TABLE_6_0 = "placeholders_%d";
    const TABLE_6_1 = "survey_placeholders";

    /**
     * __construct
     *
     * @param dbwrap $db
     *
     * @return \efs_models_wildcard
     */
    public function __construct(/** @noinspection PhpUndefinedClassInspection */
      dbwrap &$db)
    {
      $this->db           = & $db;
      $this->cache        = array();
      $this->cache_prefix = array();
      $this->six_1        = NULL;
    }

    /**
     * is_6_1
     *
     * @return NULL
     */
    private function is_6_1()
    {
      if ($this->six_1 !== NULL)
        return $this->six_1;

      /** @noinspection PhpUndefinedMethodInspection */
      $this->six_1 = $this->db->table_exists(self::TABLE_6_1);

      return $this->six_1;
    }

    /**
     * load
     *
     * @param int $syid
     *
     * @throws Exception
     */
    private function load($syid)
    {
      $syid = (int)$syid;

      /* Which table do we need to use */
      if ($this->is_6_1())
        $query = "SELECT * FROM " . self::TABLE_6_1 . " WHERE pid=" . $syid;
      else
        $query = "SELECT * FROM " . sprintf(self::TABLE_6_0, $syid);

      /* Load the wildcard for the given survey */
      if (method_exists($this->db, "query"))
      {
        /** @noinspection PhpUndefinedMethodInspection */
        $sth = $this->db->query($query);
      }
      else
      {
        /** @noinspection PhpUndefinedMethodInspection */
        $sth = $this->db->execute($query);
      }

      if (!$sth)
        throw new Exception("ERR::DB Unable to get wildcards!");

      /* Now cache the wildcards */
      $this->cache[$syid] = array();
      /** @noinspection PhpUndefinedMethodInspection */
      while (($data = $this->db->fetch_assoc($sth)) != false)
        $this->cache[$syid][$data["placeholder"]] = $data;
    }

    /**
     * load_prefix
     *
     * @param mixed $syid
     * @param mixed $prefix
     *
     * @return void
     */
    private function load_prefix($syid, $prefix)
    {
      if ($this->cache[$syid] == NULL)
        $this->load($syid);

      $this->cache_prefix[$syid][$prefix] = array();
      foreach ($this->cache[$syid] as $wildcard => $data)
      {
        if (strpos($wildcard, $prefix) === 0)
          $this->cache_prefix[$syid][$prefix][] = $wildcard;
      }
    }

    /** Public classes */

    /**
     * get
     *
     * @param int    $syid
     * @param string $wildcard
     * @param string $col
     *
     * @return string
     */
    public function get($syid, $wildcard, $col = "*")
    {
      /* If there are not wildcards loaded for the survey load them */
      if (!array_key_exists($syid, $this->cache))
        $this->load($syid);

      /* Return everything */
      if ($col == "*")
        return $this->cache[$syid][$wildcard];

      /* Return the information for the requested wildcard */
      if ($this->cache[$syid][$wildcard])
        if ($this->cache[$syid][$wildcard][$col])
          return $this->cache[$syid][$wildcard][$col];

      return "";
    }

    /**
     * get_from_prefix
     *
     * @param mixed  $syid
     * @param mixed  $prefix
     * @param string $col
     *
     * @return array
     */
    public function get_from_prefix($syid, $prefix, $col = "*")
    {
      /* $this->cache holds every known wildcard - $this->cache_prefix holds
       * every wildcard with the prefix. */

      /* Does the cache_prefix already know the wildcards */
      if (in_array($syid, $this->cache_prefix) && in_array($prefix, $this->cache_prefix[$syid]) && $this->cache_prefix[$syid][$prefix] !== NULL && $col == "*")
        return $this->cache_prefix[$syid][$prefix];

      /* Are the survey-wildcards loaded already */
      if (!array_key_exists($syid, $this->cache) || $this->cache[$syid] == NULL)
        $this->load($syid);

      /* does the survey have wildcards */
      if (count($this->cache[$syid]) < 1)
        return array();

      /* Is the prefix loaded yet? */
      if (!array_key_exists($syid, $this->cache_prefix) || !array_key_exists($prefix, $this->cache_prefix[$syid]))
        $this->load_prefix($syid, $prefix);

      /* If all colums are requested return everything for the prefix */
      if ($col == "*")
        return $this->cache_prefix[$syid][$prefix];

      /* Return columns for the prefix */
      $return = array();
      foreach ($this->cache_prefix[$syid][$prefix] as $wildcard)
        $return[$wildcard] = $this->cache[$syid][$wildcard][$col];

      return $return;
    }

    /**
     * wildcardToVariable
     *
     * @param mixed $varsDef
     * @param bool  $key
     *
     * @return array
     */
    public function wildcard_to_variable($varsDef, $key = true)
    {
      /* Array with the valid variables */
      $validTrans = array();

      /* The allowed var-prefixes */
      $allowPrefix = array('v_', 'u_', 'c_', 'p_');

      /* If only one var (string) was passed cast into array */
      if (is_string($varsDef))
        $varsDef = array($varsDef);

      foreach ($varsDef as $transDef)
      {
        $arrTransDef = explode("=>", $transDef);

        $prefix1 = substr($arrTransDef[0], 0, 2);
        $prefix2 = substr($arrTransDef[1], 0, 2);
        if ((in_array($prefix1, $allowPrefix) || !$key) && in_array($prefix2, $allowPrefix))
        {
          $validTrans[trim($arrTransDef[0])] = trim($arrTransDef[1]);
        }
      }

      return $validTrans;
    }

    /* Old methods from previous version of the class */

    /**
     * get_wildcard_from_placeholder
     *
     * @param int $syid
     * @param     $placeholder
     *
     * @return string
     */
    public function get_wildcard_from_placeholder($syid, $placeholder)
    {
      return $this->get($syid, $placeholder, "*");
    }

    /**
     * get_text_from_placeholder
     *
     * @param int $syid
     * @param     $placeholder
     *
     * @return string
     */
    public function get_text_from_placeholder($syid, $placeholder)
    {
      return $this->get($syid, $placeholder, "text");
    }

    /**
     * get_prefixed_wildcards
     *
     * @param int    $syid
     * @param string $prefix
     *
     * @return Ambigous <void, multitype:, multitype:NULL >
     */
    public function get_prefixed_wildcards($syid, $prefix)
    {
      return $this->get_from_prefix($syid, $prefix, "text");
    }

    /**
     * replace
     *
     * @param string $search
     * @param string $replace
     * @param string $subject
     * @param int    $count
     *
     * @return mixed
     */
    public function replace($search, $replace, $subject, &$count)
    {
      return str_replace($search, $replace, $subject, $count);
    }
  }

  /* To be downward compatible */
  /**
   * Class actionscript_wildcard
   */
  /** @noinspection PhpUndefinedClassInspection */
  class actionscript_wildcard extends efs_models_wildcard
  {
    /**
     * __construct
     *
     * @param dbwrap $db
     */
    public function __construct(/** @noinspection PhpUndefinedClassInspection */
      dbwrap $db)
    {
      parent::__construct($db);
    }
  }