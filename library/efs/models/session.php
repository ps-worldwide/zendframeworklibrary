<?php
  /**
   * efs_models_session
   * Handles the session from EFS
   *
   * @uses      efs_models_auth
   * @package   cvs:/projekte/ProductCustomizingTeam/turkcell/
   * @package   cvs:/projekte/ProductCustomizingTeam/ergo/
   * @version   $Id: session.php,v 1.15 2013/10/16 09:27:49 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @todo      support different EFS versions
   * @todo      replace by webservice / servicelayer
   * @author    $Author: funke $
   */

  class efs_models_session extends efs_models_auth
  {
    const SES_EXPIRE_TIME = 1800;
    const SES_TABLE       = 'survey_session';

    private $sesId; // EFS-Session-ID
    private $sesType; // EFS-Type
    private $sesPath; // Path of the module
    private $sesDb; // Database where the session is hold
    private $sesCookie; // Cookies sent by browser
    private $sesExpire; // Last possible timestamp for valid session
    private $session; // Zend-Session - object
    private $cookie; // Zend-Http-Cookie - object
    private $client; // Zend-Http-Client - object to set cookie
    private $conf; // Zend-Config - object

    /**
     * __construct
     *
     * @param mixed $sesId
     * @param mixed $sesPath
     * @param mixed $sesDb
     * @param null  $sesCookie
     *
     * @return \efs_models_session
     */
    public function __construct($sesId, $sesPath, $sesDb, $sesCookie)
    {
      /* Session values */
      $this->sesId   = $sesId;
      $this->sesPath = $sesPath;
      $this->sesDb   = $sesDb;

      $this->sesType   = efs_apis_const::SESSION_TYPE_ADMIN;
      $this->sesName   = $this->sesPath . '-session';
      $this->sesCookie = $sesCookie;

      $this->sesExpire = self::SES_EXPIRE_TIME + time(); /* If ses-id is not given via get-parameter get it from the cookie */
      if ($this->sesId == NULL && @$this->sesCookie[$this->sesName] != '')
        $this->sesId = $this->sesCookie[$this->sesName];

      /* Make the config available in class */
      $this->conf = Zend_Registry::get('conf');

      /* Set / Update the cookie */
      $this->setCookie();
    }

    /**
     * setCookie
     *
     * @return void
     */
    public function setCookie()
    {
      $url = parse_url($this->conf->base_all_url);

      $this->sesSet = setcookie($this->sesName, $this->sesId, $this->sesExpire, $this->sesPath,
        //$this->conf->base_all_url,
                                $url['host'], false, true);
    }

    /**
     * verifySession
     *
     * @param bool $check_uid
     *
     * @return bool
     */
    public function verifySession($check_uid = false)
    {
      /* No session-id no verification */
      if ($this->sesId == '')
        return false;

      /* Check if the session is known in EFS */
      $select = $this->sesDb->select()->from('survey_session')->where('sid=?', $this->sesId)->where('type=?', $this->sesType)->where('dt_exp>=?', gmmktime());
      if ($check_uid)
      {
        /** @noinspection PhpUndefinedMethodInspection */
        $select->where('uid!=?', 0);
      }
      $stmt = $this->sesDb->query($select);
      /** @noinspection PhpUndefinedMethodInspection */
      $result = $stmt->fetchAll();

      /* If a valid session is found update it */
      if (count($result) > 0)
      {
        $this->sesDb->update(self::SES_TABLE, array('dt_exp' => $this->sesExpire), 'sid="' . addslashes($this->sesId) . '"');
        return true;
      }

      /* No valid session */
      return false;
    }

    /**
     * setSesType
     *
     * @param $type
     */
    public function setSesType($type)
    {
      if ($type == "survey")
        $this->sesType = efs_apis_const::SESSION_TYPE_SURVEY;
      if ($type == "admin")
        $this->sesType = efs_apis_const::SESSION_TYPE_ADMIN;
      if ($type == "panel")
        $this->sesType = efs_apis_const::SESSION_TYPE_PANEL;
    }
  }
