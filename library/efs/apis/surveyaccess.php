<?php
  /**
   * efs_apis_surveyaccess
   *
   * @package   zendframeworkLibrary
   * @version   $Id: surveyaccess.php,v 1.9 2013/10/16 09:27:40 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   * @todo      remove extends
   * @todo      adjust surveyapihack to not used the EFS extend
   */

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_surveyaccess extends survey_table_access
  {
    /**
     * @param      $syid
     * @param bool $failure_notice
     */
    public function __construct($syid, $failure_notice = true)
    {
      /** @noinspection PhpUndefinedClassInspection */
      return parent::__construct($syid, $failure_notice);
    }
  }