<?php
    /** @noinspection PhpVoidFunctionResultUsedInspection */
    die(debug_print_backtrace()); /* NOFLAME */
    /**
     * efs_apis_panelapi
     * Does the EFS-API call
     *
     * @package   zendframeworkLibrary
     * @version   $Id: panelapi.php,v 1.25 2013/11/13 14:55:47 funke Exp $
     * @copyright (c) QuestBack http://www.questback.com
     * @author    $Author: funke $
     */
    //class efs_apis_panelapi extends efs_apis_panelapihack
    //{
    //  // panel api
    //  protected $efsApi = NULL;
    //
    //  /**
    //   * __construct
    //   *
    //   * @param Zend_DB_Adapter_Abstract $db
    //   * @return \efs_apis_panelapi
    //   */
    //  public function __construct(Zend_DB_Adapter_Abstract $db)
    //  {
    //    /** @noinspection PhpUndefinedClassInspection */
    //    $this->efsApi = panel_api::get_instance();
    //    parent::__construct($db);
    //  }
    //
    //  /**
    //   * __call
    //   *
    //   * @param mixed $method
    //   * @param mixed $param
    //   * @throws Exception
    //   * @return void
    //   */
    //  public function __call($method, $param)
    //  {
    //    throw new Exception('Call to undefined efs-panelapi method "' . $method . '()"');
    //  }
    //
    //  /**
    //   * Returns an array holding the masterdata for the
    //   * given panelist
    //   *
    //   * @param $ident
    //   * @param $value
    //   * @throws Exception
    //   * @return array
    //   */
    //  public function getPanelistData($ident, $value)
    //  {
    //    // then panel api throws an exception when
    //    // the panelist is not found - avoid that and
    //    // return an empty array instead
    //    try
    //    {
    //      return array_merge($this->efsApi->get_panelist_master_data($ident, $value), $this->efsApi->get_panelist_attributes($ident, $value));
    //    }
    //    /** @noinspection PhpUndefinedClassInspection */
    //    catch (panel_exception $e)
    //    {
    //      if ($e->get_errcode() == efs_apis_panelexception::ERR_UNKNOWN_PANELIST_IDENTIFIER_VALUE)
    //        return array();
    //      else
    //        throw new Exception($e->get_error_message());
    //    }
    //  }
    //
    //  // API functions
    //
    //
    //  /**
    //   * disableRecodings
    //   */
    //  public function disableRecodings()
    //  {
    //    return $this->efsApi->disable_recodings();
    //  }
    //
    //  /**
    //   * enableRecodings
    //   */
    //  public function enableRecodings()
    //  {
    //    return $this->efsApi->enable_recodings();
    //  }
    //
    //  /**
    //   * getPanelistStatus
    //   *
    //   * @param string $identifier_type
    //   * @param string $identifier_value
    //   */
    //  public function getPanelistStatus($identifier_type, $identifier_value)
    //  {
    //    return $this->efsApi->get_panelist_status($identifier_type, $identifier_value);
    //  }
    //
    //  /**
    //   * setPanelistStatus
    //   *
    //   * @param string $identifier_type
    //   * @param string $identifier_value
    //   * @param $new_status
    //   */
    //  public function setPanelistStatus($identifier_type, $identifier_value, $new_status)
    //  {
    //    return $this->efsApi->set_panelist_status($identifier_type, $identifier_value, $new_status);
    //  }
    //
    //  /**
    //     * getPanelistsByStatus
    //     *
    //     * @param $status
    //     * @param string $identifier_type
    //     */
    //  public function getPanelistsByStatus($status, $identifier_type)
    //  {
    //    return $this->efsApi->get_panelists_by_status($status, $identifier_type);
    //  }
    //
    //  /**
    //   * getPanelColumns
    //   */
    //  public function getPanelColumns()
    //  {
    //    return $this->efsApi->get_panel_columns();
    //  }
    //
    //  /**
    //   * updatePanelist
    //   *
    //   * @param string $identifier_type
    //   * @param string $identifier_value
    //   * @param array $panelist_data
    //   */
    //  public function updatePanelist($identifier_type, $identifier_value, $panelist_data)
    //  {
    //    return $this->efsApi->update_panelist($identifier_type, $identifier_value, $panelist_data);
    //  }
    //
    //  /**
    //   * isPanelist
    //   *
    //   * @param string $identifier_type
    //   * @param string $identifier_value
    //   */
    //  public function isPanelist($identifier_type, $identifier_value)
    //  {
    //    return $this->efsApi->is_panelist($identifier_type, $identifier_value);
    //  }
    //
    //  /**
    //   * addPanelist
    //   *
    //   * @param string $return_identifier_type
    //   * @param array $panelist_data
    //   * @param bool $strict_check
    //   * @param bool $check_promotion
    //   * @return
    //   */
    //  public function addPanelist($return_identifier_type, $panelist_data, $strict_check = false, $check_promotion = false)
    //  {
    //    return $this->efsApi->add_panelist($return_identifier_type, $panelist_data, $strict_check, $check_promotion);
    //  }
    //
    //  // löscht alle Ausprägungen von $md_category
    //  public function deleteMasterdataCategories($md_category)
    //  {
    //    return $this->efsApi->delete_masterdata_categories($md_category);
    //  }
    //
    //  // löscht Ausprägung mit Code $code von $md_category
    //  public function deleteMasterdataCategory($md_category, $code)
    //  {
    //    return $this->efsApi->delete_masterdata_category($md_category, $code);
    //  }
    //
    //  // erstellt die Ausprägung mit Code $code, Label $label von $md_category
    //  // Sortierungsnummer wird automatisch vergeben, falls keine übergeben wurde
    //  // Wenn es eine Ausprägung mit Code $code bereits gibt, wird sie aktualisiert.
    //  public function updateMasterdataCategory($md_category, $code, $label, $sort_number = null)
    //  {
    //    return $this->efsApi->update_masterdata_category($md_category, $code, $label, $sort_number);
    //  }
    //
    //  // erstellt die Ausprägungen in $data von m_peter.
    //  // $data = array(array("code"=>1,"label"=>"label1","sort_number"=>1),array("code"=>2,"label"=>"label2","sort_number"=>2),...)
    //  // Sortierungsnummer wird automatisch vergeben, falls keine übergeben wurde
    //  // Vorher werden alle vorhandenen Ausprägungen von md_peter gelöscht, d.h. nur noch diese Ausprägungen sind zum Schluss vorhanden.
    //  public function replaceMasterdataCategories($md_category, $data)
    //  {
    //    return $this->efsApi->replace_masterdata_categories($md_category, $data);
    //  }
    //}
