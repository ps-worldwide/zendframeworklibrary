<?php
    /** @noinspection PhpVoidFunctionResultUsedInspection */
    die(debug_print_backtrace()); /* NOFLAME */
    /**
     * efs_apis_webservice
     * Calls the EFS-Webservices
     *
     * @package   zendframeworkLibrary
     * @version   $Id: webservice.php,v 1.46 2013/11/13 14:55:47 funke Exp $
     * @copyright (c) QuestBack http://www.questback.com
     * @author    $Author: funke $
     */

    //class efs_apis_webservice
    //{
    //  private $client   = null;
    //  private $url      = null;
    //
    //  /**
    //   * @var array Soap client options
    //   */
    //  private $options  = array(
    //    'URI'       => 'http://www.global-park.com/OPST/services',
    //    'style'     => SOAP_RPC,
    //    'use'       => SOAP_ENCODED,
    //    'encoding'  => 'UTF-8',
    //    'features'  => SOAP_SINGLE_ELEMENT_ARRAYS,
    //    'exceptions'=>true,
    //  );
    //
    //  /**
    //   * Constructor
    //   *
    //   * All Arguments are optional.
    //   * If not explicitly passed baseUrl and clientId are determined from config,
    //   * which is loaded via <code>Zend_Registry::get('conf')</code> if not passed.
    //   *
    //   * WSDL cache is default disabled.
    //   *
    //   * @param array $options (optional) Soap client options
    //   * @param Zend_Config $config (optional) Application config instance (used to determine <code>$baseUrl</code> and <code>$clientId</code> if not passed.
    //   * @param string $baseUrl (optional)
    //   * @param string $clientId (optional)
    //   * @param bool $disableWsdlCache (optional)
    //   */
    //  public function __construct(array $options=null, Zend_Config $config=null,
    //      $baseUrl=null, $clientId = null, $disableWsdlCache = true)
    //  {
    //    die(__FILE__);
    //    /*
    //    if (defined('APPLICATION_ENV') && constant('APPLICATION_ENV') == 'development')
    //      print __FILE__ . ':' . __METHOD__ . ' is more than deprecated!!!';
    //    if ($options)
    //      $this->options = array_merge($this->options, $options);
    //
    //    if (is_null($config))
    //      $config = Zend_Registry::get('conf');
    //
    //    if (is_null($baseUrl))
    //      $baseUrl = $config->baseurl;
    //
    //    if (is_null($clientId))
    //      $clientId = $config->webservice->clientId;
    //
    //    $this->url = $baseUrl . 'ws_rpc.php?wsdl&actor=general_actions&client=' . $clientId;
    //
    //    if ($disableWsdlCache)
    //      ini_set("soap.wsdl_cache_enabled", 0);
    //    */
    //  }
    //
    //  /**
    //   * Creates a soap client instance
    //   */
    //  private function initSoapClient()
    //  {
    //    if (!is_null($this->client))
    //      return true;
    //
    //      // Avoid fatal errors in SOAPClient
    //    $addr = parse_url($this->url);
    //    if ($addr == null)
    //      throw new Zend_Exception('Webservice Api: Cannot resolve URL "' . $this->url . '"');
    //
    //    if ($addr['host'] === gethostbyname($addr['host']))
    //      throw new Zend_Exception('Webservice Api: Cannot resolve host "' . $addr['host'] . '"');
    //
    //    $this->client = new SOAPClient($this->url, $this->options);
    //    return true;
    //  }
    //
    //  /**
    //   * Returns a initlilized soap client
    //   *
    //   * @return SOAPClient
    //   */
    //  private function getSoapClient()
    //  {
    //    if ( false == $this->initSoapClient())
    //      return null;
    //
    //    return $this->client;
    //  }
    //
    //  /**
    //   * Adds a new panelist
    //   *
    //   * @param $data
    //   * @param $identifier
    //   */
    //  public function addNewPanelist(array $data, $identifier='pcode')
    //  {
    //    $this->initSoapClient();
    //    return $this->client->add_new_panelist($data,$identifier);
    //  }
    //
    //  /**
    //   * Adds the given panelist to the given survey.
    //   * $data[ident], $data[identValue] $data[reset], $data[syid]
    //   *
    //   * @param array $data
    //   * @return \stdClass
    //   */
    //  public function addPanelistToSurvey(array $data)
    //  {
    //    $this->initSoapClient();
    //
    //    $defaultConf = array(
    //      'ident'         => 'u_email',
    //      'identValue'    => '',
    //      'reset'         => 'reset_no_change',
    //      'syid'          => 0,
    //      '' );
    //    $conf = array_merge($defaultConf, $data);
    //
    //    // Check needed parameter
    //    if($conf['ident']=='' || $conf['identValue']==''
    //      || $conf['reset']=='' || $conf['syid'] == 0) {
    //          return new stdClass();
    //    }
    //
    //    return $this->client->add_panelist_to_survey(
    //        $conf['ident'], $conf['identValue'], $conf['syid'], $conf['reset']);
    //  }
    //
    //  /**
    //   * Returns the template with the given ID
    //   * @param $id
    //   */
    //  public function getMailTemplate( $id )
    //  {
    //    $this -> initSoapClient();
    //    return $this -> client -> get_mail_template( $id );
    //  }
    //
    //  /**
    //   * getSurvey
    //   *
    //   * @param $pid
    //   * @return mixed $surveyData
    //   */
    //  public function getSurvey($pid)
    //  {
    //    $this->initSoapClient ();
    //    return $this->client->get_survey((int) $pid);
    //  }
    //
    //  /**
    //   * getSurveyExport
    //   *
    //   * @param mixed $arrParam
    //   */
    //  public function getSurveyExport($arrParam)
    //  {
    //    $this -> initSoapClient();
    //
    //    return $this->client->get_survey_export(
    //       @array("survey_id"               => $arrParam["pid"],
    //              "sample_id"               => $arrParam["sample-id"],
    //              "language"                => $arrParam["language"],
    //              "compression"             => $arrParam["compression"],
    //              "skip_missings"           => $arrParam["skip_missings"],
    //              "restrict"                => $arrParam["restrict"],
    //              "anonymize"               => $arrParam["anonymize"],
    //              "kill_newlines"           => $arrParam["kill_newlines"],
    //              "subst_codes"             => $arrParam["subst_codes"],
    //              "use_metainfo"            => $arrParam["use_metainfo"],
    //              "shrink"                  => $arrParam["shrink"],
    //              "adjustwidths"            => $arrParam["adjustwidth"],
    //              "include_sysmissings"     => $arrParam["include_sysmissings"],
    //              "lfdn_from"               => $arrParam["lfdn_from"],
    //              "lfdn_to"                 => $arrParam["lfdn_to"],
    //              "protected"               => $arrParam["protected"],
    //              "dispositioncode"         => $arrParam["dispositioncode"],
    //              "export_format"           => $arrParam["export_format"],
    //              "format"                  => $arrParam["format"],
    //              "template"                => $arrParam["template"],
    //              "charset"                 => $arrParam["charset"],
    //              "missingvalue_text"       => $arrParam["missingvalue_text"],
    //              "missingvalue_text_empty" => $arrParam["missingvalue_text_empty"],
    //              "missingvalue_numeric"    => $arrParam["missingvalue_numeric"],
    //              "missingvalue_timestamp"  => $arrParam["missingvalue_timestamp"],
    //              "missingvalue_datetime"   => $arrParam["missingvalue_datetime"],
    //              "conjoint_selection"      => $arrParam["conjoint_selection"],
    //              "startdate"               => $arrParam["startdate"],
    //              "enddate"                 => $arrParam["enddate"],
    //              "varname_type"            => $arrParam["varname_type"],
    //              "skip_text_contents"      => $arrParam["skip_text_contents"],
    //              "panelgroup"              => $arrParam["panelgroup"],
    //              "panelstatus"             => $arrParam["panelstatus"],
    //              "return_gzcompressed"     => $arrParam["return_gzcompressed"],
    //              "skip_loop_vars"          => $arrParam["skip_loop_vars"]
    //             )
    //       );
    //  }
    //
    //  /**
    //   * getSurveyData
    //   *
    //   * @param mixed $pid
    //   */
    //  public function getSurveyData($pid)
    //  {
    //    $arrParam["pid"] = $pid[0];
    //    $arrParam["compression"] = "none";
    //    $arrParam["language"] = "1";
    //    $arrParam["skip_missings"]=false;
    //    $arrParam["anonymize"]=true;
    //    $arrParam["kill_newlines"]=true;
    //    $arrParam["include_sysmissings"]=true;
    //    $arrParam["dispositioncode"]=array("31,32");
    //    $arrParam["export_format"]="results";
    //    $arrParam["format"]="csv";
    //    $arrParam["template"]="project_all";
    //    $arrParam["charset"]="UTF-8";
    //    $arrParam["return_gzcompressed"]=true;
    //    $arrParam["missingvalue_text"] = "-66";
    //    $arrParam["missingvalue_text_empty"] = "-99";
    //    $arrParam["missingvalue_numeric"] = "-77";
    //    $arrParam["missingvalue_timestamp"] = "0";
    //    $arrParam["missingvalue_datetime"] = "0000-00-00 00:00:00";
    //    $arrParam["skip_loop_vars"]=true;
    //    return $this->getSurveyExport($arrParam);
    //  }
    //
    //  /**
    //   * getSurveyLoopData
    //   *
    //   * @param mixed $pid
    //   */
    //  public function getSurveyLoopData($pid)
    //  {
    //    $arrParam["pid"]                 = $pid[0];
    //    $arrParam["export_format"]       = "raw_loopdata";
    //    $arrParam["format"]              = "csv";
    //    $arrParam["compression"]         = "none";
    //    $arrParam["template"]            = "project_all";
    //    $arrParam["language"]            = 1;
    //    $arrParam["return_gzcompressed"] = true;
    //
    //    $arrParam["skip_missings"]       = false;
    //    $arrParam["anonymize"]           = true;
    //    $arrParam["kill_newlines"]       = true;
    //    $arrParam["include_sysmissings"] = true;
    //    $arrParam["dispositioncode"]     = array("31,32");
    //    $arrParam["charset"]             = "UTF-8";
    //
    //    return $this->getSurveyExport($arrParam);
    //  }
    //
    //  /**
    //   * getSurveyVariableLabel
    //   *
    //   * @param mixed $pid
    //   */
    //  public function getSurveyVariableLabel($pid)
    //  {
    //    $arrParam["pid"]                 = $pid[0];
    //    $arrParam["compression"]         = "none";
    //    $arrParam["skip_missings"]       = false;
    //    $arrParam["anonymize"]           = true;
    //    $arrParam["kill_newlines"]       = true;
    //    $arrParam["include_sysmissings"] = true;
    //    $arrParam["dispositionscode"]    = "31,32";
    //    $arrParam["export_format"]       = "results";
    //    $arrParam["format"]              = "sss";
    //    $arrParam["template"]            = "project_all";
    //    $arrParam["charset"]             = "UTF-8";
    //    $arrParam["return_gzcompressed"] = true;
    //
    //    return $this->getSurveyExport($arrParam);
    //  }
    //
    //  /**
    //   * getSurveyVariableLabel
    //   *
    //   * @param mixed $pid
    //   * @param bool $user
    //   * @return bool
    //   */
    //  public function getSurveyVariables($pid, $user = false)
    //  {
    //    if ($pid < 1)
    //      return false;
    //
    //    $this->initSoapClient();
    //
    //    return $this->client->get_survey_variables($pid, $user);
    //  }
    //
    //  /**
    //   * getServerTime
    //   *
    //   * @param string $format
    //   * @return
    //   */
    //  public function getServerTime($format = 'timestamp')
    //  {
    //    $this->initSoapClient();
    //    return $this->client->get_server_time($format);
    //  }
    //
    //  /**
    //   * getMasterdataStructure
    //   */
    //  public function getMasterdataStructure()
    //  {
    //    $this->initSoapClient();
    //    return $this->client->get_masterdata_structure();
    //  }
    //
    //  /**
    //   * importCsv
    //   *
    //   * @param $syid
    //   * @param $data
    //   * @param $ident
    //   * @param $options
    //   * @return boolean
    //   */
    //  public function importCsv($syid, $data, $ident, $options)
    //  {
    //    if (!is_integer($syid) || !is_string($data) || !is_string($ident) || !is_array($options))
    //      return false;
    //
    //    $this->initSoapClient();
    //
    //    return $this->client->import_csv($syid, $data, $ident, $options);
    //  }
    //
    //  /**
    //   * sendMailSurveyId
    //   *
    //   * @param $syid
    //   * @param $template
    //   * @param $ident
    //   */
    //  public function sendMailSurveyId($syid, $template, $ident)
    //  {
    //    $this->initSoapClient();
    //    return $this->client->send_mail_survey_id($syid, $template, $ident);
    //  }
    //
    //  /**
    //   * Returns the results of an EFS survey as xml
    //   *
    //   * DOCME describe possible arguments
    //   *
    //   * @param array $args webservice arguments
    //   * @param bool $compressed (optional) if true
    //   *
    //   * @throws Exception
    //   * @return string survey results
    //   */
    //  public function getSurveyResultsXml(array $args, $compressed = true)
    //  {
    //    $defaultArgs = array('surveyId' => 0, 'withUserData' => false, 'dispositionCodes' => null, 'includeVarnames' => null);
    //    $args = array_merge($defaultArgs, $args);
    //
    //    // SurveyID is as must as it cannot be clever default setting
    //    if ($args['surveyId'] == 0)
    //      throw new Exception('$args["surveyId"] must be set in "' . __FUNCTION__ . '"');
    //
    //    $this->initSoapClient();
    //
    //    $result = $this->client->get_survey_results_xml($args['surveyId'], $args['withUserData'], $args['dispositionCodes'], $args['includeVarnames']);
    //
    //    if ($compressed == true)
    //      return $result;
    //
    //    return gzuncompress($result);
    //  }
    //
    //  /**
    //   * getPanelistByCondition
    //   *
    //   * @param string $ident - identifier for the panelist
    //   * @param string $cond - condition to find the panelist(s)
    //   * @throws Exception
    //   * @return
    //   */
    //  public function getPanelistByCondition($ident, $cond)
    //  {
    //    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    //    if (efs_apis_version::getVersion() < 7.1)
    //      throw new Exception('Method available since EFS 7.1 only');
    //
    //    $this->initSoapClient();
    //    return $this->client->get_panelists_by_condition($ident, $cond);
    //  }
    //
    //  /**
    //   * Returns a list of surveys
    //   *
    //   * The result can be restricted to specific survey types (e.g. panel or
    //   * masterdata surveys) and survey statuses (e.g. active, inactive, archived)
    //   *
    //   * Only surveys that the user has read privileges on will be returned.
    //   *
    //   * @param array $surveyTypes (optional) List of survey types
    //   * @param array $surveyStatuses (optional) List of survey statuses
    //   */
    //  public function getSurveysList(array $surveyTypes = null, array $surveyStatuses = null)
    //  {
    //    return $this->getSoapClient()->get_surveys_list($surveyTypes, $surveyStatuses);
    //  }
    //
    //  /**
    //   * Returns profiles of all survey participants
    //   *
    //   * @param int $survey_id
    //   * @param array $surveyFields
    //   * @param bool $includeResults
    //   */
    //  public function getSurveyParticipants($survey_id, array $surveyFields, $includeResults = false)
    //  {
    //    return $this->getSoapClient()->get_survey_participants($survey_id, $surveyFields, $includeResults);
    //  }
    //
    //
    //  public function isValidPanelistLogin(array $ident)
    //  {
    //     $this->initSoapClient();
    //
    //     $data['login_type'] = 'login_type_auto_detect';
    //     $data=array_merge($data,$ident);
    //
    //     return (bool) $this->client->is_valid_panelist_login($data['login_type'],$data['login_value1'],$data['login_value2']);
    //  }
    //
    //  /**
    //   * Updates the panelist via Ws instead of panel-api directly
    //   *
    //   * @param $options - ident, value, record
    //   */
    //  public function updatePanelistWs($options)
    //  {
    //    $this->initSoapClient();
    //    return $this->client->update_panelist($options['ident'], $options['value'], $options['record']);
    //  }
    //
    //  /**
    //   * Updates the profile information for a panelist.
    //   *
    //   * participant data plus master data
    //   * The complete profile or just parts of the profile can be updated.
    //   * The panelist can be identified by different criteria (email, user_id,
    //   * panelist code, pseudonym etc.).
    //   *
    //   * @param string $identifier_type Panelist identifier type
    //   * @param mixed $identifier_value Panelist identifier value
    //   * @param array $panelist_record Panelist record
    //   */
    //  /*public function updatePanelist($identifierType, $identifierValue, $panelistRecord)
    //  {
    //    $this->getSoapClient()->update_panelist(
    //      $identifierType,
    //      $identifierValue,
    //      $panelistRecord
    //    );
    //  }*/
    //
    //  /**
    //   * Returns a list of all panelists for a given group filter.
    //   *
    //   * The panelist identifier for the returned panelists can be user_id,
    //   * panelist code, pseudonym etc.
    //   *
    //   * @param string $identifierType Panelist identifier type
    //   * @param int $filterId Filter id
    //   * @return array Panelists identifiers
    //   */
    //  public function getPanelistsByFilter($identifierType, $filterId)
    //  {
    //    return $this->getSoapClient()->get_panelists_by_filter($identifierType, $filterId);
    //  }
    //
    //  /**
    //   * Returns the full profile for a panelist.
    //   *
    //   * participant data, behavioural information plus master data
    //   * The panelist can be identified by different criteria (email, user_id,
    //   * panelist code, pseudonym etc.).
    //   *
    //   * @param string $identifierType Panelist identifier type
    //   * @param mixed $identifierValue Panelist identifier value
    //   * @throws InvalidArgumentException
    //   * @return array Panelist record
    //   */
    //  public function getPanelist($identifierType, $identifierValue = null)
    //  {
    //    if (!is_null($identifierValue))
    //    {
    //      if (!is_string($identifierType))
    //        throw new InvalidArgumentException('Wrong type for indentifier type, has to be a string of identifier value is passed.');
    //
    //      return $this->getSoapClient()->get_panelist($identifierType, $identifierValue);
    //    }
    //
    //    // old style param usage introduced by M.Umschlag
    //
    //    if (!is_array($identifierType))
    //      throw new InvalidArgumentException('When omitting the second param first param has to be an associative array.');
    //
    //    if (!isset($identifierType['ident']))
    //      throw new InvalidArgumentException('No ident given.');
    //
    //    if (!isset($identifierType['value']))
    //      throw new InvalidArgumentException('No value given.');
    //
    //    $identifierValue = $identifierType['value'];
    //    $identifierType = $identifierType['ident'];
    //
    //    return $this->getSoapClient()->get_panelist($identifierType, $identifierValue);
    //  }
    //
    //  /**
    //   * Creates an admin session ID for the specified staff account (root usage only).
    //   *
    //   * @param $identifier_type
    //   * @param $identifier_value
    //   * @return
    //   * @internal param string $identifierType Panelist identifier type
    //   * @internal param mixed $identifierValue Panelist identifier value
    //   */
    //  public function createAdminSession($identifier_type, $identifier_value)
    //  {
    //    return $this->getSoapClient()->create_admin_session($identifier_type, $identifier_value);
    //  }
    //}
