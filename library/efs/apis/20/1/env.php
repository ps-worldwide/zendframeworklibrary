<?php
  /**
   * efs_apis_19_4_env  #EFSUPGRADE#
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.1 2020/05/06 08:09:35 fiedler Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: fiedler $
   */

  class efs_apis_20_1_env extends efs_apis_19_5_env #EFSUPGRADE#
  {
  }
