<?php
  /**
   * efs_apis_19_4_env  #EFSUPGRADE#
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.1 2021/02/04 10:10:16 fiedler Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: fiedler $
   */

  class efs_apis_20_4_env extends efs_apis_20_3_env #EFSUPGRADE#
  {
  }
