<?php
  /**
   * survey_api_exception
   *
   * @package   zendframeworkLibrary
   * @version   $Id: survey_exception.php,v 1.8 2013/10/16 09:27:40 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   * @todo      remove extends
   * @todo      check if this file is needed from a project
   */

  // This file should not be used
  if (APPLICATION_ENV != 'testing')
    mail('actionscript@questback.de', __FILE__ . ' was called', implode("\n", debug_backtrace()));

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_survey_exception extends survey_api_exception
  {
  }