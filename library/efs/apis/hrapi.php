<?php
    /** @noinspection PhpVoidFunctionResultUsedInspection */
    die(debug_print_backtrace()); /* NOFLAME */
    /**
     * efs_apis_hrapi
     * Does the EFS-API call
     *
     * @package       zendframeworkLibrary
     * @version       $Id: hrapi.php,v 1.11 2013/11/13 14:55:47 funke Exp $
     * @copyright (c) QuestBack http://www.questback.com
     * @author        $Author: funke $
     * @deprecated
     */

    //class efs_apis_hrapi extends efs_apis_hrapihack
    //{
    //  // hr api
    //  protected $api = NULL;
    //
    //  /**
    //   * __construct
    //   *
    //   */
    //  public function __construct(Zend_DB_Adapter_Abstract $db)
    //  {
    //    // so far there is no hr-api in EFS
    //    // till then methods should be hacked
    //    // into class hrapihack
    //    //$this->api = hr_api::get_instance();
    //    parent::__construct($db);
    //  }
    //
    //  /**
    //   * __call
    //   *
    //   * @param mixed $method
    //   * @param mixed $param
    //   * @throws Exception
    //   */
    //  public function __call($method, $param)
    //  {
    //    throw new Exception('Call to undefined efs-panelapi method "' . $method . '()"');
    //  }
    //}
