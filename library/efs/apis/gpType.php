<?php
  /**
   * efs_apis_gpType
   *
   * @package   zendframeworkLibrary
   * @version   $Id: gpType.php,v 1.5 2013/10/16 09:27:40 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   * @deprecated
   * @todo      8.1 check if gp_type still exists
   * @todo      remove EFS dependence (gp_type)
   */

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_gpType extends gp_type
  {
  }