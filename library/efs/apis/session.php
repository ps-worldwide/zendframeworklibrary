<?php
/**
 * efs_apis_session
 * @see modules/efs/components/session.inc.php
 *
 * @package zendframeworkLibrary
 * @version $Id: session.php,v 1.6 2013/10/16 09:27:40 funke Exp $
 * @copyright (c) QuestBack http://www.questback.com
 * @author $Author: funke $
 */

  class efs_apis_session
  {
    const TYPE_SURVEY       = 0;
    const TYPE_PANEL        = 1;
    const TYPE_PREVIEW      = 2;
    const TYPE_ADMIN        = 3;
    const TYPE_MONITOR      = 5;
    const TYPE_PROVIDER     = 8;  //ub
    const TYPE_PROJECT_TEST = 10; //ub 06 Jul 2007: gppounder-Projekttest
    const TYPE_REPORT       = 11; // stj: eportplattform
    const TYPE_MOBILE_GUI   = 12; //pk: mobile_gui
    const TYPE_DIARY        = 13;
    const TYPE_SMM          = 14;
  }