<?php
  /**
   * efs_apis_19_4_env  #EFSUPGRADE#
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.1 2021/04/06 14:06:50 fiedler Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: fiedler $
   */

  class efs_apis_21_1_env extends efs_apis_20_4_env #EFSUPGRADE#
  {
  }
