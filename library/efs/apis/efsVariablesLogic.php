<?php
  /**
   * efs_apis_efsVariablesLogic
   *
   * @see       modules/efs/components/variables/logic.inc.php
   *
   * @package   zendframeworkLibrary
   * @version   $Id: efsVariablesLogic.php,v 1.6 2013/10/16 09:27:40 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class efs_apis_efsVariablesLogic
  {
    // from class efs_variables_logic
    const TABLE_PANEL_USERS    = "panelists";
    const TABLE_PANEL_GRP      = "panelist_grp";
    const TABLE_PANEL_ATT      = "panel_att";
    const TABLE_PANEL_ATT_VAR  = "panel_att_var";
    const TABLE_PANEL_TRACKING = "panel_tracking";
    const TABLE_MD_DATA        = "md_data";
    const TABLE_CREDIT_POINTS  = "panelist_bonus_points";
    const TABLE_ADMIN_USERS    = "users";
    const TABLE_ADMIN_USER_GRP = "user_grp";

    // these are not in 'class efs_variables_logic', therefore implemented statically
    const EFS_TABLE_PROJECT                = "project";
    const EFS_TABLE_SAMPLE_DATA            = "sample_data";
    const EFS_TABLE_SAMPLE_PROJECT         = "sample_project";
    const EFS_TABLE_ITEM                   = "item";
    const EFS_TABLE_CONTAINER              = "container";
    const EFS_TABLE_QUESTION               = "question";
    const EFS_TABLE_PAGE                   = "page";
    const EFS_TABLE_SURVEY_ATT             = "survey_att";
    const EFS_TABLE_SURVEY                 = "survey_%d";
    const EFS_TABLE_BONUS_CONFIGURATION    = 'panel_bonus_configuration';
    const EFS_TABLE_PANELIST_GROUP_ARCHIVE = 'panelist_grp_archive';
    const EFS_TABLE_PRIZE_DRAWS            = 'panel_prize_draws';
    const EFS_TABLE_PRIZE_DRAW_WINNERS     = 'panel_prize_draw_winners';
    const EFS_TABLE_PRIZE_DRAW_PRIZES      = 'panel_prize_draw_prizes';
    const EFS_TABLE_USER_ATT               = 'user_att';
  }