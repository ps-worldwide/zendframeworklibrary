<?php
    /** @noinspection PhpVoidFunctionResultUsedInspection */
    die(debug_print_backtrace()); /* NOFLAME */

    /**
     * efs_apis_surveyapi
     *
     * @package   zendframeworkLibrary
     * @version   $Id: surveyapi.php,v 1.9 2013/11/13 14:55:47 funke Exp $
     * @copyright (c) QuestBack http://www.questback.com
     * @author    $Author: funke $
     */

    //abstract class efs_apis_surveyapi
    //{
    //  public function getApi()
    //  {
    //    /** @noinspection PhpUndefinedClassInspection */
    //    return survey_api::get_instance();
    //  }
    //}