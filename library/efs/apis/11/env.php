<?php
  /**
   * efs_apis_11_env  
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.2 2016/06/22 09:36:58 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class efs_apis_11_env extends efs_apis_10_9_env 
  {
  }
