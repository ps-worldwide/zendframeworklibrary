<?php
  // This file should not be used
  if (APPLICATION_ENV != 'testing')
    mail('actionscript@questback.de', __FILE__ . ' was called', implode("\n", debug_backtrace()));

  /**
   * efs_apis_env
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: enigma.php,v 1.8 2013/10/16 09:27:40 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   * @todo      : adjust efs_models_auth to support version
   * @usedby    efs_models_auth
   */

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_enigma
  {
    /**
     * check_equal
     *
     * @param string $credential
     * @param string $passwd
     */
    public function check_equal($credential, $passwd)
    {
      /** @noinspection PhpUndefinedClassInspection */
      return enigma::check_equal($credential, $passwd);
    }
  }