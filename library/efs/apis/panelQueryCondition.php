<?php
  /**
   * efs_apis_panelQueryCondition
   *
   * @see       modules/panel/components/query/condition.inc.php
   *
   * @package   zendframeworkLibrary
   * @version   $Id: panelQueryCondition.php,v 1.5 2013/10/16 09:27:40 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class efs_apis_panelQueryCondition
  {
    const OPERATOR_CONTAINS      = 1;
    const OPERATOR_BEGINS_WITH   = 2;
    const OPERATOR_ENDS_WITH     = 3;
    const OPERATOR_IN            = 4;
    const OPERATOR_EQUAL         = 5;
    const OPERATOR_UNEQUAL       = 6;
    const OPERATOR_LESS_THAN     = 7;
    const OPERATOR_GREATER_THAN  = 8;
    const OPERATOR_LESS_EQUAL    = 9;
    const OPERATOR_GREATER_EQUAL = 10;
    const OPERATOR_IS_NULL       = 11;
    const OPERATOR_IS_NOT_NULL   = 12;
    const OPERATOR_NOT_IN        = 13;
  }