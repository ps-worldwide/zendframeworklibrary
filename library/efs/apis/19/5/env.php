<?php
  /**
   * efs_apis_19_4_env  #EFSUPGRADE#
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.1 2020/02/10 16:55:12 fiedler Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: fiedler $
   */

  class efs_apis_19_5_env extends efs_apis_19_4_env #EFSUPGRADE#
  {
  }
