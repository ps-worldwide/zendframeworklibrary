<?php
  /**
   * efs_apis_19_4_env  #EFSUPGRADE#
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.1 2019/10/17 12:12:44 khalafiniya Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: khalafiniya $
   */

  class efs_apis_19_4_env extends efs_apis_19_3_env #EFSUPGRADE#
  {
  }
