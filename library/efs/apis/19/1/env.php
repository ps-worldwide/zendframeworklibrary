<?php
  /**
   * efs_apis_18_1_env  #EFSUPGRADE#
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.1 2019/03/13 16:26:47 khalafiniya Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: khalafiniya $
   */

  class efs_apis_19_1_env extends efs_apis_18_4_env #EFSUPGRADE#
  {
  }
