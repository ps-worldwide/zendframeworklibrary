<?php
  /**
   * efs_apis_19_2_env  #EFSUPGRADE#
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.1 2019/05/13 15:06:57 khalafiniya Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: khalafiniya $
   */

  class efs_apis_19_2_env extends efs_apis_19_1_env #EFSUPGRADE#
  {
  }
