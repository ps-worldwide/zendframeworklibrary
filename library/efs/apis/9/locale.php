<?php
  /**
   * efs_apis_locale
   * Extends efs_localehack (alias local0r in 7.0) in 8.0.
   *
   * @package   zendframeworkLibrary
   * @version   $Id: locale.php,v 1.5 2013/10/16 09:27:29 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   * @todo      write test
   */

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_locale extends efs_localehack
  {
  }