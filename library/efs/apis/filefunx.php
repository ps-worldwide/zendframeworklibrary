<?php

  if (APPLICATION_ENV != 'testing')
  {
    mail('actionscript@questback.de', __FILE__ . ' was called', implode("\n", debug_backtrace()));
  }

  /**
   * efs_apis_filefunx
   *
   * @package   zendframeworkLibrary
   *
   * @version   $Id: filefunx.php,v 1.9 2013/10/16 09:27:40 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   * @deprecated
   * @tested
   * @todo      : check if some projects needs this file
   */

  class efs_apis_filefunx //extends filefunx
  {
  }