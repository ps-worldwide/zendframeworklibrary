<?php
  /**
   * efs_apis_7_surveyapihack
   *
   * @package   zendframeworkLibrary
   * @version   $Id: surveyapihack.php,v 1.21 2014/04/07 15:50:06 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  abstract class efs_apis_7_surveyapihack
  {
    protected $db;

    /**
     * __construct
     *
     * @param Zend_DB_Adapter_Abstract $db
     *
     * @return \efs_apis_7_surveyapihack
     */
    public function __construct(Zend_DB_Adapter_Abstract $db)
    {
      $this->db = $db;
    }

    /**
     * deleteScaleQuestions
     *
     * @param mixed $qid
     *
     * @return bool
     */
    public function deleteScaleQuestions($qid)
    {
      $stmt = $this->db->query('DELETE FROM scale_question WHERE q_id = ?', array($qid));
      return $stmt->execute();
    }

    /**
     * insertScaleQuestions
     *
     * @param mixed $qid
     * @param       $questions
     */
    public function insertScaleQuestions($qid, $questions)
    {
      $count = 0;
      foreach ($questions as $label)
      {
        $count++; // scale_order must start with "1"

        $data                = array();
        $data['q_id']        = $qid;
        $data['scale_order'] = $count;
        $data['scale_title'] = $label;

        $this->db->insert('scale_question', $data);
      }
    }

    /**
     * changeQuestionText
     *
     * @param mixed $qid
     * @param mixed $text
     * @param mixed $inst
     * @param array $optPar
     *
     * @return int
     */
    public function changeQuestionText($qid, $text, $inst, $optPar = array())
    {
      /* Data to update */
      $data               = array();
      $data['q_text']     = $text;
      $data['q_instruct'] = $inst;
      if (array_key_exists('udf', $optPar))
        $data['udf'] = $optPar['udf'];

      /* Where */
      $where = 'q_id=' . (int)$qid;

      return $this->db->update('question', $data, $where);
    }

    /**
     * changeCotextByQid
     *
     * @param mixed $qid
     * @param mixed $text
     *
     * @return bool
     */
    public function changeCotextByQid($qid, $text)
    {
      $stmt = $this->db->query('UPDATE question, container SET' . ' container.cotext=? WHERE container.coid=question.coid AND' . ' question.q_id=?', array($text, $qid));
      return $stmt->execute();
    }

    /**
     * copy911HtmlCode
     *
     * @param mixed $src
     * @param mixed $dst
     *
     * @return bool
     */
    public function copy911HtmlCode($src, $dst)
    {
      $stmt = $this->db->query('UPDATE question q1, question q2 SET' . ' q1.udf=q2.udf WHERE q1.q_id=? AND q2.q_id=?', array($dst, $src));
      return $stmt->execute();
    }

    /**
     * scaleImagize
     *
     * @param mixed $pid
     * @param mixed $qid
     * @param mixed $code
     * @param mixed $path
     * @param mixed $name
     *
     * @return bool
     */
    protected function scaleImageize($pid, $qid, $code, $path, $name)
    {
      $stmt = $this->db->query('UPDATE r_cat r, file_manager fm SET' . ' r.image=fm.id WHERE fm.path=? AND fm.name=? AND fm.pid=?' . ' AND r.q_id=? AND r.code=?', array($path, $name, $pid, $qid, $code));
      return $stmt->execute();
    }

    /**
     * itemImageize
     *
     * @param mixed $pid
     * @param mixed $qid
     * @param mixed $code
     * @param mixed $path
     * @param mixed $name
     *
     * @return bool
     */
    protected function itemImageize($pid, $qid, $code, $path, $name)
    {
      $stmt = $this->db->query('UPDATE item i, file_manager fm SET' . ' i.image=fm.id WHERE fm.path=? AND fm.name=? AND fm.pid=?' . ' AND i.q_id=? AND i.nr=?', array($path, $name, $pid, $qid, $code));
      return $stmt->execute();
    }

    /**
     * registerTrigger
     *
     * @param mixed $pgid
     * @param mixed $iid
     * @param mixed $action
     * @param mixed $optPar
     *
     * @return int
     * @throws Exception
     * @internal param mixed $pid
     */
    protected function registerTrigger($pgid, $iid, $action, $optPar)
    {
      /* Add a new trigger - tr_id is auto-increment */
      $data                 = array();
      $data['pgid']         = $pgid;
      $data['iid']          = $iid;
      $data['post_page']    = $optPar['post_page'];
      $data['post_filter']  = $optPar['post_filter'];
      $data['trigger_name'] = $optPar['trigger_name'];
      $data['trigger_desc'] = $optPar['trigger_desc'];
      $data['filter_id']    = $optPar['filter_id'];
      $data['exec_order']   = $optPar['exec_order'];
      $data['exec_preview'] = $optPar['exec_preview'];
      $data['exec_mult']    = $optPar['exec_mult'];
      $data['action']       = $action;
      $this->db->insert('triggers', $data);
      $trId = (int)$this->db->lastInsertId();

      /* Select max execution order plus one */
      $select  = $this->db->select()
                 ->from('triggers', array('exec' => 'MAX(exec_order)+1'))
                 ->where('triggers.pgid=?', $pgid)
                 ->group('triggers.pgid');
      $stmt    = $select->query();
      $result  = $stmt->fetchObject();
      $maxExec = $result->exec;

      /* Adjust the execution order of the newly added trigger */
      $stmt = $this->db->query('UPDATE triggers SET exec_order=?'
                               . ' WHERE tr_id=?', array($maxExec, $trId));
      $res  = $stmt->execute();
      if (!$res)
        throw new Exception('Cannot adjust exec_order due to SQL error');

      return $trId;
    }

    /**
     * registerRecodeTrigger
     *
     */
    protected function registerTriggerRecode()
    {
      $data         = array();
      $data['data'] = serialize(array());
      $this->db->insert('trigger_recode', $data);
      return $this->db->lastInsertId();
    }

    /**
     * addRecodeTriggerRecodings
     *
     * @param mixed $trId
     * @param mixed $rId
     * @param array $recodes
     *
     * @throws Exception
     * @return bool
     */
    protected function addTriggerRecodeRecodings($trId, $rId, array $recodes)
    {
      if (count($recodes) < 1)
        return false;

      /* Max order-no for this trigger */
      $select = $this->db->select()
                ->from('trigger_recodes', array('rId' => 'MAX(nr)'))
                ->where('r_id=?', $rId);
      $stmt   = $select->query();
      $result = $stmt->fetchObject();
      $max    = (int)$result->rId;
      foreach ($recodes as $recodeDef)
      {
        $data            = array();
        $data['nr']      = ++$max;
        $data['varname'] = $recodeDef['varname'];
        $data['value']   = $recodeDef['value'];
        $data['r_id']    = $rId;

        /* Filter needed? */
        if (array_key_exists('filter', $recodeDef))
          $data['filter_id'] = $this->addFilter($recodeDef['filter']);
        else
          $data['filter_id'] = 0;

        $suc = $this->db->insert('trigger_recodes', $data);
        if (!$suc)
          throw new Exception('Cannot add recoding due to SQL error');
      }
      return true;
    }

    /**
     * addFilter
     *
     * @param mixed $def
     *
     * @throws Exception
     * @return string
     */
    private function addFilter($def)
    {
      $data                 = array();
      $data['filter_def']   = $def;
      $data['filter_type']  = 1;
      $data['filter_state'] = 1;

      $suc = $this->db->insert('filter', $data);
      if (!$suc)
        throw new Exception('Cannot add filter for recoding trigger');
      return $this->db->lastInsertId();
    }

    /**
     * questionSetDynamicBreak
     *
     * @param integer $qid
     * @param integer $value
     *
     * @return int
     */
    protected function questionSetDynamicBreak($qid, $value)
    {
      $data  = array('dynamic_break' => (int)$value);
      $where = 'q_id=' . (int)$qid;
      return $this->db->update('question', $data, $where);
    }

    /**
     * addAutoSubmit
     *
     * @param mixed $pgid
     * @param mixed $event
     *
     * @return int
     */
    protected function addAutoSubmit($pgid, $event)
    {
      $data  = array('submit_event' => $event);
      $where = 'pgid=' . (int)$pgid;
      return $this->db->update('page', $data, $where);
    }

    /**
     * changeTranslation
     *
     * @param mixed $pid
     * @param mixed $lang
     * @param mixed $id
     * @param       $type
     * @param mixed $text
     *
     * @return int
     */
    public function changeTranslation($pid, $lang, $id, $type, $text)
    {
      $pid = (int)$pid;

      $table = 'project_languages_' . $pid;
      $where = array('lang_id=' . (int)$lang,
                     'text_type=\'' . addslashes($type) . '\'',
                     'type_id=' . (int)$id);
      $data  = array('text' => $text);

      /* Is there already an existing entry? */
      $select = $this->db->select();
      $select->from($table);
      $select->where('lang_id=?', $lang);
      $select->where('text_type=?', $type);
      $select->where('type_id=?', $id);
      $stmt   = $this->db->query($select);
      $result = $stmt->fetchAll();

      /* If there is a record update it */
      if (count($result) > 0)
      {
        return $this->db->update($table, $data, $where);
      }
      /* No entry so add one */
      else
      {
        $data['lang_id']   = (int)$lang;
        $data['text_type'] = addslashes($type);
        $data['type_id']   = (int)$id;
        return $this->db->insert($table, $data);
      }
    }

    /**
     * hackGetAddedItems
     *
     * @param $qid
     *
     * @return array
     */
    protected function hackGetAddedItems($qid)
    {
      $select = $this->db->select();
      $select->from('item', array('v_id', 'nr', 'varname'));
      $select->where('q_id=?', $qid);
      $select->order('nr');
      $stmt = $this->db->query($select);
      return $stmt->fetchAll();
    }

    /**
     * hackChange911Php
     *
     * @param $qid
     * @param $code
     *
     * @return int
     */
    public function hackChange911Php($qid, $code)
    {
      $data  = array('php' => $code);
      $where = 'q_id=' . (int)$qid;
      return $this->db->update('question', $data, $where);
    }

    /**
     * hackAddNewPage
     *
     * @param $pid
     * @param $title
     * @param $parent
     * @param $type
     */
    protected function hackAddNewPage($pid, $title, $parent = -1, $type = efs_apis_const::PAGETYPE_STANDARD)
    {
      /* edit-admin can add pages */
      /** @noinspection PhpUndefinedClassInspection */
      $ea = new edit_admin(efs_env::get_db());

      /** @noinspection PhpUndefinedMethodInspection */
      return $ea->proc_new_page($pid, $parent, -1
        , $title, '', $type, 0, '', false);
    }

    /**
     * hackAddScaleOptions
     *
     * @param       $pid
     * @param       $q_id
     * @param array $scales
     */
    protected function hackAddScaleOptions($pid, $q_id, array $scales)
    {
      $sc_access     = $this->hackGetInternalScaleAccessKeys();
      $it_access     = $this->hackGetInternalItemAccessKeys($pid, $q_id);
      $prepared_data = array();
      $nr            = 1;
      foreach ($scales AS $scale)
      {
        $prepared_data["idcollection"] [$sc_access["prefix"] . "new_" . $nr] = 1;
        foreach ((array)$sc_access["create_props"] AS $external_name => $internal_name)
        {
          $prepared_data[$internal_name][$sc_access["prefix"] . "new_" . $nr] = $scale[$external_name];
        }
        $nr++;
      }
      $existing_scales = $this->hackGetScaleOptions($pid, $q_id);
      foreach ($existing_scales AS $existing_scale)
      {
        $prepared_data["idcollection"][$sc_access["prefix"] . $existing_scale[$sc_access["db_key_field"]]]           = 1;
        $prepared_data[$sc_access["editor_key"]][$sc_access["prefix"] . $existing_scale[$sc_access["db_key_field"]]] = $existing_scale[$sc_access["db_sort_field"]];
        foreach ((array)$sc_access["edit_props"] AS $external_name => $internal_name)
        {
          $prepared_data[$internal_name][$sc_access["prefix"] . $existing_scale[$sc_access["db_key_field"]]] = $existing_scale[$external_name];
        }
      }

      $existing_items = $this->hackGetAnswerOptions($pid, $q_id);
      foreach ($existing_items AS $existing_item)
      {
        $prepared_data["idcollection"][$it_access["prefix"] . $existing_item[$it_access["db_key_field"]]]           = 1;
        $prepared_data[$it_access["editor_key"]][$it_access["prefix"] . $existing_item[$it_access["db_key_field"]]] = $existing_item[$it_access["db_sort_field"]];
        foreach ((array)$it_access["edit_props"] AS $external_name => $internal_name)
        {
          $prepared_data[$internal_name][$it_access["prefix"] . $existing_item[$it_access["db_key_field"]]] = $existing_item[$external_name];
        }
      }

      /** @noinspection PhpUndefinedClassInspection */
      $question_editor = new question_editor(efs_env::get_db(), (int)$pid, (int)$q_id);

      /** @noinspection PhpUndefinedMethodInspection */
      return $question_editor->proc_edit($prepared_data, true);
    }

    /**
     * hackGetInternalScaleAccessKeys
     */
    protected function hackGetInternalScaleAccessKeys()
    {
      $access                  = array();
      $access["prefix"]        = "SR_"; //prefix
      $access["editor_key"]    = "rc_code"; //name des array-keys fuers anlegen
      $access["db_sort_field"] = "option_code"; //"code" name des sort-feldes in db
      $access["db_key_field"]  = "option_id"; //"rc_id" name des key-feldes in db
      //quest_editor-properties fuer anlegen eines skalenelem.
      $access["create_props"] = array("option_text" => "label",
                                      "option_code" => "rc_code");
      //quest_editor-property=>db_feld fuer editieren eines skalenelem.
      $access["edit_props"] = array("option_text"       => "label", //"r_cat"
                                    "option_code"       => "rc_code",
                                    "option_randomized" => "rot", //"in_rotation",
                                    "option_missing"    => "missing", //"missing"
      );
      return $access;
    }

    /**
     * hackGetInternalItemAccessKeys
     *
     * @param $pid
     * @param $q_id
     *
     * @return array
     */
    protected function hackGetInternalItemAccessKeys($pid, $q_id)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      $q_data = $this->getQuestion($pid, $q_id);

      $access                  = array();
      $access["prefix"]        = "IT_"; //prefix
      $access["editor_key"]    = "sort_nr"; //name des array-keys fuers anlegen
      $access["db_sort_field"] = "option_number"; //"nr"     //name des sort-feldes in db
      $access["db_key_field"]  = "option_id"; //"v_id"    //name des key-feldes in db

      //quest_editor-properties fuer anlegen eines items
      $access["create_props"] = array("option_text"        => "label",
                                      "option_sort_number" => "sort_nr");
      //quest_editor-property=>db_feld fuer editieren eines items
      $access["edit_props"] = array("option_text"                 => "label", //"itemtext"
                                    "option_sort_number"          => "sort_nr",
                                    "option_randomized"           => "rot", //"in_rotation"
                                    "option_answer_check"         => "dac", //"do_answercheck"),
                                    "option_allways_visible"      => "allways_visible", //"allways_visible"
                                    "option_output_variable_name" => "varname" //"output_varname"));
      );

      /** @noinspection PhpDeprecationInspection */
      /** @noinspection PhpUndefinedMethodInspection */
      if (efs_apis_gpType::get_response_type($q_data["question_type"]) == efs_apis_gpType::TYPE_SINGLE_RESPONSE)
      {
        $access["prefix"]        = "RC_";
        $access["editor_key"]    = "rc_code";
        $access["db_sort_field"] = "option_code"; //"code"
        $access["db_key_field"]  = "option_id"; //"rc_id"

        $access["create_props"] = array("option_text"        => "label",
                                        "option_sort_number" => "rc_code");

        $access["edit_props"] = array("option_text"         => "label", //"r_cat"
                                      "option_code"         => "rc_code",
                                      "option_randomized"   => "rot", //"in_rotation"
                                      "option_missing"      => "missing", //"missing"
                                      "option_column_break" => "cob", //"col_break"
                                      "option_sort_number"  => "sort_nr",
        );
      }

      if ($q_data["question_type"] == efs_apis_const::Q340)
      {
        $access["create_props"]["option_text_right"] = "label_right";
        $access["edit_props"]["option_text_right"]   = "label_right";
      }

      /** @noinspection PhpDeprecationInspection */
      /** @noinspection PhpUndefinedMethodInspection */
      if (in_array($q_data["question_type"], efs_apis_gpType::imagetypes()))
      {
        $access["create_props"]["option_assigned_image"] = "image";
        $access["edit_props"]["option_assigned_image"]   = "image";
      }
      return $access;
    }

    /**
     * hackGetScaleOptions
     *
     * @param $pid
     * @param $q_id
     *
     * @return array
     */
    protected function hackGetScaleOptions($pid, $q_id)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      $q_data = $this->getQuestion((int)$pid, (int)$q_id);
      /** @noinspection PhpUndefinedClassInspection */
      $question_editor = new question_editor(efs_env::get_db(), (int)$pid, (int)$q_id);
      /** @noinspection PhpUndefinedFieldInspection */
      $items = $question_editor->question_db->get_items();
      /** @noinspection PhpUndefinedFieldInspection */
      $item_id = $question_editor->question_db->get_usable_item_id($items);
      /** @noinspection PhpUndefinedFieldInspection */
      $result_arr = $question_editor->question_db->get_r_cats_by_item($item_id);

      return $this->hackInternalFormatRCats($result_arr, $q_data["question_type"]);
    }

    /**
     * hackInternalFormatRCats
     *
     * @param $result_arr
     * @param $gp_type
     *
     * @return array
     */
    private function hackInternalFormatRCats($result_arr, $gp_type)
    {
      $formatted = array();
      foreach ((array)$result_arr AS $key => $result)
      {
        $formatted[$key]["option_id"]              = (int)$result["rc_id"];
        $formatted[$key]["option_text"]            = (string)$result["r_cat"];
        $formatted[$key]["option_number"]          = (int)$result["nr"];
        $formatted[$key]["option_column_break"]    = (bool)$result["col_break"];
        $formatted[$key]["option_randomized"]      = (bool)$result["in_rotation"];
        $formatted[$key]["option_filter_id"]       = (int)$result["filter_id"];
        $formatted[$key]["option_list_element_id"] = (int)$result["list_element_id"];
        $formatted[$key]["option_scale_order"]     = (int)$result["scale_order"];

        /** @noinspection PhpDeprecationInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        if (efs_apis_gpType::is_supported_feature($gp_type, efs_apis_gpType::FEATURE_GROUPING))
          $formatted[$key]["option_grouping"] = (int)$result["grouping"];
        /** @noinspection PhpDeprecationInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        if (efs_apis_gpType::is_supported_feature($gp_type, efs_apis_gpType::FEATURE_IMAGE))
          $formatted[$key]["option_assigned_image"] = (int)$result["image"];

        $formatted[$key]["option_code"]         = (int)$result["code"];
        $formatted[$key]["option_sort_number"]  = (int)$result["sort_nr"];
        $formatted[$key]["option_missing"]      = (bool)$result["missing"];
        $formatted[$key]["option_element_type"] = (int)$result["element_type"];
        $formatted[$key]["option_parent_id"]    = (int)$result["v_id"];
      }
      return $formatted;
    }

    /**
     * hackGetAnswerOptions
     *
     * @param $pid
     * @param $q_id
     *
     * @return array
     */
    protected function hackGetAnswerOptions($pid, $q_id)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      $q_data = $this->getQuestion((int)$pid, (int)$q_id);
      /** @noinspection PhpDeprecationInspection */
      /** @noinspection PhpUndefinedMethodInspection */
      switch (efs_apis_gpType::get_response_type($q_data["question_type"]))
      {
        /** @noinspection PhpDeprecationInspection */
        case efs_apis_gpType::TYPE_SINGLE_RESPONSE:
          return $this->hackInternalGetRCats($pid, $q_id, $q_data["question_type"]);

        default:
          return $this->hackInternalGetItems($pid, $q_id, $q_data["question_type"]);
      }
    }

    /**
     * hackInternalGetRCats
     *
     * @param $pid
     * @param $q_id
     * @param $gp_type
     *
     * @return array
     */
    private function hackInternalGetRCats($pid, $q_id, $gp_type)
    {
      /** @noinspection PhpUndefinedClassInspection */
      $question_editor = new question_editor(efs_env::get_db(), (int)$pid, (int)$q_id);
      /** @noinspection PhpUndefinedFieldInspection */
      $result_arr = $question_editor->question_db->get_r_cats();
      return $this->hackInternalFormatRCats($result_arr, $gp_type);
    }

    /**
     * hackInternalGetItems
     *
     * @param $pid
     * @param $q_id
     * @param $gp_type
     *
     * @return array
     */
    private function hackInternalGetItems($pid, $q_id, $gp_type)
    {
      /** @noinspection PhpUndefinedClassInspection */
      $question_editor = new question_editor(efs_env::get_db(), (int)$pid, (int)$q_id);
      /** @noinspection PhpUndefinedFieldInspection */
      $result_arr = $question_editor->question_db->get_items();
      $formatted  = array();

      foreach ((array)$result_arr AS $key => $result)
      {
        $formatted[$key]["option_id"]              = (int)$result["v_id"];
        $formatted[$key]["option_text"]            = (string)$result["itemtext"];
        $formatted[$key]["option_number"]          = (int)$result["nr"];
        $formatted[$key]["option_column_break"]    = (bool)$result["col_break"];
        $formatted[$key]["option_randomized"]      = (bool)$result["in_rotation"];
        $formatted[$key]["option_filter_id"]       = (int)$result["filter_id"];
        $formatted[$key]["option_list_element_id"] = (int)$result["list_element_id"];
        $formatted[$key]["scale_order"]            = (int)$result["scale_order"];

        /** @noinspection PhpDeprecationInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        if (efs_apis_gpType::is_supported_feature($gp_type, efs_apis_gpType::FEATURE_GROUPING))
          $formatted[$key]["option_grouping"] = (int)$result["grouping"];
        /** @noinspection PhpDeprecationInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        if (efs_apis_gpType::is_supported_feature($gp_type, efs_apis_gpType::FEATURE_IMAGE))
          $formatted[$key]["option_assigned_image"] = (int)$result["image"];

        $formatted[$key]["option_variable_name"]        = (string)$result["varname"];
        $formatted[$key]["option_variable_type"]        = (string)$result["vartype"];
        $formatted[$key]["option_variable_size"]        = (string)$result["varsize"];
        $formatted[$key]["option_output_variable_name"] = (string)$result["output_varname"];
        $formatted[$key]["option_answer_check"]         = (bool)$result["do_answercheck"];
        $formatted[$key]["option_allways_visible"]      = (bool)$result["allways_visible"];
        $formatted[$key]["option_is_exclusive"]         = (bool)$result["is_exclusive"];
        $formatted[$key]["option_text_right"]           = (string)$result["itemtext_right"];
        if ($gp_type == efs_apis_const::Q141)
        {
          $formatted[$key]["option_input_format"] = (int)$result["vdecl"]; //Input format
        }
        $formatted[$key]["option_num_rows"]        = (int)$result["num_rows"];
        $formatted[$key]["option_num_cols"]        = (int)$result["num_cols"];
        $formatted[$key]["option_htmlsize"]        = (int)$result["htmlsize"];
        $formatted[$key]["option_background_text"] = (string)$result["background_text"];
        $formatted[$key]["option_jsspecial"]       = (string)$result["jsspecial"];
      }
      return $formatted;
    }

    /**
     * hackGetVarnameForRcat
     *
     * @param $pId
     * @param $qId
     * @param $items
     */
    protected function hackGetVarnameForRcat($pId, $qId, $items)
    {
      $qData = $this->efsApi->get_question($pId, $qId);
      /** @noinspection PhpDeprecationInspection */
      /** @noinspection PhpUndefinedMethodInspection */
      $responseType = efs_apis_gpType::get_response_type($qData['question_type']);

      /** @noinspection PhpDeprecationInspection */
      if ($responseType == efs_apis_gpType::TYPE_SINGLE_RESPONSE)
      {
        foreach ($items as $vId => $itemData)
        {
          $select = $this->db->select()->from('item', array('varname' => 'varname'))
                    ->from('r_cat')
                    ->where('item.v_id=r_cat.v_id')
                    ->where('r_cat.rc_id=?', $itemData['option_id']);
          $stmt   = $select->query();
          $result = $stmt->fetchObject();

          $items[$vId]['option_variable_name'] = $result->varname;
        }
      }

      return $items;
    }

    /**
     * getTableFields
     * gets fields and datatypes from table
     *
     * @param string $table
     *
     * @return array fields
     */
    public function getTableFields($table)
    {
      if ($table == "")
        return array();

      $result = $this->db->describeTable($table);
      if (!is_array($result) || count($result) == 0)
        return array();

      $resultData = array();
      foreach ($result as $field => $fieldData)
      {
        $resultData[$field] = $fieldData["DATA_TYPE"];
      }

      return $resultData;
    }

    /**
     * Returns an array holding the sample ids for the given project
     * ID or an empty array when no sample ids were found.
     *
     * @package surveyApp
     *
     * @param integer $pid
     *
     * @return array $sids
     */
    protected function hackGetSidsFromPid($pid)
    {
      $sids = array();

      if (!is_integer($pid))
      {
        return $sids;
      }

      // get sid from pid
      $stmt = $this->db->query('SELECT sid FROM sample_project WHERE pid=?', array($pid));
      $res  = $stmt->fetchAll();

      foreach ($res as $index => $arrData)
      {
        if ((int)$res[$index]['sid'] > 0)
        {
          $sids[] = (int)$res[$index]['sid'];
        }
      }
      return $sids;
    }

    /**
     * Increments the interview counter for the given survey id.
     * Query is copied from EFS itself.
     *
     * @param integer $syid
     *
     * @throws Exception
     * @return \Zend_Db_Statement_Interface
     */
    protected function hackAddInterviewCount($syid)
    {
      $syid = (int)$syid;
      if ($syid == 0)
      {
        throw new Exception('Cannot add interview count to syid ' . $syid);
      }

      try
      {
        $stmt = $this->db->query(
          'INSERT INTO interview_count'
          . ' SET pid=?, count=1, year=YEAR( CURDATE() ), month=MONTH( CURDATE() ), status=31'
          . ' ON DUPLICATE KEY UPDATE count=count+1',
          array($syid));
      } catch (Zend_Db_Statement_Exception $e)
      {
        throw new Exception('DB::ERR::Cannot add interview count', $e->getCode(), $e);
      }
      return $stmt;
    }

    /**
     * Adds an entry into the survey table via efs_apis_surveyaccess /
     * survey_table_access
     *
     * @param integer $syid
     * @param array   $vVars
     *
     * @return integer lfdn
     */
    protected function hackAddSurveyEntry($syid, $vVars)
    {
      $surveyAccess = new efs_apis_surveyaccess ($syid);

      /** @noinspection PhpUndefinedMethodInspection */
      return (int)$surveyAccess->insert_simple($vVars);
    }

    /**
     * Return the code from the sample data for the provided user-id (uid)
     * in the given project-id (pid)
     *
     * @param $syid
     * @param $uid
     *
     * @return string $code
     */
    protected function hackGetCodeForPidUid($syid, $uid)
    {
      if (false == is_integer($syid) || false == is_integer($uid))
      {
        return '';
      }

      $stmt = $this->db->query(
        'SELECT code FROM sample_data sd, sample_project sp WHERE sd.uid=? AND sp.sid=sd.sid and sp.pid=?',
        array($uid, $syid));
      $res  = $stmt->fetchAll();
      return $res[0]['code'];
    }

    /**
     * Returns reports for a project id
     *
     * Return value is an id => name mapping
     *
     * @param int $pid project ID
     *
     * @return array reports
     */
    protected function hackGetReportIds($pid)
    {
      $results = array();

      $reports = $this->db->fetchAll(
        'SELECT rid, name FROM rep WHERE pid = ?',
        array($pid)
      );

      foreach ($reports as $r)
      {
        $results[$r['rid']] = $r['name'];
      }

      return $results;
    }

    /**
     * Returns a <code>Zend_Http_Client</code> that is able to call EFS backend URIs
     *
     * @param string $path      subpath under http://<domain>/www/
     * @param string $sessionId session id
     *
     * @return Zend_Http_Client http client
     */
    protected function hackGetHttpClientForEfs($path, $sessionId)
    {
      $client = new Zend_Http_Client(
        Zend_Registry::get('conf')->baseurl . $path,
        array('strict' => false)
      );

      $dbConfig = $this->db->getConfig();

      $sessionName = $dbConfig['dbname'] . '-session';

      $client->setCookie($sessionName, $sessionId);

      return $client;
    }

    /**
     * Updates a project
     *
     * Valid data fields:
     * - ptyp
     * - ptitle
     * - pdescr
     * - pauth
     * - pstaff
     * - pcomment
     * - pstatus
     * - pstart
     * - pfinish
     *
     * @param int   $pid  project id
     * @param array $data project data (mapping field => value)
     *
     * @return int number of affected rows
     * @throws InvalidArgumentException if any field in project data is invalid or project status is invalid
     */
    protected function hackUpdateProject($pid, array $data)
    {
      if (!count($data))
      {
        return 0; //Nothing to do
      }

      //Fixme: ptyp cannot just be updated... there is more than just change the
      // column values in the project table
      $validFields = array(
        'ptyp',
        'ptitle',
        'pdescr',
        'pauth',
        'pstaff',
        'pcomment',
        'pstatus',
        'pstart',
        'pfinish',
      );

      $invalidFields = array_diff(array_keys($data), $validFields);
      if (count($invalidFields) > 0)
      {
        throw new InvalidArgumentException(
          sprintf(
            'Invalid project data fields "%s".',
            implode(', ', $invalidFields)
          )
        );
      }

      if (isset($data['pstatus']) &&
          !in_array($data['pstatus'], efs_apis_projectStatus::get_project_statuses())
      )
      {
        throw new InvalidArgumentException(
          sprintf('Invalid project status %d', $data['pstatus'])
        );
      }

      $where            = array();
      $where['pid = ?'] = $pid;

      return $this->db->update('project', $data, $where);
    }

    /**
     * Returns available project statuses
     *
     * @return array statuses
     */
    protected function hackGetProjectStatuses()
    {
      return efs_apis_projectStatus::get_project_statuses();
    }

    /**
     * Returns valid page types
     *
     * @return array valid page types
     */
    private function getValidPageTypes()
    {
      return array(1, 2, 3, 4, 8, 9, 10, 16, 17, 23, 24, 32, 41, 51);
    }

    /**
     * Returns page ids by project id and type
     *
     * @param int $pid  project id
     * @param int $type page type
     *
     * @return array page ids
     * @throws InvalidArgumentException if page type is invalid
     */
    protected function hackGetPageIdsByType($pid, $type)
    {
      if (!in_array($type, $this->getValidPageTypes()))
      {
        throw new InvalidArgumentException(sprintf('Invalid page type "%d"', $type));
      }

      $results = array();

      $pages = $this->db->fetchAll(
        'SELECT pgid FROM page WHERE syid = ? AND pgtype = ?',
        array($pid, $type)
      );

      foreach ($pages as $p)
      {
        $results[] = $p['pgid'];
      }

      return $results;
    }

    /**
     * Updates a survey page
     *
     * Valid data fields:
     * - pgtitle
     * - external_url
     *
     * @param int   $pid    project id project id
     * @param int   $pageId pageid
     * @param array $data   page data (mapping field => value)
     *
     * @return int
     * @throws InvalidArgumentException if any field in project data is invalid or external url is invalid
     */
    protected function hackUpdatePage($pid, $pageId, array $data)
    {
      if (!count($data))
      {
        return 0; //Nothing to do
      }

      $validFields = array(
        'pgtitle',
        'external_url',
      );

      $invalidFields = array_diff(array_keys($data), $validFields);
      if (count($invalidFields) > 0)
      {
        throw new InvalidArgumentException(
          sprintf(
            'Invalid page data fields "%s".',
            implode(', ', $invalidFields)
          )
        );
      }

      if (isset($data['external_url']) && !Zend_Uri::check($data['external_url']))
      {
        throw new InvalidArgumentException(
          sprintf(
            'Invalid external url "%s".',
            $data['external_url']
          )
        );
      }

      $where             = array();
      $where['syid = ?'] = $pid;
      $where['pgid = ?'] = $pageId;

      return $this->db->update('page', $data, $where);
    }

    /**
     * Returns a projects absolute URL
     *
     * @param int $pid project id
     *
     * @return string projects absolute URL
     */
    protected function hackGetAbsoluteProjectUrl($pid)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return efs_apis_projectInfo::get_abs_url($pid);
    }

    /**
     * Updates the sample-data record
     *
     * @param array $arrLfdn
     * @param array $arrWhere
     *
     * @return int
     */
    protected function hackUpdateSampleData($arrLfdn, $arrWhere)
    {
      $where['uid=?'] = $arrWhere['uid'];
      $where['sid=?'] = $arrWhere['sid'];
      return $this->db->update('sample_data', array('lfdn' => $arrLfdn['lfdn']), $where);
    }

    /**
     * Selects sample information from database
     *
     * @param int $sid
     *
     * @return mixed
     */
    protected function hackGetSample($sid)
    {
      $sid = (int)$sid;

      $select = $this->db->select()
                ->from(array('sp' => 'sample_project'))
                ->join(array('p' => 'project'), 'sp.pid=p.pid')
                ->where('sp.sid=?', array($sid));
      $stmt   = $this->db->query($select);
      return $stmt->fetch();
    }
  }