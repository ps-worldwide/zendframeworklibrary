<?php
  /**
   * efs_apis_7_hrapihack
   * EFS-API - methods that are not yet part of EFS itself
   *
   * @package   zendframeworkLibrary
   * @version   $Id: hrapihack.php,v 1.10 2013/10/16 09:27:07 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  abstract class efs_apis_7_hrapihack
  {
    private $db;
    private $org_processor_model;

    /**
     * __construct
     *
     * @param Zend_DB_Adapter_Abstract $db
     *
     * @return \efs_apis_7_hrapihack
     */
    public function __construct(Zend_DB_Adapter_Abstract $db)
    {
      $config   = Zend_Registry::get("conf")->toArray();
      $this->db = $db;

      if (array_key_exists('efs', $config))
      {
        if (array_key_exists('pid', $config['efs']))
        {
          /** @noinspection PhpUndefinedClassInspection */
          $this->org_processor_model = org_objects_factory::get_org_model($config["efs"]["pid"]);
        }
      }
    }

    /**
     * HelloHrWorld
     */
    public function helloHrWorld()
    {
      return 'Welcome to HR world';
    }

    /**
     * getResponsibleNodes
     *
     * @param int $uid
     */
    public function getResponsibleNodes($uid)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->org_processor_model->get_matrix_user_units($uid);
    }

    /**
     * setNodeValues
     *
     * @param array $values
     * @param int   $uid
     *
     * @return string
     */
    public function setNodeValues($values, $uid)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      if (!$this->org_processor_model->lock_structure())
        return "blocked";

      $node_vals = $this->getResponsibleNodes($uid);
      foreach ($values as $unit => $data)
      {
        if ($node_vals[$unit]["co_target"] == $data)
          continue;
        $node_vals[$unit]["co_target"] = $data;
        /** @noinspection PhpUndefinedMethodInspection */
        $this->org_processor_model->change_org_unit($node_vals[$unit], (int)$unit);
      }
      /** @noinspection PhpUndefinedMethodInspection */
      $this->org_processor_model->unlock_structure();
      return "sucess";
    }
  }