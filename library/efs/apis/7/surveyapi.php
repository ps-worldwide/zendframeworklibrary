<?php
  /**
   * efs_apis_7_surveyapi
   *
   * @package   zendframeworkLibrary
   * @version   $Id: surveyapi.php,v 1.22 2014/02/18 18:02:44 umschlag Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: umschlag $
   */

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_7_surveyapi extends efs_apis_7_surveyapihack
  {
    protected $efsApi = NULL;

    /**
     * Validated project ids cahce
     *
     * @var array
     */
    private $validProjectIds = array();

    /**
     * __construct
     *
     * @param Zend_DB_Adapter_Abstract $db
     *
     * @return \efs_apis_7_surveyapi
     */
    public function __construct(Zend_DB_Adapter_Abstract $db)
    {
      /** @noinspection PhpUndefinedClassInspection */
      $this->efsApi = survey_api::get_instance();
      parent::__construct($db);
    }

    /**
     * __call
     *
     * @param mixed $method
     * @param mixed $param
     *
     * @throws Exception
     * @return void
     */
    public function __call($method, $param)
    {
      throw new Exception(sprintf('Call to undefined efs-api method "%s()"', $method));
    }

    /**
     * setOptPar
     *
     * @param array $optPar
     * @param array $parSet
     *
     * @return array $retPar
     */
    private function setOptPar(array $optPar, array $parSet)
    {
      $retPar = array();
      foreach ($optPar as $field => $value)
      {
        if (array_key_exists($field, $parSet))
          $retPar[$field] = $parSet;
        else
          $retPar[$field] = $value;
      }

      return $retPar;
    }

    /* PROJECT */
    /**
     * getProjectInfo
     *
     * @param mixed $pid
     *
     * @throws Exception
     */
    public function getProjectInfo($pid)
    {
      if ($pid < 1)
        throw new Exception('Invalid project-id');

      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->get_project($pid);
    }

    /**
     * Copies a project
     *
     * @param int    $pid   base projects id
     * @param int    $type  new projects type
     * @param string $title new projects title
     *
     * @return int new projects id
     */
    public function copyProject($pid, $type, $title)
    {
      $this->validateProjectId($pid);

      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->copy_project($pid, $type, $title);
    }

    /**
     * deleteProject
     *
     * @param mixed $pid
     */
    public function deleteProject($pid)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->delete_project($pid);
    }

    /**
     * getListByName
     *
     * @param mixed $pid
     * @param mixed $name
     * @param array $optPar
     */
    public function getListByName($pid, $name, $optPar = array())
    {
      if (array_key_exists('first_only', $optPar) && $optPar['first_only'] == true)
        $first_only = true;
      else
        $first_only = false;

      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->get_list_by_name($pid, $name, $first_only);
    }

    /**
     * getListAccessKeys
     *
     */
    public function getListAccessKeys()
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->get_list_element_access_keys();
    }

    /**
     * addListElement
     *
     * @param mixed $pid
     * @param mixed $listId
     * @param mixed $eleNo
     * @param mixed $wc1
     */
    public function addListElement($pid, $listId, $eleNo, $wc1)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->add_static_list_item($pid, $listId, array("list_element_number" => $eleNo, "list_element_wildcard_1" => $wc1));
    }

    /**
     * getListItems
     *
     * @param mixed $pid
     * @param mixed $listId
     */
    public function getListItems($pid, $listId)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->get_list_items($pid, $listId);
    }

    /**
     * changeListElementCondition
     *
     * @param mixed $pid
     * @param mixed $listId
     * @param mixed $eleId
     * @param mixed $cond
     *
     * @throws Exception
     */
    public function changeListElementCondition($pid, $listId, $eleId, $cond)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      if (!$this->efsApi->change_list_item_inclusion($pid, $listId, $eleId, array("list_element_condition_type" => efs_apis_const::CONDITION_TYPE_IN_LIST_IF)))
        throw new Exception("Error in API call. Cannot adjust inclusion condition type! " . efs_apis_const::CONDITION_TYPE_IN_LIST_IF);

      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->change_list_item_condition($pid, $listId, $eleId, array("filter_string" => $cond));
    }

    /**
     * changeListWildcards
     *
     * @param mixed $pid
     * @param mixed $listId
     * @param mixed $eleId
     * @param mixed $arrWc
     */
    public function changeListWildcards($pid, $listId, $eleId, $arrWc)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->change_list_item_wildcard($pid, $listId, $eleId, array("list_element_wildcard_2" => (string)$arrWc[2], "list_element_wildcard_3" => (string)$arrWc[3], "list_element_wildcard_4" => (string)$arrWc[4], "list_element_wildcard_5" => (string)$arrWc[5]));
    }

    /**
     * getWildcardAccessKeys
     *
     */
    public function getWildcardAccessKeys()
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->get_placeholder_access_keys();
    }

    /**
     * getWildcards
     *
     * @param mixed  $pid
     * @param string $needle
     */
    public function getWildcards($pid, $needle = '')
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->get_placeholders($pid, $needle);
    }

    /**
     * addWildcardsToSurvey
     *
     * @param mixed $pid
     * @param mixed $arrWc
     *
     * @return array
     * @throws Exception
     */
    public function addWildcardsToSurvey($pid, $arrWc)
    {
      /* Assignment ID -> text */
      $return = array();
      if (!is_array($arrWc) || count($arrWc) < 1)
        throw new Exception('No wildcards given');
      foreach ($arrWc as $def)
      {
        list ($placeholder, $text) = each($def);
        /** @noinspection PhpUndefinedMethodInspection */
        $phId = $this->efsApi->add_placeholder($pid, array("placeholder" => $placeholder, "placeholder_text" => $text));
        if (!$phId)
          throw new Exception("Adding wildcard failed! " . var_export($def, true));
        $return[$phId] = $placeholder;
      }

      return $return;
    }

    /**
     * getMediaLibraryStructure
     *
     * @param mixed $pid Survey ID of the survey from what the structure is read
     *
     * @return array Contains structure of media library elements used to add files
     */
    public function getMediaLibraryStructure($pid)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->get_media_library_structure($pid);
    }

    /**
     * addFileToMediaLibrary
     *
     * @param mixed  $pid
     * @param mixed  $dirId
     * @param mixed  $cont
     * @param mixed  $name
     * @param string $wildcard
     */
    public function addFileToMediaLibrary($pid, $dirId, $cont, $name, $wildcard = '')
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->add_media_library_file($name, $cont, $dirId, $pid, $wildcard);
    }

    /**
     * getQidByCotitle
     *
     * @param mixed $pid
     * @param mixed $title
     */
    public function getQidByCotitle($pid, $title)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->get_q_id_by_cotitle($pid, $title);
    }

    /**
     * getAnswerOptions
     *
     * @param mixed $pid
     * @param mixed $qid
     */
    public function getAnswerOptions($pid, $qid)
    {
      return $this->getItems($pid, $qid);
    }

    /**
     * getItems
     *
     * @param mixed $pid
     * @param mixed $qid
     */
    public function getItems($pid, $qid)
    {
      //Todo / Fixme: how does this look in EFS > 7.0
      /** @noinspection PhpUndefinedMethodInspection */
      $items = $this->efsApi->get_answer_options($pid, $qid);
      /* um: $items might bring their answer-options - since EFS > 7.0 the EFS-API
       * itself should do that */
      $items = $this->hackGetVarnameForRcat($pid, $qid, $items);

      return $items;
    }

    /**
     * getItemAccessKeys
     *
     * @param mixed $pid
     * @param mixed $qid
     */
    public function getItemAccessKeys($pid, $qid)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      $res             = $this->efsApi->get_item_access_keys($pid, $qid);
      $res['create'][] = 'option_imageid';
      $res['create']   = array_combine($res['create'], $res['create']);

      $res['edit'][] = 'option_imageid';
      $res['edit']   = array_combine($res['edit'], $res['edit']);

      return $res;
    }

    /**
     * getItemFilterAccessKeys
     *
     */
    public function getItemFilterAccessKeys()
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->get_filter_access_keys(efs_apis_const::FILTER_SCOPE_ITEM);
    }

    /**
     * setItemFilter
     *
     * @param mixed $pid
     * @param mixed $filter
     * @param mixed $pageId
     * @param mixed $vId
     */
    public function setItemFilter($pid, $filter, $pageId, $vId)
    {
      $errors                 = array();
      $param                  = array();
      $param["project_id"]    = $pid;
      $param["filter_string"] = $filter;
      $param["page_id"]       = $pageId;
      $param["option_id"]     = $vId;

      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->set_filter(efs_apis_const::FILTER_SCOPE_ITEM, $param, $errors);
    }

    /**
     * setScaleFilter
     *
     * @param integer $pid
     * @param string  $filter
     * @param integer $pageId
     * @param integer $rcId
     */
    public function setScaleFilter($pid, $filter, $pageId, $rcId)
    {
      $errors                 = array();
      $param                  = array('project_id' => (int)$pid);
      $param['filter_string'] = $filter;
      $param['page_id']       = (int)$pageId;
      $param['option_id']     = (int)$rcId;

      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->set_filter(efs_apis_const::FILTER_SCOPE_RCAT, $param, $errors);
    }

    /**
     * getScaleFilterAccessKeys
     *
     */
    public function getScaleFilterAccessKeys()
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->get_filter_access_keys(efs_apis_const::FILTER_SCOPE_RCAT);
    }

    /**
     * deleteItems
     *
     * @param mixed $pid
     * @param mixed $qid
     * @param array $optPar
     */
    public function deleteItems($pid, $qid, $optPar = array())
    {
      /* If not IDs where given delete all items */
      if (!array_key_exists('ids', $optPar))
      {
        $items = $this->getItems($pid, $qid);
        $ids   = array_keys($items);
      }
      else
      {
        $ids = $optPar['ids'];
      }

      /* Call the EFS - API */

      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->delete_answer_option($pid, $qid, $ids);
    }

    /**
     * addItems
     *
     * @param       $pid
     * @param       $qid
     * @param       $items
     * @param array $optPar
     *
     * @return array
     * @throws Exception
     * @internal param mixed $param
     */
    public function addItems($pid, $qid, $items, $optPar = array())
    {
      /* Assign image to the item */
      $imageizeItems = array();
      foreach ($items as $code => $item)
      {
        if (array_key_exists('option_imageid', $item))
        {
          $imageizeItems[$code] = $item;
          unset($items[$code]['option_imageid']);
        }
      }
      /** @noinspection PhpUndefinedMethodInspection */
      $res = $this->efsApi->add_answer_options($pid, $qid, $items);
      /* Assign file-ID (file-manager) to the items */
      foreach ($imageizeItems as $item)
      {
        $imageInfo = pathinfo($item['option_imageid']);
        $path      = $imageInfo['dirname'];
        $name      = $imageInfo['basename'];
        if (!$this->itemImageize($pid, $qid, $item['option_sort_number'], $path, $name))
          throw new Exception("Cannot add image to item (path: $path name: $name)");
      }
      if (array_key_exists('dynamic_break', $optPar))
      {
        if ($optPar['dynamic_break'] > 0)
          $this->questionSetDynamicBreak($qid, $optPar['dynamic_break']);
      }

      return $this->hackGetAddedItems($qid);
    }

    /**
     * getScaleAccessKeys
     *
     * @package Csv2Questionnaire
     */
    public function getScaleAccessKeys()
    {
      /** @noinspection PhpUndefinedMethodInspection */
      $keys             = $this->efsApi->get_scale_access_keys();
      $keys['create'][] = 'option_imageid';
      $keys['create']   = array_combine($keys['create'], $keys['create']);

      $keys['edit'][] = 'option_imageid';
      $keys['edit']   = array_combine($keys['edit'], $keys['edit']);

      return $keys;
    }

    /**
     * getScaleOptions
     *
     * @package  Csv2Questionnaire
     *
     * @param $pid
     * @param $qid
     *
     * @internal param mixed $param
     */
    public function getScaleOptions($pid, $qid)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->get_scale_options($pid, $qid);
    }

    /**
     * addScaleOptions
     *
     * @param mixed $pid
     * @param mixed $qid
     * @param array $scales
     * @param array $optPar
     *
     * @throws Exception
     */
    public function addScaleOptions($pid, $qid, array $scales, $optPar = array())
    {
      $imageizeScales = array();
      /* Remove the customized API options */
      foreach ($scales as $code => $scale)
      {
        if (array_key_exists('option_imageid', $scale))
        {
          $imageizeScales[$code] = $scale;
          unset($scales[$code]['option_imageid']);
        }
      }
      /* Let EFS add the scales */
      $ret = $this->hackAddScaleOptions($pid, $qid, $scales);
      /* Adjust the customized options that are not supported by the native
       * EFS API */
      foreach ($imageizeScales as $code => $scale)
      {
        $imageInfo = pathinfo($scale['option_imageid']);
        $path      = $imageInfo['dirname'];
        $name      = $imageInfo['basename'];
        if (!$this->scaleImageize($pid, $qid, $code, $path, $name))
        {
          throw new Exception('Cannot add image to scale option (path: ' . $path . ' name: ' . $name . ')');
        }
      }
      /* Add colbreak settings */
      if (array_key_exists('dynamic_break', $optPar))
      {
        if ($optPar['dynamic_break'] > 0)
        {
          $this->questionSetDynamicBreak($qid, $optPar['dynamic_break']);
        }
      }

      return $ret;
    }

    /**
     * getPageIdByName
     *
     * @param mixed $pid
     * @param mixed $name
     * @param bool  $first
     *
     * @internal param array $optPar
     */
    public function getPageIdByName($pid, $name, $first = true)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->get_pgid_by_name($pid, $name, $first);
    }

    /**
     * addNewPage
     *
     * @param mixed $pid
     * @param mixed $title
     * @param mixed $parent
     * @param mixed $type
     * @param array $optPar
     */
    public function addNewPage($pid, $title, $parent, $type, $optPar = array())
    {
      $pgid = $this->hackAddNewPage($pid, $title, $parent, $type);
      $this->addAutoSubmit($pgid, $optPar['submit_event']);

      return $pgid;
    }

    // TRIGGER
    /**
     * addTrigger
     *
     * @param mixed $pid
     * @param mixed $pgid
     * @param mixed $iid
     * @param mixed $action
     * @param array $optPar
     *
     * @return int
     * @package Csv2Questionnaire
     */
    private function addTrigger($pid, $pgid, $iid, $action, $optPar = array())
    {
      $posOptPar = array('post_page' => 1, 'post_filter' => 0, 'trigger_name' => '(unknown)', 'trigger_desc' => '(no desc)', 'exec_order' => 0, 'exec_preview' => 1, 'filter_id' => 0, 'exec_mult' => 1);
      $optPar    = $this->setOptPar($posOptPar, $optPar);

      /* First simply add / register new trigger on page */

      return $this->registerTrigger($pgid, $iid, $action, $optPar);
    }

    /**
     * addTriggerRecode
     *
     * @param mixed $pid
     * @param mixed $pgid
     * @param array $recodes
     * @param array $optPar
     *
     * @return int
     * @throws Exception
     * @package Csv2Questionnaire
     */
    public function addTriggerRecode($pid, $pgid, array $recodes, $optPar = array())
    {
      /* Add recoding-trigger to get the recoding-trigger-id */
      $rId = $this->registerTriggerRecode();
      if ($rId < 1)
        throw new Exception('Got no ID for the recoding');
      /* Add the trigger connected to the recoding-trigger */
      $action = efs_apis_const::TRIGGER_RECODE;
      $trId   = $this->addTrigger($pid, $pgid, $rId, $action, $optPar);
      $suc    = $this->addTriggerRecodeRecodings($trId, $rId, $recodes);
      if (!$suc)
      {
        throw new Exception('Cannot the recoding rules');
      }

      return $trId;
    }

    /**
     * getQuestion
     *
     * @package Csv2Questionnaire
     *
     * @param mixed $pid
     * @param mixed $qid
     */
    public function getQuestion($pid, $qid)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->get_question($pid, $qid);
    }

    /**
     * addNewQuestion
     *
     * @package  Csv2Questionnaire
     *
     * @param       $pid
     * @param mixed $pgid
     * @param mixed $title
     * @param mixed $type
     * @param array $optPar
     *
     * @throws Exception
     * @internal param mixed $syid
     */
    public function addNewQuestion($pid, $pgid, $title, $type, $optPar = array())
    {
      /** @noinspection PhpUndefinedMethodInspection */
      $qid = $this->efsApi->add_question($pid, $pgid, $title, $type);
      if ($qid < 0)
        throw new Exception('Cannot add new question');
      /* Additional settings the API does not support, yet */
      if (array_key_exists('q_text', $optPar) || array_key_exists('q_instruct', $optPar))
      {
        $text     = (string)$optPar['q_text'];
        $instruct = (string)$optPar['q_instruct'];
        $this->changeQuestionText($qid, $text, $instruct);
      }

      return $qid;
    }

    /**
     * addPlausicheck
     *
     * @package Csv2Questionnaire
     *
     * @param mixed $pid
     * @param mixed $pgid
     * @param mixed $cond
     * @param mixed $errMsg
     * @param array $optPar
     */
    public function addPlausi($pid, $pgid, $cond, $errMsg, $optPar = array())
    {
      if (!array_key_exists('type', $optPar))
        $optPar['type'] = efs_apis_const::PLAUSICHECK_TYPE_COMBINED;
      if (!array_key_exists('ignore', $optPar))
        $optPar['ignore'] = false;
      if (!array_key_exists('hidden', $optPar))
        $optPar['hidden'] = true;

      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->add_plausicheck($pid, $pgid, $cond, $errMsg, $optPar['type'], $optPar['ignore'], $optPar['hidden']);
    }

    /**
     * Increments the interview counter for the given survey ID
     *
     * @package surveyApp
     *
     * @param $syid
     *
     * @return \Zend_Db_Statement_Interface
     */
    public function addInterviewCount($syid)
    {
      return $this->hackAddInterviewCount((int)$syid);
    }

    /**
     * CHRE-84 asks for an ability to determine possible survey variables
     *
     * @package surveyApp
     *
     * @param $syid
     * @param $vVars
     *
     * @return integer $lfdn - the lfdn that was assigned via auto increment
     */
    public function addSurveyEntry($syid, $vVars)
    {
      return $this->hackAddSurveyEntry($syid, $vVars);
    }

    /**
     * Return the survey code (invitation code) for the given UserId
     * in the given survey with ID syid.
     *
     * @package turkcell
     *
     * @param $uid
     * @param $syid
     *
     * @return string $code
     */
    public function getCodeForUidInSyid($uid, $syid)
    {
      return $this->hackGetCodeForPidUid($syid, $uid);
    }

    /**
     * Returns reports for a project id
     *
     * Return value is an id => name mapping
     *
     * @param int $pid project ID
     *
     * @return array reports
     */
    public function getReportIds($pid)
    {
      return $this->hackGetReportIds($pid);
    }

    /**
     * Updates a project
     *
     * Valid data fields:
     * - ptyp
     * - ptitle
     * - pdescr
     * - pauth
     * - pstaff
     * - pcomment
     * - pstatus
     * - pstart
     * - pfinish
     *
     * @param int   $pid  project id
     * @param array $data project data (mapping field => value)
     *
     * @return int number of affected rows
     * @throws RuntimeException if project id is not valid
     * @throws InvalidArgumentException if any field in project data is invalid or project status is invalid
     */
    public function updateProject($pid, array $data)
    {
      $this->validateProjectId($pid);

      return $this->hackUpdateProject($pid, $data);
    }

    /**
     * Returns a <code>Zend_Http_Client</code> that is able to call EFS backend URIs
     *
     * @param string $path      subpath under http://<domain>/www/
     * @param string $sessionId session id
     *
     * @return Zend_Http_Client http client
     */
    public function getHttpClientForEfs($path, $sessionId)
    {
      return $this->hackGetHttpClientForEfs($path, $sessionId);
    }

    /**
     * We cannot properly add an interview with prefilled data. So the surveyApp
     * imports the participant and then updates the survey-table with the uid
     * from users-table. Then after insert into the survey-table the lfdn in the
     * sample_data must be updated.
     *
     * @param $values
     * @param $where
     *
     * @return int
     * @package surveyApp
     */
    public function updateSampleData($values, $where)
    {
      // Do not pass too much in the hack-stuff
      return $this->hackUpdateSampleData(array('lfdn' => $values['lfdn']), array('uid' => $where['uid'], 'sid' => $where['sid']));
    }

    /**
     * Returns the type of a project
     *
     * @param int $pid project id
     *
     * @return int project type
     */
    public function getProjectType($pid)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->get_project_type($pid);
    }

    /**
     * Returns available project statuses
     *
     * @return array statuses
     */
    public function getProjectStatuses()
    {
      return $this->hackGetProjectStatuses();
    }

    /**
     * Determines the sample IDs for the given survey-API
     *
     * @param int $pid
     *
     * @return array sample IDs
     */
    public function getSidsFromPid($pid)
    {
      return $this->hackGetSidsFromPid($pid);
    }

    /**
     * Returns the efs survey api
     *
     * @return survey_api efs survey api
     */
    protected function getEfsApi()
    {
      return $this->efsApi;
    }

    /**
     * Returns true if the fiven project id is valid
     *
     * @param string $pid project id
     *
     * @return bool true if the fiven project id is valid
     */
    public function isValidProjectId($pid)
    {
      if (in_array($pid, $this->validProjectIds))
        return true;

      $info = $this->getProjectInfo($pid);

      if (empty($info['project_id']))
        return false;

      $this->validProjectIds[] = $pid;

      return true;
    }

    /**
     * Validates a project id
     *
     * @param int $pid
     *
     * @throws RuntimeException if project id is not valid
     */
    public function validateProjectId($pid)
    {
      if (!$this->isValidProjectId($pid))
        throw new RuntimeException(sprintf('Invalid project id "%d".', $pid));
    }

    /**
     * Returns page data
     *
     * @param int $pid    project id
     * @param int $pageId page id
     *
     * @return array page data
     * @throws RuntimeException if project id is not valid or page is not found
     */
    public function getPage($pid, $pageId)
    {
      $this->validateProjectId($pid);

      /** @noinspection PhpUndefinedMethodInspection */
      $page = $this->getEfsApi()->get_page($pid, $pageId);

      if (!$page)
        throw new RuntimeException(sprintf('Invalid page id "%d" (project id "%s").', $pageId, $pid));

      return $page;
    }

    /**
     * Returns page ids by project id and type
     *
     * @param int $pid  project id
     * @param int $type page type
     *
     * @return array pages
     * @throws InvalidArgumentException if page type is invalid
     * @throws RuntimeException if project id is not valid
     */
    public function getPageIdsByType($pid, $type)
    {
      $this->validateProjectId($pid);

      return $this->hackGetPageIdsByType($pid, $type);
    }

    /**
     * Updates a survey page
     *
     * @param int   $pid    project id project id
     * @param int   $pageId page id
     * @param array $data   page data (mapping field => value)
     *
     * @return int
     * @throws InvalidArgumentException if any field in project data is invalid or page type does not allow for external url or external url is invalid
     * @throws RuntimeException if project id is not valid or trying to set an external url for a page type other that 41 or 51
     */
    public function updatePage($pid, $pageId, array $data)
    {
      $this->validateProjectId($pid);

      if (isset($data['external_url']))
      {
        $page = $this->getPage($pid, $pageId);

        // external url only allowed on external survey start pages (type 51) and end pages (type 41)
        if (!in_array($page['page_type'], array(41, 51)))
        {
          throw new RuntimeException(sprintf('External url is allowed only for page types 41 and 51 (actual page type: "%s").', $page['page_type']));
        }
      }

      return $this->hackUpdatePage($pid, $pageId, $data);
    }

    /**
     * Returns a projects absolute URL
     *
     * @param int $pid project id
     *
     * @return string projects absolute URL
     * @throws RuntimeException if project id is not valid
     */
    public function getAbsoluteProjectUrl($pid)
    {
      $this->validateProjectId($pid);

      return $this->hackGetAbsoluteProjectUrl($pid);
    }

    /**
     * removes a panelist from a survey
     *
     * @param string $identifier_type
     * @param string $identifier_value
     * @param int    $survey_id
     *
     * @return bool
     */
    public function deleteSurveyParticipation($identifier_type, $identifier_value, $survey_id)
    {
      // check parameters
      if ($identifier_type == '' || $identifier_value == '' || $survey_id == '')
        return false;

      // remove panelist from survey
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->efsApi->delete_survey_participation($identifier_type, $identifier_value, $survey_id);
    }

    /**
     * Determines the sample-date for the sample with the given id.
     *
     * @param int $sid bigger 0
     *
     * @return array|bool sample information
     */
    public function getSample($sid)
    {
      if ($sid < 1)
        return false;
      $result = $this->hackGetSample((int)$sid);
      if (count($result) < 2)
        return false;

      return array('project_id' => $result['pid'], 'project_title' => $result['ptitle']);
    }

    /**
     * Wrap the getSample and make it unique as there exists another function getSample
     * in the servicelayer since 8.0
     *
     * @param integer $sid
     * @return Ambigous <multitype:, boolean, multitype:unknown mixed >
     */
    public function getSample70($sid)
    {
      return $this->getSample($sid);
    }
  }