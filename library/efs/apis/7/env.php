<?php
  /**
   * efs_apis_7_env
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.15 2021/02/10 11:49:22 fiedler Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: fiedler $
   */

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_7_env
  {
    private static $instance;

    /**
     * __construct
     */
    private function __construct()
    {
      // EFS expects static gp_conf to be loaded
      self::loadStaticGpConf();

      // 7.0 needs some global functions for different API calls
      // Todo: Check if Pct_Efs_Autoloader is able to load those
      Zend_Loader::loadFile(APPLICATION_EFS_PATH . "/www/include/miscfunc.inc.php3", null, true);
      Zend_Loader::loadFile(APPLICATION_EFS_PATH . 'wcp/framework/class_efs_assertion.inc.php', null, true);
      Zend_Loader::loadFile(APPLICATION_EFS_PATH . 'wcp/framework/env/class_efs_env.inc.php', null, true);
      Zend_Loader::loadFile(APPLICATION_EFS_PATH . 'wcp/framework/class_efs_config.inc.php', null, true);
      Zend_Loader::loadFile(APPLICATION_EFS_PATH . 'wcp/framework/env/class_efs_env_nonhttp.inc.php', null, true);
      Zend_Loader::loadFile(APPLICATION_EFS_PATH . 'wcp/framework/env/class_efs_env_api.inc.php', null, true);

      // Some API methods in EFS want to print html via htmlfunc3
      if (file_exists(APPLICATION_EFS_PATH . "/wcp/include/htmlfunc3.inc.php3") === true) { // does not exist anymore since (20)16.2
        Zend_Loader::loadFile(APPLICATION_EFS_PATH . "/wcp/include/htmlfunc3.inc.php3", null, true);
      }

      // EFS is able to load the rest itself
      /** @noinspection PhpUndefinedClassInspection */
      self::$instance = new efs_env_api();

      // EFS 7.0 needs dbwrap in global scope
      $GLOBALS['db'] = efs_apis_7_env::getDb();

      // EFS pushes its own autoload onto the spl-stack which we do not want
      // to use to avoid redelcaration of classes es PCT uses another Zend -
      // Framework
      spl_autoload_unregister('efs_autoload::init');
    }

    /**
     *
     * @return efs_apis_7_1_env $instance
     */
    public static function getInstance()
    {
      if (self::$instance == NULL)
        self::$instance = new self();

      return self::$instance;
    }

    /**
     * In EFS 7.0 the module-manger reads out htdocs/module
     * and registers via filefunx found files in EFS'
     * autoloader dictionary.
     *
     * @return efs_env get_module_manager
     */
    public function get_module_manager()
    {
      $env = self::getInstance();

      /** @noinspection PhpUndefinedClassInspection */
      return efs_env::get_module_manager();
    }

    /**
     * Loads the static EFS gp_conf class
     */
    protected function loadStaticGpConf()
    {
      // In cli - mode EFS determines the config.inc.php3 via the PWD
      $oldPwd         = $_SERVER['PWD'];
      $_SERVER['PWD'] = APPLICATION_EFS_PATH . 'www/';

      // In cgi - mode EFS determines the config.inc.php3 via the DOCUMENT_ROOT
      $oldDocmentRoot           = $_SERVER['DOCUMENT_ROOT'];
      $_SERVER['DOCUMENT_ROOT'] = APPLICATION_EFS_PATH . 'www/';

      // Avoid notice about not set index
      $oldErrorReporting = error_reporting(E_ALL&~E_NOTICE);
      Zend_Loader::loadFile(APPLICATION_EFS_PATH . "/www/include/class_gp_conf.inc.php", null, true);
      /** @noinspection PhpUndefinedClassInspection */
      gp_conf::load_config();

      // Restore the old values
      error_reporting($oldErrorReporting);
      $_SERVER['PWD']           = $oldPwd;
      $_SERVER['DOCUMENT_ROOT'] = $oldDocmentRoot;

      spl_autoload_unregister('efs_autoload::init');
    }

    /**
     * Returns the dbwrap instance from the EFS env
     *
     * @throws Exception
     * @return dbwrap
     */
    public function getDb(Zend_Config $conf)
//    protected function getDb()
    {
      // Avoid EFS prints
      ob_start();

      /** @noinspection PhpUndefinedClassInspection */
      $db = efs_env::get_db();

      // Dump EFS prints
      $error = ob_get_contents();
      ob_end_clean();

      if ($db->opened == false)
        throw new Exception('Cannot connect to EFS database: ' . $error);

      return $db;
    }
  }
