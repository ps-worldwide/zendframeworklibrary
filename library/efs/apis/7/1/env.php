<?php
  /**
   * efs_apis_7_1_env
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.11 2016/07/27 08:07:04 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_7_1_env extends efs_apis_7_env
  {
    private static $instance;

    /**
     * __construct
     */
    private function __construct()
    {
      // EFS expect static gp_conf to be loaded
      parent::loadStaticGpConf();

      // Some API methods in EFS want to print html via htmlfunc3
      if (file_exists(APPLICATION_EFS_PATH . "/wcp/include/htmlfunc3.inc.php3") === true) { // does not exist anymore since (20)16.2
        Zend_Loader::loadFile(APPLICATION_EFS_PATH . "/wcp/include/htmlfunc3.inc.php3", null, true);
      }

      // EFS 7.1 needs dbwrap in Zend-Registry
      Zend_Registry::set('db', self::getDb(Zend_Registry::get('conf')));

      // EFS pushes its own autoload onto the spl-stack which we do not want
      // to use to avoid redelcaration of classes es PCT uses another Zend -
      // Framework
      spl_autoload_unregister('efs_autoload::init');
    }

    /**
     * getInstance
     *
     * @return efs_apis_7_1_env $instance
     */
    public static function getInstance()
    {
      if (self::$instance == NULL)
        self::$instance = new self();

      return self::$instance;
    }

    /**
     * Since 7.1 classes from htdocs/modules are found via autoloader
     * (see Pct_Efs_Autoload).
     *
     * @return NULL
     */
    public function get_module_manager()
    {
      return null;
    }

    /**
     * Returns the dbwrap instance from the EFS env
     *
     * @param Zend_Config $conf
     *
     * @throws Exception
     * @return dbwrap
     */
    public function getDb(Zend_Config $conf)
    {
      // Avoid EFS prints
      ob_start();

      /** @noinspection PhpUndefinedClassInspection */
      $db = dbwrap::connect_static($conf->dbhost, $conf->dbuser, $conf->dbpasswd, $conf->dbname, $conf->dbport, $conf->dbsocket);

      // Dump EFS prints
      $error = ob_get_contents();
      ob_end_clean();

      if ($db->opened == false)
        throw new Exception('Cannot connect to EFS database: ' . $error);

      return $db;
    }
  }
