<?php
  /**
   * efs_apis_locale
   * Extends efs local0r in 7.0.
   *
   * @package   zendframeworkLibrary
   * @version   $Id: locale.php,v 1.6 2013/10/16 09:27:07 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_locale extends local0r
  {
  }