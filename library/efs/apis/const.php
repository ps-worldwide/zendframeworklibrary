<?php
  /**
   * efs_apis_const
   *
   * @package       zendframeworkLibrary
   * @version       $Id: const.php,v 1.45 2013/11/18 09:49:44 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author        $Author: funke $
   */

  class efs_apis_const
  {
    /*** PROJECT TYPES ***/
    const PROJECTTYPE_PANEL          = efs_apis_surveyApiBase::SURVEY_API_PROJECTTYPE_PANEL;
    const PROJECTTYPE_GEN_MASTERDATA = efs_apis_surveyApiBase::SURVEY_API_PROJECTTYPE_GEN_MASTERDATA;
    const PROJECTTYPE_PERSONALIZED   = efs_apis_surveyApiBase::SURVEY_API_PROJECTTYPE_PERSONALIZED;
    const PROJECTTYPE_ES             = efs_apis_surveyApiBase::SURVEY_API_PROJECTTYPE_ES;
    const PROJECTTYPE_LOFT           = efs_apis_surveyApiBase::SURVEY_API_PROJECTTYPE_LOFT;
    const PROJECTTYPE_ANONYMOUS      = efs_apis_surveyApiBase::SURVEY_API_PROJECTTYPE_ANONYMOUS;

    /*** PROJECT TYPE as text as returned by survey.surveys.get by Service Layer ***/
    const PROJECTTYPE_PERSONALIZED_TXT = 'PERSONALIZED';
    const PROJECTTYPE_PANEL_TXT        = 'PANEL';

    /*** PROJECT STATUS ***/
    const PROJECTSTATUS_CREATED         = efs_apis_projectStatus::PSTATUS_CREATED;
    const PROJECTSTATUS_INSTR_SPEC      = efs_apis_projectStatus::PSTATUS_INSTR_SPEC;
    const PROJECTSTATUS_PARTIP_ASSIGNED = efs_apis_projectStatus::PSTATUS_PARTIP_ASSIGNED;
    const PROJECTSTATUS_PARTIP_NOTIFIED = efs_apis_projectStatus::PSTATUS_PARTIP_NOTIFIED;
    const PROJECTSTATUS_VARS_ASSIGNED   = efs_apis_projectStatus::PSTATUS_VARS_ASSIGNED;
    const PROJECTSTATUS_SURVEY_GEN      = efs_apis_projectStatus::PSTATUS_SURVEY_GEN;
    const PROJECTSTATUS_ACTIVE          = efs_apis_projectStatus::PSTATUS_ACTIVE;
    const PROJECTSTATUS_CLOSING         = efs_apis_projectStatus::PSTATUS_CLOSING;
    const PROJECTSTATUS_INACTIVE        = efs_apis_projectStatus::PSTATUS_INACTIVE;
    const PROJECTSTATUS_FINISHED        = efs_apis_projectStatus::PSTATUS_FINISHED;
    const PROJECTSTATUS_TO_ARCHIVE      = efs_apis_projectStatus::PSTATUS_TO_ARCHIVE;
    const PROJECTSTATUS_ARCHIVATED      = efs_apis_projectStatus::PSTATUS_ARCHIVATED;

    /*** PROJECT STATUS TEXT ***/
    const PROJECTSTATUS_TXT_CREATED         = efs_apis_projectStatus::PSTATUS_CREATED_TXT;
    const PROJECTSTATUS_TXT_PARTIP_ASSIGNED = efs_apis_projectStatus::PSTATUS_PARTIP_ASSIGNED_TXT;
    const PROJECTSTATUS_TXT_PARTIP_NOTIFIED = efs_apis_projectStatus::PSTATUS_PARTIP_NOTIFIED_TXT;
    const PROJECTSTATUS_TXT_VARS_ASSIGNED   = efs_apis_projectStatus::PSTATUS_VARS_ASSIGNED_TXT;
    const PROJECTSTATUS_TXT_SURVEY_GEN      = efs_apis_projectStatus::PSTATUS_SURVEY_GEN_TXT;
    const PROJECTSTATUS_TXT_ACTIVE          = efs_apis_projectStatus::PSTATUS_ACTIVE_TXT;
    const PROJECTSTATUS_TXT_CLOSING         = efs_apis_projectStatus::PSTATUS_CLOSING_TXT;
    const PROJECTSTATUS_TXT_INACTIVE        = efs_apis_projectStatus::PSTATUS_INACTIVE_TXT;
    const PROJECTSTATUS_TXT_FINISHED        = efs_apis_projectStatus::PSTATUS_FINISHED_TXT;
    const PROJECTSTATUS_TXT_TO_ARCHIVE      = efs_apis_projectStatus::PSTATUS_TO_ARCHIVE_TXT;
    const PROJECTSTATUS_TXT_ARCHIVATED      = efs_apis_projectStatus::PSTATUS_ARCHIVATED_TXT;

    /*** OPERATORS (FOR PANEL GROUPS) ***/
    const CONDITION_OPERATOR_CONTAINS      = efs_apis_panelQueryCondition::OPERATOR_CONTAINS;
    const CONDITION_OPERATOR_BEGINS_WITH   = efs_apis_panelQueryCondition::OPERATOR_BEGINS_WITH;
    const CONDITION_OPERATOR_ENDS_WITH     = efs_apis_panelQueryCondition::OPERATOR_ENDS_WITH;
    const CONDITION_OPERATOR_IN            = efs_apis_panelQueryCondition::OPERATOR_IN;
    const CONDITION_OPERATOR_EQUAL         = efs_apis_panelQueryCondition::OPERATOR_EQUAL;
    const CONDITION_OPERATOR_UNEQUAL       = efs_apis_panelQueryCondition::OPERATOR_UNEQUAL;
    const CONDITION_OPERATOR_LESS_THAN     = efs_apis_panelQueryCondition::OPERATOR_LESS_THAN;
    const CONDITION_OPERATOR_GREATER_THAN  = efs_apis_panelQueryCondition::OPERATOR_GREATER_THAN;
    const CONDITION_OPERATOR_LESS_EQUAL    = efs_apis_panelQueryCondition::OPERATOR_LESS_EQUAL;
    const CONDITION_OPERATOR_GREATER_EQUAL = efs_apis_panelQueryCondition::OPERATOR_GREATER_EQUAL;
    const CONDITION_OPERATOR_IS_NULL       = efs_apis_panelQueryCondition::OPERATOR_IS_NULL;
    const CONDITION_OPERATOR_IS_NOT_NULL   = efs_apis_panelQueryCondition::OPERATOR_IS_NOT_NULL;
    const CONDITION_OPERATOR_NOT_IN        = efs_apis_panelQueryCondition::OPERATOR_NOT_IN;

    /*** PANEL GROUP ACTIONS ***/
    const GROUP_ACTION_FILTER_ADD   = efs_apis_panelGroupsUpdaterLogic::FILTER_ADD;
    const GROUP_ACTION_FILTER_MERGE = efs_apis_panelGroupsUpdaterLogic::FILTER_MERGE;

    /*** VARIABLES ***/
    const FORMAT_MULTI_DIM                        = efs_apis_surveyApiBase::SURVEY_API_FORMAT_MULTI_DIM;
    const FORMAT_ASSOC_LABEL                      = efs_apis_surveyApiBase::SURVEY_API_FORMAT_ASSOC_LABEL;
    const FORMAT_ASSOC_VARNAME_LABEL              = efs_apis_surveyApiBase::SURVEY_API_FORMAT_ASSOC_VARNAME_LABEL;
    const FORMAT_ASSOC_ABBREVIATION_LABEL         = efs_apis_surveyApiBase::SURVEY_API_FORMAT_ASSOC_ABBREVIATION_LABEL;
    const FORMAT_ASSOC_ABBREVIATION_VARNAME_LABEL = efs_apis_surveyApiBase::SURVEY_API_FORMAT_ASSOC_ABBREVIATION_VARNAME_LABEL;
    const FORMAT_ASSOC_TABLENAME_VARNAME_LABEL    = efs_apis_surveyApiBase::SURVEY_API_FORMAT_ASSOC_TABLENAME_VARNAME_LABEL;
    const FORMAT_RCATS                            = efs_apis_surveyApiBase::SURVEY_API_FORMAT_RCATS;
    const FORMAT_VARNAME_ARRAY                    = efs_apis_surveyApiBase::SURVEY_API_FORMAT_VARNAME_ARRAY;
    const FORMAT_VARNAME_VARNAME_SHORT_LABEL      = efs_apis_surveyApiBase::SURVEY_API_FORMAT_VARNAME_VARNAME_SHORT_LABEL;

    const VARIABLE_TYPE_URL_PARAMS                = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_URL_PARAMS;
    const VARIABLE_TYPE_SURVEY_CUSTOM             = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_SURVEY_CUSTOM;
    const VARIABLE_TYPE_NON_LOOP_PAGE_VARS        = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_NON_LOOP_PAGE_VARS;
    const VARIABLE_TYPE_LOOP_PAGE_VARS            = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_LOOP_PAGE_VARS;
    const VARIABLE_TYPE_LOOP                      = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_LOOP;
    const VARIABLE_TYPE_LOOP_STATUS               = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_LOOP_STATUS;
    const VARIABLE_TYPE_LIST_PROPERTIES           = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_LIST_PROPERTIES;
    const VARIABLE_TYPE_ORG_ALLOCATION            = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_ORG_ALLOCATION;
    const VARIABLE_TYPE_ORG_CODE                  = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_ORG_CODE;
    const VARIABLE_TYPE_ORG_FUNCTION              = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_ORG_FUNCTION;
    const VARIABLE_TYPE_USERS_NON_PANEL           = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_USERS_NON_PANEL;
    const VARIABLE_TYPE_MD_DATA                   = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_MD_DATA;
    const VARIABLE_TYPE_PANEL_ATT                 = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_PANEL_ATT;
    const VARIABLE_TYPE_PANEL_ATT_VAR             = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_PANEL_ATT_VAR;
    const VARIABLE_TYPE_PANEL_TRACKING            = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_PANEL_TRACKING;
    const VARIABLE_TYPE_SURVEY_STANDARD           = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_SURVEY_STANDARD;
    const VARIABLE_TYPE_SAMPLE_DATA               = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_SAMPLE_DATA;
    const VARIABLE_TYPE_PANEL_USERS_VAR           = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_PANEL_USERS_VAR;
    const VARIABLE_TYPE_SURVEY_INTERNAL           = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_SURVEY_INTERNAL;
    const VARIABLE_TYPE_SURVEY_INTERNAL_DYNAMIC   = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_SURVEY_INTERNAL_DYNAMIC;
    const VARIABLE_TYPE_SURVEY_INTERNAL_RECODABLE = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_SURVEY_INTERNAL_RECODABLE;
    const VARIABLE_TYPE_SAMPLE_DATA_DATES         = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_TYPE_SAMPLE_DATA_DATES;

    const VARIABLE_COMBI_FILTER_SURVEY     = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_COMBI_FILTER_SURVEY;
    const VARIABLE_COMBI_SURVEY_STRUCTURE  = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_COMBI_SURVEY_STRUCTURE;
    const VARIABLE_COMBI_PANEL             = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_COMBI_PANEL;
    const VARIABLE_COMBI_SURVEY_GENERATOR  = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_COMBI_SURVEY_GENERATOR;
    const VARIABLE_COMBI_PROJECT_USERS_VAR = efs_apis_surveyApiBase::SURVEY_API_VARIABLE_COMBI_PROJECT_USERS_VAR;

    const QUERY_OPTION_TEXT          = efs_apis_surveyApiBase::SURVEY_API_QUERY_OPTION_TEXT;
    const QUERY_OPTION_NUMERIC       = efs_apis_surveyApiBase::SURVEY_API_QUERY_OPTION_NUMERIC;
    const QUERY_OPTION_DATE          = efs_apis_surveyApiBase::SURVEY_API_QUERY_OPTION_DATE;
    const QUERY_OPTION_WITH_RCATS    = efs_apis_surveyApiBase::SURVEY_API_QUERY_OPTION_WITH_RCATS;
    const QUERY_OPTION_WRITABLE_ONLY = efs_apis_surveyApiBase::SURVEY_API_QUERY_OPTION_WRITABLE_ONLY;
    const QUERY_OPTION_WITH_INTERNAL = efs_apis_surveyApiBase::SURVEY_API_QUERY_OPTION_WITH_INTERNAL;

    const QUERY_OPTION_EVERYTHING        = efs_apis_surveyApiBase::SURVEY_API_QUERY_OPTION_EVERYTHING;
    const QUERY_OPTION_EXCLUDE_PROTECTED = efs_apis_surveyApiBase::SURVEY_API_QUERY_OPTION_EXCLUDE_PROTECTED;
    const QUERY_OPTION_SYSTEM            = efs_apis_surveyApiBase::SURVEY_API_QUERY_OPTION_SYSTEM;

    /*** PAGE TYPES ***/
    const PAGETYPE_STANDARD   = efs_apis_surveyApiBase::SURVEY_API_PAGETYPE_STANDARD;
    const PAGETYPE_FILTER     = efs_apis_surveyApiBase::SURVEY_API_PAGETYPE_FILTER;
    const PAGETYPE_SYSTEM     = efs_apis_surveyApiBase::SURVEY_API_PAGETYPE_SYSTEM;
    const PAGETYPE_RAND_SEL   = efs_apis_surveyApiBase::SURVEY_API_PAGETYPE_RAND_SEL;
    const PAGETYPE_RAND_ROT   = efs_apis_surveyApiBase::SURVEY_API_PAGETYPE_RAND_ROT;
    const PAGETYPE_LOOP       = efs_apis_surveyApiBase::SURVEY_API_PAGETYPE_LOOP;
    const PAGETYPE_CONT_DEPOT = efs_apis_surveyApiBase::SURVEY_API_PAGETYPE_CONT_DEPOT;
    const PAGETYPE_MIX_EXTERN = efs_apis_surveyApiBase::SURVEY_API_PAGETYPE_MIX_EXTERN;
    const PAGETYPE_CONJOINT   = efs_apis_surveyApiBase::SURVEY_API_PAGETYPE_CONJOINT;
    const PAGETYPE_GRID       = efs_apis_surveyApiBase::SURVEY_API_PAGETYPE_GRID;
    const PAGETYPE_ENDPAGE    = efs_apis_surveyApiBase::SURVEY_API_PAGETYPE_ENDPAGE;
    const PAGETYPE_EXT_START  = efs_apis_surveyApiBase::SURVEY_API_PAGETYPE_EXT_START;

    // above page-types are integer, but Survey Layer delivers text
    const PAGETYPE_STANDARD_TXT   = 'STANDARD';
    const PAGETYPE_SYSTEM_TXT     = 'SYSTEM';
    const PAGETYPE_CONT_DEPOT_TXT = 'CONTAINER_DEPOT';

    /* PLAUSICHECK TYPES */
    const PLAUSICHECK_TYPE_JS       = efs_apis_surveyApiBase::SURVEY_API_PLAUSICHECK_TYPE_JS;
    const PLAUSICHECK_TYPE_SERVER   = efs_apis_surveyApiBase::SURVEY_API_PLAUSICHECK_TYPE_SERVER;
    const PLAUSICHECK_TYPE_COMBINED = efs_apis_surveyApiBase::SURVEY_API_PLAUSICHECK_TYPE_COMBINED;

    /*** QUESTION TYPES ***/
    const Q111 = efs_apis_surveyApiBase::SURVEY_API_Q111;
    const Q112 = efs_apis_surveyApiBase::SURVEY_API_Q112;
    const Q113 = efs_apis_surveyApiBase::SURVEY_API_Q113;
    const Q121 = efs_apis_surveyApiBase::SURVEY_API_Q121;
    const Q122 = efs_apis_surveyApiBase::SURVEY_API_Q122;
    const Q131 = efs_apis_surveyApiBase::SURVEY_API_Q131;
    const Q132 = efs_apis_surveyApiBase::SURVEY_API_Q132;
    const Q141 = efs_apis_surveyApiBase::SURVEY_API_Q141;
    const Q142 = efs_apis_surveyApiBase::SURVEY_API_Q142;
    const Q641 = efs_apis_surveyApiBase::SURVEY_API_Q641;
    const Q143 = efs_apis_surveyApiBase::SURVEY_API_Q143;
    const Q144 = efs_apis_surveyApiBase::SURVEY_API_Q144;
    const Q153 = efs_apis_surveyApiBase::SURVEY_API_Q153;
    const Q311 = efs_apis_surveyApiBase::SURVEY_API_Q311;
    const Q312 = efs_apis_surveyApiBase::SURVEY_API_Q312;
    const Q313 = efs_apis_surveyApiBase::SURVEY_API_Q313;
    const Q321 = efs_apis_surveyApiBase::SURVEY_API_Q321;
    const Q322 = efs_apis_surveyApiBase::SURVEY_API_Q322;
    const Q340 = efs_apis_surveyApiBase::SURVEY_API_Q340;
    const Q341 = efs_apis_surveyApiBase::SURVEY_API_Q341;
    const Q342 = efs_apis_surveyApiBase::SURVEY_API_Q342;
    const Q351 = efs_apis_surveyApiBase::SURVEY_API_Q351;
    const Q361 = efs_apis_surveyApiBase::SURVEY_API_Q361;
    const Q362 = efs_apis_surveyApiBase::SURVEY_API_Q362;
    const Q363 = efs_apis_surveyApiBase::SURVEY_API_Q363;
    const Q364 = efs_apis_surveyApiBase::SURVEY_API_Q364;
    const Q411 = efs_apis_surveyApiBase::SURVEY_API_Q411;
    const Q511 = efs_apis_surveyApiBase::SURVEY_API_Q511;
    const Q611 = efs_apis_surveyApiBase::SURVEY_API_Q611;
    const Q512 = efs_apis_surveyApiBase::SURVEY_API_Q512;
    const Q513 = efs_apis_surveyApiBase::SURVEY_API_Q513;
    const Q514 = efs_apis_surveyApiBase::SURVEY_API_Q514;
    const Q515 = efs_apis_surveyApiBase::SURVEY_API_Q515;
    const Q517 = efs_apis_surveyApiBase::SURVEY_API_Q517;
    const Q521 = efs_apis_surveyApiBase::SURVEY_API_Q521;
    const Q621 = efs_apis_surveyApiBase::SURVEY_API_Q621;
    const Q522 = efs_apis_surveyApiBase::SURVEY_API_Q522;
    const Q531 = efs_apis_surveyApiBase::SURVEY_API_Q531;
    const Q661 = efs_apis_surveyApiBase::SURVEY_API_Q661;
    const Q711 = efs_apis_surveyApiBase::SURVEY_API_Q711;
    const Q911 = efs_apis_surveyApiBase::SURVEY_API_Q911;
    const Q921 = efs_apis_surveyApiBase::SURVEY_API_Q921;
    const Q930 = efs_apis_surveyApiBase::SURVEY_API_Q930;
    const Q931 = efs_apis_surveyApiBase::SURVEY_API_Q931;
    const Q932 = efs_apis_surveyApiBase::SURVEY_API_Q932;
    const Q997 = efs_apis_surveyApiBase::SURVEY_API_Q997;
    const Q998 = efs_apis_surveyApiBase::SURVEY_API_Q998;
    const Q999 = efs_apis_surveyApiBase::SURVEY_API_Q999;

    /*** LISTS ***/
    const CONDITION_TYPE_IN_LIST_IF_SELECTED       = efs_apis_surveyApiBase::SURVEY_API_CONDITION_TYPE_IN_LIST_IF_SELECTED;
    const CONDITION_TYPE_ALWAYS_IN_LIST            = efs_apis_surveyApiBase::SURVEY_API_CONDITION_TYPE_ALWAYS_IN_LIST;
    const CONDITION_TYPE_NEVER_IN_LIST             = efs_apis_surveyApiBase::SURVEY_API_CONDITION_TYPE_NEVER_IN_LIST;
    const CONDITION_TYPE_IN_LIST_IF                = efs_apis_surveyApiBase::SURVEY_API_CONDITION_TYPE_IN_LIST_IF;
    const CONDITION_TYPE_ALWAYS_IN_RANDOM          = efs_apis_surveyApiBase::SURVEY_API_CONDITION_TYPE_ALWAYS_IN_RANDOM;
    const CONDITION_TYPE_IN_RANDOM_IF_SELECTED     = efs_apis_surveyApiBase::SURVEY_API_CONDITION_TYPE_IN_RANDOM_IF_SELECTED;
    const CONDITION_TYPE_IN_RANDOM_IF              = efs_apis_surveyApiBase::SURVEY_API_CONDITION_TYPE_IN_RANDOM_IF;
    const CONDITION_TYPE_IN_LIST_IF_NOT_SELECTED   = efs_apis_surveyApiBase::SURVEY_API_CONDITION_TYPE_IN_LIST_IF_NOT_SELECTED;
    const CONDITION_TYPE_IN_RANDOM_IF_NOT_SELECTED = efs_apis_surveyApiBase::SURVEY_API_CONDITION_TYPE_IN_RANDOM_IF_NOT_SELECTED;
    const LIST_TYPE_STATIC                         = efs_apis_surveyApiBase::SURVEY_API_LIST_TYPE_STATIC;
    const LIST_TYPE_DYNAMIC                        = efs_apis_surveyApiBase::SURVEY_API_LIST_TYPE_DYNAMIC;

    /*** DISPCODES ***/
    const DISPCODE_ZERO = efs_apis_surveyApiBase::SURVEY_API_DISPCODE_ZERO; // 11: Noch nicht eingeladen
    const DISPCODE_ACTIVE = efs_apis_surveyApiBase::SURVEY_API_DISPCODE_ACTIVE; // 12: Aktiv
    const DISPCODE_INACTIVE = efs_apis_surveyApiBase::SURVEY_API_DISPCODE_INACTIVE; // 13: Inaktiv
    const DISPCODE_NO_CONTACT = efs_apis_surveyApiBase::SURVEY_API_DISPCODE_NO_CONTACT; // 14: E-Mail unzustellbar
    const DISPCODE_NOT_AVAILABLE = efs_apis_surveyApiBase::SURVEY_API_DISPCODE_NOT_AVAILABLE; // 15: Nicht erreichbar
    const DISPCODE_SAW_FIRST_PAGE = efs_apis_surveyApiBase::SURVEY_API_DISPCODE_SAW_FIRST_PAGE; // 20: Noch nicht begonnen (nur bei AN)
    const DISPCODE_RESPONDING = efs_apis_surveyApiBase::SURVEY_API_DISPCODE_RESPONDING; // 21: Antwortet gerade
    const DISPCODE_SUSPENDED = efs_apis_surveyApiBase::SURVEY_API_DISPCODE_SUSPENDED; // 22: Unterbrochen
    const DISPCODE_RETURNED = efs_apis_surveyApiBase::SURVEY_API_DISPCODE_RETURNED; // 23: Wiederaufgenommen
    const DISPCODE_COMPLETED = efs_apis_surveyApiBase::SURVEY_API_DISPCODE_COMPLETED; // 31: Beendet
    const DISPCODE_COMPLETED_CONTINUED = efs_apis_surveyApiBase::SURVEY_API_DISPCODE_COMPLETED_CONTINUED; // 32: Beendet nach Unterbrechung
    const DISPCODE_QUOTED_OUT_ON_START = efs_apis_surveyApiBase::SURVEY_API_DISPCODE_QUOTED_OUT_ON_START; // 35: Abgewiesen bei Login (Quote geschlossen)
    const DISPCODE_QUOTED_OUT_ON_SEND = efs_apis_surveyApiBase::SURVEY_API_DISPCODE_QUOTED_OUT_ON_SEND; // 36: Abgewiesen (Quote geschlossen)
    const DISPCODE_SCREENED_OUT = efs_apis_surveyApiBase::SURVEY_API_DISPCODE_SCREENED_OUT; // 37: Ausgescreen
    const DISPCODE_QUOTE_CLOSED = efs_apis_surveyApiBase::SURVEY_API_DISPCODE_QUOTE_CLOSED; // 41: Vor Umfragebginn ausgefiltert ,weil Quote bereits geschlossen

    /*** BEHAVIOR ON RESUMPTION ***/
    const RELAUNCH_LAST_SENT = efs_apis_surveyApiBase::SURVEY_API_RELAUNCH_LAST_SENT;
    const RELAUNCH_NEXT      = efs_apis_surveyApiBase::SURVEY_API_RELAUNCH_NEXT;
    const RELAUNCH_LAST_SEEN = efs_apis_surveyApiBase::SURVEY_API_RELAUNCH_LAST_SEEN;
    const RELAUNCH_FIRST     = efs_apis_surveyApiBase::SURVEY_API_RELAUNCH_FIRST;

    /*** QUOTA ***/
    const CONFIG_FULL_TERMINATES      = efs_apis_surveyApiBase::SURVEY_API_CONFIG_FULL_TERMINATES;
    const CONFIG_ADD_TO_ALL_MATCHING  = efs_apis_surveyApiBase::SURVEY_API_CONFIG_ADD_TO_ALL_MATCHING;
    const CONFIG_PROCESS_SURVEY_START = efs_apis_surveyApiBase::SURVEY_API_CONFIG_PROCESS_SURVEY_START;
    const CONFIG_FULL_NO_ASSIGNMENT   = efs_apis_surveyApiBase::SURVEY_API_CONFIG_FULL_NO_ASSIGNMENT;
    const ASSIGN_FIRST                = efs_apis_surveyApiBase::SURVEY_API_ASSIGN_FIRST;
    const ASSIGN_LAST                 = efs_apis_surveyApiBase::SURVEY_API_ASSIGN_LAST;
    const ASSIGN_RANDOM               = efs_apis_surveyApiBase::SURVEY_API_ASSIGN_RANDOM;
    const ASSIGN_LOWEST               = efs_apis_surveyApiBase::SURVEY_API_ASSIGN_LOWEST;

    /*** EXTERNAL SURVEY ***/
    const EXTERNAL_URL_TYPE_1 = efs_apis_surveyApiBase::SURVEY_API_EXTERNAL_URL_TYPE_1;
    const EXTERNAL_URL_TYPE_2 = efs_apis_surveyApiBase::SURVEY_API_EXTERNAL_URL_TYPE_2;

    /*** SLIDER QUESTION OPTIONS ***/
    const SLIDER_POS_NONE          = efs_apis_surveyApiBase::SURVEY_API_SLIDER_POS_NONE;
    const SLIDER_POS_LT            = efs_apis_surveyApiBase::SURVEY_API_SLIDER_POS_LT;
    const SLIDER_POS_RB            = efs_apis_surveyApiBase::SURVEY_API_SLIDER_POS_RB;
    const POINTER_BEHAVIOR_ANALOG  = efs_apis_surveyApiBase::SURVEY_API_POINTER_BEHAVIOR_ANALOG;
    const POINTER_BEHAVIOR_DISCRET = efs_apis_surveyApiBase::SURVEY_API_POINTER_BEHAVIOR_DISCRET;
    const POINTER_MIDDLE           = efs_apis_surveyApiBase::SURVEY_API_POINTER_MIDDLE;
    const POINTER_HIDDEN           = efs_apis_surveyApiBase::SURVEY_API_POINTER_HIDDEN;

    /*** ANSWER OPTIONS ***/
    const TYPE_R_CAT        = efs_apis_surveyApiBase::SURVEY_API_TYPE_R_CAT;
    const TYPE_R_CAT_TEXT   = efs_apis_surveyApiBase::SURVEY_API_TYPE_R_CAT_TEXT;
    const TYPE_INSIDE_TITLE = efs_apis_surveyApiBase::SURVEY_API_TYPE_INSIDE_TITLE;
    const TYPE_TEXTFIELD    = efs_apis_surveyApiBase::SURVEY_API_TYPE_TEXTFIELD;
    const TYPE_GROUP        = efs_apis_surveyApiBase::SURVEY_API_TYPE_GROUP;

    /*** MAIL ***/
    const CONTENT_TEXT = efs_apis_surveyApiBase::SURVEY_API_CONTENT_TEXT;
    const CONTENT_HTML = efs_apis_surveyApiBase::SURVEY_API_CONTENT_HTML;
    const EMAIL        = efs_apis_surveyApiBase::SURVEY_API_EMAIL;
    const SMS          = efs_apis_surveyApiBase::SURVEY_API_SMS;

    /*** FILE MANAGER ***/
    const MODE_PROJECT = efs_apis_surveyApiBase::SURVEY_API_MODE_PROJECT;

    /*** FILTER ACCESS ***/
    const FILTER_SCOPE_QUESTION     = efs_apis_surveyApiBase::SURVEY_API_FILTER_SCOPE_QUESTION;
    const FILTER_SCOPE_ITEM         = efs_apis_surveyApiBase::SURVEY_API_FILTER_SCOPE_ITEM;
    const FILTER_SCOPE_RCAT         = efs_apis_surveyApiBase::SURVEY_API_FILTER_SCOPE_RCAT;
    const FILTER_SCOPE_LIST         = efs_apis_surveyApiBase::SURVEY_API_FILTER_SCOPE_LIST;
    const FILTER_SCOPE_LIST_ELEMENT = efs_apis_surveyApiBase::SURVEY_API_FILTER_SCOPE_LIST_ELEMENT;

    const TRIGGER_MAIL       = efs_apis_surveyTriggers::TRIGGER_MAIL;
    const TRIGGER_SAMPLE     = efs_apis_surveyTriggers::TRIGGER_SAMPLE;
    const TRIGGER_BONUS      = efs_apis_surveyTriggers::TRIGGER_BONUS;
    const TRIGGER_PAGE       = efs_apis_surveyTriggers::TRIGGER_PAGE;
    const TRIGGER_LOGOUT     = efs_apis_surveyTriggers::TRIGGER_LOGOUT;
    const TRIGGER_RESET      = efs_apis_surveyTriggers::TRIGGER_RESET;
    const TRIGGER_VSPLIT     = efs_apis_surveyTriggers::TRIGGER_VSPLIT;
    const TRIGGER_RECODE     = efs_apis_surveyTriggers::TRIGGER_RECODE;
    const TRIGGER_RANDOM     = efs_apis_surveyTriggers::TRIGGER_RANDOM;
    const TRIGGER_LIST       = efs_apis_surveyTriggers::TRIGGER_LIST;
    const TRIGGER_PANELGROUP = efs_apis_surveyTriggers::TRIGGER_PANELGROUP;
    const TRIGGER_CUSTOM     = efs_apis_surveyTriggers::TRIGGER_CUSTOM;

    const EXEC_PRE_FILTER  = efs_apis_surveyTriggers::EXEC_PRE_FILTER;
    const EXEC_POST_FILTER = efs_apis_surveyTriggers::EXEC_POST_FILTER;
    const EXEC_DIRECTLY    = efs_apis_surveyTriggers::EXEC_DIRECTLY;

    /*** SESSION ***/
    const SESSION_TYPE_SURVEY       = efs_apis_session::TYPE_SURVEY;
    const SESSION_TYPE_PANEL        = efs_apis_session::TYPE_PANEL;
    const SESSION_TYPE_PREVIEW      = efs_apis_session::TYPE_PREVIEW;
    const SESSION_TYPE_ADMIN        = efs_apis_session::TYPE_ADMIN;
    const SESSION_TYPE_MONITOR      = efs_apis_session::TYPE_MONITOR;
    const SESSION_TYPE_PROVIDER     = efs_apis_session::TYPE_PROVIDER;
    const SESSION_TYPE_PROJECT_TEST = efs_apis_session::TYPE_PROJECT_TEST;
    const SESSION_TYPE_REPORT       = efs_apis_session::TYPE_REPORT;
    const SESSION_TYPE_MOBILE_GUI   = efs_apis_session::TYPE_MOBILE_GUI;

    /*** TRACKING ***/
    const TRACKING_ACTION_INVITE            = efs_apis_panelTracking::ACTION_INVITE;
    const TRACKING_ACTION_REMIND            = efs_apis_panelTracking::ACTION_REMIND;
    const TRACKING_ACTION_SURVEY_START      = efs_apis_panelTracking::ACTION_SURVEY_START;
    const TRACKING_ACTION_SURVEY_COMPLETE   = efs_apis_panelTracking::ACTION_SURVEY_COMPLETE;
    const TRACKING_ACTION_SURVEY_INCOMPLETE = efs_apis_panelTracking::ACTION_SURVEY_INCOMPLETE;
    const TRACKING_ACTION_SCREEN_OUT        = efs_apis_panelTracking::ACTION_SCREEN_OUT;
    const TRACKING_ACTION_QUOTA_FULL        = efs_apis_panelTracking::ACTION_QUOTA_FULL;
    const TRACKING_ACTION_NOT_AVAILABLE     = efs_apis_panelTracking::ACTION_NOT_AVAILABLE;
    const TRACKING_ACTION_SAMPLE            = efs_apis_panelTracking::ACTION_SAMPLE;
    const TRACKING_ACTION_QUALITY_ISSUE     = efs_apis_panelTracking::ACTION_QUALITY_ISSUE;

    /*** DB ***/
    const EFS_TABLE_PROJECT        = efs_apis_efsVariablesLogic::EFS_TABLE_PROJECT;
    const EFS_TABLE_SAMPLE_DATA    = efs_apis_efsVariablesLogic::EFS_TABLE_SAMPLE_DATA;
    const EFS_TABLE_SAMPLE_PROJECT = efs_apis_efsVariablesLogic::EFS_TABLE_SAMPLE_PROJECT;
    const EFS_TABLE_ITEM           = efs_apis_efsVariablesLogic::EFS_TABLE_ITEM;
    const EFS_TABLE_CONTAINER      = efs_apis_efsVariablesLogic::EFS_TABLE_CONTAINER;
    const EFS_TABLE_QUESTION       = efs_apis_efsVariablesLogic::EFS_TABLE_QUESTION;
    const EFS_TABLE_PAGE           = efs_apis_efsVariablesLogic::EFS_TABLE_PAGE;
    const EFS_TABLE_SURVEY_ATT     = efs_apis_efsVariablesLogic::EFS_TABLE_SURVEY_ATT;
    const EFS_TABLE_SURVEY         = efs_apis_efsVariablesLogic::EFS_TABLE_SURVEY;
    const EFS_TABLE_USER_ATT       = efs_apis_efsVariablesLogic::EFS_TABLE_USER_ATT;

    /*** PANEL ***/
    const PANEL_SAMPLE_STATUS_CREATED   = efs_apis_panelTypes::SAMPLE_STATUS_CREATED;
    const PANEL_SAMPLE_STATUS_FILTERED  = efs_apis_panelTypes::SAMPLE_STATUS_FILTERED;
    const PANEL_SAMPLE_STATUS_QUOTED    = efs_apis_panelTypes::SAMPLE_STATUS_QUOTED;
    const PANEL_SAMPLE_STATUS_GENERATED = efs_apis_panelTypes::SAMPLE_STATUS_GENERATED;
    const PANEL_SAMPLE_STATUS_INFORMED  = efs_apis_panelTypes::SAMPLE_STATUS_INFORMED;
    const PANEL_SAMPLE_STATUS_REMINDED  = efs_apis_panelTypes::SAMPLE_STATUS_REMINDED;

    const PANEL_PANELIST_IDENTIFIER_UID               = efs_apis_panelTypes::PANELIST_IDENTIFIER_UID;
    const PANEL_PANELIST_IDENTIFIER_EMAIL             = efs_apis_panelTypes::PANELIST_IDENTIFIER_EMAIL;
    const PANEL_PANELIST_IDENTIFIER_PSEUDONYM         = efs_apis_panelTypes::PANELIST_IDENTIFIER_PSEUDONYM;
    const PANEL_PANELIST_IDENTIFIER_OTHER_ID          = efs_apis_panelTypes::PANELIST_IDENTIFIER_OTHER_ID;
    const PANEL_PANELIST_IDENTIFIER_PCODE             = efs_apis_panelTypes::PANELIST_IDENTIFIER_PCODE;
    const PANEL_PANELIST_IDENTIFIER_PANELIST_CODE     = efs_apis_panelTypes::PANELIST_IDENTIFIER_PANELIST_CODE;
    const PANEL_PANELIST_IDENTIFIER_GROUP             = efs_apis_panelTypes::PANELIST_IDENTIFIER_GROUP;
    const PANEL_PANELIST_IDENTIFIER_SAMPLE            = efs_apis_panelTypes::PANELIST_IDENTIFIER_SAMPLE;
    const PANEL_PANELIST_IDENTIFIER_SAMPLE_TESTERS    = efs_apis_panelTypes::PANELIST_IDENTIFIER_SAMPLE_TESTERS;
    const PANEL_PANELIST_IDENTIFIER_SAMPLE_NONTESTERS = efs_apis_panelTypes::PANELIST_IDENTIFIER_SAMPLE_NONTESTERS;
    const PANEL_PANELIST_IDENTIFIER_MAIL_CODE         = efs_apis_panelTypes::PANELIST_IDENTIFIER_MAIL_CODE;
    const PANEL_PANELIST_IDENTIFIER_MOBILE            = efs_apis_panelTypes::PANELIST_IDENTIFIER_MOBILE;
    const PANEL_PANELIST_IDENTIFIER_ACCOUNT           = efs_apis_panelTypes::PANELIST_IDENTIFIER_ACCOUNT;
    const PANEL_PANELIST_IDENTIFIER_STORAGE           = efs_apis_panelTypes::PANELIST_IDENTIFIER_STORAGE;

    const PANEL_STATUS_CANDIDATE       = efs_apis_panelTypes::STATUS_CANDIDATE;
    const PANEL_STATUS_ACTIVE          = efs_apis_panelTypes::STATUS_ACTIVE;
    const PANEL_STATUS_ACTIVE_CUSTOM   = efs_apis_panelTypes::STATUS_ACTIVE_CUSTOM;
    const PANEL_STATUS_INACTIVE        = efs_apis_panelTypes::STATUS_INACTIVE;
    const PANEL_STATUS_INACTIVE_CUSTOM = efs_apis_panelTypes::STATUS_INACTIVE_CUSTOM;
    const PANEL_STATUS_CUSTOM1         = efs_apis_panelTypes::STATUS_CUSTOM1;
    const PANEL_STATUS_CUSTOM2         = efs_apis_panelTypes::STATUS_CUSTOM2;
    const PANEL_STATUS_DELETED         = efs_apis_panelTypes::STATUS_DELETED;
    const PANEL_STATUS_TESTER          = efs_apis_panelTypes::STATUS_TESTER;
    const PANEL_STATUS_ADMIN           = efs_apis_panelTypes::STATUS_ADMIN;
    const PANEL_STATUS_CUSTOM3         = efs_apis_panelTypes::STATUS_CUSTOM3;
    const PANEL_STATUS_CUSTOM4         = efs_apis_panelTypes::STATUS_CUSTOM4;
    const PANEL_STATUS_CUSTOM5         = efs_apis_panelTypes::STATUS_CUSTOM5;
    const PANEL_STATUS_CUSTOM6         = efs_apis_panelTypes::STATUS_CUSTOM6;
    const PANEL_STATUS_CUSTOM7         = efs_apis_panelTypes::STATUS_CUSTOM7;
    const PANEL_STATUS_CUSTOM8         = efs_apis_panelTypes::STATUS_CUSTOM8;
    const PANEL_STATUS_CUSTOM9         = efs_apis_panelTypes::STATUS_CUSTOM9;

    const PANEL_INPUT_NONE    = efs_apis_panelTypes::INPUT_NONE;
    const PANEL_INPUT_IMPORT  = efs_apis_panelTypes::INPUT_IMPORT;
    const PANEL_INPUT_ADMIN   = efs_apis_panelTypes::INPUT_ADMIN;
    const PANEL_INPUT_SELF    = efs_apis_panelTypes::INPUT_SELF;
    const PANEL_INPUT_FRIENDS = efs_apis_panelTypes::INPUT_FRIENDS;
    const PANEL_INPUT_TEST    = efs_apis_panelTypes::INPUT_TEST;

    const PANEL_TABLE_BONUS_POINTS = efs_apis_efsVariablesLogic::TABLE_CREDIT_POINTS; // 8.0: panelist_bonus_points
    const PANEL_TABLE_PANELISTS = efs_apis_efsVariablesLogic::TABLE_PANEL_USERS; // 8.0: panelists
    const PANEL_TABLE_MD_DATA = efs_apis_efsVariablesLogic::TABLE_MD_DATA; // 8.0: md_data
    const PANEL_TABLE_USERS = efs_apis_efsVariablesLogic::TABLE_ADMIN_USERS; // 8.0: users
    const PANEL_TABLE_PANEL_ATT = efs_apis_efsVariablesLogic::TABLE_PANEL_ATT; // 8.0: panel_att
    const PANEL_TABLE_PANELIST_GROUP = efs_apis_efsVariablesLogic::TABLE_PANEL_GRP; // 8.0: panelist_grp
    const PANEL_TABLE_BONUS_CONFIGURATION    = efs_apis_efsVariablesLogic::EFS_TABLE_BONUS_CONFIGURATION;
    const PANEL_TABLE_PANELIST_GROUP_ARCHIVE = efs_apis_efsVariablesLogic::EFS_TABLE_PANELIST_GROUP_ARCHIVE;
    const PANEL_TABLE_PRIZE_DRAWS            = efs_apis_efsVariablesLogic::EFS_TABLE_PRIZE_DRAWS;
    const PANEL_TABLE_PRIZE_DRAW_WINNERS     = efs_apis_efsVariablesLogic::EFS_TABLE_PRIZE_DRAW_WINNERS;
    const PANEL_TABLE_PRIZE_DRAW_PRIZES      = efs_apis_efsVariablesLogic::EFS_TABLE_PRIZE_DRAW_PRIZES;

    const PANEL_TABLE_PERMANENT = efs_apis_panelTracking::PERMANENT_TABLE; // 8.0: panel_tracking_log_permanent

    const PANEL_TABLE_GROUPS_LOG = efs_apis_panelGroupsManager::LOG_TABLE; // 8.0: panel_groups_log
    const PANEL_TABLE_GROUPS = efs_apis_panelGroupsManager::GROUPS_TABLE; // 8.0: panelgrp

    const PANEL_TABLE_FORUM_FORUMS  = 'panel_community_forums';
    const PANEL_TABLE_FORUM_TOPICS  = 'panel_community_forum_topics';
    const PANEL_TABLE_FORUM_THREADS = 'panel_community_forum_threads';
    const PANEL_TABLE_FORUM_POSTS   = 'panel_community_forum_posts';

    /*** EXPORT TYPES ***/
    const EXPORT_FORMAT_CSV = "csv"; // CSV (columns separated by character, no labels)
    const EXPORT_FORMAT_XLS = "xls"; // XLS (MS Excel binary format)
    const EXPORT_FORMAT_TRIPLES = "sss"; // Triple-S (XML specification file and data file)
    const EXPORT_FORMAT_TOPSTUD = "ssss"; // topStud
    const EXPORT_FORMAT_XML = "xmlres"; // XML data file
    const EXPORT_FORMAT_HTML = "htmlres"; // HTML data file
    const EXPORT_FORMAT_SPSS = "spss"; // SPSS (labeled data file in SPSS binary format)
    const EXPORT_FORMAT_SPSSPORT = "por"; // SPSS portable file format
    const EXPORT_FORMAT_SAS = "sas"; // SAS
    const EXPORT_FORMAT_FIXED = "ff"; // Fixed format (e.g. to use in Quantum)
    const EXPORT_FORMAT_QUANTUM = "quantum"; // Quantum files (base file, tab file, axis file)

    /*** BONUS POINTS ***/
    // "status" from table "panelist_bonus_points"
    // NOTE: "status" can also be "pay_id" from table "panel_bonuses" if points have been turned into a bonus by a panelist
    const BONUS_STATUS_TRANSFER = efs_apis_panelTypes::BONUS_TRANSFER; // 10 = "Zurücksetzen von Punkten"
    const BONUS_STATUS_TRANSFER_CREDIT = efs_apis_panelTypes::BONUS_TRANSFER_CREDIT; // 11 = "Übertrag Haben"
    const BONUS_STATUS_BONUS_ASSIGN = efs_apis_panelTypes::BONUS_BONUS_ASSIGN; // 20 = "Zuweisung Prämiensystem"
    const BONUS_STATUS_SURVEY_PARTICIPATION = efs_apis_panelTypes::BONUS_SURVEY_PARTICIPATION; // 21 = "Teilnahme an einer Umfrage"
    const BONUS_STATUS_SURVEY_BLOCK = efs_apis_panelTypes::BONUS_SURVEY_BLOCK; // 22 = "Erreichen eines bestimmten Umfrageabschnitts"
    const BONUS_STATUS_QUOTA_FULL = efs_apis_panelTypes::BONUS_QUOTA_FULL; // 23 = "Versuch der Teilnahme und Zurueckweisung wg. Ueberschreitung max. Netto"
    const BONUS_STATUS_MASTERDATA = efs_apis_panelTypes::BONUS_MASTERDATA; // 24 = "Teilnahme an einer Stammdatenbefragung"
    const BONUS_STATUS_REGISTRATION = efs_apis_panelTypes::BONUS_REGISTRATION; // 25 = "Registrierung"
    const BONUS_STATUS_TRIGGER = efs_apis_panelTypes::BONUS_TRIGGER; // 26 = "Zuweisung von Punkten durch Bonustrigger"
    const BONUS_STATUS_PROMOTION_GRATIFICATION = efs_apis_panelTypes::BONUS_PROMOTION_GRATIFICATION; // 30 = "Gratifikation bei Freundschaftswerbung"
    const BONUS_STATUS_PROMOTION_ASSIGN = efs_apis_panelTypes::BONUS_PROMOTION_ASSIGN; // 31 = "Zuweisung bei Freundschaftswerbung"
    const BONUS_STATUS_MISC = efs_apis_panelTypes::BONUS_MISC; // 40 = "Sonstige Gratifikation"
    const BONUS_STATUS_BY_ADMINISTRATOR = efs_apis_panelTypes::BONUS_BY_ADMINISTRATOR; // 41 = "Zuweisung von Bonuspunkten durch Moderator"
    const BONUS_STATUS_STORNO = efs_apis_panelTypes::BONUS_STORNO; // 42 = "Storno"
    const BONUS_STATUS_TRANSFER2 = efs_apis_panelTypes::BONUS_TRANSFER2; // 50 = "Zurücksetzen von Punkten"
    const BONUS_STATUS_TRANSFER_DEBIT = efs_apis_panelTypes::BONUS_TRANSFER_DEBIT; // 51 = "Übertrag Soll"
    const BONUS_STATUS_COUPON_PAYOUTS = efs_apis_panelTypes::BONUS_COUPON_PAYOUTS; // 60 = "Auszahlungen Gutscheine"
    const BONUS_STATUS_DONATION_PAYOUTS = efs_apis_panelTypes::BONUS_DONATION_PAYOUTS; // 70 = "Auszahlungen Spenden"
    const BONUS_STATUS_MISC_DEDUCTION = efs_apis_panelTypes::BONUS_MISC_DEDUCTION; // 80 = "Sonstige Abzüge"
    const BONUS_STATUS_ADMIN_DEDUCTION = efs_apis_panelTypes::BONUS_ADMIN_DEDUCTION; // 81 = "Abzug von Bonuspunkten durch Administrator"
    const BONUS_STATUS_COMMUNITY_PARTICIPATION = efs_apis_panelTypes::BONUS_COMMUNITY_PARTICIPATION; // 91 = "Teilnahme an Forumsdiskussion"
    const BONUS_STATUS_TODOLIST = efs_apis_panelTypes::BONUS_TODOLIST; // 92 = "Bonuspunkte für Aufgabe"
  }
