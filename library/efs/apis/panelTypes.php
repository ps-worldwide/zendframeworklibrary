<?php
  /**
   * efs_apis_panelTypes
   *
   * @see       modules/panel/components/types.inc.php
   *
   * @package   zendframeworkLibrary
   * @version   $Id: panelTypes.php,v 1.2 2013/10/16 09:27:40 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class efs_apis_panelTypes
  {
    const SAMPLE_STATUS_CREATED   = 10;
    const SAMPLE_STATUS_FILTERED  = 20;
    const SAMPLE_STATUS_QUOTED    = 30;
    const SAMPLE_STATUS_GENERATED = 40;
    const SAMPLE_STATUS_INFORMED  = 50;
    const SAMPLE_STATUS_REMINDED  = 60;

    const PANELIST_IDENTIFIER_UID               = "uid";
    const PANELIST_IDENTIFIER_EMAIL             = "u_email";
    const PANELIST_IDENTIFIER_EMAIL_OLD         = "email";
    const PANELIST_IDENTIFIER_PSEUDONYM         = "pseudonym";
    const PANELIST_IDENTIFIER_OTHER_ID          = "u_other_id";
    const PANELIST_IDENTIFIER_OTHER_ID_OLD      = "other_id";
    const PANELIST_IDENTIFIER_PCODE             = "pcode";
    const PANELIST_IDENTIFIER_PANELIST_CODE     = "panelist_code";
    const PANELIST_IDENTIFIER_GROUP             = "group";
    const PANELIST_IDENTIFIER_SAMPLE            = "sample";
    const PANELIST_IDENTIFIER_SAMPLE_TESTERS    = "sample_testers";
    const PANELIST_IDENTIFIER_SAMPLE_NONTESTERS = "sample_nontesters";
    const PANELIST_IDENTIFIER_MAIL_CODE         = "mail_code";
    const PANELIST_IDENTIFIER_MOBILE            = "u_mobile";
    const PANELIST_IDENTIFIER_MOBILE_OLD        = "mobile";
    const PANELIST_IDENTIFIER_ACCOUNT           = "u_account";
    const PANELIST_IDENTIFIER_ACCOUNT_OLD       = "account";
    const PANELIST_IDENTIFIER_STORAGE           = "efs_system_storage";

    const STATUS_CANDIDATE       = 1;
    const STATUS_ACTIVE          = 2;
    const STATUS_ACTIVE_CUSTOM   = 3;
    const STATUS_INACTIVE        = 4;
    const STATUS_INACTIVE_CUSTOM = 5;
    const STATUS_CUSTOM1         = 6;
    const STATUS_CUSTOM2         = 7;
    const STATUS_DELETED         = 8;
    const STATUS_TESTER          = 16;
    const STATUS_ADMIN           = 32;
    const STATUS_CUSTOM3         = 9;
    const STATUS_CUSTOM4         = 10;
    const STATUS_CUSTOM5         = 11;
    const STATUS_CUSTOM6         = 12;
    const STATUS_CUSTOM7         = 13;
    const STATUS_CUSTOM8         = 14;
    const STATUS_CUSTOM9         = 15;

    const INPUT_NONE    = 1;
    const INPUT_IMPORT  = 2;
    const INPUT_ADMIN   = 4;
    const INPUT_SELF    = 8;
    const INPUT_FRIENDS = 16;
    const INPUT_TEST    = 32;

    const BONUS_TRANSFER                = 10;
    const BONUS_TRANSFER_CREDIT         = 11;
    const BONUS_BONUS_ASSIGN            = 20;
    const BONUS_SURVEY_PARTICIPATION    = 21;
    const BONUS_SURVEY_BLOCK            = 22;
    const BONUS_QUOTA_FULL              = 23;
    const BONUS_MASTERDATA              = 24;
    const BONUS_REGISTRATION            = 25;
    const BONUS_TRIGGER                 = 26;
    const BONUS_PROMOTION_GRATIFICATION = 30;
    const BONUS_PROMOTION_ASSIGN        = 31;
    const BONUS_MISC                    = 40;
    const BONUS_BY_ADMINISTRATOR        = 41;
    const BONUS_STORNO                  = 42;
    const BONUS_TRANSFER2               = 50;
    const BONUS_TRANSFER_DEBIT          = 51;
    const BONUS_COUPON_PAYOUTS          = 60;
    const BONUS_DONATION_PAYOUTS        = 70;
    const BONUS_MISC_DEDUCTION          = 80;
    const BONUS_ADMIN_DEDUCTION         = 81;
    const BONUS_COMMUNITY_PARTICIPATION = 91;
    const BONUS_TODOLIST                = 92;
  }