<?php
  /**
   * efs_apis_projectStatus
   *
   * @see       modules/survey/components/project/status.inc.php
   *
   * @package   zendframeworkLibrary
   * @version   $Id: projectStatus.php,v 1.6 2013/10/16 09:27:39 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  abstract class efs_apis_projectStatus
  {
    const PSTATUS_CREATED         = 10;
    const PSTATUS_PARTIP_ASSIGNED = 30;
    const PSTATUS_PARTIP_NOTIFIED = 40;
    const PSTATUS_VARS_ASSIGNED   = 42;
    const PSTATUS_SURVEY_GEN      = 44;
    const PSTATUS_ACTIVE          = 50;
    const PSTATUS_CLOSING         = 51;
    const PSTATUS_INACTIVE        = 52;
    const PSTATUS_FINISHED        = 60;
    const PSTATUS_TO_ARCHIVE      = 69;
    const PSTATUS_ARCHIVATED      = 70;

    // added because Service Layer returns text instead of integer (not in status.inc.php), used by efs/apis/const.php
    const PSTATUS_CREATED_TXT         = 'CREATED';
    const PSTATUS_PARTIP_ASSIGNED_TXT = 'PARTICIPANTS_ASSIGNED';
    const PSTATUS_PARTIP_NOTIFIED_TXT = 'PARTICIPANTS_NOTIFIED';
    const PSTATUS_VARS_ASSIGNED_TXT   = 'VARIABLES_ASSIGNED';
    const PSTATUS_SURVEY_GEN_TXT      = 'GENERATED';
    const PSTATUS_ACTIVE_TXT          = 'ACTIVE';
    const PSTATUS_CLOSING_TXT         = 'CLOSING';
    const PSTATUS_INACTIVE_TXT        = 'INACTIVE';
    const PSTATUS_FINISHED_TXT        = 'FINISHED';
    const PSTATUS_TO_ARCHIVE_TXT      = 'TO_ARCHIVE';
    const PSTATUS_ARCHIVATED_TXT      = 'ARCHIVED';

    private static $project_statuses = array(
      self::PSTATUS_CREATED         => 'gl_pstatus_created',
      self::PSTATUS_PARTIP_ASSIGNED => 'gl_pstatus_partip_assigned',
      self::PSTATUS_PARTIP_NOTIFIED => 'gl_pstatus_partip_notified',
      self::PSTATUS_VARS_ASSIGNED   => 'gl_pstatus_vars_assigned',
      self::PSTATUS_SURVEY_GEN      => 'gl_pstatus_survey_gen',
      self::PSTATUS_ACTIVE          => 'gl_pstatus_active',
      self::PSTATUS_CLOSING         => 'gl_pstatus_closing',
      self::PSTATUS_INACTIVE        => 'gl_pstatus_inactive',
      self::PSTATUS_FINISHED        => 'gl_pstatus_finished',
      self::PSTATUS_TO_ARCHIVE      => 'gl_pstatus_to_archive',
      self::PSTATUS_ARCHIVATED      => 'gl_pstatus_archivated');

    /**
     * project_statuses
     *
     * @return array
     */
    public static function get_project_statuses()
    {
      return array_keys(self::$project_statuses);
    }

    /**
     * enumGetSurveyStatuses
     *
     * @return array
     */
    public static function enumGetSurveyStatuses()
    {
      return array(
        'CREATED'               => self::PSTATUS_CREATED,
        'PARTICIPANTS_ASSIGNED' => self::PSTATUS_PARTIP_ASSIGNED,
        'PARTICIPANTS_NOTIFIED' => self::PSTATUS_PARTIP_NOTIFIED,
        'VARIABLES_ASSIGNED'    => self::PSTATUS_VARS_ASSIGNED,
        'GENERATED'             => self::PSTATUS_SURVEY_GEN,
        'ACTIVE'                => self::PSTATUS_ACTIVE,
        'CLOSING'               => self::PSTATUS_CLOSING,
        'INACTIVE'              => self::PSTATUS_INACTIVE,
        'FINISHED'              => self::PSTATUS_FINISHED,
        'TO_ARCHIVE'            => self::PSTATUS_TO_ARCHIVE,
        'ARCHIVED'              => self::PSTATUS_ARCHIVATED,
      );
    }

    /**
     * project statuses w/ labels
     *
     * @return array
     */
    public static function get_labeled_project_statuses()
    {
      return self::$project_statuses;
    }
  }
