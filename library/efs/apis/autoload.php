<?php
  if (APPLICATION_ENV != 'testing')
    mail('actionscript@questback.de', __FILE__ . ' was called', implode("\n", debug_backtrace()));

  /**
   * efs_apis_autoload
   *
   * @package   zendframeworkLibrary
   *
   * @version   $Id: autoload.php,v 1.20 2013/10/16 09:27:40 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   * @deprecated
   * @todo      check if some project still needs this file
   */

  class efs_apis_autoload //extends efs_autoload
  {
    /**
     * Returns the class-file-extensions. Existed as constant till EFS 7.0
     *
     * @return string .inc.php
     */
    public function getFileExtension()
    {
      return '.inc.php';
    }

    /**
     * Initialises the objectmaster from EFS
     */
    public static function initObjectmaster()
    {
      // the autoloader in EFS
      /** @noinspection PhpDynamicAsStaticMethodCallInspection */
      /** @noinspection PhpDeprecationInspection */
      if (efs_apis_version::getVersion() == 7.0)
        Zend_Loader::loadFile(APPLICATION_EFS_PATH . "/wcp/framework/class_efs_autoload.inc.php", null, true);

      /** @noinspection PhpDynamicAsStaticMethodCallInspection */
      /** @noinspection PhpDeprecationInspection */
      if (efs_apis_version::getVersion() < 8.0)
      {
        /** @noinspection PhpDynamicAsStaticMethodCallInspection */
        /** @noinspection PhpUndefinedClassInspection */
        parent::$base_all_path = efs_apis_config::get("base_all_path");
      }

      // 7.1 uses $initialized
      /** @noinspection PhpDynamicAsStaticMethodCallInspection */
      /** @noinspection PhpDeprecationInspection */
      if (efs_apis_version::getVersion() > 7.0 && efs_apis_version::getVersion() < 8.0)
      {
        /** @noinspection PhpUndefinedClassInspection */
        parent::$initialized = true;
        /** @noinspection PhpDynamicAsStaticMethodCallInspection */
        /** @noinspection PhpUndefinedClassInspection */
        parent::$debuglevel = efs_apis_config::get("debuglevel");
      }

      /** @noinspection PhpDynamicAsStaticMethodCallInspection */
      /** @noinspection PhpDeprecationInspection */
      /** @noinspection PhpUndefinedClassInspection */
      if (efs_apis_version::getVersion() < 8.0 && !parent::$skip_functions)
      {
        /** @noinspection PhpUndefinedClassInspection */
        parent::init_functions();
      }
    }

    /**
     * Returns file for given class if class-name is registered in
     * EFS-objectmaster
     *
     * @param $class
     *
     * @return null
     */
    public function isObjectmaster($class)
    {
      /** @noinspection PhpUndefinedClassInspection */
      if (array_key_exists($class, parent::$dictionary))
      {
        /** @noinspection PhpUndefinedClassInspection */
        return parent::$dictionary[$class];
      }

      return null;
    }

    /**
     * Returns the filename for the given classname if found in
     * EFS module directory
     *
     * @param $class
     *
     * @internal param string $filename
     * @return bool|string
     */
    public function isModuleManager($class)
    {
      if (strpos($class, "/") !== false)
        return false;

      $classnameLower = strtolower($class);
      $partsLower     = explode("_", $classnameLower);
      $moduleLower    = array_shift($partsLower);

      /** @noinspection PhpDeprecationInspection */
      /** @noinspection PhpUndefinedClassInspection */
      $filename = parent::$base_all_path . "modules/" . $moduleLower . "/components/" . implode("/", $partsLower) . self::getFileExtension();
      $filename = realpath($filename);
      if ($filename !== false)
        return $filename;

      return false;
    }
  }