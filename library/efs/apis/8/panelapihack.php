<?php
  /**
   * efs_apis_8_panelapihack
   * EFS-API - methods that are not yet part of EFS itself
   *
   * @package   zendframeworkLibrary
   * @version   $Id: panelapihack.php,v 1.6 2013/10/16 09:27:19 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  abstract class efs_apis_8_panelapihack extends efs_apis_7_1_panelapi
  {
    private $db;

    /**
     * __construct
     *
     * @param Zend_DB_Adapter_Abstract $db
     *
     * @return \efs_apis_8_panelapihack
     */
    public function __construct(Zend_DB_Adapter_Abstract $db)
    {
      $this->db = $db;
    }

    /**
     * getTableFields
     * gets fields and datatypes from table
     *
     * @param string $table
     *
     * @return array fields
     */
    public function getTableFields($table)
    {
      if ($table == "")
        return array();

      $result = $this->db->describeTable($table);
      if (!is_array($result) || count($result) == 0)
        return array();

      $resultData = array();
      foreach ($result as $field => $fieldData)
        $resultData[$field] = $fieldData["DATA_TYPE"];

      return $resultData;
    }
  }
