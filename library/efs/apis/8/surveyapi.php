<?php
  /**
   * efs_apis_8_surveyapi
   * The class extends the survey API of 7.1 (see efs_apis_8_surveyapihack) to
   * ensure the methods of 7.1 stay backward compatible.
   *
   * @package   efs_models_api
   * @component efs_apis_surveyapi
   * @author    umschlag
   * @version   $id
   */

  class efs_apis_8_surveyapi extends efs_apis_8_surveyapihack
  {
    /**
     * markNotImplemented
     *
     * Throws an exceptions presenting the text that the method is not supported
     * in EFS 8.0 and still needs to be adjusted.
     *
     * @throws Exception Messge text shows that method needs to be adjusted
     */
    private function markNotImplemented()
    {
      throw new Exception('Method needs to be adjusted for EFS 8.0');
    }

    /**
     * getMediaLibraryStructure
     *
     * Returns and array showing the elements of the media libray of survey
     * identified by the first parameter.
     *
     * @todo Fix for EFS 8.0
     *
     * @param mixed $pid Survey ID of the survey from what the structure is read
     *
     * @return array|void
     */
    public function getMediaLibraryStructure($pid)
    {
      $this->markNotImplemented();
    }

    /**
     * getAnswerOptions
     *
     * Returns an array holding the answer options (items) of the given question
     * and survey. Alias for getItems().
     *
     * @todo Fix for EFS 8.0
     * @see  getItems
     *
     * @param mixed $pid Survey ID used the get the answer options from
     * @param mixed $qid The question ID where the answer options are read from
     */
    public function getAnswerOptions($pid, $qid)
    {
      $this->markNotImplemented();
    }

    /**
     * getItems
     *
     * Returns an array holding the answer options (items) of the given question
     * and survey.
     *
     * @todo Fix for EFS 8.0
     * @see  getAnswerOptions
     *
     * @param mixed $pid Survey ID used the get the answer options from
     * @param mixed $qid The question ID where the answer options are read from
     */
    public function getItems($pid, $qid)
    {
      $this->markNotImplemented();
    }

    /**
     * Returns available project statuses
     *
     * @todo Fix for EFS 8.0
     */
    public function getProjectStatuses()
    {
      $this->markNotImplemented();
    }
  }