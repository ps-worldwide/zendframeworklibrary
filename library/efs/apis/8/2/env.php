<?php
  /**
   * efs_apis_8_2_env
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.8 2016/07/27 08:07:04 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class efs_apis_8_2_env extends efs_apis_8_1_env
  {
    private static $instance;

    /**
     * __construct
     */
    private function __construct()
    {
      // EFS expect static gp_conf to be loaded
      parent::loadStaticGpConf();

      // Some API methods in EFS want to print html via htmlfunc3, this file moved in 8.2 from wcp to modules
      if (file_exists(APPLICATION_EFS_PATH . "/modules/efs/htmlfunc.inc.php") === true) { // does not exist anymore since (20)16.2
        Zend_Loader::loadFile(APPLICATION_EFS_PATH . "/modules/efs/htmlfunc.inc.php", null, true);
      }

      // no dbwrap in EFS 8.1 anymore, efs_database_connection needed
      Zend_Registry::set('db', self::getDb(Zend_Registry::get('conf')));

      // EFS pushes its own autoload onto the spl-stack which we do not want
      // to use to avoid redelcaration of classes es PCT uses another Zend -
      // Framework
      spl_autoload_unregister('efs_autoload::init');
    }

    /**
     *
     * @return efs_apis_8_1_env $instance
     */
    public static function getInstance()
    {
      if (self::$instance == NULL)
        self::$instance = new self();

      return self::$instance;
    }
  }
