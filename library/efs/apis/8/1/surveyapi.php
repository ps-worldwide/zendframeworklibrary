<?php
  /**
   * efs_apis_8_1_surveyapi
   *
   * @package   zendframeworkLibrary
   * @version   $Id: surveyapi.php,v 1.7 2013/10/16 09:27:18 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class efs_apis_8_1_surveyapi extends efs_apis_8_surveyapi
  {
    /**
     * __construct
     *
     * @param Zend_DB_Adapter_Abstract $db
     *
     * @return \efs_apis_8_1_surveyapi
     */
    public function __construct(Zend_DB_Adapter_Abstract $db)
    {
      /** @noinspection PhpUndefinedClassInspection */
      $this->efsApi = survey_api::get_instance();
      parent::__construct($db);
    }
  }