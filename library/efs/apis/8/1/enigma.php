<?php
  /**
   * efs_apis_enigma
   *
   * @package   zendframeworkLibrary
   * @version   $Id: enigma.php,v 1.7 2013/10/16 09:27:18 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_enigma
  {
    /**
     * check_equal
     *
     * @param $credential
     * @param $passwd
     */
    public function check_equal($credential, $passwd)
    {
      /** @noinspection PhpUndefinedClassInspection */
      return efs_crypt_enigma::check_equal($credential, $passwd);
    }
  }