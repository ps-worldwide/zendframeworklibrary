<?php
  /**
   * efs_apis_8_hrapi
   * Does the EFS-API call
   *
   * @package   zendframeworkLibrary
   * @version   $Id: hrapi.php,v 1.9 2013/10/16 09:27:19 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class efs_apis_8_hrapi extends efs_apis_8_hrapihack
  {
    // hr api
    protected $api = NULL;

    /**
     * __construct
     *
     * @param Zend_DB_Adapter_Abstract $db
     *
     * @return \efs_apis_8_hrapi
     */
    public function __construct(Zend_DB_Adapter_Abstract $db)
    {
      // so far there is no hr-api in EFS
      // till then methods should be hacked
      // into class hrapihack
      //$this->api = hr_api::get_instance();
      parent::__construct($db);
    }

    /**
     * __call
     *
     * @param mixed $method
     * @param mixed $param
     *
     * @throws Exception
     * @return void
     */
    public function __call($method, $param)
    {
      throw new Exception('Call to undefined efs-panelapi method "' . $method . '()"');
    }
  }