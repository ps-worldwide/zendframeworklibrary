<?php
  /**
   * efs_apis_version
   *
   * @deprecated replaced by Pct_Efs_Version::getVersion();
   * @todo       remove deprecated warnings to be downward compatible
   *             or remove usage in projects
   * @todo       remove from
   *             ProductCustomizingTeam/AutomatedExport/application/Bootstrap.php and
   *             make this function abstract or replace directly by new Pct-class
   * @author     umschlag
   *
   */

  class efs_apis_version
  {
    private static $efsVersion = NULL;
    private static $isInit = false;

    /**
     * Returns the EFS version as String
     *
     * @return string efsVersion
     */
    public function getVersion()
    {
      /** @noinspection PhpDeprecationInspection */
      self::printDeprecated(__METHOD__, 'Pct_Efs_Version::getVersion()' . "\n" . debug_print_backtrace());

      /** @noinspection PhpDeprecationInspection */
      if (self::$isInit == false)
      {
        /** @noinspection PhpDeprecationInspection */
        self::getInstance();
      }

      /** @noinspection PhpDeprecationInspection */
      return self::$efsVersion;
    }

    /**
     * Creates singelton instance
     */
    private static function getInstance()
    {
      // Since 7.1 a file shows the EFS - version
      if (file_exists(APPLICATION_EFS_PATH . 'modules/efs/components/version.inc.php'))
      {
        /** @noinspection PhpDeprecationInspection */
        self::$efsVersion = self::getVersionFromEfs();
      }
      else
      {
        /** @noinspection PhpDeprecationInspection */
        self::$efsVersion = self::getVersionFromCvsTag();
      }

      /** @noinspection PhpDeprecationInspection */
      self::$isInit = true;
    }

    /**
     * Determines the EFS - version from a possible CVS - tag
     * only for EFS <7.1
     */
    private static function getVersionFromCvsTag()
    {
      if (file_exists(APPLICATION_EFS_PATH . 'www/CVS/Entries'))
      {
        $cvs   = file_get_contents(APPLICATION_EFS_PATH . 'www/CVS/Entries');
        $start = strpos($cvs, '/init.inc.php3');
        if ($start == 0)
          return (float)0.0; // dev

        $initInc = substr($cvs, $start, 128);
        $version = preg_match('/\/\/T(.*?)\n/', $initInc, $matches);
        if ($version < 1)
          throw new Exception('Cannot determine EFS version');

        // pass the match as version
        $version = $matches[1];

        // major branches
        if ($version == 'SECHS-0')
          return (float)6.0;
        elseif ($version == 'SIEBEN-0')
          return (float)7.0;

        // support tags
        $version = str_replace('v_', '', $version);
        $version = preg_replace('/(_\d+$)/', '', $version);
        $version = str_replace('_', '.', $version);
        return (float)$version;
      }

      return 0;
    }

    /**
     * Reads the version from the EFS function get_version() in
     * modules/efs/components/version.inc.php
     */
    private static function getVersionFromEfs()
    {
      $versionClass    = file_get_contents(APPLICATION_EFS_PATH . 'modules/efs/components/version.inc.php');
      $versionFunction = preg_match('/function getVersion\(\)[^}]*}/', $versionClass, $matches);
      $versionString   = preg_match('/return ([\d\.]+);/', $matches[0], $matches);
      return (float)$matches[1];
    }

    /**
     * Prints a deprecated message to stout
     *
     * @param string $method
     * @param string $instead
     */
    private static function printDeprecated($method, $instead)
    {
      if (defined('APPLICATION_ENV') && APPLICATION_ENV == 'development')
        print PHP_EOL . $method . ' is deprecated. Instead use: ' . $instead;
    }
  }
