<?php
  /**
   * efs_apis_locale
   *
   * @package   zendframeworkLibrary
   * @version   $Id: locale.php,v 1.8 2013/10/16 09:27:40 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   * @deprecated
   * @todo      local0r does not exist in EFS > 7.1
   */

  // This file should not be used as version specific file exists
  if (APPLICATION_ENV != 'testing')
    mail('actionscript@questback.de', __FILE__ . ' was called', implode("\n", debug_backtrace()));

  /**
   * Class efs_apis_locale
   */
  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_locale extends local0r
  {
  }