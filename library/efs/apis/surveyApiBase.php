<?php
  /**
   * efs_apis_apiBase
   *
   * @see       modules/survey/components/api/base.inc.php
   *
   * @package   zendframeworkLibrary
   * @version   $Id: surveyApiBase.php,v 1.3 2013/10/16 09:27:40 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   * @todo      check if survey_api_base still exists
   * @todo      remove EFS dependence (survey_api_base)
   */

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_surveyApiBase extends survey_api_base
  {
  }