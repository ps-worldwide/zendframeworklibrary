<?php
  /**
   * efs_apis_10_6_env 
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.3 2015/06/30 16:22:35 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class efs_apis_10_6_env extends efs_apis_10_5_env 
  {
  }