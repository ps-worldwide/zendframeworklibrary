<?php
  /**
   * efs_apis_10_env
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.2 2013/10/16 09:27:29 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class efs_apis_10_env extends efs_apis_9_1_env
  {
  }
