<?php
  /**
   * efs_apis_10_7_env
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.10 2015/10/06 10:08:01 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class efs_apis_10_7_env extends efs_apis_10_6_env
  {
  }
