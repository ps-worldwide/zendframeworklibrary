<?php
  /**
   * efs_apis_10_2_env 
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.4 2014/10/24 12:30:27 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class efs_apis_10_2_env extends efs_apis_10_1_env 
  {
  }
