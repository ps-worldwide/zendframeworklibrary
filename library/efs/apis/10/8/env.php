<?php
  /**
   * efs_apis_10_8_env  
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.2 2015/12/15 11:38:52 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class efs_apis_10_8_env extends efs_apis_10_7_env 
  {
  }
