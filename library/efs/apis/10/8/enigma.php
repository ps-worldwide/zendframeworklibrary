<?php
/**
 * efs_apis_enigma
 * password hashing has changed in EFS 9.1
 *
 * @package zendframeworkLibrary
 * @version $Id: enigma.php,v 1.1 2015/10/06 10:08:01 faust Exp $
 * @copyright (c) QuestBack http://www.questback.com
 * @author $Author: faust $
 */

/**
 * @noinspection PhpUndefinedClassInspection
 */
abstract class efs_apis_enigma
{
  const ALGORITHM_DES = 'des';
  const ALGORITHM_MD5 = 'md5';
  const ALGORITHM_SHA512 = 'sha512';

  /**
   * cryptPassword
   * create password, needs hash to extract salt
   *
   * @param string $password
   * @param string $hash
   *
   * @return string
   */
  public function cryptPassword($password, $hash)
  {
    return crypt($password, self::extractSalt($hash));
  }

  // from here: copied from modules/efs/components/crypt/enigma.inc.php

  /**
   * check_equal
   *
   * @param string $password
   * @param string $hash
   *
   * @return bool
   */
  public function check_equal($password, $hash)
  {
    return self::compare($hash, crypt($password, self::extractSalt($hash)));
  }

  /**
   * Compares two strng.
   * This method implements a constant-time algorithm to compare strings to
   * avoid (remote) timing attacks.
   * Copied from symfony security component.
   *
   * @param string $first The first string
   * @param string $second The second string
   * @return boolean True if the two string are the same, false otherwise
   */
  private function compare($first, $second)
  {
    if(strlen($first) !== strlen($second)) {
      return false;
    }

    $result = 0;
    for($i = 0; $i < strlen($first); $i++) {
      $result |= ord($first[$i]) ^ ord($second[$i]);
    }

    return 0 === $result;
  }

  /**
   *
   * @param string $hash Hash
   * @return string Salt
   */
  private function extractSalt($hash)
  {
    $algorithm = self::detectAlgorithm($hash);

    self::checkAlgorithm($algorithm);

    switch ($algorithm) {
      case self::ALGORITHM_SHA512 :
        return substr($hash, 0, strrpos($hash, '$') + 1);

      case self::ALGORITHM_MD5 :
        return substr($hash, 0, 12);

      case self::ALGORITHM_DES :
        return substr($hash, 0, 2);
    }

    return '';
  }

  /**
   *
   * @param string $hash Hash
   * @throws InvalidArgumentException
   * @return string Algorithm
   */
  private function detectAlgorithm($hash)
  {
    if(substr($hash, 0, 3) === '$6$') {
      return self::ALGORITHM_SHA512;
    }

    if(substr($hash, 0, 3) === '$1$') {
      return self::ALGORITHM_MD5;
    }

    if(strlen($hash) === 13) {
      // REVIEW: is this a valid check for std_des?
      return self::ALGORITHM_DES;
    }

    throw new InvalidArgumentException('Unsupported algorithm');
  }

  /**
   *
   * @param string $algorithm Algorithm
   * @throws InvalidArgumentException If algorithm is not supported or not available in PHP runtime
   */
  private function checkAlgorithm($algorithm)
  {
    switch ($algorithm) {
      case self::ALGORITHM_SHA512 :
        if(0 === CRYPT_SHA512) {
          throw new InvalidArgumentException('Algorithm not available.');
        }
        break;

      case self::ALGORITHM_DES :
        if(CRYPT_STD_DES !== 1) {
          throw new InvalidArgumentException('Algorithm not available.');
        }
        break;

      case self::ALGORITHM_MD5 :
        if(CRYPT_MD5 !== 1) {
          throw new InvalidArgumentException('Algorithm not available.');
        }
        break;

      default :
        throw new InvalidArgumentException('Unsupported algorithm');
    }
  }
}