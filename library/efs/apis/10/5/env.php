<?php
  /**
   * efs_apis_10_5_env 
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.2 2015/07/03 15:03:39 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class efs_apis_10_5_env extends efs_apis_10_4_env 
  {
  }
