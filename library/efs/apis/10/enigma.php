<?php
  /**
   * efs_apis_enigma
   *
   * @package   zendframeworkLibrary
   * @version   $Id: enigma.php,v 1.5 2013/11/13 12:59:31 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_enigma
  {
    /**
     * check_equal
     *
     * @param $credential
     * @param $passwd
     *
     * @return bool
     */
    public function check_equal($credential, $passwd)
    {
      /** @noinspection PhpUndefinedClassInspection */
      return efs_crypt_enigma::check_equal($credential, $passwd);
    }
  }