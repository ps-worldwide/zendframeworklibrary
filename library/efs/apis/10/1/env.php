<?php
  /**
   * efs_apis_10_1_env
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.3 2014/04/07 16:13:31 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class efs_apis_10_1_env extends efs_apis_10_env
  {
  }
