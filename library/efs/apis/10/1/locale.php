<?php
  /**
   * efs_apis_locale
   * Extends efs_localehack (alias local0r in 7.0) in 8.0.
   *
   * @package   zendframeworkLibrary
   * @version   $Id: locale.php,v 1.1 2013/11/13 12:59:31 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   * @todo      write test
   */

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_locale extends efs_localehack
  {
  }