<?php
  /**
   * efs_apis_10_4_env 
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.2 2015/01/05 13:17:38 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class efs_apis_10_4_env extends efs_apis_10_3_env 
  {
  }
