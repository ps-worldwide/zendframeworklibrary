<?php
  /**
   * efs_apis_10_3_env
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.6 2014/10/28 12:58:44 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class efs_apis_10_3_env extends efs_apis_10_2_env
  {
  }
