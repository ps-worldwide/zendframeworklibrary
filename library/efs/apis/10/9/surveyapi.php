<?php
/**
 * efs_apis_10_9_surveyapi
 *
 * @package   zendframeworkLibrary
 * @version   $Id: surveyapi.php,v 1.2 2016/04/26 16:26:20 faust Exp $
 * @copyright (c) QuestBack http://www.questback.com
 * @author    $Author: faust $
 */

class efs_apis_10_9_surveyapi extends efs_apis_8_1_surveyapi
{
    /**
     * __construct
     *
     * @param Zend_DB_Adapter_Abstract $db
     *
     * @return \efs_apis_8_1_surveyapi
     */
    public function __construct(Zend_DB_Adapter_Abstract $db)
    {
        /** @noinspection PhpUndefinedClassInspection */
        $this->efsApi = survey_api::get_instance();
        parent::__construct($db);
    }

    /**
     * addItems
     *
     * @param       $pid
     * @param       $qid
     * @param       $items
     * @param array $optPar
     * @param bool  $useCommonLibrary
     *
     * @return array
     * @throws Exception
     * @internal param mixed $param
     */
    public function addItems($pid, $qid, $items, $optPar = array(), $useCommonLibrary = false)
    {
        /* Assign image to the item */
        $imageizeItems = array();
        foreach ($items as $code => $item) {
            if (array_key_exists('option_imageid', $item)) {
                $imageizeItems[$code] = $item;
                unset($items[$code]['option_imageid']);
            }
        }
        /** @noinspection PhpUndefinedMethodInspection */
        $res = $this->efsApi->add_answer_options($pid, $qid, $items);
        /* Assign file-ID (file-manager) to the items */
        foreach ($imageizeItems as $item) {
            $imageInfo = pathinfo($item['option_imageid']);
            $path      = $imageInfo['dirname'];
            $name      = $imageInfo['basename'];
            $libraryPid = $pid;
            if ($useCommonLibrary === true) {
                $libraryPid = 0;
            }
            if (!$this->itemImageize($libraryPid, $qid, $item['option_sort_number'], $path, $name)) {
                throw new Exception("Cannot add image to item (path: $path name: $name library: $libraryPid)");
            }
        }
        if (array_key_exists('dynamic_break', $optPar)) {
            if ($optPar['dynamic_break'] > 0) {
                $this->questionSetDynamicBreak($qid, $optPar['dynamic_break']);
            }
        }

        return $this->hackGetAddedItems($qid);
    }

}