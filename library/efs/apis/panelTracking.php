<?php
  /**
   * efs_apis_panelTracking
   *
   * @see       modules/panel/components/tracking.inc.php
   *
   * @package   zendframeworkLibrary
   * @version   $Id: panelTracking.php,v 1.5 2013/10/16 09:27:40 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class efs_apis_panelTracking
  {
    const ACTION_INVITE            = 1;
    const ACTION_REMIND            = 2;
    const ACTION_SURVEY_START      = 3;
    const ACTION_SURVEY_COMPLETE   = 4;
    const ACTION_SURVEY_INCOMPLETE = 5;
    const ACTION_SCREEN_OUT        = 6;
    const ACTION_QUOTA_FULL        = 7;
    const ACTION_NOT_AVAILABLE     = 8;
    const ACTION_SAMPLE            = 9;
    const ACTION_QUALITY_ISSUE     = 10;

    const CURRENT_TABLE     = "panel_tracking_log_current";
    const PERMANENT_TABLE   = "panel_tracking_log_permanent";
    const TEMP_TABLE        = "panel_tracking_log_temp";
    const BLANK_TABLE       = "panel_tracking_log_new";
    const CALCULATION_TABLE = "panel_tracking_calculation";
  }