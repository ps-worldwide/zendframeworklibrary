<?php
    /** @noinspection PhpVoidFunctionResultUsedInspection */
    die(debug_print_backtrace()); /* NOFLAME */
    /**
     * efs_apis_panelapihack
     * EFS-API - methods that are not yet part of EFS itself
     *
     * @package   zendframeworkLibrary
     * @version   $Id: panelapihack.php,v 1.11 2013/11/13 14:55:47 funke Exp $
     * @copyright (c) QuestBack http://www.questback.com
     * @author    $Author: funke $
     */

    //class efs_apis_panelapihack
    //{
    //  private $db;
    //
    //  /**
    //   * __construct
    //   *
    //   * @param Zend_DB_Adapter_Abstract $db
    //   * @return \efs_apis_panelapihack
    //   */
    //  public function __construct(Zend_DB_Adapter_Abstract $db)
    //  {
    //    $this->db = $db;
    //  }
    //
    //  /**
    //   * getTableFields
    //   * gets fields and datatypes from table
    //   *
    //   * @param string $table
    //   * @return array fields
    //   */
    //  public function getTableFields($table)
    //  {
    //    if ($table == "")
    //      return array();
    //
    //    $result = $this->db->describeTable($table);
    //    if (!is_array($result) || count($result) == 0)
    //      return array();
    //
    //    $resultData = array();
    //    foreach ($result as $field => $fieldData)
    //      $resultData[$field] = $fieldData["DATA_TYPE"];
    //
    //    return $resultData;
    //  }
    //}
