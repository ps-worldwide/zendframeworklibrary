<?php
  /**
   * efs_apis_surveyTriggers
   *
   * @see       modules/survey/components/ospe/triggers.inc.php
   *
   * @package   zendframeworkLibrary
   * @version   $Id: surveyTriggers.php,v 1.2 2013/10/16 09:27:39 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class efs_apis_surveyTriggers
  {
    const TRIGGER_MAIL       = 1;
    const TRIGGER_SAMPLE     = 2;
    const TRIGGER_BONUS      = 3;
    const TRIGGER_PAGE       = 4;
    const TRIGGER_LOGOUT     = 5;
    const TRIGGER_RESET      = 6;
    const TRIGGER_VSPLIT     = 7;
    const TRIGGER_RECODE     = 10;
    const TRIGGER_RANDOM     = 11;
    const TRIGGER_LIST       = 12;
    const TRIGGER_PANELGROUP = 13;
    const TRIGGER_CUSTOM     = 14;

    const EXEC_PRE_FILTER  = 1;
    const EXEC_POST_FILTER = 2;
    const EXEC_DIRECTLY    = 3;
  }