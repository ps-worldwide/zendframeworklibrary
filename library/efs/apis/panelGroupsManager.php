<?php
  /**
   * efs_apis_panelGroupsManager
   *
   * @package   zendframeworkLibrary
   * @version   $Id: panelGroupsManager.php,v 1.6 2013/10/16 09:27:40 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class efs_apis_panelGroupsManager
  {
    const GROUPS_TABLE = "panelgrp";
    const CAT_TABLE    = "panel_group_categories";
    const LOG_TABLE    = "panel_groups_log";
  }