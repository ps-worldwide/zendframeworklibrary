<?php
  // This file should not be used as only real panel_exception can be catched
  // and not its children
  if (APPLICATION_ENV != 'testing')
    mail('actionscript@questback.de', __FILE__ . ' was called', implode("\n", debug_backtrace()));

  /**
   * efs_apis_panelexception
   *
   * @package   zendframeworkLibrary
   *
   * @version   $Id: panelexception.php,v 1.7 2013/10/16 09:27:40 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   * @todo      check if some project needs this file
   * @deprecated
   */

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_panelexception extends panel_exception
  {
  }
