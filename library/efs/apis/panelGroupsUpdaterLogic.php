<?php
  /**
   * efs_apis_panelGroupsUpdaterLogic
   *
   * @see       modules/panel/components/groups/updater/logic.inc.php
   *
   * @package   zendframeworkLibrary
   * @version   $Id: panelGroupsUpdaterLogic.php,v 1.5 2013/10/16 09:27:40 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class efs_apis_panelGroupsUpdaterLogic
  {
    const FILTER_ADD   = "add";
    const FILTER_SUB   = "sub";
    const FILTER_MERGE = "mrg";
  }