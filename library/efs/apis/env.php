<?php
  // This file should not be used but is tested in unittests
  // via efs_models_autoload::get_module_manager();
  // Usually the Autoloader should load a version specific file
  // which defines the same class (-name)
  if (APPLICATION_ENV != 'testing')
    mail('actionscript@questback.de', __FILE__ . ' was called', implode("\n", debug_backtrace()));

  /**
   * efs_apis_env
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package    efs_models_autoload_test::testModuleManager
   * @version    $Id: env.php,v 1.24 2016/07/27 08:07:04 faust Exp $
   * @copyright  (c) QuestBack http://www.questback.com
   * @author     $Author: faust $
   * @deprecated api uses certain version (eg. efs_apis_7_env)
   * @todo       : test if efs_models_autoload works without this one here in projects
   */

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_env
  {
    private static $instance;

    /**
     *
     */
    private function __construct()
    {
      // Avoid EFS notices
      $oldErrorReporting = error_reporting(E_ALL&~E_NOTICE);

      // Load the config file itself
      self::loadGpConf();

      // 7.0 needs some global functions for different API calls
      // Todo: Check if Pct_Efs_Autoloader is able to load those
      if (Pct_Efs_Version::getVersion() < 7.1)
      {
        Zend_Loader::loadFile(APPLICATION_EFS_PATH . "/www/include/miscfunc.inc.php3", null, true);
        Zend_Loader::loadFile(APPLICATION_EFS_PATH . 'wcp/framework/class_efs_assertion.inc.php', null, true);
        Zend_Loader::loadFile(APPLICATION_EFS_PATH . 'wcp/framework/env/class_efs_env.inc.php', null, true);
        Zend_Loader::loadFile(APPLICATION_EFS_PATH . 'wcp/framework/class_efs_config.inc.php', null, true);
        Zend_Loader::loadFile(APPLICATION_EFS_PATH . 'wcp/framework/env/class_efs_env_nonhttp.inc.php', null, true);
        Zend_Loader::loadFile(APPLICATION_EFS_PATH . 'wcp/framework/env/class_efs_env_api.inc.php', null, true);
        /** @noinspection PhpUndefinedClassInspection */
        self::$instance = new efs_env_api();
      }

      // Especially those that do direct html output
      if (file_exists(APPLICATION_EFS_PATH . "/wcp/include/htmlfunc3.inc.php3") === true) { // does not exist anymore since (20)16.2
        Zend_Loader::loadFile(APPLICATION_EFS_PATH . "/wcp/include/htmlfunc3.inc.php3", null, true);
      }
      // Set the error-reporting back
      error_reporting($oldErrorReporting);
    }

    /**
     *
     * @return efs_apis_env $instance
     */
    public static function getInstance()
    {
      if (self::$instance == NULL)
      {
        self::$instance = new self();
      }

      return self::$instance;
    }

    /**
     * In EFS 7.0 the module-manger reads out htdocs/module
     * and registers via filefunx found files in EFS'
     * autoloader dictionary.
     * Since 7.1 autoload did that (see Pct_Efs_Autoload).
     *
     * @return NULL
     */
    public function get_module_manager()
    {
      if (Pct_Efs_Version::getVersion() < 7.1)
      {
        $env = self::getInstance();

        /** @noinspection PhpUndefinedClassInspection */
        return efs_env::get_module_manager();
      }
      else
      {
        return null;
      }
    }

    /**
     * Loads the static EFS gp_conf class
     */
    public function loadGpConf()
    {
      // EFS needs loaded static class gp_conf
      $oldPwd         = $_SERVER['PWD'];
      $_SERVER['PWD'] = APPLICATION_EFS_PATH . 'www/';
      Zend_Loader::loadFile(APPLICATION_EFS_PATH . "/www/include/class_gp_conf.inc.php", null, true);
      /** @noinspection PhpUndefinedClassInspection */
      gp_conf::load_config();
      $_SERVER['PWD'] = $oldPwd;
      spl_autoload_unregister('efs_autoload::init');
    }

    /**
     * Returns the dbwrap instance from the EFS env
     *
     * @param             $version
     * @param Zend_Config $conf
     *
     * @throws Exception
     * @return dbwrap
     */
    public function getDb($version, Zend_Config $conf)
    {
      // Avoid EFS prints
      ob_start();

      if ($version == 7.0)
      {
        /** @noinspection PhpUndefinedClassInspection */
        $db = efs_env::get_db();
      }
      else
      {
        /** @noinspection PhpUndefinedClassInspection */
        $db = dbwrap::connect_static($conf->dbhost, $conf->dbuser, $conf->dbpasswd, $conf->dbname, $conf->dbport, $conf->dbsocket);
      }

      // Dump EFS prints
      $error = ob_get_contents();
      ob_end_clean();

      if ($db->opened == false)
      {
        throw new Exception('Cannot connect to EFS database: ' . $error);
      }

      return $db;
    }
  }
