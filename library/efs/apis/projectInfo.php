<?php
  /**
   * efs_apis_projectInfo
   *
   * @package   zendframeworkLibrary
   * @version   $Id: projectInfo.php,v 1.7 2013/10/16 09:27:40 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   * @todo      8.1 check if project_info still exists
   * @todo      remove EFS dependence (project_info)
   */

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_projectInfo extends project_info
  {
  }