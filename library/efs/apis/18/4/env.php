<?php
  /**
   * efs_apis_18_4_env  #EFSUPGRADE#
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.1 2019/01/23 12:30:48 khalafiniya Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: khalafiniya $
   */

  class efs_apis_18_4_env extends efs_apis_18_3_env #EFSUPGRADE#
  {
  }
