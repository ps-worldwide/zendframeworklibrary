<?php
  /**
   * efs_apis_locale
   * Extends efs_localehack (alias local0r in 7.0) in 8.0.
   *
   * @package   zendframeworkLibrary
   * @version   $Id: locale.php,v 1.1 2018/10/22 07:59:24 khalafiniya Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: khalafiniya $
   * @todo      write test
   */

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_locale extends efs_localehack
  {
  }