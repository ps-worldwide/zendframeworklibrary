<?php
  /**
   * efs_apis_18_3_env  #EFSUPGRADE#
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.1 2018/10/22 07:59:24 khalafiniya Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: khalafiniya $
   */

  class efs_apis_18_3_env extends efs_apis_18_2_env #EFSUPGRADE#
  {
  }
