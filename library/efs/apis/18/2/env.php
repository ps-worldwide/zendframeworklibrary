<?php
  /**
   * efs_apis_18_2_env  #EFSUPGRADE#
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.3 2018/10/22 09:07:38 khalafiniya Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: khalafiniya $
   */

  class efs_apis_18_2_env extends efs_apis_18_1_env #EFSUPGRADE#
  {
  }
