<?php
  /**
   * efs_apis_18_1_env  #EFSUPGRADE#
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.1 2018/05/04 10:50:25 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class efs_apis_18_1_env extends efs_apis_17_4_env #EFSUPGRADE#
  {
  }
