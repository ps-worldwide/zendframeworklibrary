<?php
  /**
   * efs_apis_17_2_env  
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.2 2017/10/24 14:24:27 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class efs_apis_17_2_env extends efs_apis_17_1_env 
  {
  }
