<?php
  /**
   * efs_apis_17_1_env  
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.2 2017/06/30 05:16:43 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class efs_apis_17_1_env extends efs_apis_16_4_env 
  {
  }
