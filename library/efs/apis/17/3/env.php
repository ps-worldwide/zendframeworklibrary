<?php
  /**
   * efs_apis_17_3_env  
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.2 2018/01/25 11:09:01 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class efs_apis_17_3_env extends efs_apis_17_2_env 
  {
  }
