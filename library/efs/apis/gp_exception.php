<?php

  if (APPLICATION_ENV != 'testing')
    mail('actionscript@questback.de', __FILE__ . ' was called', implode("\n", debug_backtrace()));

  /** @noinspection PhpUndefinedClassInspection */
  class efs_apis_gp_exception extends gp_exception
  {
  }