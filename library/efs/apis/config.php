<?php
  /**
   * efs_apis_config
   * Loads the configuration for EFS' config.inc.php3
   * and provides the config flags
   *
   * 2011-05-05: Do not extend gp_conf anymore to be more independent from EFS
   *
   * @package   zendframeworkLibrary
   * @version   $Id: config.php,v 1.23 2013/10/16 09:27:40 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class efs_apis_config //extends gp_conf
  {
    private static $config;

    /**
     * Load the EFS configuration config.inc.php
     *
     * @param bool|string $include_maintenance
     *
     * @throws Zend_Exception
     */
    public static function load_config($include_maintenance = true)
    {
      // Default
      $__base_all_path = '';

      // Avoid EFS notices
      $oldErrorLevel = error_reporting(E_ALL&~E_NOTICE);
      /** @noinspection PhpIncludeInspection */
      include APPLICATION_EFS_PATH . "/conf/config.inc.php3";

      // Restore the old error level
      error_reporting($oldErrorLevel);

      // um: mop: evil....catch that if someone calls scripts from within a different installation.
      if (realpath(
            APPLICATION_EFS_PATH . "www/include/class_gp_conf.inc.php") !=
          realpath(
            $__base_all_path . "www/include/class_gp_conf.inc.php")
      )
      {
        throw new Zend_Exception(
          'Trying to call a script from a different installation!');
      }

      self::$config = array();

      $test = "_";
      foreach (get_defined_vars() as $var => $value)
      {
        if ($var[0] == $test && $var[1] == $test)
          self::$config[substr($var, 2)] = $value;
      }

      if ($include_maintenance)
        self::$config['maintenance'] = self::checkEfsMaintenancePage();
    }

    /**
     * Returns the whole config settings
     */
    protected static function getConfig()
    {
      return self::$config;
    }

    /**
     * Check if maintenance page is active for EFS installation and IP
     */
    private static function checkEfsMaintenancePage()
    {
      // onDefault no access
      $__show_maintenance_page = true;
      // onDefault deny all IPs
      $__skip_maintenance_for_ip = '0.0.0.0-255.255.255.255';

      // Avoid EFS notices
      $maintenanceFile = self::$config['base_all_path'] . 'conf/maintenance_conf.inc.php';
      if (Zend_Loader::isReadable($maintenanceFile))
      {
        // Include the file
        /** @noinspection PhpIncludeInspection */
        require $maintenanceFile;
      }
      else
      {
        $__show_maintenance_page = false;
      }

      return (bool)$__show_maintenance_page;
    }

    /**
     * Returns the value for the given configuration flag
     *
     * @param string $configFlag
     * @param null   $default
     *
     * @return null
     */
    public function get($configFlag, $default = null)
    {
      self::printDeprecated(__METHOD__, '$conf=new efs_models_config(); $conf->get(FLAG, [DEFAULT]);');

      if (self::$config == null)
        self::load_config(true);

      if (isset(self::$config[$configFlag]))
        return self::$config[$configFlag];

      return $default;
    }

    /**
     * Returns the full path to the zip-binary using zip-path from EFS'
     * config.inc.php3
     */
    public static function get_zip_binary()
    {
      self::printDeprecated(__METHOD__, '$conf=new efs_models_config(); $conf->getZipBinary();');

      return self::get('zip_path') . 'zip';
    }

    /**
     * Returns the full path to the unzip-binary using the zip-path from EFS'
     * config.inc.php3
     */
    public static function get_unzip_binary()
    {
      self::printDeprecated(__METHOD__, '$conf=new efs_models_config(); $conf->getUnzipBinary();');

      return self::get('zip_path') . 'unzip';
    }

    /**
     * Prints a deprecated message to stout
     *
     * @param string $method
     * @param string $instead
     * @param null   $env
     */
    public static function printDeprecated($method, $instead, $env = null)
    {
      if ($env == null)
        $env = getenv('APPLICATION_ENV');

      if ($env == 'development')
        print PHP_EOL . $method . ' is deprecated. Instead use: ' . $instead;
    }
  }
