<?php
  /**
   * efs_apis_16_4_env  
   * Necessary environment variables needed by EFS to perform API calls
   *
   * @package   zendframeworkLibrary
   * @version   $Id: env.php,v 1.2 2017/04/04 14:04:50 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class efs_apis_16_4_env extends efs_apis_16_3_env 
  {
  }
