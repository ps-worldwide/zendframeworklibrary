<?php
  /**
   * Pct_Network_Client_Scp
   *
   * @package   zendframeworkLibrary
   *
   * @version   $Id: Scp.php,v 1.5 2013/10/16 09:28:21 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Network_Client_Scp
  {

    // command executed via exec
    private $command;

    // the user for the scp account
    private $user;

    // ths users' password for the scp account
    private $passwd;

    // the scp-host
    private $host;

    // options used for the scp command
    private $options;

    // the shell output returned from the exec call
    private $shellOutput;

    private $shellReturnValue;

    /**
     * Logger
     *
     * @var Zend_Log
     */
    private $log;

    /**
     * Constructor
     *
     * @param Zend_Log $log (optional) logger
     */
    public function __construct(Zend_Log $log = null)
    {
      if ($log instanceof Zend_Log)
      {
        $this->log = $log;
      }
      else
      {
        $this->log = new Zend_Log(
          new Zend_Log_Writer_Null());
      }
    }

    /**
     * Returns logger
     *
     * @return Zend_Log logger
     */
    private function getLog()
    {
      return $this->log;
    }

    /**
     *
     * @param string $cmd
     *
     * @return $this
     */
    public function setCommand($cmd)
    {
      $this->command = $cmd;
      return $this;
    }

    /**
     * @param $user
     *
     * @return $this
     */
    public function setUser($user)
    {
      $this->user = $user;
      return $this;
    }

    /**
     * @param $passwd
     *
     * @return $this
     */
    public function setPasswd($passwd)
    {
      $this->passwd = $passwd;
      return $this;
    }

    /**
     * @param $host
     *
     * @return $this
     */
    public function setHost($host)
    {
      $this->host = $host;
      return $this;
    }

    /**
     * @param $opts
     *
     * @return $this
     * @throws Exception
     */
    public function setScpOptions($opts)
    {
      if ($opts instanceof Zend_Config)
      {
        $this->options = $opts->toArray();
      }
      elseif (is_array($opts))
      {
        $this->options = $opts;
      }
      else
      {
        throw new Exception(
          'Scp options must be passed as array or Zend_Config object');
      }
      return $this;
    }

    public function getOutput()
    {
      return $this->shellOutput;
    }

    public function getReturnValue()
    {
      return $this->shellReturnValue;
    }

    /**
     * Copies all entries from given from directory to the given
     * to directory. If rec is set to true the directory is copied
     * recursive.
     *
     * @param $from
     * @param $to
     * @param $rec
     *
     * @return $this
     */
    public function cpDir($from, $to, $rec = false)
    {
      $cmd = $this->command;

      if ($rec == true)
      {
        $cmd .= ' -r';
      }

      $opt   = $this->getShellCmdOptions();
      $opt[] = $this->getShellCmdRemoteHost() . ':' . $from;
      $opt[] = $to;

      $this->execCmd($cmd, $opt);
      return $this;
    }

    /**
     * Uploads a file to the remote server
     *
     * @param string $localFile full path of local file
     * @param string $remoteDir remove directory
     *
     * @return $this
     * @throws RuntimeException if file does not exist or is not readable
     */
    public function uploadFile($localFile, $remoteDir)
    {
      if (!file_exists($localFile))
      {
        throw new RuntimeException(
          sprintf(
            'Upload failed. File "%s" does not exist.',
            $localFile));
      }

      if (!is_readable($localFile))
      {
        throw new RuntimeException(
          sprintf(
            'Upload failed. File "%s" is not readable.',
            $localFile));
      }

      if ('/' != substr($remoteDir, -1))
      {
        $remoteDir .= '/';
      }

      $remote = $this->getShellCmdRemoteHost() . ':' . $remoteDir;

      $options = array_merge($this->getShellCmdOptions(),
                             array($localFile, $remote));

      $this->execCmd($this->command, $options);

      return $this;
    }

    /**
     * @return array
     */
    private function getShellCmdOptions()
    {
      $opts = array();
      foreach ($this->options as $name => $value)
      {
        $opts[] = '-o ' . $name . '=' . $value;
      }
      return $opts;
    }

    /**
     * @return string
     */
    private function getShellCmdRemoteHost()
    {
      return $this->user . '@' . $this->host;
    }

    /**
     * @param       $cmd
     * @param array $opt
     *
     * @return $this
     */
    private function execCmd($cmd, array $opt)
    {
      $exe = escapeshellcmd($cmd);

      foreach ($opt as $value)
      {
        $exe .= ' ' . escapeshellarg($value);
      }

      $this->getLog()->debug('Executing shell command: ' . $exe);

      $oldHome = getenv('HOME');
      putenv('HOME=/tmp');
      exec($exe, $this->shellOutput, $this->shellReturnValue);
      putenv('HOME=' . $oldHome);

      return $this;
    }
  }