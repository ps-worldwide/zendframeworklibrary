<?php
  /**
   * Creates values used for Digest authentication.
   *
   * @author umschlag
   * @todo   Support more then md5 algorithm
   */

  class Pct_Auth_Http_Digest
  {

    /**
     * Parses the nonce value from the given header that is send by the webserver
     * when asked for digest authentication data (401).
     *
     * @param string $header
     *
     * @return string
     */
    public function getNonce($header)
    {
      $nonce = '';
      $count = preg_match('/nonce="([\w\d]+)"/i', $header, $matches);
      if ($count > 0)
        $nonce = $matches[1];

      return $nonce;
    }

    /**
     * Parses the opaque value from the given header that is send by the webserver
     * when asked for digest authentication data (401).
     *
     * @param string $header
     *
     * @return string
     */
    public function getOpaque($header)
    {
      $opaque = '';
      $count  = preg_match('/opaque="([\w\d]+)"/i', $header, $matches);
      if ($count > 0)
      {
        $opaque = $matches[1];
      }

      return $opaque;
    }

    /**
     * Parses the realm value from the given header that is send by the webserver
     * when asked for digest authentication data (401).
     *
     * @param string $header
     *
     * @return string
     */
    public function getRealm($header)
    {
      $realm = '';
      $count = preg_match('/realm="([\s\w\d]+)"/i', $header, $matches);
      if ($count > 0)
      {
        $realm = $matches[1];
      }

      return $realm;
    }

    /**
     * Parses the qop value from the given header that is send by the webserver
     * when asked for digest authentication data (401).
     *
     * @param string $header
     *
     * @return string
     */
    public function getQop($header)
    {
      $qop   = '';
      $count = preg_match('/qop="([\w\d]+)"/i', $header, $matches);
      if ($count > 0)
      {
        $qop = $matches[1];
      }

      return $qop;
    }

    /**
     * Parses the domain value from the given header that is send by the webserver
     * when asked for digest authentication data (401).
     *
     * @param string $header
     *
     * @return string
     */
    public function getDomain($header)
    {
      $domain = '';
      $count  = preg_match('/domain="([\/\w\d]+)"/i', $header, $matches);
      if ($count > 0)
      {
        $domain = $matches[1];
      }

      return $domain;
    }

    /**
     * Returns the support algorithms that he server send is its 401 response.
     *
     * @param string $header
     *
     * @return string
     */
    public function getAlgorithm($header)
    {
      $alg   = '';
      $count = preg_match('/algorithm="([\-\w\d]+)"/i', $header, $matches);
      if ($count > 0)
      {
        $alg = $matches[1];
      }

      return strtoupper($alg);
    }

    /**
     * Returns the md5 value from username, realm and password used for digest
     * authentication.
     *
     * @param string $username
     * @param string $realm
     * @param string $password
     *
     * @return string
     */
    public function getHash1($username, $realm, $password)
    {
      return md5(sprintf('%s:%s:%s', $username, $realm, $password));
    }

    /**
     * Returns the md5 value from http method and uri used for digest
     * authentication.
     *
     * @param string $method
     * @param string $uri
     *
     * @return string
     */
    public function getHash2($method, $uri)
    {
      return md5(sprintf('%s:%s', strtoupper($method), $uri));
    }

    /**
     * Get the number (nc) with prefixed 0s
     *
     * @param integer $no
     *
     * @return string
     */
    public function getNc($no)
    {
      return sprintf('%08d', $no);
    }

    /**
     * Creates a random client nonce
     *
     * @return string
     */
    public function getCnonce()
    {
      return md5(uniqid());
    }

    /**
     * Returns the response value (kind of checksum) used for digest
     * authentication
     *
     * @param $hash1
     * @param $nonce
     * @param $nc
     * @param $cnonce
     * @param $qop
     * @param $hash2
     *
     * @return string
     */
    public function getResponse($hash1, $nonce, $nc, $cnonce, $qop, $hash2)
    {
      return md5(
        sprintf('%s:%s:%s:%s:%s:%s', $hash1, $nonce, $nc, $cnonce, $qop, $hash2));
    }

    /**
     * When webserver requests digest authentication it sends some values in the
     * Www-Authenticate header and expects these to be present in a specific way
     * in the next, authorized response.
     * This response header is created in this function.
     *
     * @param $header401
     * @param $username
     * @param $password
     *
     * @throws Exception
     * @internal param $ string*          string
     * @internal param $ string*          string
     * @internal param $ string*          string
     * @return string
     * @todo     support more than md5 algorithms
     */
    public function getDigestHeaderFromServerResponseHeader($header401, $username,
                                                            $password)
    {
      // Supported algorithm by server
      $algorithm = self::getAlgorithm($header401);
      if ($algorithm != 'MD5')
      {
        throw new Exception('Algorithm not supported');
      }

      // Get needed parameter nonce
      $nonce = self::getNonce($header401);

      // Get needed parameter opaque
      $opaque = self::getOpaque($header401);

      // Get needed parameter realm
      $realm = self::getRealm($header401);

      // Get needed parameter qop
      $qop = self::getQop($header401);

      // Get needed parameter domain / uri
      $uri = self::getDomain($header401);

      // Create digest header value
      $hash1 = self::getHash1($username, $realm, $password);

      $hash2 = self::getHash2('get', $uri);

      $nc     = self::getNc(1);
      $cnonce = self::getCnonce();

      $response = self::getResponse($hash1, $nonce, $nc, $cnonce, $qop, $hash2);

      return sprintf(
        'Digest username="%s", realm="%s", algorithm="%s", uri="%s", response="%s", nc="%s", qop="%s", nonce="%s", opaque="%s", cnonce="%s"',
        $username, $realm, $algorithm, $uri, $response, $nc, $qop, $nonce,
        $opaque, $cnonce);
    }
  }
