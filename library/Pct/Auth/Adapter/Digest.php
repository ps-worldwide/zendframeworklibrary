<?php
  /**
   * Summarize some steps that are needed to to da digest authentication from
   * a Zend_Controller. Following can be added to preDispatch() method:
   *  $auth = new Pct_Auth_Adapter_Digest(Zend_Registry::get('conf'));
   *  $result = $auth->authenticate($this->getRequest(), $this->getResponse());
   *  if(!$result->isValid()){
   *    $this->getLog()->debug(implode(PHP_EOL,$result->getMessages()));
   *    $this->_forward('error', 'error', 'default');
   *  }
   * The 401 response code for failed authentication is set automatically in
   * authenticate() method.
   *
   * @author umschlag
   *
   */

  class Pct_Auth_Adapter_Digest
  {
    const CODE_FAILED_AUTH = 401;

    const CODE_SUCCESS_AUTH = 200;

    /**
     * The auth adapter
     *
     * @var Zend_Auth_Adapter_Http
     */
    protected $authAdapterHttp = null;

    /**
     * The resolver
     *
     * @var Zend_Auth_Adapter_Http_Resolver_Interface
     */
    protected $resolver = null;

    /**
     * The configuration used for the adapter
     *
     * @var array
     */
    protected $configAdapter = array('accept_schemes' => 'digest',
                                     'realm'          => 'Authentication realm', 'digest_domains' => '/',
                                     'nonce_timeout'  => 1800);

    /**
     * The filename holding the digest authentication data
     *
     * @var string
     */
    protected $resolveFile = 'digestPasswd.txt';

    /**
     * Constructs the digest auth
     *
     * @param Zend_Config $conf
     */
    public function __construct($conf = null)
    {
      // Config passed
      if (true == $conf instanceof Zend_Config)
      {
        $this->setConfig($conf->toArray());
      }
      $this->setAuthAdapter()->setResolver();
    }

    /**
     * Pass the config to the local config array.
     *
     * @param array $conf
     *
     * @return Pct_Auth_Adapter_Digest
     */
    public function setConfig(array $conf)
    {
      // Search for digest configuration
      if (false == array_key_exists('digest', $conf))
      {
        return $this;
      }

      $digest = $conf['digest'];
      foreach ($digest as $name => $value)
      {
        switch ($name)
        {
          //case 'accept_schemes':
          case 'realm':
          case 'digest_domains':
          case 'nonce_timeout':
            $this->configAdapter[$name] = $value;
            break;
          case 'filename':
            $this->resolveFile = $value;
            break;
        }
      }
      return $this;
    }

    /**
     * @return $this
     */
    public function setAuthAdapter()
    {
      $this->authAdapterHttp = new Zend_Auth_Adapter_Http($this->configAdapter);
      return $this;
    }

    /**
     * @return $this
     */
    public function setResolver()
    {
      if (is_readable($this->resolveFile))
      {
        $this->resolver = new Zend_Auth_Adapter_Http_Resolver_File(
          $this->resolveFile);
      }
      else
      {
        $this->resolver = new Zend_Auth_Adapter_Http_Resolver_File(
          sprintf('%s/%s', constant('APPLICATION_PATH') . '/configs/',
                  $this->resolveFile));
      }
      $this->authAdapterHttp->setDigestResolver($this->resolver);
      return $this;
    }

    /**
     * Authenticates the request.
     *
     * @param Zend_Controller_Request_Http  $request
     * @param Zend_Controller_Response_Http $response
     *
     * @return Zend_Auth_Result
     */
    public function authenticate(Zend_Controller_Request_Http $request,
                                 Zend_Controller_Response_Http $response)
    {
      $this->authAdapterHttp->setRequest($request)->setResponse($response);
      $result = $this->authAdapterHttp->authenticate();
      if (false == $result->isValid())
        $response->setHttpResponseCode(self::CODE_FAILED_AUTH);
      else
        $response->setHttpResponseCode(self::CODE_SUCCESS_AUTH);
      return $result;
    }
  }