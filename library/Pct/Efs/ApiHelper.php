<?php
/**
 * Pct_Efs_ApiHelper
 * Dummy class that will be generated automatically to provide Intellisense for zendframeworkLibrary
 *
 * @package zendframeworkLibrary
 * @version $Id: ApiHelper.php,v 1.73 2019/10/17 12:12:44 khalafiniya Exp $
 * @copyright (c) QuestBack http://www.questback.com
 * @author $Author: khalafiniya $
 */

class Pct_Efs_ApiHelper
{
  /**
     * surveyParticipantsImportParticipantAndSurveyResultFromCSV [EFS 17.2]
     *
     * Description (from WSDL):
     * Adds multiple participants including their survey results from a CSV file. Please note thet this operation works on surveys of type "personalized"  or "employee" only
     *
     * @param $sampleId
     * @param $csvData
     * @param $returnIdentifierType
     * @param $allowDuplicateEmails
     * @param $mappingColumn
     * @param $executeTrigger
     * @return mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyParticipantsImportParticipantAndSurveyResultFromCSV()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyParticipantsImportParticipantAndSurveyResultFromCSV', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_17_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyParticipantsImportParticipantAndSurveyResultFromCSV();
  }

  /**
     * Adds a new panelist [EFS 6] [EFS 7]
     *
     * @param $data
     * @param $identifier
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function addNewPanelist()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('addNewPanelist', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->addNewPanelist();
    $class = new Pct_Efs_7_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->addNewPanelist();
  }

  /**
     * Adds the given panelist to the given survey. [EFS 6]
     * $data[ident], $data[identValue] $data[reset], $data[syid]
     *
     * @param array $data
     *
     * @return \stdClass
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function addPanelistToSurvey()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('addPanelistToSurvey', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->addPanelistToSurvey();
  }

  /**
     * Returns the template with the given ID [EFS 6]
     *
     * @param $id
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getMailTemplate()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getMailTemplate', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getMailTemplate();
  }

  /**
     * getSurvey [EFS 6] [EFS 8]
     *
     * @param $pid
     *
     * @return mixed $surveyData
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSurvey()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSurvey', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getSurvey();
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getSurvey();
  }

  /**
     * getSurveyExport [EFS 6]
     *
     * @param mixed $arrParam
     *
     * @return $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSurveyExport()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSurveyExport', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getSurveyExport();
  }

  /**
     * getSurveyData [EFS 6]
     *
     * @param mixed $pid
     *
     * @return $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSurveyData()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSurveyData', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getSurveyData();
  }

  /**
     * getSurveyLoopData [EFS 6]
     *
     * @param mixed $pid
     *
     * @return $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSurveyLoopData()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSurveyLoopData', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getSurveyLoopData();
  }

  /**
     * getSurveyVariableLabel [EFS 6]
     *
     * @param mixed $pid
     *
     * @return $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSurveyVariableLabel()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSurveyVariableLabel', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getSurveyVariableLabel();
  }

  /**
     * getSurveyVariableLabel [EFS 6]
     *
     * @param mixed $pid
     * @param bool  $user
     *
     * @return bool $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSurveyVariables()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSurveyVariables', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getSurveyVariables();
  }

  /**
     * getServerTime [EFS 6]
     *
     * @param string $format
     *
     * @return $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getServerTime()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getServerTime', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getServerTime();
  }

  /**
     * getServerLoad [EFS 6]
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getServerLoad()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getServerLoad', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getServerLoad();
  }

  /**
     * getMasterdataStructure [EFS 6]
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getMasterdataStructure()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getMasterdataStructure', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getMasterdataStructure();
  }

  /**
     * importCsv [EFS 6]
     *
     * @param $syid
     * @param $data
     * @param $ident
     * @param $options
     *
     * @return boolean
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function importCsv()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('importCsv', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->importCsv();
  }

  /**
     * sendMailSurveyId [EFS 6]
     *
     * @param $syid
     * @param $template
     * @param $ident
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function sendMailSurveyId()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('sendMailSurveyId', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->sendMailSurveyId();
  }

  /**
     * Returns the results of an EFS survey as xml [EFS 6]
     *
     * DOCME describe possible arguments
     *
     * @param array $args       webservice arguments
     * @param bool  $compressed (optional) if true
     *
     * @throws Exception
     * @return string survey results
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSurveyResultsXml()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSurveyResultsXml', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getSurveyResultsXml();
  }

  /**
     * getPanelistByCondition [EFS 6]
     *
     * @param string $ident - identifier for the panelist
     * @param string $cond  - condition to find the panelist(s)
     *
     * @throws Exception
     * @return
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getPanelistByCondition()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getPanelistByCondition', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getPanelistByCondition();
  }

  /**
     * Returns a list of surveys [EFS 6]
     *
     * The result can be restricted to specific survey types (e.g. panel or
     * masterdata surveys) and survey statuses (e.g. active, inactive, archived)
     *
     * Only surveys that the user has read privileges on will be returned.
     *
     * @param array $surveyTypes    (optional) List of survey types
     * @param array $surveyStatuses (optional) List of survey statuses
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSurveysList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSurveysList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getSurveysList();
  }

  /**
     * Returns profiles of all survey participants [EFS 6]
     *
     * @param int   $survey_id
     * @param array $surveyFields
     * @param bool  $includeResults
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSurveyParticipants()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSurveyParticipants', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getSurveyParticipants();
  }

  /**
     * isValidPanelistLogin [EFS 6]
     *
     * @param array $ident
     *
     * @return boolean
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function isValidPanelistLogin()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('isValidPanelistLogin', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->isValidPanelistLogin();
  }

  /**
     * Updates the panelist via Ws instead of panel-api directly [EFS 6]
     *
     * @param $options - ident, value, record
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function updatePanelistWs()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('updatePanelistWs', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->updatePanelistWs();
  }

  /**
     * Returns a list of all panelists for a given group filter. [EFS 6] [EFS 8.1]
     *
     * The panelist identifier for the returned panelists can be user_id,
     * panelist code, pseudonym etc.
     *
     * @param string $identifierType Panelist identifier type
     * @param int    $filterId       Filter id
     *
     * @return array Panelists identifiers
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getPanelistsByFilter()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getPanelistsByFilter', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getPanelistsByFilter();
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getPanelistsByFilter();
  }

  /**
     * Returns the full profile for a panelist. [EFS 6] [EFS 8]
     *
     * participant data, behavioural information plus master data
     * The panelist can be identified by different criteria (email, user_id,
     * panelist code, pseudonym etc.).
     *
     * @param string $identifierType  Panelist identifier type
     * @param mixed  $identifierValue Panelist identifier value
     *
     * @throws InvalidArgumentException
     * @return array Panelist record
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getPanelist()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getPanelist', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getPanelist();
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getPanelist();
  }

  /**
     * createAdminSession [EFS 6]
     * Creates an admin session ID for the specified staff account (root usage only).
     *
     * @param $identifier_type
     * @param $identifier_value
     *
     * @return
     * @internal param string $identifierType Panelist identifier type
     * @internal param mixed $identifierValue Panelist identifier value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function createAdminSession()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('createAdminSession', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->createAdminSession();
  }

  /**
     * addAdmin [EFS 6]
     * Adds an admin area staff account.
     *
     * @param array  $adminRecord
     * @param string $returnType
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function addAdmin()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('addAdmin', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->addAdmin();
  }

  /**
     * addAdminGroup [EFS 6]
     * Adds a staff team.
     *
     * @param array $adminGroupRecord
     * @param int   $rightTemplateId
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function addAdminGroup()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('addAdminGroup', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->addAdminGroup();
  }

  /**
     * addAdminOrg [EFS 6]
     * Adds an organization.
     *
     * @param array $adminOrgRecord
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function addAdminOrg()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('addAdminOrg', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->addAdminOrg();
  }

  /**
     * addAdminToGroup [EFS 6]
     * Adds a staff member to a team.
     *
     * @param string  $adminUserIdentifierType
     * @param string  $adminIdentifierValue
     * @param integer $adminGroupIdentifierValue
     * @param string  $userRole
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function addAdminToGroup()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('addAdminToGroup', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->addAdminToGroup();
  }

  /**
     * addAdminToOrg [EFS 6]
     * Adds a staff member to an organization.
     *
     * @param string  $admin_user_identifier_type
     * @param string  $admin_identifier_value
     * @param integer $admin_org_identifier_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function addAdminToOrg()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('addAdminToOrg', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->addAdminToOrg();
  }

  /**
     * addExternalSurveyParticipants [EFS 6]
     * Adds or updates the participation status of multiple participants in an external study.
     *
     * @param string $external_survey_identifier_type
     * @param string $external_survey_identifier_value
     * @param string $panelist_identifier_type
     * @param array  $external_survey_participations
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function addExternalSurveyParticipants()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('addExternalSurveyParticipants', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->addExternalSurveyParticipants();
  }

  /**
     * alterUserGroupRole [EFS 6]
     * Alters a staff members role within a given team.
     *
     * @param integer $uid
     * @param integer $gid
     * @param string  $new_role
     * @param integer $new_owner_uid
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function alterUserGroupRole()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('alterUserGroupRole', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->alterUserGroupRole();
  }

  /**
     * createExternalSurvey [EFS 6]
     * Creates an external study in EFS.
     *
     * @param string  $external_survey_id
     * @param string  $external_survey_title
     * @param integer $external_survey_type
     * @param array   $external_survey_attributes
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function createExternalSurvey()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('createExternalSurvey', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->createExternalSurvey();
  }

  /**
     * createReportSession [EFS 6]
     * Creates a report session ID for the specified staff account (root usage only).
     *
     * @param string $admin_user_identifier_type
     * @param string $admin_user_identifier_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function createReportSession()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('createReportSession', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->createReportSession();
  }

  /**
     * createSurvey [EFS 6]
     * Creates a new survey.
     *
     * @param array $survey
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function createSurvey()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('createSurvey', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->createSurvey();
  }

  /**
     * deleteAdminOrg [EFS 6]
     * Deletes an organization.
     *
     * @param integer $admin_org_identifier_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function deleteAdminOrg()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('deleteAdminOrg', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->deleteAdminOrg();
  }

  /**
     * deleteExternalPanelistAuthentication [EFS 6]
     * Deletes an external authentication mapping from EFS. Please mind: Only the mapping will be deleted. The panelist will not be removed from EFS.
     *
     * @param integer $external_service_id
     * @param string  $external_authentication_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function deleteExternalPanelistAuthentication()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('deleteExternalPanelistAuthentication', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->deleteExternalPanelistAuthentication();
  }

  /**
     * deleteExternalSurvey [EFS 6]
     * Deletes an external study with all its participation data.
     *
     * @param string $external_survey_identifier_type
     * @param string $external_survey_identifier_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function deleteExternalSurvey()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('deleteExternalSurvey', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->deleteExternalSurvey();
  }

  /**
     * deletePanelist [EFS 6]
     * Deletes a panelist (i.e. sets the panelist to status deleted).
     *
     * @param string $panelist_identifier_type
     * @param string $panelist_identifier_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function deletePanelist()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('deletePanelist', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->deletePanelist();
  }

  /**
     * deletePanelistFromGroup [EFS 6]
     * Deletes a panelist from a panel group.
     *
     * @param string  $panelist_identifier_type
     * @param string  $panelist_identifier_value
     * @param integer $group_id
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function deletePanelistFromGroup()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('deletePanelistFromGroup', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->deletePanelistFromGroup();
  }

  /**
     * disableAdminUser [EFS 6]
     * Disables a staff member account.
     *
     * @param string $admin_user_identifier_type
     * @param string $admin_identifier_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function disableAdminUser()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('disableAdminUser', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->disableAdminUser();
  }

  /**
     * enableAdminUser [EFS 6]
     * Enables a staff member account account.
     *
     * @param $admin_user_identifier_type
     * @param $admin_identifier_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function enableAdminUser()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('enableAdminUser', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->enableAdminUser();
  }

  /**
     * generateSurvey [EFS 6]
     * Compiles the survey. Creates all missing variable names and resets the participants if requested.
     *
     * @param integer $survey_id
     * @param string  $generate_survey_type
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function generateSurvey()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('generateSurvey', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->generateSurvey();
  }

  /**
     * getAdminGroup [EFS 6]
     * Returns a staff team.
     *
     * @param integer $admin_group_identifier_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getAdminGroup()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getAdminGroup', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getAdminGroup();
  }

  /**
     * getAdminGroupList [EFS 6]
     * Returns a list of teams specified by a search value.
     *
     * @param string  $admin_group_field
     * @param string  $admin_group_value
     * @param integer $page
     * @param integer $limit
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getAdminGroupList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getAdminGroupList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getAdminGroupList();
  }

  /**
     * getAdminList [EFS 6]
     * Returns a list of admin area staff accounts specified by a search value.
     *
     * @param string  $admin_field
     * @param string  $admin_value
     * @param integer $page
     * @param integer $limit
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getAdminList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getAdminList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getAdminList();
  }

  /**
     * getAdminOrg [EFS 6]
     * Returns an organization.
     *
     * @param integer $admin_org_identifier_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getAdminOrg()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getAdminOrg', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getAdminOrg();
  }

  /**
     * getAdminOrgList [EFS 6]
     * Returns a list of organizations specified by a search value.
     *
     * @param string  $admin_org_field
     * @param string  $admin_org_value
     * @param integer $page
     * @param integer $limit
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getAdminOrgList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getAdminOrgList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getAdminOrgList();
  }

  /**
     * getCbContent [EFS 6]
     * Returns the codebook structure for the specified EFS survey.
     *
     * @param integer $survey_id
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getCbContent()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getCbContent', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getCbContent();
  }

  /**
     * getExternalPanelistAuthentication [EFS 6]
     * Returns the EFS identifier for a member of an external system. If the member has not been registered in EFS using the save_external_panelist_authentication web service before, this service will throw an exception.
     *
     * @param integer $external_service_id
     * @param string  $external_authentication_value
     * @param string  $panelist_identifier_return_type
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getExternalPanelistAuthentication()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getExternalPanelistAuthentication', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getExternalPanelistAuthentication();
  }

  /**
     * getExternalSurvey [EFS 6]
     * Retrieves the data for an external study.
     *
     * @param string $external_survey_identifier_type
     * @param string $external_survey_identifier_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getExternalSurvey()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getExternalSurvey', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getExternalSurvey();
  }

  /**
     * getExternalSurveys [EFS 6]
     * Retrieves a list of all external studies.
     *
     * @param string $external_survey_identifier_return_type
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getExternalSurveys()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getExternalSurveys', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getExternalSurveys();
  }

  /**
     * getFrpCountAllCodes [EFS 6]
     * Returns the absolute number of participations for all disposition codes.
     *
     * @param int   $survey_id
     * @param array $restrict
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getFrpCountAllCodes()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getFrpCountAllCodes', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getFrpCountAllCodes();
  }

  /**
     * getFrpCountByCode [EFS 6]
     * Returns the absolute number of participations by disposition code.
     *
     * @param integer $survey_id
     * @param integer $disposition_code
     * @param array   $restrict
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getFrpCountByCode()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getFrpCountByCode', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getFrpCountByCode();
  }

  /**
     * getFrpCountByCodeSplit [EFS 6]
     * Returns the absolute number of participations by disposition code split by variable.
     *
     * @param integer $survey_id
     * @param integer $disposition_code
     * @param string  $split_var
     * @param array   $restrict
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getFrpCountByCodeSplit()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getFrpCountByCodeSplit', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getFrpCountByCodeSplit();
  }

  /**
     * getFrpCountByMulticode [EFS 6]
     * Returns the absolute number of participations by disposition code (array).
     *
     * @param integer $survey_id
     * @param array   $disposition_codes
     * @param array   $restrict
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getFrpCountByMulticode()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getFrpCountByMulticode', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getFrpCountByMulticode();
  }

  /**
     * getFrpCountByMulticodeSplit [EFS 6]
     * Returns the absolute number of participations by disposition codes split by variable.
     *
     * @param integer $survey_id
     * @param integer $disposition_code
     * @param string  $split_var
     * @param array   $restrict
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getFrpCountByMulticodeSplit()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getFrpCountByMulticodeSplit', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getFrpCountByMulticodeSplit();
  }

  /**
     * getFrpGraph [EFS 6]
     * Returns the png graph which represents the participation.
     *
     * @param integer $survey_id
     * @param integer $lang
     * @param array   $restrict
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getFrpGraph()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getFrpGraph', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getFrpGraph();
  }

  /**
     * getFrpGross1 [EFS 6]
     * Returns the total sample (Gross 1) for the project.
     *
     * @param integer $survey_id
     * @param array   $restrict
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getFrpGross1()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getFrpGross1', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getFrpGross1();
  }

  /**
     * getFrpGross1Split [EFS 6]
     * Returns the total sample (Gross 1) split by variable.
     *
     * @param integer $survey_id
     * @param string  $split_var
     * @param array   $restrict
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getFrpGross1Split()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getFrpGross1Split', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getFrpGross1Split();
  }

  /**
     * getFrpGross2 [EFS 6]
     * Returns the adjusted total sample (Gross 2) for the project.
     *
     * @param integer $survey_id
     * @param array   $restrict
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getFrpGross2()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getFrpGross2', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getFrpGross2();
  }

  /**
     * getFrpGross2Split [EFS 6]
     * Returns the adjusted total sample (Gross 2) split by variable.
     *
     * @param integer $survey_id
     * @param string  $split_var
     * @param array   $restrict
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getFrpGross2Split()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getFrpGross2Split', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getFrpGross2Split();
  }

  /**
     * getFrpNet [EFS 6]
     * Returns the net participation for the project.
     *
     * @param integer $survey_id
     * @param array   $restrict
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getFrpNet()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getFrpNet', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getFrpNet();
  }

  /**
     * getFrpNetSplit [EFS 6]
     * Returns the net participation split by variable.
     *
     * @param integer $survey_id
     * @param string  $split_var
     * @param array   $restrict
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getFrpNetSplit()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getFrpNetSplit', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getFrpNetSplit();
  }

  /**
     * getMasterdataVariables [EFS 6]
     * Returns the names and labels of all master data variables for the EFS Panel installation. The master data variables are global for the whole installation and not specific for any panelist.
     *
     * @param array $record
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getMasterdataVariables()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getMasterdataVariables', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getMasterdataVariables();
  }

  /**
     * getModifiedExternalSurveys [EFS 6]
     * Retrieves all external studies that were modified since a given timestamp.
     *
     * @param string   $external_survey_identifier_return_type
     * @param datetime $timestamp
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getModifiedExternalSurveys()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getModifiedExternalSurveys', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getModifiedExternalSurveys();
  }

  /**
     * getModifiedPanelists [EFS 6]
     * Returns the identifiers of all panelists whose data (participant data plus master data) has changed since this given timestamp. The return identifier type (email, user_id, panelist code, pseudonym etc.) can be specified.
     *
     * @param string   $panelist_identifier_type
     * @param datetime $timestamp
     * @param integer  $exclude_modifier
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getModifiedPanelists()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getModifiedPanelists', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getModifiedPanelists();
  }

  /**
     * getOspeTimeLogResults [EFS 6]
     * Returns a performance data struct for a survey.
     *
     * @param integer $survey_id
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getOspeTimeLogResults()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getOspeTimeLogResults', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getOspeTimeLogResults();
  }

  /**
     * getPanelComposition [EFS 6]
     * Returns the current panel composition (number of panelists by panel status).
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getPanelComposition()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getPanelComposition', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getPanelComposition();
  }

  /**
     * getPanelistCompletedSurveys [EFS 6]
     * Returns the list of EFS Panel surveys (surveys of type panel survey or master data survey) a panelist has completed so far (complete survey history of the panelist). The panelist can be identified by different criteria (email, user_id, panelist code, pseudonym etc.).
     *
     * @param string $panelist_identifier_type
     * @param string $panelist_identifier_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getPanelistCompletedSurveys()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getPanelistCompletedSurveys', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getPanelistCompletedSurveys();
  }

  /**
     * getPanelistGroupMembership [EFS 6]
     * Returns group membership information for a panelist.
     *
     * @param string $panelist_identifier_type
     * @param string $panelist_identifier_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getPanelistGroupMembership()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getPanelistGroupMembership', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getPanelistGroupMembership();
  }

  /**
     * getPanelistPassword [EFS 6]
     * Returns the panel login password of a panelist.
     *
     * @param string $panelist_identifier_type
     * @param string $panelist_identifier_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getPanelistPassword()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getPanelistPassword', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getPanelistPassword();
  }

  /**
     * getPanelistPointsHistory [EFS 6]
     * Returns the account balance/bonus points positions for a panelist that happened in the last months. The panelist can be identified by different criteria (user_id, panelist code, pseudonym etc.).
     *
     * @param string  $panelist_identifier_type
     * @param string  $panelist_identifier_value
     * @param integer $months_range
     * @param boolean $skip_empty
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getPanelistPointsHistory()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getPanelistPointsHistory', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getPanelistPointsHistory();
  }

  /**
     * getPanelistSurveysList [EFS 6]
     * Returns the current list of active available EFS Panel surveys (surveys of type panel survey or master data survey) for a panelist. The panelist can be identified by different criteria (email, user_id, panelist code, pseudonym etc.).
     *
     * @param string $panelist_identifier_type
     * @param string $panelist_identifier_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getPanelistSurveysList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getPanelistSurveysList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getPanelistSurveysList();
  }

  /**
     * getPanelistsOnline [EFS 6]
     * Returns the panelists who are online on the website. Panelists are returned as an array of e-mail addresses, pseudonyms, panelist codes etc. together with the language ID of the website they are active on. A panelist might be active in multiple languages at the same time (e.g. by viewing the English version of the website and then changing to the French version). In this case, the panelist will be returned twice in the result array, but with different language IDs each. The interval for counting as being online can be specified in the global configuration of the website in the EFS admin area.
     *
     * @param string $panelist_identifier_return_type
     * @param array  $language_ids
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getPanelistsOnline()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getPanelistsOnline', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getPanelistsOnline();
  }

  /**
     * getPointsStatus [EFS 6]
     * Returns the account balance/bonus points positions for all panelists of the installation that happened since the last call to the service. This method can be used to do a regular, incremental replication of the panel's bonus points to another application (where EFS is used as the master system for managing the points). The panelist identifier for the returned balance positions can be user_id, panelist code, pseudonym etc.
     *
     * @param string  $panelist_identifier_type
     * @param boolean $fetch_all
     * @param boolean $log_transfer
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getPointsStatus()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getPointsStatus', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getPointsStatus();
  }

  /**
     * getPromotionalStatusForEmailAddress [EFS 6]
     * Returns the status of the e-mail address in the panel promotional system (tell-a-friend campaign system). This service can be used to check whether an e-mail address has already been entered into the promotional system and what its status is (invited only or already converted into a panelist).
     *
     * @param string $email
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getPromotionalStatusForEmailAddress()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getPromotionalStatusForEmailAddress', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getPromotionalStatusForEmailAddress();
  }

  /**
     * getRandomAztecGod [EFS 6]
     * Returns a random aztec god. Unleashes its power every sunday at 2 am when printed on blue paper. Please note that some gods may unleash evil powers!
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getRandomAztecGod()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getRandomAztecGod', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getRandomAztecGod();
  }

  /**
     * getRedemptionsStatus [EFS 6]
     * Returns the bonus points redemption positions for all panelists of the installation that happened since the last call to the service. This method can be used to do a regular, incremental replication of the panel's bonus points redemptions to another application (where EFS is used as the master system for managing the points). The panelist identifier for the returned redemptions can be user_id, panelist code, pseudonym etc.
     *
     * @param string  $panelist_identifier_type
     * @param boolean $fetch_all
     * @param boolean $log_transfer
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getRedemptionsStatus()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getRedemptionsStatus', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getRedemptionsStatus();
  }

  /**
     * getWsdlVersions [EFS 6]
     * Returns the available WSDL versions on the installation.
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getWsdlVersions()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getWsdlVersions', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getWsdlVersions();
  }

  /**
     * modifyPanelistPassword [EFS 6]
     * Updates the panel login password of a panelist.
     *
     * @param string $panelist_identifier_type
     * @param string $panelist_identifier_value
     * @param string $panelist_password
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function modifyPanelistPassword()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('modifyPanelistPassword', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->modifyPanelistPassword();
  }

  /**
     * modifyPanelistPoints [EFS 6]
     * Adds or subtracts bonus points for a panelist's account. The panelist can be identified by different criteria (user_id, panelist code, pseudonym etc.). The transaction will be shown in the panelists' account balance history afterwards.
     *
     * @param string $panelist_identifier_type
     * @param string $panelist_identifier_value
     * @param float  $points_value
     * @param string $points_reason
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function modifyPanelistPoints()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('modifyPanelistPoints', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->modifyPanelistPoints();
  }

  /**
     * resetOspeTimeLog [EFS 6]
     * Resets all performance data used when accessing the OSPE time log.
     *
     * @param integer $survey_id
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function resetOspeTimeLog()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('resetOspeTimeLog', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->resetOspeTimeLog();
  }

  /**
     * resetSurveyParticipation [EFS 6]
     * Resets a survey participation for one participant. Existing survey data can be kept or deleted.
     *
     * @param string  $identifier_type
     * @param string  $identifier_value
     * @param integer $survey_id
     * @param string  $reset_type
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function resetSurveyParticipation()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('resetSurveyParticipation', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->resetSurveyParticipation();
  }

  /**
     * saveExternalPanelistAuthentication [EFS 6]
     * Establishes a mapping between a member of an external system (specified by the external_service_id and the external_service_authentication_value) and an EFS panelist. This can be used to implement seamless logins for EFS panelists coming from external web applications.
     *
     * @param integer $external_service_id
     * @param string  $external_authentication_value
     * @param string  $panelist_identifier_type
     * @param string  $panelist_identifier_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function saveExternalPanelistAuthentication()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('saveExternalPanelistAuthentication', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->saveExternalPanelistAuthentication();
  }

  /**
     * sendMailSampleId [EFS 6]
     * Sends e-mails by a panel sample ID. Participant data and master data wildcards in the mail template will be dynamically replaced for each participant. Optionally you may restrict to specific participants by specifying participant identifiers. An empty array will mail everybody.
     *
     * @param integer $sample_id
     * @param array   $mail_template
     * @param array   $participant_identifiers
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function sendMailSampleId()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('sendMailSampleId', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->sendMailSampleId();
  }

  /**
     * sendMailSampleIdDispcodes [EFS 6]
     * Sends e-mails to participants by sample ID and disposition code. See send_mail_sample_id. Only participants with one of the specified disposition codes will be mailed.
     *
     * @param integer $sample_id
     * @param array   $mail_template
     * @param array   $mail_disposition_codes
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function sendMailSampleIdDispcodes()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('sendMailSampleIdDispcodes', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->sendMailSampleIdDispcodes();
  }

  /**
     * sendMailSampleIdIncompletes [EFS 6]
     * Convenience function to send a mail to all participants who didn't complete the survey yet (disposition codes 11,12,20,21,22,23). See send_mail_sample_id.
     *
     * @param integer $sample_id
     * @param array   $mail_template
     * @param array   $participant_identifiers
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function sendMailSampleIdIncompletes()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('sendMailSampleIdIncompletes', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->sendMailSampleIdIncompletes();
  }

  /**
     * sendMailSampleIdUnaccessed [EFS 6]
     * Convenience function to send a mail to all participants who didn't access the survey yet (disposition codes 11,12). See send_mail_sample_id.
     *
     * @param integer $sample_id
     * @param array   $mail_template
     * @param array   $participant_identifiers
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function sendMailSampleIdUnaccessed()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('sendMailSampleIdUnaccessed', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->sendMailSampleIdUnaccessed();
  }

  /**
     * sendMailSurveyIdDispcodes [EFS 6]
     * Sends e-mails to users by survey ID and disposition code. See send_mail_survey_id. Only participants with one of the specified disposition codes will be mailed.
     *
     * @param integer $survey_id
     * @param array   $mail_template
     * @param array   $mail_disposition_codes
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function sendMailSurveyIdDispcodes()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('sendMailSurveyIdDispcodes', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->sendMailSurveyIdDispcodes();
  }

  /**
     * sendMailSurveyIdIncompletes [EFS 6]
     * Convenience function to send a mail to all participants who didn't complete the survey yet (disposition codes 11,12,20,21,22,23). See send_mail_survey_id.
     *
     * @param integer $survey_id
     * @param array   $mail_template
     * @param array   $participant_identifiers
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function sendMailSurveyIdIncompletes()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('sendMailSurveyIdIncompletes', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->sendMailSurveyIdIncompletes();
  }

  /**
     * sendMailSurveyIdUnaccessed [EFS 6]
     *
     * @param integer $survey_id
     * @param array   $mail_template
     * @param array   $participant_identifiers
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function sendMailSurveyIdUnaccessed()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('sendMailSurveyIdUnaccessed', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->sendMailSurveyIdUnaccessed();
  }

  /**
     * sendMailToAdmin [EFS 6]
     * Sends an e-mail to an administrator.
     *
     * @param string $admin_identifier_type
     * @param string $admin_identifier_value
     * @param array  $mail_template
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function sendMailToAdmin()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('sendMailToAdmin', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->sendMailToAdmin();
  }

  /**
     * setAdminGroupAclTemplate [EFS 6]
     * Sets a teams rights to a predefined template.
     *
     * @param integer $admin_group_identifier_value
     * @param integer $acl_template_identifier_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function setAdminGroupAclTemplate()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('setAdminGroupAclTemplate', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->setAdminGroupAclTemplate();
  }

  /**
     * setAdminOrgValue [EFS 6]
     * Sets an organization's value.
     *
     * @param integer $admin_org_identifier_value
     * @param string  $admin_org_field
     * @param string  $admin_org_value
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function setAdminOrgValue()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('setAdminOrgValue', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->setAdminOrgValue();
  }

  /**
     * setUserGroupObjectRight [EFS 6]
     * Set read|write privileges for a system object.
     *
     * @param integer $gid
     * @param integer $oid
     * @param integer $right
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function setUserGroupObjectRight()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('setUserGroupObjectRight', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->setUserGroupObjectRight();
  }

  /**
     * updateExternalSurvey [EFS 6]
     * Updates an existing external study with new attributes. The survey's external ID cannot be changed.
     *
     * @param string  $external_survey_identifier_type
     * @param string  $external_survey_identifier_value
     * @param string  $external_survey_title
     * @param integer $external_survey_type
     * @param array   $external_survey_attributes
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function updateExternalSurvey()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('updateExternalSurvey', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->updateExternalSurvey();
  }

  /**
     * getEfsVersion [EFS 6]
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getEfsVersion()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getEfsVersion', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_6_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getEfsVersion();
  }

  /**
     * efsMailblacklistAdd [EFS 16.4]
     *
     * Description (from WSDL):
     * Add a list of email addresses to blacklist. If surveyId is specified the email will be blacklisted for given survey, otherwise global.
     *
     * @param $emailList
     * @return mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsMailblacklistAdd()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsMailblacklistAdd', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_16_4_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsMailblacklistAdd();
  }

  /**
     * efsMailblacklistDelete [EFS 16.4]
     *
     * Description (from WSDL):
     * Deletes a list of email addresses from blacklist
     *
     * @param $emailList
     * @return mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsMailblacklistDelete()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsMailblacklistDelete', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_16_4_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsMailblacklistDelete();
  }

  /**
     * efsMailblacklistGetList [EFS 16.4]
     *
     * Description (from WSDL):
     * Get a list of blacklisted email addresses (both global and survey related)
     *
     * @return mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsMailblacklistGetList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsMailblacklistGetList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_16_4_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsMailblacklistGetList();
  }

  /**
     * efsMailblacklistIsBlacklisted [EFS 16.4]
     *
     * Description (from WSDL):
     * Check whether email address is blacklisted for given survey. If surveyId is not specified checks whether email is in global blacklist.
     *
     * @param $emailAddress
     * @param $surveyId
     * @return mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsMailblacklistIsBlacklisted()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsMailblacklistIsBlacklisted', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_16_4_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsMailblacklistIsBlacklisted();
  }

  /**
     * @param int $adminId - Admin User ID [EFS 16.3]
     * @return surveyList - list of surveys
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function customSurveysurveysGetListByAdminId()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('customSurveysurveysGetListByAdminId', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_16_3_Api_Custom();
    /** @noinspection PhpParamsInspection */
    $class->customSurveysurveysGetListByAdminId();
  }

  /**
     * getPanelGroups [EFS 8.2]
     *
     * @param int $categoryId
     *
     * @return array $sampleData
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getPanelGroups()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getPanelGroups', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getPanelGroups();
  }

  /**
     * addPanelGroup [EFS 8.2]
     *
     * @param string $name
     * @param string $description
     * @param int    $categoryId
     *
     * @return array $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function addPanelGroup()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('addPanelGroup', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->addPanelGroup();
  }

  /**
     * getPanelistsByGroup [EFS 8.2]
     *
     * Description (from WSDL):
     * returns a list of panelists in a specific panel group
     *
     * @param int   $groupId
     * @param array $returnDataFields
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getPanelistsByGroup()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getPanelistsByGroup', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getPanelistsByGroup();
  }

  /**
     * deleteGratification [EFS 8.2]
     *
     * Description (from WSDL)
     * Delete Gratification, includes deletion of prizes and winners
     *
     * @param int $gratId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function deleteGratification()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('deleteGratification', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->deleteGratification();
  }

  /**
     * deletePanelGroup [EFS 8.2]
     *
     * Description (from WSDL)
     * delete a panel group along with any panelist-group membership records plus any update rules for group.
     * Panelist data of group members will not be deleted.
     *
     * @param int $groupId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function deletePanelGroup()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('deletePanelGroup', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->deletePanelGroup();
  }

  /**
     * addPanelistsToGroup [EFS 8.2]
     *
     * Description (from WSDL)
     * add panelists to an existing panel group
     *
     * @param int    $groupId
     * @param string $identifierType
     * @param array  $identifiers
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function addPanelistsToGroup()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('addPanelistsToGroup', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->addPanelistsToGroup();
  }

  /**
     * createGratification [EFS 8.2]
     *
     * Description (from WSDL)
     * Create lottery
     *
     * @param string $title
     * @param string $description
     * @param int    $status
     * @param int    $groupId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function createGratification()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('createGratification', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->createGratification();
  }

  /**
     * getGratification [EFS 8.2]
     *
     * Description (from WSDL)
     * Get Lottery by Lottery id * Provides lottery object to access gratification and other parameters (??)
     *
     * @param int $gratId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getGratification()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getGratification', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getGratification();
  }

  /**
     * emptyPanelGroup [EFS 8.2]
     *
     * @param int $groupId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function emptyPanelGroup()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('emptyPanelGroup', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->emptyPanelGroup();
  }

  /**
     * setGratificationStatus [EFS 8.2]
     *
     * Description (from WSDL)
     * Update status of lottery with given Lottery id
     *
     * @param int $gratId
     * @param int $status
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function setGratificationStatus()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('setGratificationStatus', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->setGratificationStatus();
  }

  /**
     * deleteGratificationWinners [EFS 8.2]
     *
     * Description (from WSDL)
     * Delete winner
     *
     * @param int $gratId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function deleteGratificationWinners()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('deleteGratificationWinners', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->deleteGratificationWinners();
  }

  /**
     * getGratificationWinners [EFS 8.2]
     *
     * Description (from WSDL)
     * Get winners by gratification id
     *
     * @param int $gratId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getGratificationWinners()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getGratificationWinners', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getGratificationWinners();
  }

  /**
     * resetBonusPoints [EFS 8.2]
     *
     * Description (from WSDL)
     * resets the bonus points of one or multiple panelists to zero.
     * this action will also add a note to each panelists' bonus points balance so panelists get informed about the nullification
     * if a psnelist already has a balance of zero points, no additional note will be added to his/her points log
     *
     * @param string $identifierType
     * @param array  $identifierList
     * @param string $description
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function resetBonusPoints()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('resetBonusPoints', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->resetBonusPoints();
  }

  /**
     * panelistIncrementValue [EFS 8.2]
     *
     * Description (from WSDL)
     * increment/decrement a master data variable's value for one or multiple panelists
     *
     * @param string $identifierType
     * @param array  $identifierList
     * @param string $variableName
     * @param float  $step
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelistIncrementValue()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelistIncrementValue', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelistIncrementValue();
  }

  /**
     * getRawDataCSVCompleted [EFS 8.2]
     *
     * Description (from WSDL)
     * return the survey results raw data in CSV format. The export is restricted to completed interviews and to a specifiable date range
     *
     * @param $surveyId
     * @param $exportTypes
     * @param $dateRange
     * @param survey:variableList $includeVariables - optional list of variables to include
     *
     * @return array|mixed $result - raw survey results data in CSV format
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSurveyRawDataCsvCompleted()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSurveyRawDataCsvCompleted', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getSurveyRawDataCsvCompleted();
  }

  /**
     * getSurveyRawDataCsvAll [EFS 8.2]
     *
     * Description (from WSDL)
     * return the survey results raw data in CSV format. The export is restricted to a specifiable date range
     *
     * @param      $surveyId         - id of survey
     * @param      $exportTypes      - types of values (scope) to export
     * @param      $dateRange        - date range the export is restricted to
     * @param null $includeVariables - optional list of variables to include
     * @param null $sort             - optional list of columns to sort by
     *
     * @return array|mixed $result - raw survey results data in CSV format
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSurveyRawDataCsvAll()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSurveyRawDataCsvAll', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getSurveyRawDataCsvAll();
  }

  /**
     * getEfsBuildName [EFS 8.2]
     *
     * Description (from WSDL)
     * get buildname of the EFS installation
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getEfsBuildName()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getEfsBuildName', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getEfsBuildName();
  }

  /**
     * addMasterdataVariable [EFS 8.2]
     *
     * @param string  $name
     * @param string  $label
     * @param string  $type
     * @param int     $categoryId
     * @param boolean $optional
     *
     * @return array|mixed $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function addMasterdataVariable()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('addMasterdataVariable', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->addMasterdataVariable();
  }

  /**
     * addSampleToSurvey [EFS 8.2]
     *
     * @param int    $pid
     * @param string $title
     * @param string $description
     *
     * @return array|mixed $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function addSampleToSurvey()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('addSampleToSurvey', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->addSampleToSurvey();
  }

  /**
     * grantBonusPoints [EFS 8.2]
     *
     * Description (from WSDL)
     * Grants Bonuspoints to a list of panelists
     *
     * @param string $identifierType
     * @param array  $identifierList
     * @param int    $surveyId
     * @param int    $points
     * @param string $description
     *
     * @return array|mixed $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function grantBonusPointsForSurvey()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('grantBonusPointsForSurvey', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->grantBonusPointsForSurvey();
  }

  /**
     * getResponseCategoryByVarname [EFS 8.2]
     *
     * Description (from WSDL)
     * get response-categories by variable name and survey id
     *
     * @param int    $surveyId
     * @param string $variableName
     *
     * @return array|mixed $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getResponseCategoryByVarname()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getResponseCategoryByVarname', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getResponseCategoryByVarname();
  }

  /**
     * panelExternalsurveysDelete [EFS 8.2]
     *
     * Description (from WSDL):
     * Deletes an external study with all its participation data.
     *
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelExternalsurveysDelete()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelExternalsurveysDelete', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelExternalsurveysDelete();
  }

  /**
     * panelExternalsurveysGet [EFS 8.2]
     *
     * Description (from WSDL):
     * Retrieves the data of the external survey
     *
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelExternalsurveysGet()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelExternalsurveysGet', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelExternalsurveysGet();
  }

  /**
     * panelExternalsurveysGetList [EFS 8.2]
     *
     * Description (from WSDL):
     * Retrieves a list of all external surveys.
     *
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelExternalsurveysGetList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelExternalsurveysGetList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelExternalsurveysGetList();
  }

  /**
     * panelExternalsurveysGetListModified [EFS 8.2]
     *
     * Description (from WSDL):
     * Retrieves a list of external surveys that were modified since a given timestamp
     *
     * @param $timestamp
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelExternalsurveysGetListModified()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelExternalsurveysGetListModified', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelExternalsurveysGetListModified();
  }

  /**
     * panelGroupsEmptyGroup [EFS 8.2]
     *
     * Description (from WSDL):
     * empty a panel group by deleting all members from it
     *
     * @param $id
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelGroupsEmptyGroup()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelGroupsEmptyGroup', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelGroupsEmptyGroup();
  }

  /**
     * panelGroupsGetGroupMembers [EFS 8.2]
     *
     * Description (from WSDL):
     * Supplies a list of panelists which are members in a given group
     *
     * @param $returnIdentifierType
     * @param $id
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelGroupsGetGroupMembers()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelGroupsGetGroupMembers', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelGroupsGetGroupMembers();
  }

  /**
     * panelGroupsRemovePanelists [EFS 8.2]
     *
     * Description (from WSDL):
     * Removes panelist from group
     *
     * @param $id
     * @param $identifierType
     * @param $identifierValues
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelGroupsRemovePanelists()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelGroupsRemovePanelists', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelGroupsRemovePanelists();
  }

  /**
     * panelPanelistsAddV1 [EFS 8.2]
     *
     * Description (from WSDL):
     * Adds a panelist to the panel.
     *
     * @param $returnIdentifierType
     * @param $panelistData
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsAddV1()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsAddV1', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsAddV1();
  }

  /**
     * panelPanelistsChangeV1 [EFS 8.2]
     *
     * Description (from WSDL):
     * Change an existing panelist's data
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $panelistData
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsChangeV1()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsChangeV1', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsChangeV1();
  }

  /**
     * panelPanelistsDelete [EFS 8.2]
     *
     * Description (from WSDL):
     * Delete panelist
     *
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsDelete()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsDelete', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsDelete();
  }

  /**
     * panelPanelistsDeleteExternalAuthentication [EFS 8.2]
     *
     * Description (from WSDL):
     * Deletes an external authentication mapping from EFS. Please mind: Only the mapping will be deleted. The panelist will not be removed from EFS.
     *
     * @param $externalServiceId
     * @param $externalAuthenticationValue
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsDeleteExternalAuthentication()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsDeleteExternalAuthentication', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsDeleteExternalAuthentication();
  }

  /**
     * panelPanelistsGetBonusPoints [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns the bonus points of a given panelist.
     *
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsGetBonusPoints()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsGetBonusPoints', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsGetBonusPoints();
  }

  /**
     * panelPanelistsGetExternalAuthentication [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns the EFS identifier for a member of an external system. If the member has not been registered in EFS using the save_external_panelist_authentication web service before, this service will throw an exception.
     *
     * @param $externalServiceId
     * @param $externalAuthenticationValue
     * @param $identifierReturnType
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsGetExternalAuthentication()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsGetExternalAuthentication', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsGetExternalAuthentication();
  }

  /**
     * panelPanelistsGetGroupMembership [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns a list of panel groups in which the panelist is a member
     *
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsGetGroupMembership()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsGetGroupMembership', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsGetGroupMembership();
  }

  /**
     * panelPanelistsGetListOfModified [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns the identifiers of all panelists whose data (participant data plus master data) has changed since this given timestamp.
     *
     * @param $identifierType
     * @param $modificationDate
     * @param $excludeModifier
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsGetListOfModified()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsGetListOfModified', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsGetListOfModified();
  }

  /**
     * panelPanelistsGetOnline [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns the panelists who are online on the website. Panelists are returned as an array of e-mail addresses, pseudonyms, panelist codes etc. together with the language ID of the website they are active on. A panelist might be active in multiple languages at the same time (e.g. by viewing the English version of the website and then changing to the French version). In this case, the panelist will be returned twice in the result array, but with different language IDs each. The interval for counting as being online can be specified in the global configuration of the website in the EFS admin area.
     *
     * @param $identifierReturnType
     * @param $languageIds
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsGetOnline()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsGetOnline', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsGetOnline();
  }

  /**
     * panelPanelistsGetPointsHistory [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns the account balance/bonus points positions for a panelist that happened in the last months. The panelist can be identified by different criteria (user_id, panelist code, pseudonym etc.).
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $monthsRange
     * @param $skipEmpty
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsGetPointsHistory()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsGetPointsHistory', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsGetPointsHistory();
  }

  /**
     * panelPanelistsGetPointsStatus [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns the account balance/bonus points positions for all panelists of the installation that happened since the last call to the service. This method can be used to do a regular, incremental replication of the panel's bonus points to another application (where EFS is used as the master system for managing the points). The panelist identifier for the returned balance positions can be user_id, panelist code, pseudonym etc.
     *
     * @param $returnIdentifierType
     * @param $fetchAll
     * @param $logTransfer
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsGetPointsStatus()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsGetPointsStatus', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsGetPointsStatus();
  }

  /**
     * panelPanelistsGetRedemptionStatus [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns the bonus points redemption positions for all panelists of the installation that happened since the last call to the service. This method can be used to do a regular, incremental replication of the panel's bonus points redemption to another application (where EFS is used as the master system for managing the points). The panelist identifier for the returned redemption can be user_id, panelist code, pseudonym etc.
     *
     * @param $identifierType
     * @param $fetchAll
     * @param $logTransfer
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsGetRedemptionStatus()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsGetRedemptionStatus', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsGetRedemptionStatus();
  }

  /**
     * panelPanelistsGetStatus [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns the current panel status of a panelist and accepts single uid only if given.
     *
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsGetStatus()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsGetStatus', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsGetStatus();
  }

  /**
     * panelPanelistsGetV1 [EFS 8.2]
     *
     * Description (from WSDL):
     * get information about a panelist
     *
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsGetV1()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsGetV1', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsGetV1();
  }

  /**
     * panelPanelistsGetValue [EFS 8.2]
     *
     * Description (from WSDL):
     * Get a master data variable's value for one or multiple panelists
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $variableName
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsGetValue()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsGetValue', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsGetValue();
  }

  /**
     * panelPanelistsIsValidLogin [EFS 8.2]
     *
     * Description (from WSDL):
     * Checks whether a panelist can login to the panel website with the login data specified.
     *
     * @param $loginType
     * @param $account
     * @param $password
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsIsValidLogin()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsIsValidLogin', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsIsValidLogin();
  }

  /**
     * panelPanelistsModifyPoints [EFS 8.2]
     *
     * Description (from WSDL):
     * Adds or subtracts bonus points for a panelist's account. The panelist can be identified by different criteria (user_id, panelist code, pseudonym etc.). The transaction will be shown in the panelists' account balance history afterwards.
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $pointsValue
     * @param $pointsReason
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsModifyPoints()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsModifyPoints', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsModifyPoints();
  }

  /**
     * panelPanelistsSaveExternalAuthentication [EFS 8.2]
     *
     * Description (from WSDL):
     * Establishes a mapping between a member of an external system (specified by the external_service_id and the external_service_authentication_value) and an EFS panelist. This can be used to implement seamless logins for EFS panelists coming from external web applications.
     *
     * @param $externalServiceId
     * @param $externalAuthenticationValue
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsSaveExternalAuthentication()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsSaveExternalAuthentication', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsSaveExternalAuthentication();
  }

  /**
     * panelPanelistsSendMail [EFS 8.2]
     *
     * Description (from WSDL):
     * Sends mails to every panelist in the sample
     *
     * @param $identifierType
     * @param $identifierValues
     * @param $mailTemplate
     * @param $dispcodes
     * @param $sendOptions
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsSendMail()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsSendMail', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsSendMail();
  }

  /**
     * panelPanelistsUpdateStatus [EFS 8.2]
     *
     * Description (from WSDL):
     * Updates the panel status of a panelist.
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $status
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsUpdateStatus()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsUpdateStatus', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsUpdateStatus();
  }

  /**
     * panelPromotionsGetStatus [EFS 8.2]
     *
     * Description (from WSDL):
     * This service can be used to check whether an e-mail address has already been entered into the promotional system and what its status is. Invited only or already converted into a panelist.
     *
     * @param $recipientEmail
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPromotionsGetStatus()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPromotionsGetStatus', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPromotionsGetStatus();
  }

  /**
     * panelPromotionsTellafriend [EFS 8.2]
     *
     * Description (from WSDL):
     * Sends an invitation as e-mail to a friend.
     *
     * @param $promotionId
     * @param $recipientEmail
     * @param $header
     * @param $footer
     * @param $panelistUid
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPromotionsTellafriend()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPromotionsTellafriend', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPromotionsTellafriend();
  }

  /**
     * panelSamplesSendMail [EFS 8.2]
     *
     * Description (from WSDL):
     * Sends mails to every panelist in the sample
     *
     * @param $sampleId
     * @param $mailTemplate
     * @param $dispcodes
     * @param $sendOptions
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelSamplesSendMail()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelSamplesSendMail', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelSamplesSendMail();
  }

  /**
     * panelStatisticsGetStatus [EFS 8.2]
     *
     * Description (from WSDL):
     * Panel status distribution
     *
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelStatisticsGetStatus()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelStatisticsGetStatus', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelStatisticsGetStatus();
  }

  /**
     * panelStatisticsGetVariables [EFS 8.2]
     *
     * Description (from WSDL):
     * Dynamic panel status distribution
     *
     * @param $panelStatisticsFilter
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelStatisticsGetVariables()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelStatisticsGetVariables', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelStatisticsGetVariables();
  }

  /**
     * panelSurveysGetListCompleted [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns the list of EFS Panel surveys (surveys of type panel survey or master data survey) a panelist has completed so far
     *
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelSurveysGetListCompleted()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelSurveysGetListCompleted', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelSurveysGetListCompleted();
  }

  /**
     * surveyLanguagesAdd [EFS 8.2]
     *
     * Description (from WSDL):
     * adds a new language
     *
     * @param $surveyId
     * @param $language
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyLanguagesAdd()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyLanguagesAdd', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyLanguagesAdd();
  }

  /**
     * surveyLanguagesChange [EFS 8.2]
     *
     * Description (from WSDL):
     * change language data
     *
     * @param $surveyId
     * @param $language
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyLanguagesChange()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyLanguagesChange', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyLanguagesChange();
  }

  /**
     * surveyLanguagesDelete [EFS 8.2]
     *
     * Description (from WSDL):
     * deletes a language
     *
     * @param $surveyId
     * @param $languageId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyLanguagesDelete()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyLanguagesDelete', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyLanguagesDelete();
  }

  /**
     * surveyLanguagesGet [EFS 8.2]
     *
     * Description (from WSDL):
     * get a language
     *
     * @param $surveyId
     * @param $languageId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyLanguagesGet()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyLanguagesGet', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyLanguagesGet();
  }

  /**
     * surveyLanguagesGetList [EFS 8.2]
     *
     * Description (from WSDL):
     * get a list of all languages in a survey
     *
     * @param $surveyId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyLanguagesGetList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyLanguagesGetList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyLanguagesGetList();
  }

  /**
     * surveyParticipantsReset [EFS 8.2]
     *
     * Description (from WSDL):
     * Resets a survey participation for participant. Existing survey data can be kept or deleted. The operation is allowed for personalized surveys only.
     *
     * @param $participantIdentifierType
     * @param $participantIdentifierValue
     * @param $surveyId
     * @param $resetType
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyParticipantsReset()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyParticipantsReset', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyParticipantsReset();
  }

  /**
     * surveyParticipantsSendMailV1 [EFS 8.2]
     *
     * Description (from WSDL):
     * Sends mails to every participiant in a survey. Will fail on anonymous surveys.
     *
     * @param $surveyId
     * @param $mailTemplate
     * @param $dispcodes
     * @param $sendOptions
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyParticipantsSendMailV1()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyParticipantsSendMailV1', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyParticipantsSendMailV1();
  }

  /**
     * surveyResultsGetSimpleStatisticsByPanelist [EFS 8.2]
     *
     * Description (from WSDL):
     * getSimpleStatisticsByPanelist
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $surveyId
     * @param $includeVariables
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyResultsGetSimpleStatisticsByPanelist()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyResultsGetSimpleStatisticsByPanelist', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyResultsGetSimpleStatisticsByPanelist();
  }

  /**
     * surveyStatisticsGetAllCodesCount [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns the absolute number of participation for all disposition codes.
     *
     * @param $surveyId
     * @param $restriction
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyStatisticsGetAllCodesCount()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyStatisticsGetAllCodesCount', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyStatisticsGetAllCodesCount();
  }

  /**
     * surveyStatisticsGetCountBy [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns the absolute number of participation by disposition code/s and/or split-variable.
     *
     * @param $surveyId
     * @param $criteria
     * @param $restriction
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyStatisticsGetCountBy()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyStatisticsGetCountBy', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyStatisticsGetCountBy();
  }

  /**
     * surveyStatisticsGetGraph [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns the png graph which represents the participation.
     *
     * @param $surveyId
     * @param $lang
     * @param $restriction
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyStatisticsGetGraph()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyStatisticsGetGraph', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyStatisticsGetGraph();
  }

  /**
     * surveyStatisticsGetGross1 [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns the total sample (Gross 1) for the project.
     *
     * @param $surveyId
     * @param $restriction
     * @param $splitVariable
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyStatisticsGetGross1()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyStatisticsGetGross1', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyStatisticsGetGross1();
  }

  /**
     * surveyStatisticsGetGross2 [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns the adjusted total sample (Gross 2) for the project.
     *
     * @param $surveyId
     * @param $restriction
     * @param $splitVariable
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyStatisticsGetGross2()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyStatisticsGetGross2', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyStatisticsGetGross2();
  }

  /**
     * surveyStatisticsGetListOfCountsBy [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns a list of the absolute number/s of participation by disposition code/s and/or split-variable.
     *
     * @param $surveyId
     * @param $criteria
     * @param $restriction
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyStatisticsGetListOfCountsBy()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyStatisticsGetListOfCountsBy', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyStatisticsGetListOfCountsBy();
  }

  /**
     * surveyStatisticsGetNet [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns the net-participation for the project.
     *
     * @param $surveyId
     * @param $restriction
     * @param $splitVariable
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyStatisticsGetNet()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyStatisticsGetNet', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyStatisticsGetNet();
  }

  /**
     * surveySurveysAdd [EFS 8.2]
     *
     * Description (from WSDL):
     * creates a new survey
     *
     * @param $surveyData
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveySurveysAdd()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveySurveysAdd', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveySurveysAdd();
  }

  /**
     * surveySurveysCompile [EFS 8.2]
     *
     * Description (from WSDL):
     * Compiles the survey. Creates all missing variable names and resets the participants if requested. You need the write ACL right to perform this action.
     *
     * @param $surveyId
     * @param $surveyCompilationType
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveySurveysCompile()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveySurveysCompile', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveySurveysCompile();
  }

  /**
     * surveySurveysGetExport [EFS 8.2]
     *
     * Description (from WSDL):
     * Exports survey results according to the given configuration
     *
     * @param $exportConfig
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveySurveysGetExport()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveySurveysGetExport', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveySurveysGetExport();
  }

  /**
     * surveyTimelogGetList [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns a performance data structure for a survey.
     *
     * @param $surveyId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyTimelogGetList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyTimelogGetList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyTimelogGetList();
  }

  /**
     * surveyTimelogReset [EFS 8.2]
     *
     * Description (from WSDL):
     * Resets all performance data used when accessing the OSPE time log.
     *
     * @param $surveyId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyTimelogReset()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyTimelogReset', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyTimelogReset();
  }

  /**
     * efsMailtemplatesAdd [EFS 8.2]
     *
     * Description (from WSDL):
     * Add mail template
     *
     * @param $mailtemplate
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsMailtemplatesAdd()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsMailtemplatesAdd', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsMailtemplatesAdd();
  }

  /**
     * efsMailtemplatesChange [EFS 8.2]
     *
     * Description (from WSDL):
     * Change mail template data
     *
     * @param $mailtemplate
     * @param $id
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsMailtemplatesChange()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsMailtemplatesChange', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsMailtemplatesChange();
  }

  /**
     * efsMailtemplatesDelete [EFS 8.2]
     *
     * Description (from WSDL):
     * Delete mail template by id
     *
     * @param $id
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsMailtemplatesDelete()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsMailtemplatesDelete', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsMailtemplatesDelete();
  }

  /**
     * efsMailtemplatesGet [EFS 8.2]
     *
     * Description (from WSDL):
     * Get mail template by id
     *
     * @param $id
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsMailtemplatesGet()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsMailtemplatesGet', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsMailtemplatesGet();
  }

  /**
     * efsMailtemplatesGetList [EFS 8.2]
     *
     * Description (from WSDL):
     * Get a list of mailtemplates
     *
     * @param $mailTemplateTypes
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsMailtemplatesGetList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsMailtemplatesGetList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsMailtemplatesGetList();
  }

  /**
     * efsOrganizationsAdd [EFS 8.2]
     *
     * Description (from WSDL):
     * Adds an organization.
     *
     * @param $adminOrgRecord
     * @param $adminOrgIdentifierReturnType
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsOrganizationsAdd()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsOrganizationsAdd', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsOrganizationsAdd();
  }

  /**
     * efsOrganizationsAddUser [EFS 8.2]
     *
     * Description (from WSDL):
     * Can add and update an existing staff member to an organization.
     *
     * @param $adminUserIdentifierType
     * @param $adminUserIdentifierValue
     * @param $adminOrgIdentifierType
     * @param $adminOrgIdentifierValue
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsOrganizationsAddUser()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsOrganizationsAddUser', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsOrganizationsAddUser();
  }

  /**
     * efsOrganizationsChange [EFS 8.2]
     *
     * Description (from WSDL):
     * Sets an organization`s value.
     *
     * @param $adminOrgIdentifierType
     * @param $adminOrgIdentifierValue
     * @param $adminOrgField
     * @param $adminOrgValue
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsOrganizationsChange()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsOrganizationsChange', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsOrganizationsChange();
  }

  /**
     * efsOrganizationsDelete [EFS 8.2]
     *
     * Description (from WSDL):
     * Deletes an organization.
     *
     * @param $adminOrgIdentifierType
     * @param $adminOrgIdentifierValue
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsOrganizationsDelete()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsOrganizationsDelete', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsOrganizationsDelete();
  }

  /**
     * efsOrganizationsGet [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns an organization.
     *
     * @param $adminOrgIdentifierType
     * @param $adminOrgIdentifierValue
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsOrganizationsGet()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsOrganizationsGet', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsOrganizationsGet();
  }

  /**
     * efsOrganizationsGetList [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns a list of organizations specified by a search value.
     *
     * @param $adminOrgField
     * @param $adminOrgValue
     * @param $pageFrom
     * @param $pageLimit
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsOrganizationsGetList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsOrganizationsGetList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsOrganizationsGetList();
  }

  /**
     * efsPrivilegesChangeRight [EFS 8.2]
     *
     * Description (from WSDL):
     * Sets a right on an object for given team.
     *
     * @param $objectId
     * @param $groupId
     * @param $right
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsPrivilegesChangeRight()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsPrivilegesChangeRight', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsPrivilegesChangeRight();
  }

  /**
     * efsStaffAdd [EFS 8.2]
     *
     * Description (from WSDL):
     * Create a new staff member for the EFS admin interface To set the staff members organization you need to have the appropriate right. If no organization is set the staff member will inherit the creating staff members organization.
     *
     * @param $staffMember
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsStaffAdd()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsStaffAdd', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsStaffAdd();
  }

  /**
     * efsStaffChange [EFS 8.2]
     *
     * Description (from WSDL):
     * Changes a staff members data
     *
     * @param $identifierType
     * @param $identifier
     * @param $changes
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsStaffChange()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsStaffChange', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsStaffChange();
  }

  /**
     * efsStaffChangeRole [EFS 8.2]
     *
     * Description (from WSDL):
     * Alters a staff members role within a given team
     *
     * @param $userId
     * @param $teamId
     * @param $newRole
     * @param $newOwnerUserId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsStaffChangeRole()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsStaffChangeRole', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsStaffChangeRole();
  }

  /**
     * efsStaffDelete [EFS 8.2]
     *
     * Description (from WSDL):
     * Deletes a staff member
     *
     * @param $identifierType
     * @param $identifier
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsStaffDelete()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsStaffDelete', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsStaffDelete();
  }

  /**
     * efsStaffGet [EFS 8.2]
     *
     * Description (from WSDL):
     * Return information about a staff member
     *
     * @param $identifierType
     * @param $identifier
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsStaffGet()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsStaffGet', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsStaffGet();
  }

  /**
     * efsStaffGetList [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns a list of EFS staff members
     *
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsStaffGetList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsStaffGetList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsStaffGetList();
  }

  /**
     * efsStaffSendMail [EFS 8.2]
     *
     * Description (from WSDL):
     * Sends e-mail to an administrator
     *
     * @param string $identifierType
     * @param        $identifierValue
     * @param        $mailTemplateData
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsStaffSendMail()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsStaffSendMail', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsStaffSendMail();
  }

  /**
     * efsSystemGetRandomAztecGod [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns a random aztec god. Unleashes its power every sunday at 2 am when printed on blue paper. Please note that some gods may unleash evil powers!
     *
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsSystemGetRandomAztecGod()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsSystemGetRandomAztecGod', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsSystemGetRandomAztecGod();
  }

  /**
     * efsSystemGetTime [EFS 8.2]
     *
     * Description (from WSDL):
     * Returns the current server time
     *
     * @param $format
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsSystemGetTime()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsSystemGetTime', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsSystemGetTime();
  }

  /**
     * efsTeamsAdd [EFS 8.2]
     *
     * Description (from WSDL):
     * Adds a new EFS team
     *
     * @param $team
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsTeamsAdd()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsTeamsAdd', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsTeamsAdd();
  }

  /**
     * efsTeamsChange [EFS 8.2]
     *
     * Description (from WSDL):
     * Changes the EFS team details
     *
     * @param $teamId
     * @param $team
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsTeamsChange()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsTeamsChange', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsTeamsChange();
  }

  /**
     * efsTeamsDelete [EFS 8.2]
     *
     * Description (from WSDL):
     * Deletes an EFS team
     *
     * @param $teamId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsTeamsDelete()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsTeamsDelete', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsTeamsDelete();
  }

  /**
     * efsTeamsGet [EFS 8.2]
     *
     * Description (from WSDL):
     * Retrieves the data of an EFS team
     *
     * @param $teamId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsTeamsGet()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsTeamsGet', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsTeamsGet();
  }

  /**
     * efsTeamsGetList [EFS 8.2]
     *
     * Description (from WSDL):
     * Retrieves a list with the data of an EFS teams
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $page
     * @param $limit
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsTeamsGetList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsTeamsGetList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsTeamsGetList();
  }

  /**
     * panelExternalsurveysAdd [EFS 8.2]
     *
     * Description (from WSDL):
     * Adds a new external survey
     *
     * @param $id
     * @param $title
     * @param $type
     * @param $attributes
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelExternalsurveysAdd()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelExternalsurveysAdd', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelExternalsurveysAdd();
  }

  /**
     * panelExternalsurveysAddParticipants [EFS 8.2]
     *
     * Description (from WSDL):
     * Adds or updates the participation status of multiple participants in an external survey
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $panelistIdentifierType
     * @param $surveyParticipations
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelExternalsurveysAddParticipants()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelExternalsurveysAddParticipants', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelExternalsurveysAddParticipants();
  }

  /**
     * panelExternalsurveysChange [EFS 8.2]
     *
     * Description (from WSDL):
     * Updates an existing external survey with new attributes. The survey's external ID cannot be changed.
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $title
     * @param $type
     * @param $attributes
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelExternalsurveysChange()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelExternalsurveysChange', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelExternalsurveysChange();
  }

  /**
     * surveyChangeDescription [EFS 8.1]
     *
     * @param int    $surveyId
     * @param string $title
     * @param string $description
     * @param string $author
     * @param string $staff
     * @param string $comment
     * @param bool   $isMarked
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyChangeDescription()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyChangeDescription', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyChangeDescription();
  }

  /**
     * getSampleUsers [EFS 8.1] [EFS 8]
     *
     * @param int   $sampleId
     * @param array $returnDataFields
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSampleUsers()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSampleUsers', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getSampleUsers();
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getSampleUsers();
  }

  /**
     * getPanelistsByIdentifier [EFS 8.1] [EFS 8]
     *
     * Description (from WSDL):
     * returns a list of actual panelists from a list of possible panelist identifiers
     * using this service it can be checked if specific panelists are already contained in the panel or not
     *
     * @param string $identifierType
     * @param array  $identifierList
     * @param array  $returnDataFields
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getPanelistsByIdentifier()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getPanelistsByIdentifier', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getPanelistsByIdentifier();
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getPanelistsByIdentifier();
  }

  /**
     * addPanelistToSample [EFS 8.1]
     *
     * Description (from WSDL):
     * Add a panelist to a sample
     *
     * @param string $identifierType
     * @param string $identifierValue
     * @param int    $sampleId
     * @param string $resetParticipation
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function addPanelistToSample()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('addPanelistToSample', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->addPanelistToSample();
  }

  /**
     * getQuestionnaireStructure [EFS 8.1]
     *
     * Description (from WSDL):
     * return the questionnaire structure for a survey
     *
     * @param int $surveyId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getQuestionnaireStructure()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getQuestionnaireStructure', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getQuestionnaireStructure();
  }

  /**
     * efsPrivilegesHasAccessToFeature [EFS 8.1]
     *
     * Description (from WSDL):
     * Tells current user has access to a feature
     *
     * @param string $feature
     * @param string $right
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsPrivilegesHasAccessToFeature()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsPrivilegesHasAccessToFeature', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsPrivilegesHasAccessToFeature();
  }

  /**
     * efsPrivilegesHasAccessToObject [EFS 8.1]
     *
     * Description (from WSDL):
     * Tells current user has access to an object
     *
     * @param integer $objectId
     * @param string  $right
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsPrivilegesHasAccessToObject()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsPrivilegesHasAccessToObject', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsPrivilegesHasAccessToObject();
  }

  /**
     * efsSystemGetVersion [EFS 8.1]
     *
     * Description (from WSDL):
     * determine the version number of the EFS installation
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsSystemGetVersion()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsSystemGetVersion', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsSystemGetVersion();
  }

  /**
     * panelPanelistsAddToSurvey [EFS 8.1]
     *
     * Description (from WSDL):
     * Add a panelist to a survey
     *
     * @param string $identifierType
     * @param string $identifierValue
     * @param int    $surveyId
     * @param string $resetParticipation
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsAddToSurvey()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsAddToSurvey', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsAddToSurvey();
  }

  /**
     * panelSurveysGetList [EFS 8.1]
     *
     * Description (from WSDL):
     * get the list of surveys for a panelist
     *
     * @param string $identifierType
     * @param string $identifierValue
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelSurveysGetList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelSurveysGetList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelSurveysGetList();
  }

  /**
     * panelWebsitesGetList [EFS 8.1]
     *
     * Description (from WSDL):
     * returns a list of panel websites
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelWebsitesGetList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelWebsitesGetList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelWebsitesGetList();
  }

  /**
     * surveyListsAddStaticListElement [EFS 8.1]
     *
     * Description (from WSDL):
     * Adds a static list element
     *
     * @param integer $surveyId
     * @param integer $listId
     * @param string  $label
     * @param integer $number
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyListsAddStaticListElement()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyListsAddStaticListElement', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyListsAddStaticListElement();
  }

  /**
     * surveyListsAddStaticListElements [EFS 8.1]
     *
     * Description (from WSDL):
     * Adds a bunch of static list elements
     *
     * @param integer $surveyId
     * @param integer $listId
     * @param array   $items
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyListsAddStaticListElements()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyListsAddStaticListElements', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyListsAddStaticListElements();
  }

  /**
     * surveyListsClearList [EFS 8.1]
     *
     * Description (from WSDL):
     * Clears a list
     *
     * @param integer $surveyId
     * @param integer $listId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyListsClearList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyListsClearList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyListsClearList();
  }

  /**
     * surveyListsGetStaticListsList [EFS 8.1]
     *
     * Description (from WSDL):
     * Returns the list of available static lists for a survey
     *
     * @param integer $surveyId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyListsGetStaticListsList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyListsGetStaticListsList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyListsGetStaticListsList();
  }

  /**
     * surveyListsCreateStaticList [EFS 8.1]
     *
     * Description (from WSDL):
     * Returns the list of available static lists for a survey
     *
     * @param int    $surveyId
     * @param string $label
     * @param string $placeholder
     * @param string $delimiter
     * @param string $lastDelimiter
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyListsCreateStaticList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyListsCreateStaticList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyListsCreateStaticList();
  }

  /**
     * surveyListsDeleteList [EFS 8.1]
     *
     * Description (from WSDL):
     * Deletes a list
     *
     * @param int $surveyId
     * @param     $listId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyListsDeleteList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyListsDeleteList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyListsDeleteList();
  }

  /**
     * surveyQuestionnaireCreatePage [EFS 8.1]
     *
     * Description (from WSDL):
     * create a questionnaire page
     *
     * @param $surveyId
     * @param $pageTitle
     * @param $pageType
     * @param $parentPageId
     * @param $previousPageId
     * @param $options
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyQuestionnaireCreatePage()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyQuestionnaireCreatePage', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyQuestionnaireCreatePage();
  }

  /**
     * surveyQuestionnaireDeletePage [EFS 8.1]
     *
     * Description (from WSDL):
     * delete a questionnaire page
     *
     * @param $surveyId
     * @param $pageId
     * @param $force
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyQuestionnaireDeletePage()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyQuestionnaireDeletePage', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyQuestionnaireDeletePage();
  }

  /**
     * surveyTranslationsGetTranslationList [EFS 8.1]
     *
     * Description (from WSDL):
     * getTranslationList Gets a list of all translations for the specified  language
     *
     * @param $surveyId
     * @param $languageId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyTranslationsGetTranslationList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyTranslationsGetTranslationList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyTranslationsGetTranslationList();
  }

  /**
     * surveyTranslationsChangeTranslation [EFS 8.1]
     *
     * Description (from WSDL):
     * changeTranslation - changes a translation
     *
     * @param int $surveyId
     * @param int $languageId
     * @param     $textType
     * @param     $typeId
     * @param     $text
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyTranslationsChangeTranslation()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyTranslationsChangeTranslation', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyTranslationsChangeTranslation();
  }

  /**
     * surveyTranslationsGetLanguagesList [EFS 8.1]
     *
     * Description (from WSDL):
     * getLanguagesList - get a list of all languages in a survey
     *
     * @param int $surveyId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyTranslationsGetLanguagesList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyTranslationsGetLanguagesList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyTranslationsGetLanguagesList();
  }

  /**
     * surveyTranslationsCreateLanguage [EFS 8.1]
     *
     * Description (from WSDL):
     * addLanguage adds a new language
     *
     * @param int    $surveyId
     * @param string $language
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyTranslationsCreateLanguage()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyTranslationsCreateLanguage', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyTranslationsCreateLanguage();
  }

  /**
     * surveyTranslationsChangeLanguage [EFS 8.1]
     *
     * Description (from WSDL):
     * _8100000_editLanguage
     *
     * @param int    $surveyId
     * @param int    $languageId
     * @param string $language
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyTranslationsChangeLanguage()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyTranslationsChangeLanguage', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyTranslationsChangeLanguage();
  }

  /**
     * surveyTranslationsDeleteLanguage [EFS 8.1]
     *
     * Description (from WSDL):
     * deleteLanguage deletes a language
     *
     * @param int $surveyId
     * @param int $languageId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyTranslationsDeleteLanguage()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyTranslationsDeleteLanguage', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyTranslationsDeleteLanguage();
  }

  /**
     * panelPanelistsGetListByGroup [EFS 8.1]
     *
     * Description (from WSDL):
     * returns a list of panelists in a specific panel group
     *
     * @param int   $groupId
     * @param array $returnDataFields
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsGetListByGroup()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsGetListByGroup', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsGetListByGroup();
  }

  /**
     * Custom: checkEfsSession [EFS 8]
     *
     * Description (from WSDL):
     * test if panel session is currently active, returns panelist-uid or 0 if session is not active
     *
     * @param string $sesId
     * @param string $sesType
     *
     * @return
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function checkEfsSession()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('checkEfsSession', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Custom();
    /** @noinspection PhpParamsInspection */
    $class->checkEfsSession();
  }

  /**
     * Custom: customSurveyparticipantsSendMailByTemplateId [EFS 8]
     *
     * Description (from WSDL):
     * Sends mails to participiants in a sample. Will fail on anonymous surveys.
     *
     * @param int   $sampleId
     * @param array $uids
     * @param int   $mailTemplateId
     * @param array $dispcodes
     * @param array $sendOptions
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function customSurveyparticipantsSendMailByTemplateId()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('customSurveyparticipantsSendMailByTemplateId', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Custom();
    /** @noinspection PhpParamsInspection */
    $class->customSurveyparticipantsSendMailByTemplateId();
  }

  /**
     * getPanelistPartial [EFS 8]
     *
     * @param $identifier
     * @param $identifier_value
     * @param $variables
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getPanelistPartial()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getPanelistPartial', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->getPanelistPartial();
  }

  /**
     * PanelGetPanelist [EFS 8]
     *
     * @param string $identifierType
     * @param string $identifierValue
     *
     * @return array
     * @throws InvalidArgumentException
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function PanelGetPanelist()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('PanelGetPanelist', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->PanelGetPanelist();
  }

  /**
     * Changes the panelists status [EFS 8]
     *
     * @param string  $idType
     * @param string  $idValue
     * @param integer $status
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function PanelModifyPanelistStatus()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('PanelModifyPanelistStatus', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->PanelModifyPanelistStatus();
  }

  /**
     * Adds the panelist to the given panel group [EFS 8]
     *
     * @param string  $idType
     * @param string  $idValue
     * @param integer $groupId
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function PanelAddPanelistToGroup()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('PanelAddPanelistToGroup', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->PanelAddPanelistToGroup();
  }

  /**
     * Sent the given mail template to the given panelist [EFS 8]
     *
     * @param string $idType
     * @param string $idValue
     * @param object $template
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function PanelSendMailToPanelist()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('PanelSendMailToPanelist', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Webservice();
    /** @noinspection PhpParamsInspection */
    $class->PanelSendMailToPanelist();
  }

  /**
     * getSamples [EFS 8]
     *
     * @return array $sampleData
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSamples()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSamples', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getSamples();
  }

  /**
     * getSample [EFS 8]
     *
     * @param int $sid
     *
     * @return array $sampleData
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSample()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSample', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getSample();
  }

  /**
     * getSurveys [EFS 8]
     *
     * @return array $surveyData
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSurveys()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSurveys', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getSurveys();
  }

  /**
     * getSurveysByCriteria [EFS 8]
     *
     * @param array $params
     *
     * @return array $surveyData
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSurveysByCriteria()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSurveysByCriteria', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getSurveysByCriteria();
  }

  /**
     * getSurveyPreviewLink [EFS 8]
     *
     * @param int $pid
     *
     * @return array $surveyData
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSurveyPreviewLink()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSurveyPreviewLink', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getSurveyPreviewLink();
  }

  /**
     * copySurvey [EFS 8]
     *
     * @param int    $pid
     * @param string $title
     *
     * @return array $surveyData
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function copySurvey()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('copySurvey', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->copySurvey();
  }

  /**
     * changeFieldData [EFS 8]
     *
     * @param int    $pid
     * @param string $surveyStatus
     * @param string $surveyFieldStartTime
     * @param string $surveyFieldEndTime
     *
     * @internal param array $surveyFieldTime
     * @return array|mixed $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function changeFieldData()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('changeFieldData', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->changeFieldData();
  }

  /**
     * createStandardReport [EFS 8]
     *
     * @param int    $pid
     * @param string $title
     * @param string $description
     *
     * @return array|mixed $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function createStandardReport()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('createStandardReport', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->createStandardReport();
  }

  /**
     * createReportPublication [EFS 8]
     *
     * @param int    $reportId
     * @param string $reportType
     * @param string $fileName
     *
     * @return array|mixed $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function createReportPublication()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('createReportPublication', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->createReportPublication();
  }

  /**
     * checkReportPublication [EFS 8]
     *
     * @param int $reportId
     * @param int $reportPublicationId
     *
     * @return array|mixed $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function checkReportPublication()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('checkReportPublication', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->checkReportPublication();
  }

  /**
     * getReportPublication [EFS 8]
     *
     * @param int $reportId
     * @param int $reportPublicationId
     *
     * @return array|mixed $result base64
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getReportPublication()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getReportPublication', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getReportPublication();
  }

  /**
     * get PlaceholderList [EFS 8]
     *
     * @param int $surveyId
     *
     * @return array|mixed $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function placeholdersGetList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('placeholdersGetList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->placeholdersGetList();
  }

  /**
     * get PlaceholderList for a language id [EFS 8]
     *
     * @param int $surveyId
     * @param int $languageId
     *
     * @return array|mixed $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function placeholdersGetListTranslated()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('placeholdersGetListTranslated', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->placeholdersGetListTranslated();
  }

  /**
     * change Placeholder [EFS 8]
     *
     * @param int    $surveyId
     * @param int    $id
     * @param string $text
     *
     * @return array|mixed $result
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function placeholdersChange()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('placeholdersChange', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->placeholdersChange();
  }

  /**
     * surveyPlaceholdersChangeTranslated [EFS 8]
     *
     * Description (from WSDL):
     * change an existings survey placeholder's text or handle (identifier) in a certain survey language
     *
     * @param int    $surveyId
     * @param int    $languageId
     * @param int    $id
     * @param string $text
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyPlaceholdersChangeTranslated()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyPlaceholdersChangeTranslated', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyPlaceholdersChangeTranslated();
  }

  /**
     * getDescriptionList [EFS 8]
     *
     * @param int    $surveyId
     * @param string $varTypes
     *
     * @return array $items
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getDescriptionList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getDescriptionList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getDescriptionList();
  }

  /**
     * getAdminUserId [EFS 8]
     *
     * @param string $sesId
     *
     * @return int $userId
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getAdminUserId()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getAdminUserId', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getAdminUserId();
  }

  /**
     * getAdminUser [EFS 8]
     *
     * @return array|mixed $user
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getAdminUser()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getAdminUser', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getAdminUser();
  }

  /**
     * addPanelistsCsv [EFS 8]
     *
     * Note: Data must contain column headers in line 1
     *
     * @param string $identifier
     * @param string $csvData
     * @param string $mappingColumn (optional)
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function addPanelistsCsv()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('addPanelistsCsv', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->addPanelistsCsv();
  }

  /**
     * panelPanelistsAdd [EFS 8]
     * adds a panelist to the panel
     *
     * @param string $returnIdentifierType
     * @param array  $panelistData
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelPanelistsAdd()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelPanelistsAdd', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelPanelistsAdd();
  }

  /**
     * updatePanelistsCsv [EFS 8]
     *
     * Note: Data must contain column headers in line 1
     *
     * @param string $identifier
     * @param string $csvData
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function updatePanelistsCsv()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('updatePanelistsCsv', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->updatePanelistsCsv();
  }

  /**
     * changePanelist [EFS 8] [EFS 9.1] [EFS 10.1]
     *
     * change an existing panelist's data
     * Note: this is identical to "updatePanelist" in the webservice but uses the Service Layer
     *       the Service Layer is BR0KSEN if u_gender != 1 && u_gender != 2 !! (should be fixed by now, BF 25.10.11)
     *
     * @param string $identifierType
     * @param string $identifierValue
     * @param array  $panelistData
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function changePanelist()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('changePanelist', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->changePanelist();
    $class = new Pct_Efs_9_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->changePanelist();
    $class = new Pct_Efs_10_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->changePanelist();
  }

  /**
     * surveySamplesGetList [EFS 8]
     *
     * Description (from WSDL):
     * return the list of samples for a survey
     *
     * @param int $surveyId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveySamplesGetList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveySamplesGetList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveySamplesGetList();
  }

  /**
     * sampleGetUsers [EFS 8]
     *
     * Description (from WSDL):
     * returns a list of participants
     *
     * @param int $sampleId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function sampleGetUsers()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('sampleGetUsers', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->sampleGetUsers();
  }

  /**
     * surveyParticipantsSendMail [EFS 8]
     *
     * Description (from WSDL):
     * notify survey participants via email/sms
     *
     * @param int            $sampleId
     * @param array          $uids
     * @param array          $templateArray
     * @param bool|\datetime $sendDate
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyParticipantsSendMail()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyParticipantsSendMail', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyParticipantsSendMail();
  }

  /**
     * surveyParticipantsImportCsv [EFS 8]
     *
     * Description (from WSDL):
     * add multiple participants from a CSV file to a survey or update existing participants using data from a CSV file
     *
     * @param int    $sampleId
     * @param array  $csvData
     * @param string $returnIdentifierType, enum { 'u_email', 'code', 'uid' }
     * @param bool   $allowDuplicateEmails
     * @param string $mappingColumn       (seems to be optional)
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyParticipantsImportCsv()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyParticipantsImportCsv', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyParticipantsImportCsv();
  }

  /**
     * addParticipantsToSurvey [EFS 8]
     *
     * Description (from WSDL):
     * add multiple participants to a survey
     *
     * @param int    $sampleId
     * @param string $returnIdentifierType
     * @param array  $participantList
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function addParticipantsToSurvey()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('addParticipantsToSurvey', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->addParticipantsToSurvey();
  }

  /**
     * getMasterdataVars [EFS 8]
     *
     * Description (from WSDL):
     * returns the panel's master data variables along with their predefined answer codes
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getMasterdataVars()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getMasterdataVars', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getMasterdataVars();
  }

  /**
     * getParticipantsByCriteria [EFS 8]
     *
     * Description (from WSDL):
     * returns a list of participants according to the specified sample. the operation is allowed for personalized surveys only.
     * NOTE: does NOT return uid!
     *
     * @param $sampleId
     * @param $condition
     *
     * @internal param array $params
     * @return array $surveyData
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getParticipantsByCriteria()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getParticipantsByCriteria', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getParticipantsByCriteria();
  }

  /**
     * getSurveyResultsRawdataCsv [EFS 8]
     *
     * Description (from WSDL):
     * return the survey results raw data in CSV format
     *
     * @param int   $surveyId
     * @param array $exportTypes, enum { 'QUESTIONNAIRE', 'PARTICIPANT' }
     * @param array $includeVariables
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSurveyRawDataCsv()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSurveyRawDataCsv', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getSurveyRawDataCsv();
  }

  /**
     * emptySurvey [EFS 8]
     *
     * Description (from WSDL):
     * delete all participants from a sample, including their already collected survey results. the operation is allowed for personalized and anonymous surveys only.
     *
     * @param int $sampleId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function emptySurvey()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('emptySurvey', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->emptySurvey();
  }

  /**
     * deleteParticipantFromSurvey [EFS 8]
     *
     * Description (from WSDL):
     * delete a participant from a survey sample, along with any already collected survey results for the participant
     *
     * @param int $sampleId
     * @param int $userId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function deleteParticipantFromSurvey()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('deleteParticipantFromSurvey', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->deleteParticipantFromSurvey();
  }

  /**
     * surveyMailtemplatesGetList [EFS 8]
     *
     * Description (from WSDL):
     * get a list of mail templates the user has access to
     *
     * @param array $mailTemplateTypes
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyMailtemplatesGetList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyMailtemplatesGetList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyMailtemplatesGetList();
  }

  /**
     * efsJobsGetStatus [EFS 8]
     *
     * Description (from WSDL):
     * query the status of a single EFS job
     *
     * @param string $jobId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsJobsGetStatus()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsJobsGetStatus', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsJobsGetStatus();
  }

  /**
     * efsStaffAddAdminUser [EFS 8]
     *
     * Description (from WSDL):
     * Create a new user for the EFS admin interface
     *
     * @param array $adminData
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsStaffAddAdminUser()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsStaffAddAdminUser', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsStaffAddAdminUser();
  }

  /**
     * efsSystemGetLoad [EFS 8]
     *
     * Description (from WSDL):
     * query the current system's 1-minute load average
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsSystemGetLoad()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsSystemGetLoad', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsSystemGetLoad();
  }

  /**
     * panelVariablesGetDescriptionList [EFS 8]
     *
     * Description (from WSDL):
     * returns the panel's variables along with their predefined answer codes (if any)
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelVariablesGetDescriptionList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelVariablesGetDescriptionList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelVariablesGetDescriptionList();
  }

  /**
     * qtoolResultsGetSurveyQuotes [EFS 8]
     *
     * Description (from WSDL):
     * return the completed surveys results for specific variables in a structured format
     *
     * @param integer $surveyId
     * @param array   $codedVariables
     * @param array   $quoteVariables
     * @param integer $count
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function qtoolResultsGetSurveyQuotes()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('qtoolResultsGetSurveyQuotes', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->qtoolResultsGetSurveyQuotes();
  }

  /**
     * surveyLayoutsApply [EFS 8]
     *
     * Description (from WSDL):
     * apply a survey layout in a certain survey
     *
     * @param integer $surveyId
     * @param integer $layoutId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyLayoutsApply()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyLayoutsApply', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyLayoutsApply();
  }

  /**
     * surveyLayoutsGetList [EFS 8]
     *
     * Description (from WSDL):
     * returns a list of all survey layouts the user has access to
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyLayoutsGetList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyLayoutsGetList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyLayoutsGetList();
  }

  /**
     * surveyLayoutsGetListByCriteria [EFS 8]
     *
     * Description (from WSDL):
     * returns a list of survey layouts the user has access to, based on search criteria
     *
     * @param array $logicalCondition
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyLayoutsGetListByCriteria()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyLayoutsGetListByCriteria', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyLayoutsGetListByCriteria();
  }

  /**
     * surveyLogosChange [EFS 8]
     *
     * Description (from WSDL):
     * change a logo in a survey
     *
     * @param integer $surveyId
     * @param string  $logoType
     * @param         $image
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyLogosChange()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyLogosChange', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyLogosChange();
  }

  /**
     * surveyReportsDeleteReport [EFS 8]
     *
     * Description (from WSDL):
     * delete a report for a survey
     *
     * @param integer $reportId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyReportsDeleteReport()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyReportsDeleteReport', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyReportsDeleteReport();
  }

  /**
     * surveyResultsGetParticipations [EFS 8]
     *
     * Description (from WSDL):
     * return frequencies for all disposition codes achieved in a survey, thus basic information about the survey's participation statuses
     *
     * @param integer $surveyId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyResultsGetParticipations()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyResultsGetParticipations', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyResultsGetParticipations();
  }

  /**
     * surveyResultsGetSimpleStatistics [EFS 8]
     *
     * Description (from WSDL):
     * return frequency statistics for all closed questions of a survey
     *
     * @param integer $surveyId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyResultsGetSimpleStatistics()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyResultsGetSimpleStatistics', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyResultsGetSimpleStatistics();
  }

  /**
     * surveySurveysChangeType [EFS 8]
     *
     * Description (from WSDL):
     * Change the type of a survey
     *
     * @param integer $surveyId
     * @param string  $surveyType
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveySurveysChangeType()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveySurveysChangeType', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveySurveysChangeType();
  }

  /**
     * surveySurveysDelete [EFS 8]
     *
     * Description (from WSDL):
     * deletes a survey
     *
     * @param integer $surveyId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveySurveysDelete()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveySurveysDelete', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveySurveysDelete();
  }

  /**
     * surveySurveysGetActiveList [EFS 8]
     *
     * Description (from WSDL):
     * get a list of active surveys the user has access to
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveySurveysGetActiveList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveySurveysGetActiveList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveySurveysGetActiveList();
  }

  /**
     * surveySurveysGetQuestionnaireStructure [EFS 8]
     *
     * Description (from WSDL):
     * return the questionnaire structure for a survey
     *
     * @param integer $surveyId
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveySurveysGetQuestionnaireStructure()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveySurveysGetQuestionnaireStructure', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveySurveysGetQuestionnaireStructure();
  }

  /**
     * callCustomService [EFS 8]
     * use this to call custom Service Layer methods that are only used in a few projects
     * use with the name of the service (e.g. "custom.surveyparticipants.sendMailByTemplateId") and the parameters as required (see WSDL)
     *
     * @param string $serviceName
     * @param array  $params
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function callCustomService()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('callCustomService', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_8_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->callCustomService();
  }

  /**
     * getSurveyExport [EFS 9.1]
     * Exports survey results according to the given configuration
     * marked with "SL" to avoid confusion with old webservice-method of the same name, call will fail if old method is activated but this one isn't
     *
     * @param array $exportConfig
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getSurveyExportSL()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getSurveyExportSL', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_9_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->getSurveyExportSL();
  }

  /**
     * efsStaffGetSelf [EFS 9.1]
     *
     * Description (from WSDL):
     * Return information about the current user thats using the service
     *
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsStaffGetSelf()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsStaffGetSelf', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_9_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsStaffGetSelf();
  }

  /**
     * surveyParticipantsSendMailV2 [EFS 9.1]
     *
     * Description (from WSDL):
     * Sends mails to every participiant in a survey. Will fail on anonymous surveys.
     *
     * @param int   $surveyId
     * @param array $mailTemplate
     * @param array $uids
     * @param array $dispcodes
     * @param array $sendOptions
     *
     * @return mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyParticipantsSendMailV2()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyParticipantsSendMailV2', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_9_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyParticipantsSendMailV2();
  }

  /**
     * panelGroupsEmpty [EFS 9]
     *
     * Description (from WSDL):
     * empty a panel group by deleting all members from it
     *
     * @param $id
     *
     * @return array|mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function panelGroupsEmpty()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('panelGroupsEmpty', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_9_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->panelGroupsEmpty();
  }

  /**
     * Custom: surveySurveysGetList [EFS 10.4]
     *
     *
     * @return mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveySurveysGetList()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveySurveysGetList', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_4_Api_Custom();
    /** @noinspection PhpParamsInspection */
    $class->surveySurveysGetList();
  }

  /**
   * loftParticipantsGetUserEmail [EFS 10.2]
   *
   * Description (from WSDL):
   * Get feedback center user data
   *
   * @param $project_id
   * @param $fid
   * @return mixed
   */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function loftParticipantsGetUserEmail()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('loftParticipantsGetUserEmail', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->loftParticipantsGetUserEmail();
  }

  /**
   * loftParticipantsHasAccessToFeedbackCenter [EFS 10.2]
   *
   * Description (from WSDL):
   * Check if user has access to feedback center for project
   *
   * @param $project_id
   * @param $fid
   * @param $token
   * @return mixed
   */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function loftParticipantsHasAccessToFeedbackCenter()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('loftParticipantsHasAccessToFeedbackCenter', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->loftParticipantsHasAccessToFeedbackCenter();
  }

  /**
   * loftParticipantsImportPoolParticipants [EFS 10.2]
   *
   * Description (from WSDL):
   * add multiple feedback-participants from a CSV file to the participants pool using data from a CSV file. note: this operation works on surveys of type "leadership" only
   *
   * @param $sampleId
   * @param $csvData
   * @param $returnIdentifierType
   * @param $mappingColumn
   * @return mixed
   */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function loftParticipantsImportPoolParticipants()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('loftParticipantsImportPoolParticipants', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->loftParticipantsImportPoolParticipants();
  }

  /**
   * loftParticipantsImportProcessParticipants [EFS 10.2]
   *
   * Description (from WSDL):
   * add multiple feedback-provider and -receiver from a CSV file to a feedback-wave using data from a CSV file. note: this operation works on surveys of type "leadership" only
   *
   * @param $sampleId
   * @param $waveId
   * @param $csvData
   * @param $returnIdentifierType
   * @param $mappingColumn
   * @return mixed
   */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function loftParticipantsImportProcessParticipants()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('loftParticipantsImportProcessParticipants', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->loftParticipantsImportProcessParticipants();
  }

  /**
   * surveyParticipantsAddListV2 [EFS 10.2]
   *
   * Description (from WSDL):
   * add multiple participants to a survey. allowing duplicate emails is configurable.
   *
   * @param $sampleId
   * @param $returnIdentifierType
   * @param $participantList
   * @param $allowDuplicateEmails
   * @return mixed
   */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyParticipantsAddListV2()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyParticipantsAddListV2', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_2_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyParticipantsAddListV2();
  }

  /**
     * Custom: updateDataReport [EFS 10.1]
     *
     *
     * @param int $project_id
     * @param int $report_id
     *
     * @return mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function updateReportData()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('updateReportData', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_1_Api_Custom();
    /** @noinspection PhpParamsInspection */
    $class->updateReportData();
  }

  /**
     * Custom: deleteReportPublication [EFS 10.1]
     *
     * @param $reportId
     * @param $reportPublicationId
     *
     * @return mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function deleteReportPublication()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('deleteReportPublication', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_1_Api_Custom();
    /** @noinspection PhpParamsInspection */
    $class->deleteReportPublication();
  }

  /**
     * surveyParticipantsGetListByCriteriaV3 [EFS 10.1]
     *
     * Description (from WSDL):
     * Returns a list of participants according to the specified sample. The operation is allowed for personalized surveys only. The size of result set can be limited by using parameter 'offset' and 'limit'. Use parameter 'withAdditionalFields' in order to get the additional information for participant or suppress it
     *
     * @param $sampleId
     * @param $logicalCondition
     * @param $offset
     * @param $limit
     * @param $additionalInfo
     *
     * @return mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyParticipantsGetListByCriteriaV3()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyParticipantsGetListByCriteriaV3', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyParticipantsGetListByCriteriaV3();
  }

  /**
     * surveyParticipantsGetListV2 [EFS 10.1]
     *
     * Description (from WSDL):
     * Returns a list of participants. The operation is allowed for personalized surveys only. Use parameter 'withAdditionalFields' in order to get the additional information for participant or suppress it
     *
     * @param $sampleId
     * @param $additionalInfo
     *
     * @return mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyParticipantsGetListV2()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyParticipantsGetListV2', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyParticipantsGetListV2();
  }

  /**
     * surveyParticipantsSendMailByTemplateId [EFS 10.1]
     *
     * Description (from WSDL):
     * Sends mails to participiants in a sample. Will fail on anonymous surveys.
     *
     * @param $sampleId
     * @param $uids
     * @param $mailTemplateId
     * @param $dispcodes
     * @param $sendOptions
     *
     * @return mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyParticipantsSendMailByTemplateId()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyParticipantsSendMailByTemplateId', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyParticipantsSendMailByTemplateId();
  }

  /**
     * surveyReportsCreateReportPublicationV2 [EFS 10.1]
     *
     * Description (from WSDL):
     * create a report publication for a survey
     *
     * @param $reportId
     * @param $reportType
     * @param $fileName
     * @param $languageId
     * @param $guiLanguage
     * @return mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyReportsCreateReportPublicationV2()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyReportsCreateReportPublicationV2', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_1_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyReportsCreateReportPublicationV2();
  }

  /**
     * surveyParticipantsGetListByCriteriaV2 [EFS 10]
     *
     * Description (from WSDL):
     * returns a list of participants according to the specified sample. the operation is allowed for personalized surveys only. the size of result set can be limited by using parameter 'offset' and 'limit'
     *
     * @param $sampleId
     * @param $logicalCondition
     * @param $offset
     * @param $limit
     *
     * @return mixed
     */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function surveyParticipantsGetListByCriteriaV2()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('surveyParticipantsGetListByCriteriaV2', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->surveyParticipantsGetListByCriteriaV2();
  }

  /**
       * @param $startCid [EFS 10.5]
       * @param $endCid
       * @return array|mixed
       */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function getRedemptionRequests()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('getRedemptionRequests', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_5_Api_Custom();
    /** @noinspection PhpParamsInspection */
    $class->getRedemptionRequests();
  }

  /**
       * @param array $cids [EFS 10.5]
       * @param string $userName
       * @param string $description
       * @return array|mixed
       */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function cancelCreditPoints()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('cancelCreditPoints', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_5_Api_Custom();
    /** @noinspection PhpParamsInspection */
    $class->cancelCreditPoints();
  }

  /**
       * @param array $cids [EFS 10.5]
       * @param string $paidDate
       * @return array|mixed
       */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function updateCreditPoints()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('updateCreditPoints', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_5_Api_Custom();
    /** @noinspection PhpParamsInspection */
    $class->updateCreditPoints();
  }

  /**
       * @param $surveyId [EFS 10.5]
       * @param $sampleId
       * @param array $dispositionCodesToChange
       * @param $dispositionCodeNew
       * @return array|mixed
       */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function changeDispositionCodes()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('changeDispositionCodes', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_5_Api_Custom();
    /** @noinspection PhpParamsInspection */
    $class->changeDispositionCodes();
  }

  /**
       * efsOrganizationsChangeReportingExportTemplates [EFS 10.7]
       *
       * Description (from WSDL):
       * Changes an admin organization's export templates configuration for one file type format.
       *
       * @param $adminOrgIdentifierType
       * @param $adminOrgIdentifierValue
       * @param $reportingExportFormat
       * @param $reportingExportTemplates
       * @return mixed
       */
  /** @noinspection PhpInconsistentReturnPointsInspection */
  public function efsOrganizationsChangeReportingExportTemplates()
  {
    $args = func_get_args();
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    return Pct_Efs_Api::__call('efsOrganizationsChangeReportingExportTemplates', $args);

    // helper code for IDE-navigation
    /** @noinspection PhpUnreachableStatementInspection */
    /** @noinspection PhpParamsInspection */
    $class = new Pct_Efs_10_7_Api_Servicelayer();
    /** @noinspection PhpParamsInspection */
    $class->efsOrganizationsChangeReportingExportTemplates();
  }

}