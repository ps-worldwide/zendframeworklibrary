<?php
  /**
   * Pct_Efs_17_4_Api_Env 
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Env.php,v 1.2 2018/05/04 10:50:25 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  abstract class Pct_Efs_17_4_Api_Env extends Pct_Efs_17_3_Api_Env 
  {
    /**
     * Inititates the environment needed to perform API calls
     */
    public static function init($version)
    {
      if (self::$isInit == true)
        return;

      // First we need an autoloader which loads classes from EFS
      //$loader = efs_models_autoload::getInstance();
      Pct_Efs_Autoloader::getInstance($version);

      // avoid silly notices from EFS
      self::setEfsErrorReporting();

      // Load a lots of EFS stuff
      /** @noinspection PhpDynamicAsStaticMethodCallInspection */
      self::$efsEnv = efs_apis_17_4_env::getInstance(); 

      // we want to see our own notices
      self::resetEfsErrorReporting();

      // Mark that EFS environment was initilized
      self::$isInit = true;
    }
  }