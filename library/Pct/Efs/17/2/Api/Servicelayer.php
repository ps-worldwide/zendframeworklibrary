<?php
  /**
   * Pct_Efs_17_2_Api_Servicelayer 
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Servicelayer.php,v 1.2 2017/10/24 14:24:26 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_17_2_Api_Servicelayer extends Pct_Efs_17_2_Api_Custom 
  {
        /**
     * surveyParticipantsImportParticipantAndSurveyResultFromCSV
     *
     * Description (from WSDL):
     * Adds multiple participants including their survey results from a CSV file. Please note thet this operation works on surveys of type "personalized"  or "employee" only
     *
     * @param $sampleId
     * @param $csvData
     * @param $returnIdentifierType
     * @param $allowDuplicateEmails
     * @param $mappingColumn
     * @param $executeTrigger
     * @return mixed
     */
     public function surveyParticipantsImportParticipantAndSurveyResultFromCSV($sampleId, $csvData, $returnIdentifierType, $allowDuplicateEmails, $mappingColumn, $executeTrigger)
     {
      return $this->makeServiceLayerCall($this->url . "&method=survey.participants.importParticipantAndSurveyResultFromCSV",
                                         array(
                                               'sampleId' => $sampleId,
                                               'csvData' => $csvData,
                                               'returnIdentifierType' => $returnIdentifierType,
                                               'allowDuplicateEmails' => $allowDuplicateEmails,
                                               'mappingColumn' => $mappingColumn,
                                               'executeTrigger' => $executeTrigger,
                                              ));
     }


  }
