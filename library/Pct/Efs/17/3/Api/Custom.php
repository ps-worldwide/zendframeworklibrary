<?php
  /**
   * Pct_Efs_17_3_Api_Custom 
   * place wrappers for custom Service Layer methods here
   * NOTE: custom services need to be symlinked to htdocs/custom/service
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Custom.php,v 1.2 2018/01/25 11:09:02 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_17_3_Api_Custom extends Pct_Efs_17_2_Api_Servicelayer 
  {
  }