<?php
  /**
   * Pct_Efs_9_Api_Servicelayer
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Servicelayer.php,v 1.6 2013/10/16 09:29:05 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Efs_9_Api_Servicelayer extends Pct_Efs_8_2_Api_Servicelayer
  {
    /**
     * panelGroupsEmpty
     *
     * Description (from WSDL):
     * empty a panel group by deleting all members from it
     *
     * @param $id
     *
     * @return array|mixed
     */
    public function panelGroupsEmpty($id)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.groups.empty",
                                         array(
                                              'id' => $id,
                                         ));
    }
  }
