<?php
  /**
   * Pct_Efs_9_Autoload
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoload.php,v 1.3 2013/10/16 09:29:07 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Efs_9_Autoload extends Pct_Efs_8_2_Autoload
  {
    protected $_efsVersionExtension = '9_';
  }