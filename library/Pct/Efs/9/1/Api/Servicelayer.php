<?php
  /**
   * Pct_Efs_9_1_Api_Servicelayer
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Servicelayer.php,v 1.13 2013/10/16 09:28:20 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Efs_9_1_Api_Servicelayer extends Pct_Efs_9_Api_Servicelayer
  {
    /**
     * changePanelist
     *
     * change an existing panelist's data
     *
     * @param string $identifierType
     * @param string $identifierValue
     * @param array  $panelistData
     *
     * @return array|mixed
     */
    public function changePanelist($identifierType, $identifierValue, $panelistData)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.change",
                                         array('identifierType'  => $identifierType,
                                               'identifierValue' => $identifierValue,
                                               'panelistData'    => $panelistData));
    }

    /**
     * getSurveyExport
     * Exports survey results according to the given configuration
     * marked with "SL" to avoid confusion with old webservice-method of the same name, call will fail if old method is activated but this one isn't
     *
     * @param array $exportConfig
     *
     * @return array|mixed
     */
    public function getSurveyExportSL(array $exportConfig)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.surveys.getExport",
                                         array('exportConfig' => $exportConfig));
    }

    /**
     * efsStaffGetSelf
     *
     * Description (from WSDL):
     * Return information about the current user thats using the service
     *
     */
    public function efsStaffGetSelf()
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.staff.getSelf",
                                         array());
    }

    /**
     * surveyParticipantsSendMailV2
     *
     * Description (from WSDL):
     * Sends mails to every participiant in a survey. Will fail on anonymous surveys.
     *
     * @param int   $surveyId
     * @param array $mailTemplate
     * @param array $uids
     * @param array $dispcodes
     * @param array $sendOptions
     *
     * @return mixed
     */
    public function surveyParticipantsSendMailV2($surveyId, $mailTemplate, $uids, $dispcodes, $sendOptions)
    {
      $params = array('surveyId'     => $surveyId,
                      'mailTemplate' => $mailTemplate,
                      'uids'         => $uids,
                      'sendOptions'  => $sendOptions,
      );
      if ($dispcodes != null)
        $params['dispcodes'] = $dispcodes;

      return $this->makeServiceLayerCall($this->url . "&method=survey.participants.sendMailV2", $params);
    }
  }
