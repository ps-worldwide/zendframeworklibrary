<?php
  /**
   * Pct_Efs_10_1_Autoload
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoload.php,v 1.3 2014/04/07 16:13:31 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_10_1_Autoload extends Pct_Efs_10_Autoload
  {
    protected $_efsVersionExtension = '10_1_';
  }