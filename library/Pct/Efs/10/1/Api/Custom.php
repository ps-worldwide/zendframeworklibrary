<?php
  /**
   * Pct_Efs_10_1_Api_Custom
   * place wrappers for custom Service Layer methods here
   * NOTE: custom services need to be symlinked to htdocs/custom/service
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Custom.php,v 1.1 2014/04/16 13:03:14 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_10_1_Api_Custom extends Pct_Efs_10_Api_Servicelayer
  {
    /**
     * Custom: updateDataReport
     *
     *
     * @param int $project_id
     * @param int $report_id
     *
     * @return mixed
     */
    public function updateReportData($project_id, $report_id)
    {
      return $this->makeServiceLayerCall(
        $this->url . "&method=custom.reports.updateReportData",
        array(
          'project_id' => (int)$project_id,
          'report_id' => (int)$report_id
        )
      );
    }

    /**
     * Custom: deleteReportPublication
     *
     * @param $reportId
     * @param $reportPublicationId
     *
     * @return mixed
     */
    public function deleteReportPublication($reportId, $reportPublicationId)
    {
      return $this->makeServiceLayerCall(
        $this->url . "&method=custom.reports.deleteReportPublication",
          array(
            'reportId' => (int)$reportId,
            'reportPublicationId' => (int)$reportPublicationId
          )
      );
    }
  }
