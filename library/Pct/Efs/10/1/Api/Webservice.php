<?php
  /**
   * Pct_Efs_10_1_Api_Webservice
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Webservice.php,v 1.3 2014/04/07 16:13:30 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_10_1_Api_Webservice extends Pct_Efs_10_Api_Webservice
  {
    // nothing new is to be expected here
  }