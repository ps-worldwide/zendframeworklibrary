<?php
  /**
   * Pct_Efs_10_1_Api_Servicelayer 
   *
   * @package       zendframeworkLibrary
   * @version       $Id: Servicelayer.php,v 1.9 2018/05/04 10:50:25 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author        $Author: faust $
   */

  class Pct_Efs_10_1_Api_Servicelayer extends Pct_Efs_10_1_Api_Custom 
  {
    /**
     * surveyParticipantsGetListByCriteriaV3
     *
     * Description (from WSDL):
     * Returns a list of participants according to the specified sample. The operation is allowed for personalized surveys only. The size of result set can be limited by using parameter 'offset' and 'limit'. Use parameter 'withAdditionalFields' in order to get the additional information for participant or suppress it
     *
     * @param $sampleId
     * @param $logicalCondition
     * @param $offset
     * @param $limit
     * @param $additionalInfo
     *
     * @return mixed
     */
    public function surveyParticipantsGetListByCriteriaV3($sampleId, $logicalCondition, $offset, $limit, $additionalInfo)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.participants.getListByCriteriaV3",
                                         array(
                                              'sampleId'         => $sampleId,
                                              'logicalCondition' => $logicalCondition,
                                              'offset'           => $offset,
                                              'limit'            => $limit,
                                              'additionalInfo'   => $additionalInfo,
                                         ));
    }

    /**
     * surveyParticipantsGetListV2
     *
     * Description (from WSDL):
     * Returns a list of participants. The operation is allowed for personalized surveys only. Use parameter 'withAdditionalFields' in order to get the additional information for participant or suppress it
     *
     * @param $sampleId
     * @param $additionalInfo
     *
     * @return mixed
     */
    public function surveyParticipantsGetListV2($sampleId, $additionalInfo)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.participants.getListV2",
                                         array(
                                              'sampleId'       => $sampleId,
                                              'additionalInfo' => $additionalInfo,
                                         ));
    }

    /**
     * surveyParticipantsSendMailByTemplateId
     *
     * Description (from WSDL):
     * Sends mails to participiants in a sample. Will fail on anonymous surveys.
     *
     * @param $sampleId
     * @param $uids
     * @param $mailTemplateId
     * @param $dispcodes
     * @param $sendOptions
     *
     * @return mixed
     */
    public function surveyParticipantsSendMailByTemplateId($sampleId, $uids, $mailTemplateId, $dispcodes = array(), $sendOptions = array())
    {
      if (empty($sendOptions) || (!is_array($sendOptions) && !array_key_exists('sendOptions', $sendOptions))) {
          $sendOptions = array('sendOptions' => gmdate('c'));
      }
      return $this->makeServiceLayerCall($this->url . "&method=survey.participants.sendMailByTemplateId",
                                         array(
                                              'sampleId'       => $sampleId,
                                              'uids'           => $uids,
                                              'mailTemplateId' => $mailTemplateId,
                                              'dispcodes'      => $dispcodes,
                                              'sendOptions'    => $sendOptions,
                                         ));
    }

    /**
     * changePanelist
     *
     * change an existing panelist's data
     * Note: this is identical to "updatePanelist" in the webservice but uses the Service Layer
     *       the Service Layer is BR0KSEN if u_gender != 1 && u_gender != 2 !! (should be fixed by now, BF 25.10.11)
     * um 2014-02-07: panel.panelists.change is deprecated. Use panel.panelists.changeV1 instead
     *
     * @param string $identifierType
     * @param string $identifierValue
     * @param array  $panelistData
     *
     * @return array|mixed
     */
    public function changePanelist($identifierType, $identifierValue, $panelistData)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.changeV1",
          array('identifierType'  => $identifierType,
              'identifierValue' => $identifierValue,
              'panelistData'    => $panelistData));
    }

    /**
     * surveyReportsCreateReportPublicationV2
     *
     * Description (from WSDL):
     * create a report publication for a survey
     *
     * @param $reportId
     * @param $reportType
     * @param $fileName
     * @param $languageId
     * @param $guiLanguage
     * @return mixed
     */
     public function surveyReportsCreateReportPublicationV2($reportId, $reportType, $fileName, $languageId, $guiLanguage)
     {
      return $this->makeServiceLayerCall($this->url . "&method=survey.reports.createReportPublicationV2",
                                         array(
                                               'reportId' => $reportId,
                                               'reportType' => $reportType,
                                               'fileName' => $fileName,
                                               'languageId' => $languageId,
                                               'guiLanguage' => $guiLanguage,
                                              ));
     }

  }
