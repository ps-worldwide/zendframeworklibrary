<?php
  /**
   * Pct_Efs_10_6_Autoload 
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoload.php,v 1.3 2015/06/30 16:20:11 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_10_6_Autoload extends Pct_Efs_10_5_Autoload 
  {
    protected $_efsVersionExtension = '10_6_'; 
  }