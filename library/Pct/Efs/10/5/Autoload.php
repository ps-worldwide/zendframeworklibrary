<?php
  /**
   * Pct_Efs_10_5_Autoload 
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoload.php,v 1.2 2015/07/03 15:03:39 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_10_5_Autoload extends Pct_Efs_10_4_Autoload 
  {
    protected $_efsVersionExtension = '10_5_'; 
  }