<?php
  /**
   * Pct_Efs_10_5_Api_Custom
   * place wrappers for custom Service Layer methods here
   * NOTE: custom services need to be symlinked to htdocs/custom/service
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Custom.php,v 1.15 2015/10/06 06:52:41 mueller Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: mueller $
   */

  class Pct_Efs_10_5_Api_Custom extends Pct_Efs_10_4_Api_Servicelayer
  {
      /**
       * @param $startCid
       * @param $endCid
       * @return array|mixed
       */
    public function getRedemptionRequests($startCid, $endCid = 0)
    {
      return $this->makeServiceLayerCall(
        $this->url . "&method=custom.payback.getRedemptionRequests",
        array(
            'startCid' => $startCid,
            'endCid' => $endCid
        )
      );
    }

      /**
       * @param array $cids
       * @param string $userName
       * @param string $description
       * @return array|mixed
       */
      public function cancelCreditPoints(array $cids, $userName, $description)
      {
          return $this->makeServiceLayerCall(
              $this->url . "&method=custom.payback.cancelCreditPoints",
              array(
                  'cids' => $cids,
                  'userName' => $userName,
                  'description' => $description
              )
          );
      }

      /**
       * @param array $cids
       * @param string $paidDate
       * @return array|mixed
       */
      public function updateCreditPoints(array $cids, $paidDate)
      {
          return $this->makeServiceLayerCall(
              $this->url . "&method=custom.payback.updateCreditPoints",
              array(
                  'cids' => $cids,
                  'paidDate' => $paidDate
              )
          );
      }

      /**
       * @param $surveyId
       * @param $sampleId
       * @param array $dispositionCodesToChange
       * @param $dispositionCodeNew
       * @return array|mixed
       */
      public function changeDispositionCodes($surveyId, $sampleId, array $dispositionCodesToChange, $dispositionCodeNew)
      {
          return $this->makeServiceLayerCall(
              $this->url . "&method=custom.surveyparticipants.changeDispositionCodes",
              array(
                  'surveyId' => $surveyId,
                  'sampleId' => $sampleId,
                  'dispositionCodesToChange' => $dispositionCodesToChange,
                  'dispositionCodeNew' => $dispositionCodeNew
              )
          );
      }
  }