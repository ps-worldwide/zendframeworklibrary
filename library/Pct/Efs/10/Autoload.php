<?php
  /**
   * Pct_Efs_10_Autoload
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoload.php,v 1.2 2013/10/16 09:28:50 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Efs_10_Autoload extends Pct_Efs_9_1_Autoload
  {
    protected $_efsVersionExtension = '10_';
  }