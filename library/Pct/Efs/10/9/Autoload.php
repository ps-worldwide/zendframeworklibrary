<?php
  /**
   * Pct_Efs_10_9_Autoload 
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoload.php,v 1.2 2016/04/06 14:54:43 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_10_9_Autoload extends Pct_Efs_10_8_Autoload 
  {
    protected $_efsVersionExtension = '10_9_'; 
  }