<?php
  /**
   * Pct_Efs_10_4_Api_Custom
   * place wrappers for custom Service Layer methods here
   * NOTE: custom services need to be symlinked to htdocs/custom/service
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Custom.php,v 1.1 2014/11/26 13:31:55 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_10_4_Api_Custom extends Pct_Efs_10_3_Api_Servicelayer
  {
    /**
     * Custom: surveySurveysGetList
     *
     *
     * @return mixed
     */
    public function surveySurveysGetList()
    {
      return $this->makeServiceLayerCall(
        $this->url . "&method=custom.surveysurveys.getList",
        array()
      );
    }

  }