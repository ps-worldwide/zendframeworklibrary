<?php
  /**
   * Pct_Efs_10_4_Autoload 
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoload.php,v 1.3 2015/01/05 13:17:38 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_10_4_Autoload extends Pct_Efs_10_3_Autoload 
  {
    protected $_efsVersionExtension = '10_4_'; 
  }