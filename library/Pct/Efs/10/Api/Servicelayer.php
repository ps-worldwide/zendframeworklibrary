<?php
  /**
   * Pct_Efs_10_Api_Servicelayer
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Servicelayer.php,v 1.3 2013/10/16 09:28:51 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Efs_10_Api_Servicelayer extends Pct_Efs_9_1_Api_Servicelayer
  {
    /**
     * surveyParticipantsGetListByCriteriaV2
     *
     * Description (from WSDL):
     * returns a list of participants according to the specified sample. the operation is allowed for personalized surveys only. the size of result set can be limited by using parameter 'offset' and 'limit'
     *
     * @param $sampleId
     * @param $logicalCondition
     * @param $offset
     * @param $limit
     *
     * @return mixed
     */
    public function surveyParticipantsGetListByCriteriaV2($sampleId, $logicalCondition, $offset, $limit)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.participants.getListByCriteriaV2",
                                         array(
                                              'sampleId'         => $sampleId,
                                              'logicalCondition' => $logicalCondition,
                                              'offset'           => $offset,
                                              'limit'            => $limit,
                                         ));
    }
  }
