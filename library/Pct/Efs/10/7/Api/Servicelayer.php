<?php
  /**
   * Pct_Efs_10_7_Api_Servicelayer 
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Servicelayer.php,v 1.8 2015/12/15 11:38:52 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_10_7_Api_Servicelayer extends Pct_Efs_10_7_Api_Custom 
  {
      /**
       * efsOrganizationsChangeReportingExportTemplates
       *
       * Description (from WSDL):
       * Changes an admin organization's export templates configuration for one file type format.
       *
       * @param $adminOrgIdentifierType
       * @param $adminOrgIdentifierValue
       * @param $reportingExportFormat
       * @param $reportingExportTemplates
       * @return mixed
       */
       public function efsOrganizationsChangeReportingExportTemplates($adminOrgIdentifierType, $adminOrgIdentifierValue, $reportingExportFormat, $reportingExportTemplates)
       {
            return $this->makeServiceLayerCall(
                $this->url . "&method=efs.organizations.changeReportingExportTemplates",
                array(
                    'adminOrgIdentifierType'   => $adminOrgIdentifierType,
                    'adminOrgIdentifierValue'  => $adminOrgIdentifierValue,
                    'reportingExportFormat'    => $reportingExportFormat,
                    'reportingExportTemplates' => $reportingExportTemplates,
                )
            );
       }
  }
