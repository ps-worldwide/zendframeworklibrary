<?php
  /**
   * Pct_Efs_10_7_Api_Custom 
   * place wrappers for custom Service Layer methods here
   * NOTE: custom services need to be symlinked to htdocs/custom/service
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Custom.php,v 1.9 2015/10/06 10:08:01 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_10_7_Api_Custom extends Pct_Efs_10_6_Api_Servicelayer 
  {
  }