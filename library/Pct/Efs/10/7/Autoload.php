<?php
  /**
   * Pct_Efs_10_7_Autoload 
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoload.php,v 1.7 2015/10/06 10:08:01 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_10_7_Autoload extends Pct_Efs_10_6_Autoload 
  {
    protected $_efsVersionExtension = '10_7_'; 
  }