<?php
  /**
   * Pct_Efs_10_8_Autoload 
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoload.php,v 1.2 2015/12/15 11:38:51 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_10_8_Autoload extends Pct_Efs_10_7_Autoload 
  {
    protected $_efsVersionExtension = '10_8_'; 
  }