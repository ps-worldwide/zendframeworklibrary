<?php
  /**
   * Pct_Efs_10_8_Api_Custom 
   * place wrappers for custom Service Layer methods here
   * NOTE: custom services need to be symlinked to htdocs/custom/service
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Custom.php,v 1.2 2015/12/15 11:38:52 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_10_8_Api_Custom extends Pct_Efs_10_7_Api_Servicelayer 
  {
  }