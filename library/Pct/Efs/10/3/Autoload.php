<?php
  /**
   * Pct_Efs_10_3_Autoload 
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoload.php,v 1.4 2014/10/24 12:30:27 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_10_3_Autoload extends Pct_Efs_10_2_Autoload 
  {
    protected $_efsVersionExtension = '10_3_'; 
  }