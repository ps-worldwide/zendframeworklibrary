<?php
/**
 * Pct_Efs_10_2_Api_Servicelayer 
 *
 * @package       zendframeworkLibrary
 * @version       $Id: Servicelayer.php,v 1.6 2014/10/24 12:30:27 faust Exp $
 * @copyright (c) QuestBack http://www.questback.com
 * @author        $Author: faust $
 *
 */
class Pct_Efs_10_2_Api_Servicelayer extends Pct_Efs_10_1_Api_Servicelayer 
{
  /**
   * loftParticipantsGetUserEmail
   *
   * Description (from WSDL):
   * Get feedback center user data
   *
   * @param $project_id
   * @param $fid
   * @return mixed
   */
  public function loftParticipantsGetUserEmail($project_id, $fid)
  {
    return $this->makeServiceLayerCall(
      $this->url . "&method=loft.participants.getUserEmail",
      array(
        'project_id' => $project_id,
        'fid' => $fid,
      )
    );
  }

  /**
   * loftParticipantsHasAccessToFeedbackCenter
   *
   * Description (from WSDL):
   * Check if user has access to feedback center for project
   *
   * @param $project_id
   * @param $fid
   * @param $token
   * @return mixed
   */
  public function loftParticipantsHasAccessToFeedbackCenter($project_id, $fid, $token)
  {
    return $this->makeServiceLayerCall(
      $this->url . "&method=loft.participants.hasAccessToFeedbackCenter",
      array(
        'project_id' => $project_id,
        'fid' => $fid,
        'token' => $token,
      )
    );
  }

  /**
   * loftParticipantsImportPoolParticipants
   *
   * Description (from WSDL):
   * add multiple feedback-participants from a CSV file to the participants pool using data from a CSV file. note: this operation works on surveys of type "leadership" only
   *
   * @param $sampleId
   * @param $csvData
   * @param $returnIdentifierType
   * @param $mappingColumn
   * @return mixed
   */
  public function loftParticipantsImportPoolParticipants($sampleId, $csvData, $returnIdentifierType, $mappingColumn)
  {
    return $this->makeServiceLayerCall(
      $this->url . "&method=loft.participants.importPoolParticipants",
      array(
        'sampleId' => $sampleId,
        'csvData' => $csvData,
        'returnIdentifierType' => $returnIdentifierType,
        'mappingColumn' => $mappingColumn,
      )
    );
  }

  /**
   * loftParticipantsImportProcessParticipants
   *
   * Description (from WSDL):
   * add multiple feedback-provider and -receiver from a CSV file to a feedback-wave using data from a CSV file. note: this operation works on surveys of type "leadership" only
   *
   * @param $sampleId
   * @param $waveId
   * @param $csvData
   * @param $returnIdentifierType
   * @param $mappingColumn
   * @return mixed
   */
  public function loftParticipantsImportProcessParticipants($sampleId, $waveId, $csvData, $returnIdentifierType, $mappingColumn)
  {
    return $this->makeServiceLayerCall(
      $this->url . "&method=loft.participants.importProcessParticipants",
      array(
        'sampleId' => $sampleId,
        'waveId' => $waveId,
        'csvData' => $csvData,
        'returnIdentifierType' => $returnIdentifierType,
        'mappingColumn' => $mappingColumn,
      )
    );
  }

  /**
   * surveyParticipantsAddListV2
   *
   * Description (from WSDL):
   * add multiple participants to a survey. allowing duplicate emails is configurable.
   *
   * @param $sampleId
   * @param $returnIdentifierType
   * @param $participantList
   * @param $allowDuplicateEmails
   * @return mixed
   */
  public function surveyParticipantsAddListV2($sampleId, $returnIdentifierType, $participantList, $allowDuplicateEmails)
  {
    return $this->makeServiceLayerCall(
      $this->url . "&method=survey.participants.addListV2",
      array(
        'sampleId' => $sampleId,
        'returnIdentifierType' => $returnIdentifierType,
        'participantList' => $participantList,
        'allowDuplicateEmails' => $allowDuplicateEmails,
      )
    );
  }

}
