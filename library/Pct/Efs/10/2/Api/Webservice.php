<?php
  /**
   * Pct_Efs_10_2_Api_Webservice 
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Webservice.php,v 1.4 2014/10/24 12:30:27 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_10_2_Api_Webservice extends Pct_Efs_10_1_Api_Webservice 
  {
    // nothing new is to be expected here
  }