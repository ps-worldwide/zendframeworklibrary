<?php
  /**
   * Pct_Efs_Version
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Version.php,v 1.13 2016/02/26 15:45:33 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_Version
  {
    protected static $efsVersion = null;
    protected static $_instance = null;

    /**
     * Returns the EFS version as String
     *
     * @return float efsVersion
     */
    public static function getVersion()
    {
      if (self::$efsVersion === null)
        self::$_instance = new self();

      return self::$efsVersion;
    }

    /**
     * Read different possibilities in EFS to determine its version
     */
    private function __construct()
    {
      if (false == defined('APPLICATION_EFS_PATH'))
        throw new Exception('APPLICATION_EFS_PATH is not defined');

      // Since 7.1 a file shows the EFS - version
      if (file_exists(APPLICATION_EFS_PATH . 'modules/efs/components/version.inc.php'))
        self::$efsVersion = self::getVersionFromEfs();
      else
        self::$efsVersion = self::getVersionFromCvsTag();
    }

    /**
     * Determines the EFS - version from a possible CVS - tag
     */
    protected function getVersionFromCvsTag()
    {
      if (file_exists(APPLICATION_EFS_PATH . 'www/CVS/Entries'))
      {
        $cvs   = file_get_contents(APPLICATION_EFS_PATH . 'www/CVS/Entries');
        $start = strpos($cvs, '/init.inc.php3');
        if ($start === false)
          return (float)0.0; // dev

        // Get line including init.inc.php3
        $initInc = substr($cvs, $start, 128);
        $version = preg_match('/\/init.inc.php3.*?\/\/T(.*?)$/m', $initInc,
                              $matches);
        if ($version < 1)
          throw new Exception('Cannot determine EFS version');

        // pass the match as version
        $version = $matches[1];

        // major branches
        if ($version == 'SECHS-0')
          return (float)6.0;
        elseif ($version == 'SIEBEN-0')
          return (float)7.0;

        // support tags
        $version = str_replace('v_', '', $version);
        $version = preg_replace('/(_\d+$)/', '', $version);
        $version = str_replace('_', '.', $version);
        return (float)$version;
      }
      else
      {
        throw new Exception(APPLICATION_EFS_PATH . 'www/CVS/Entries not found');
      }
    }

    /**
     * Reads the version from the EFS function get_version() in
     * modules/efs/components/version.inc.php
     */
    protected function getVersionFromEfs()
    {
      $versionClass    = file_get_contents(APPLICATION_EFS_PATH . 'modules/efs/components/version.inc.php');
      $versionFunction = preg_match('/function getVersion\(\)[^}]*}/',
                                    $versionClass, $matches);
      $versionString   = preg_match('/return ([\d\.]+);/', $matches[0], $matches);

      $version = 0;

      if (array_key_exists(1, $matches)) {

        $version = (float)$matches[1];

      }

      if ($version == 0) {
      
        require(APPLICATION_EFS_PATH . 'modules/efs/components/version.php');
        
        if (isset($versionInformation) && is_array($versionInformation) && array_key_exists('version', $versionInformation)) {
        
          $version = (float)$versionInformation['version'];
        
        }
        
      }
      
      return $version;
      
    }
    
  }
