<?php

  /**
   * The class represents a Zend-Mail-Transport-SMTP but allows to pass a Zend-
   * Config object merged with the EFS config. Reads out the smtp-settings from
   * EFS which can (if wanted) be overwritten with smtp-settings from
   * application.ini
   *
   * @author umschlag
   *
   */
  class Pct_Efs_Smtp_Efstransport extends Zend_Mail_Transport_Smtp
  {
    /**
     * The host name or IP of the smtp server
     *
     * @var string
     */
    private $host = '127.0.0.1';

    /**
     * The port number of the smtp server where the smtp service is
     * accepting mails
     *
     * @var string
     */
    private $port = '25';

    /**
     * The constructor - Zend Config object an be one merged with efs config
     * (efs_models_config).
     *
     * @param Zend_Config $conf
     */
    public function __construct(Zend_Config $conf)
    {
      // Set host and port from efs config (efs_models_config) or application.ini
      // smtp.host / smtp.port
      $host = $this->setHost($conf)->getHost();
      $port = $this->setPort($conf)->getPort();

      // Merge the found port with the rest of the smtp.* from application ini
      $smtpConf = array('port' => $port);
      if (null != $conf->smtp)
      {
        $smtpConf = array_merge($smtpConf, $conf->smtp->toArray());
      }

      // Get SMTP-Transport from Zend with the determined configuration
      parent::__construct($host, $smtpConf);
    }

    /**
     * Returns the hostname / IP
     */
    public function getHost()
    {
      return $this->host;
    }

    /**
     * Returns the port
     */
    public function getPort()
    {
      return $this->port;
    }

    /**
     * Set the host name - ip
     *
     * @param Zend_Config $conf
     *
     * @return $this
     */
    private function setHost(Zend_Config $conf)
    {
      if (null != $conf->get('smtp_server'))
      { //config.inc.php3
        $this->host = $conf->get('smtp_server');
      }
      elseif (null != $conf->get('smtp'))
      {
        if (null != $conf->smtp->get('host'))
          $this->host = $conf->smtp->host;
      }

      return $this;
    }

    /**
     * Set the port number
     *
     * @param Zend_Config $conf
     *
     * @return $this
     */
    private function setPort(Zend_Config $conf)
    {
      if (null != $conf->get('smtp_port'))
      {
        $this->port = $conf->get('smtp_port');
      }
      elseif (null != $conf->get('smtp'))
      {
        if (null != $conf->smtp->get('port'))
          $this->port = $conf->smtp->get('port');
      }

      return $this;
    }
  }