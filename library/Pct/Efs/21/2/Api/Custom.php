<?php
  /**
   * Pct_Efs_20_4_Api_Custom #EFSUPGRADE#
   * place wrappers for custom Service Layer methods here
   * NOTE: custom services need to be symlinked to htdocs/custom/service
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Custom.php,v 1.1 2021/09/20 14:18:02 fiedler Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: fiedler $
   */

  class Pct_Efs_21_2_Api_Custom extends Pct_Efs_21_1_Api_Custom #EFSUPGRADE#
  {
  }
