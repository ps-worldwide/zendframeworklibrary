<?php
  /**
   * Pct_Efs_20_4_Api_Env #EFSUPGRADE#
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Env.php,v 1.1 2021/04/06 14:04:44 fiedler Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: fiedler $
   */

  abstract class Pct_Efs_21_1_Api_Env extends Pct_Efs_20_4_Api_Env #EFSUPGRADE#
  {
    /**
     * Inititates the environment needed to perform API calls
     */
    public static function init($version)
    {
      if (self::$isInit == true)
        return;

      // First we need an autoloader which loads classes from EFS
      //$loader = efs_models_autoload::getInstance();
      Pct_Efs_Autoloader::getInstance($version);

      // avoid silly notices from EFS
      self::setEfsErrorReporting();

      // Load a lots of EFS stuff
      /** @noinspection PhpDynamicAsStaticMethodCallInspection */
      self::$efsEnv = efs_apis_20_4_env::getInstance(); #EFSUPGRADE#

      // we want to see our own notices
      self::resetEfsErrorReporting();

      // Mark that EFS environment was initilized
      self::$isInit = true;
    }
  }
