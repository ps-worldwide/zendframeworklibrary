<?php
  /**
   * Pct_Efs_6_Api
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Api.php,v 1.10 2013/10/16 09:28:52 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Efs_6_Api
  {
    // This API connects / supports EFS 7.0
    const EFS_VERSION = 6.0;

    // The API environment used by EFS (we must "boot" EFS)
    protected $efsEnv = null;

    // The EFS-API for survey-actions
    protected $apiSurvey = null;

    // The EFS-API for panel-actions
    protected $apiPanel = null;

    // The EFS-API for hr-actions
    protected $apiHr = null;

    // The EFS-API for webservice-actions
    protected $apiWebservice = null;

    // The work directory before the API call
    protected $cwd;

    /**
     * Initialises the APIs for the particular EFS version
     *
     * @param Zend_Config              $conf
     * @param Zend_Db_Adapter_Abstract $db
     * @param array                    $params
     *
     * @throws Exception
     */
    public function init(Zend_Config $conf, Zend_Db_Adapter_Abstract $db = null, $params = array())
    {
      // Survey-API
      if ($conf->enableApi->survey == 1 || $conf->enableApi->panel == 1 || $conf->enableApi->hr == 1)
        throw new Exception('Only webservice is supported as API in EFS < 7.0');

      // Webservice-API
      if ($conf->enableApi->webservice == 1)
      {
        $baseUrl             = (array_key_exists('externalws', $params)) ? $params['externalws'] : null;
        $clientId            = (array_key_exists('clientid', $params)) ? $params['clientid'] : null;
        $timeout             = (array_key_exists('timeout', $params)) ? (int)$params['timeout'] : 0;
        $this->apiWebservice = new Pct_Efs_6_Api_Webservice(null, $conf, $baseUrl, $clientId, true, $timeout);
      }
    }

    /**
     * Search the method in one of the available APIs, call it and return
     * the result.
     *
     * @param string $method
     * @param array  $params
     *
     * @throws Exception
     * @return mixed
     */
    public function __call($method, $params)
    {
      // Set when a call resulted in an exception
      $exception = null;

      // Do what is needed before each call
      $this->prepareCall();

      // Do the API call
      try
      {
        $returnValue = $this->doCall($method, $params);
      } catch (Exception $e)
      {
        $this->finishCall();
        throw $e;
      }

      // Undo what was done for preparation
      $this->finishCall();

      // No exception then return what the call returned
      return $returnValue;
    }

    /**
     * Do necessary preparations before the call
     */
    protected function prepareCall()
    {
      // EFS might change the current working directory so back it up
      $this->cwd = getcwd();

      // EFS might print HTML (or similar) directly so catch that before the call
      ob_start();
    }

    /**
     * Reset preparations
     */
    protected function finishCall()
    {
      // Dump possible output from EFS
      ob_end_flush();

      // Change back into the previously backed up working directory
      chdir($this->cwd);
    }

    /**
     * Looks for the EFS API for the function wished to call and catches
     * possible EFS exceptions
     */
    protected function doCall($method, $params)
    {
      // Webservice API
      if (method_exists($this->apiWebservice, $method))
        return call_user_func_array(array($this->apiWebservice, $method), $params);

      // If none of the APIs offers the method throw an exception
      throw new Exception('Call to unsupported api-method "' . $method . '()"');
    }
  }