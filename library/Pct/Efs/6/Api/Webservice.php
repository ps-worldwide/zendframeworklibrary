<?php
  /**
   * Pct_Efs_6_Api_Webservice
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Webservice.php,v 1.25 2014/05/19 15:43:31 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_6_Api_Webservice
  {
    protected $client = null;
    protected $url = null;

    /**
     * @var array Soap client options
     */
    protected $options = array('URI'        => 'http://www.global-park.com/OPST/services',
                               'style'      => SOAP_RPC,
                               'use'        => SOAP_ENCODED,
                               'encoding'   => 'UTF-8',
                               'features'   => SOAP_SINGLE_ELEMENT_ARRAYS,
                               'exceptions' => true,
    );

    /**
     * Constructor
     *
     * All Arguments are optional.
     * If not explicitly passed, baseUrl and clientId are determined from config,
     * which is loaded via <code>Zend_Registry::get('conf')</code> if not passed.
     *
     * WSDL cache is default disabled.
     *
     * @param array       $options          (optional) Soap client options
     * @param Zend_Config $config           (optional) Application config instance (used to determine <code>$baseUrl</code> and <code>$clientId</code> if not passed.
     * @param string      $baseUrl          (optional)
     * @param string      $clientId         (optional)
     * @param bool        $disableWsdlCache (optional)
     * @param int         $timeout          (optional)
     */
    public function __construct(array $options = null, Zend_Config $config = null, $baseUrl = null, $clientId = null, $disableWsdlCache = true, $timeout = 0)
    {
      if ($options)
        $this->options = array_merge($this->options, $options);

      if (is_null($config))
        $config = Zend_Registry::get('conf');

      if (is_null($baseUrl))
        $baseUrl = @$config->baseurl;

      if (is_null($clientId) && is_object($config))
      {
        if (is_object($config->webservice))
          $clientId = @$config->webservice->clientId;
      }

      $baseUrlParts  = parse_url($baseUrl);
      if (strpos($baseUrlParts['path'], '/www/', 0) === false) {
        $baseUrlParts['path'] .= '/www/';
        $baseUrlParts['path'] = str_replace('//','/',$baseUrlParts['path']);
      }

      // FW: Add signs to protocol
      $baseUrlParts['scheme'] .= '://';
      $baseUrl = implode("",$baseUrlParts);
      // GF: Replaces the two lines above - if activated on the server
      //$baseUrl = http_build_url('', $baseUrlParts);


      // added '&efsnoredirect=1' to account for EFS-bug which will always return HTTP from config, even if domain is configured to use HTTPS [BF: 27.8.12]
      $this->url = $baseUrl . 'ws_rpc.php?wsdl&actor=general_actions&client=' . $clientId . '&efsnoredirect=1';

      if ($disableWsdlCache)
        ini_set("soap.wsdl_cache_enabled", 0);

      if ($timeout > 0)
        ini_set("default_socket_timeout", $timeout);
    }

    /**
     * Creates a soap client instance
     */
    protected function initSoapClient()
    {
      if (!is_null($this->client))
        return true;

      // Avoid fatal errors in SOAPClient
      $addr = parse_url($this->url);
      if ($addr == null)
        throw new Zend_Exception('Webservice Api: Cannot resolve URL "' . $this->url . '"');

      if ($addr['host'] === gethostbyname($addr['host']))
        throw new Zend_Exception('Webservice Api: Cannot resolve host "' . $addr['host'] . '"');

      $this->client = new SOAPClient($this->url, $this->options);
      return true;
    }

    /**
     * Returns a initlilized soap client
     *
     * @return SOAPClient
     */
    protected function getSoapClient()
    {
      if (false == $this->initSoapClient())
        return null;

      return $this->client;
    }

    /**
     * Adds a new panelist
     *
     * @param $data
     * @param $identifier
     */
    public function addNewPanelist(array $data, $identifier = 'pcode')
    {
      $this->initSoapClient();

      /** @noinspection PhpUndefinedMethodInspection */
      return $this->client->add_panelist($data, $identifier);
    }

    /**
     * Adds the given panelist to the given survey.
     * $data[ident], $data[identValue] $data[reset], $data[syid]
     *
     * @param array $data
     *
     * @return \stdClass
     */
    public function addPanelistToSurvey(array $data)
    {
      $this->initSoapClient();

      $defaultConf = array(
        'ident'      => 'u_email',
        'identValue' => '',
        'reset'      => 'reset_no_change',
        'syid'       => 0,
        '');
      $conf        = array_merge($defaultConf, $data);

      // Check needed parameter
      if ($conf['ident'] == '' || $conf['identValue'] == ''
          || $conf['reset'] == '' || $conf['syid'] == 0
      )
      {
        return new stdClass();
      }

      /** @noinspection PhpUndefinedMethodInspection */
      return $this->client->add_panelist_to_survey(
        $conf['ident'], $conf['identValue'], $conf['syid'], $conf['reset']);
    }

    /**
     * Returns the template with the given ID
     *
     * @param $id
     */
    public function getMailTemplate($id)
    {
      $this->initSoapClient();

      /** @noinspection PhpUndefinedMethodInspection */
      return $this->client->get_mail_template($id);
    }

    /**
     * getSurvey
     *
     * @param $pid
     *
     * @return mixed $surveyData
     */
    public function getSurvey($pid)
    {
      $this->initSoapClient();
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->client->get_survey((int)$pid);
    }

    /**
     * getSurveyExport
     *
     * @param mixed $arrParam
     *
     * @return $result
     */
    public function getSurveyExport($arrParam)
    {
      $this->initSoapClient();

      if (false == array_key_exists('export_format', $arrParam))
      {
        $arrParam['export_format'] = 'results';
      }
      if (false == array_key_exists('format', $arrParam))
      {
        $arrParam['format'] = 'csv';
      }
      if (false == array_key_exists('compression', $arrParam))
      {
        $arrParam['compression'] = 'none';
      }
      if (false == array_key_exists('template', $arrParam))
      {
        $arrParam['template'] = 'project_all';
      }

      /** @noinspection PhpUndefinedMethodInspection */
      return $this->client->get_survey_export(
        @array("survey_id"               => $arrParam["pid"],
               "sample_id"               => $arrParam["sample-id"],
               "language"                => $arrParam["language"],
               "compression"             => $arrParam["compression"],
               "skip_missings"           => $arrParam["skip_missings"],
               "restrict"                => $arrParam["restrict"],
               "anonymize"               => $arrParam["anonymize"],
               "kill_newlines"           => $arrParam["kill_newlines"],
               "subst_codes"             => $arrParam["subst_codes"],
               "use_metainfo"            => $arrParam["use_metainfo"],
               "shrink"                  => $arrParam["shrink"],
               "adjustwidths"            => $arrParam["adjustwidth"],
               "include_sysmissings"     => $arrParam["include_sysmissings"],
               "lfdn_from"               => $arrParam["lfdn_from"],
               "lfdn_to"                 => $arrParam["lfdn_to"],
               "protected"               => $arrParam["protected"],
               "dispositioncode"         => $arrParam["dispositioncode"],
               "export_format"           => $arrParam["export_format"],
               "format"                  => $arrParam["format"],
               "template"                => $arrParam["template"],
               "charset"                 => $arrParam["charset"],
               "missingvalue_text"       => $arrParam["missingvalue_text"],
               "missingvalue_text_empty" => $arrParam["missingvalue_text_empty"],
               "missingvalue_numeric"    => $arrParam["missingvalue_numeric"],
               "missingvalue_timestamp"  => $arrParam["missingvalue_timestamp"],
               "missingvalue_datetime"   => $arrParam["missingvalue_datetime"],
               "conjoint_selection"      => $arrParam["conjoint_selection"],
               "startdate"               => $arrParam["startdate"],
               "enddate"                 => $arrParam["enddate"],
               "varname_type"            => $arrParam["varname_type"],
               "skip_text_contents"      => $arrParam["skip_text_contents"],
               "panelgroup"              => $arrParam["panelgroup"],
               "panelstatus"             => $arrParam["panelstatus"],
               "return_gzcompressed"     => $arrParam["return_gzcompressed"],
               "skip_loop_vars"          => $arrParam["skip_loop_vars"]
        )
      );
    }

    /**
     * getSurveyData
     *
     * @param mixed $pid
     *
     * @return $result
     */
    public function getSurveyData($pid)
    {
      $arrParam["pid"]                     = $pid[0];
      $arrParam["compression"]             = "none";
      $arrParam["language"]                = "1";
      $arrParam["skip_missings"]           = false;
      $arrParam["anonymize"]               = true;
      $arrParam["kill_newlines"]           = true;
      $arrParam["include_sysmissings"]     = true;
      $arrParam["dispositioncode"]         = array("31,32");
      $arrParam["export_format"]           = "results";
      $arrParam["format"]                  = "csv";
      $arrParam["template"]                = "project_all";
      $arrParam["charset"]                 = "UTF-8";
      $arrParam["return_gzcompressed"]     = true;
      $arrParam["missingvalue_text"]       = "-66";
      $arrParam["missingvalue_text_empty"] = "-99";
      $arrParam["missingvalue_numeric"]    = "-77";
      $arrParam["missingvalue_timestamp"]  = "0";
      $arrParam["missingvalue_datetime"]   = "0000-00-00 00:00:00";
      $arrParam["skip_loop_vars"]          = true;

      return $this->getSurveyExport($arrParam);
    }

    /**
     * getSurveyLoopData
     *
     * @param mixed $pid
     *
     * @return $result
     */
    public function getSurveyLoopData($pid)
    {
      $arrParam["pid"]                 = $pid[0];
      $arrParam["export_format"]       = "raw_loopdata";
      $arrParam["format"]              = "csv";
      $arrParam["compression"]         = "none";
      $arrParam["template"]            = "project_all";
      $arrParam["language"]            = 1;
      $arrParam["return_gzcompressed"] = true;

      $arrParam["skip_missings"]       = false;
      $arrParam["anonymize"]           = true;
      $arrParam["kill_newlines"]       = true;
      $arrParam["include_sysmissings"] = true;
      $arrParam["dispositioncode"]     = array("31,32");
      $arrParam["charset"]             = "UTF-8";

      return $this->getSurveyExport($arrParam);
    }

    /**
     * getSurveyVariableLabel
     *
     * @param mixed $pid
     *
     * @return $result
     */
    public function getSurveyVariableLabel($pid)
    {
      $arrParam["pid"]                 = $pid[0];
      $arrParam["compression"]         = "none";
      $arrParam["skip_missings"]       = false;
      $arrParam["anonymize"]           = true;
      $arrParam["kill_newlines"]       = true;
      $arrParam["include_sysmissings"] = true;
      $arrParam["dispositionscode"]    = "31,32";
      $arrParam["export_format"]       = "results";
      $arrParam["format"]              = "sss";
      $arrParam["template"]            = "project_all";
      $arrParam["charset"]             = "UTF-8";
      $arrParam["return_gzcompressed"] = true;

      return $this->getSurveyExport($arrParam);
    }

    /**
     * getSurveyVariableLabel
     *
     * @param mixed $pid
     * @param bool  $user
     *
     * @return bool $result
     */
    public function getSurveyVariables($pid, $user = false)
    {
      if ($pid < 1)
        return false;

      $this->initSoapClient();

      /** @noinspection PhpUndefinedMethodInspection */
      return $this->client->get_survey_variables($pid, $user);
    }

    /**
     * getServerTime
     *
     * @param string $format
     *
     * @return $result
     */
    public function getServerTime($format = 'timestamp')
    {
      $this->initSoapClient();
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->client->get_server_time($format);
    }

    /**
     * getServerLoad
     */
    public function getServerLoad()
    {
      $this->initSoapClient();
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->client->get_server_load();
    }

    /**
     * getMasterdataStructure
     */
    public function getMasterdataStructure()
    {
      $this->initSoapClient();
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->client->get_masterdata_structure();
    }

    /**
     * importCsv
     *
     * @param $syid
     * @param $data
     * @param $ident
     * @param $options
     *
     * @return boolean
     */
    public function importCsv($syid, $data, $ident, $options)
    {
      if (!is_integer($syid)
          || !is_string($data)
          || !is_string($ident)
          || !is_array($options)
      )
        return false;

      $this->initSoapClient();

      /** @noinspection PhpUndefinedMethodInspection */
      return $this->client->import_csv(
        $syid,
        $data,
        $ident,
        $options);
    }

    /**
     * sendMailSurveyId
     *
     * @param $syid
     * @param $template
     * @param $ident
     */
    public function sendMailSurveyId($syid, $template, $ident)
    {
      $this->initSoapClient();
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->client->send_mail_survey_id($syid, $template, $ident);
    }

    /**
     * Returns the results of an EFS survey as xml
     *
     * DOCME describe possible arguments
     *
     * @param array $args       webservice arguments
     * @param bool  $compressed (optional) if true
     *
     * @throws Exception
     * @return string survey results
     */
    public function getSurveyResultsXml(array $args, $compressed = true)
    {
      $defaultArgs = array(
        'surveyId'         => 0,
        'withUserData'     => false,
        'dispositionCodes' => null,
        'includeVarnames'  => null,
      );
      $args        = array_merge($defaultArgs, $args);

      // SurveyID is as must as it cannot be clever default setting
      if ($args['surveyId'] == 0)
      {
        throw new Exception(
          '$args["surveyId"] must be set in "' . __FUNCTION__ . '"');
      }
      $this->initSoapClient();

      /** @noinspection PhpUndefinedMethodInspection */
      $result = $this->client->get_survey_results_xml(
        $args['surveyId'],
        $args['withUserData'],
        $args['dispositionCodes'],
        $args['includeVarnames']);

      if ($compressed == true)
      {
        return $result;
      }

      return gzuncompress($result);
    }

    /**
     * getPanelistByCondition
     *
     * @param string $ident - identifier for the panelist
     * @param string $cond  - condition to find the panelist(s)
     *
     * @throws Exception
     * @return
     */
    public function getPanelistByCondition($ident, $cond)
    {
      if (Pct_Efs_Version::getVersion() < 7.1)
      {
        throw new Exception('Method available since EFS 7.1 only');
      }

      $this->initSoapClient();
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->client->get_panelists_by_condition(
        $ident, $cond);
    }

    /**
     * Returns a list of surveys
     *
     * The result can be restricted to specific survey types (e.g. panel or
     * masterdata surveys) and survey statuses (e.g. active, inactive, archived)
     *
     * Only surveys that the user has read privileges on will be returned.
     *
     * @param array $surveyTypes    (optional) List of survey types
     * @param array $surveyStatuses (optional) List of survey statuses
     */
    public function getSurveysList(array $surveyTypes = null, array $surveyStatuses = null)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_surveys_list($surveyTypes, $surveyStatuses);
    }

    /**
     * Returns profiles of all survey participants
     *
     * @param int   $survey_id
     * @param array $surveyFields
     * @param bool  $includeResults
     */
    public function getSurveyParticipants($survey_id, array $surveyFields, $includeResults = false)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_survey_participants($survey_id, $surveyFields, $includeResults);
    }

    /**
     * isValidPanelistLogin
     *
     * @param array $ident
     *
     * @return boolean
     */
    public function isValidPanelistLogin(array $ident)
    {
      $this->initSoapClient();

      $data['login_type'] = 'login_type_auto_detect';
      $data               = array_merge($data, $ident);

      /** @noinspection PhpUndefinedMethodInspection */
      return (bool)$this->client->is_valid_panelist_login($data['login_type'], $data['login_value1'], $data['login_value2']);
    }

    /**
     * Updates the panelist via Ws instead of panel-api directly
     *
     * @param $options - ident, value, record
     */
    public function updatePanelistWs($options)
    {
      $this->initSoapClient();
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->client->update_panelist($options['ident'], $options['value'], $options['record']);
    }

    /**
     * Returns a list of all panelists for a given group filter.
     *
     * The panelist identifier for the returned panelists can be user_id,
     * panelist code, pseudonym etc.
     *
     * @param string $identifierType Panelist identifier type
     * @param int    $filterId       Filter id
     *
     * @return array Panelists identifiers
     */
    public function getPanelistsByFilter($identifierType, $filterId)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_panelists_by_filter($identifierType, $filterId);
    }

    /**
     * Returns the full profile for a panelist.
     *
     * participant data, behavioural information plus master data
     * The panelist can be identified by different criteria (email, user_id,
     * panelist code, pseudonym etc.).
     *
     * @param string $identifierType  Panelist identifier type
     * @param mixed  $identifierValue Panelist identifier value
     *
     * @throws InvalidArgumentException
     * @return array Panelist record
     */
    public function getPanelist($identifierType, $identifierValue = null)
    {
      if (!is_null($identifierValue))
      {
        if (!is_string($identifierType))
        {
          throw new InvalidArgumentException('Wrong type for indentifier type, has to be a string of identifier value is passed.');
        }

        /** @noinspection PhpUndefinedMethodInspection */
        return $this->getSoapClient()->get_panelist($identifierType, $identifierValue);
      }

      // old style param usage introduced by M.Umschlag

      if (!is_array($identifierType))
        throw new InvalidArgumentException('When omitting the second param first param has to be an associative array.');

      if (!isset($identifierType['ident']))
        throw new InvalidArgumentException('No ident given.');

      if (!isset($identifierType['value']))
        throw new InvalidArgumentException('No value given.');

      $identifierValue = $identifierType['value'];
      $identifierType  = $identifierType['ident'];

      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_panelist($identifierType, $identifierValue);
    }

    /**
     * createAdminSession
     * Creates an admin session ID for the specified staff account (root usage only).
     *
     * @param $identifier_type
     * @param $identifier_value
     *
     * @return
     * @internal param string $identifierType Panelist identifier type
     * @internal param mixed $identifierValue Panelist identifier value
     */
    public function createAdminSession($identifier_type, $identifier_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->create_admin_session($identifier_type, $identifier_value);
    }

    /**
     * addAdmin
     * Adds an admin area staff account.
     *
     * @param array  $adminRecord
     * @param string $returnType
     */
    public function addAdmin($adminRecord, $returnType)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->add_admin($adminRecord, $returnType);
    }

    /**
     * addAdminGroup
     * Adds a staff team.
     *
     * @param array $adminGroupRecord
     * @param int   $rightTemplateId
     */
    public function addAdminGroup($adminGroupRecord, $rightTemplateId)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->add_admin_group($adminGroupRecord, $rightTemplateId);
    }

    /**
     * addAdminOrg
     * Adds an organization.
     *
     * @param array $adminOrgRecord
     */
    public function addAdminOrg($adminOrgRecord)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->add_admin_org($adminOrgRecord);
    }

    /**
     * addAdminToGroup
     * Adds a staff member to a team.
     *
     * @param string  $adminUserIdentifierType
     * @param string  $adminIdentifierValue
     * @param integer $adminGroupIdentifierValue
     * @param string  $userRole
     */
    public function addAdminToGroup($adminUserIdentifierType, $adminIdentifierValue, $adminGroupIdentifierValue, $userRole)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->add_admin_to_group($adminUserIdentifierType, $adminIdentifierValue, $adminGroupIdentifierValue, $userRole);
    }

    /**
     * addAdminToOrg
     * Adds a staff member to an organization.
     *
     * @param string  $admin_user_identifier_type
     * @param string  $admin_identifier_value
     * @param integer $admin_org_identifier_value
     */
    public function addAdminToOrg($admin_user_identifier_type, $admin_identifier_value, $admin_org_identifier_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->add_admin_to_org($admin_user_identifier_type, $admin_identifier_value, $admin_org_identifier_value);
    }

    /**
     * addExternalSurveyParticipants
     * Adds or updates the participation status of multiple participants in an external study.
     *
     * @param string $external_survey_identifier_type
     * @param string $external_survey_identifier_value
     * @param string $panelist_identifier_type
     * @param array  $external_survey_participations
     */
    public function addExternalSurveyParticipants($external_survey_identifier_type, $external_survey_identifier_value, $panelist_identifier_type, $external_survey_participations)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->add_external_survey_participants($external_survey_identifier_type, $external_survey_identifier_value, $panelist_identifier_type, $external_survey_participations);
    }

    /**
     * alterUserGroupRole
     * Alters a staff members role within a given team.
     *
     * @param integer $uid
     * @param integer $gid
     * @param string  $new_role
     * @param integer $new_owner_uid
     */
    public function alterUserGroupRole($uid, $gid, $new_role, $new_owner_uid)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->alter_user_group_role($uid, $gid, $new_role, $new_owner_uid);
    }

    /**
     * createExternalSurvey
     * Creates an external study in EFS.
     *
     * @param string  $external_survey_id
     * @param string  $external_survey_title
     * @param integer $external_survey_type
     * @param array   $external_survey_attributes
     */
    public function createExternalSurvey($external_survey_id, $external_survey_title, $external_survey_type, $external_survey_attributes)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->create_external_survey($external_survey_id, $external_survey_title, $external_survey_type, $external_survey_attributes);
    }

    /**
     * createReportSession
     * Creates a report session ID for the specified staff account (root usage only).
     *
     * @param string $admin_user_identifier_type
     * @param string $admin_user_identifier_value
     */
    public function createReportSession($admin_user_identifier_type, $admin_user_identifier_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->create_report_session($admin_user_identifier_type, $admin_user_identifier_value);
    }

    /**
     * createSurvey
     * Creates a new survey.
     *
     * @param array $survey
     */
    public function createSurvey($survey)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->create_survey($survey);
    }

    /**
     * deleteAdminOrg
     * Deletes an organization.
     *
     * @param integer $admin_org_identifier_value
     */
    public function deleteAdminOrg($admin_org_identifier_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->delete_admin_org($admin_org_identifier_value);
    }

    /**
     * deleteExternalPanelistAuthentication
     * Deletes an external authentication mapping from EFS. Please mind: Only the mapping will be deleted. The panelist will not be removed from EFS.
     *
     * @param integer $external_service_id
     * @param string  $external_authentication_value
     */
    public function deleteExternalPanelistAuthentication($external_service_id, $external_authentication_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->delete_external_panelist_authentication($external_service_id, $external_authentication_value);
    }

    /**
     * deleteExternalSurvey
     * Deletes an external study with all its participation data.
     *
     * @param string $external_survey_identifier_type
     * @param string $external_survey_identifier_value
     */
    public function deleteExternalSurvey($external_survey_identifier_type, $external_survey_identifier_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->delete_external_survey($external_survey_identifier_type, $external_survey_identifier_value);
    }

    /**
     * deletePanelist
     * Deletes a panelist (i.e. sets the panelist to status deleted).
     *
     * @param string $panelist_identifier_type
     * @param string $panelist_identifier_value
     */
    public function deletePanelist($panelist_identifier_type, $panelist_identifier_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->delete_panelist($panelist_identifier_type, $panelist_identifier_value);
    }

    /**
     * deletePanelistFromGroup
     * Deletes a panelist from a panel group.
     *
     * @param string  $panelist_identifier_type
     * @param string  $panelist_identifier_value
     * @param integer $group_id
     */
    public function deletePanelistFromGroup($panelist_identifier_type, $panelist_identifier_value, $group_id)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->delete_panelist_from_group($panelist_identifier_type, $panelist_identifier_value, $group_id);
    }

    /**
     * disableAdminUser
     * Disables a staff member account.
     *
     * @param string $admin_user_identifier_type
     * @param string $admin_identifier_value
     */
    public function disableAdminUser($admin_user_identifier_type, $admin_identifier_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->disable_admin_user($admin_user_identifier_type, $admin_identifier_value);
    }

    /**
     * enableAdminUser
     * Enables a staff member account account.
     *
     * @param $admin_user_identifier_type
     * @param $admin_identifier_value
     */
    public function enableAdminUser($admin_user_identifier_type, $admin_identifier_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->enable_admin_user($admin_user_identifier_type, $admin_identifier_value);
    }

    /**
     * generateSurvey
     * Compiles the survey. Creates all missing variable names and resets the participants if requested.
     *
     * @param integer $survey_id
     * @param string  $generate_survey_type
     */
    public function generateSurvey($survey_id, $generate_survey_type)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->generate_survey($survey_id, $generate_survey_type);
    }

    /**
     * getAdminGroup
     * Returns a staff team.
     *
     * @param integer $admin_group_identifier_value
     */
    public function getAdminGroup($admin_group_identifier_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_admin_group($admin_group_identifier_value);
    }

    /**
     * getAdminGroupList
     * Returns a list of teams specified by a search value.
     *
     * @param string  $admin_group_field
     * @param string  $admin_group_value
     * @param integer $page
     * @param integer $limit
     */
    public function getAdminGroupList($admin_group_field, $admin_group_value, $page, $limit)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_admin_group_list($admin_group_field, $admin_group_value, $page, $limit);
    }

    /**
     * getAdminList
     * Returns a list of admin area staff accounts specified by a search value.
     *
     * @param string  $admin_field
     * @param string  $admin_value
     * @param integer $page
     * @param integer $limit
     */
    public function getAdminList($admin_field, $admin_value, $page, $limit)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_admin_list($admin_field, $admin_value, $page, $limit);
    }

    /**
     * getAdminOrg
     * Returns an organization.
     *
     * @param integer $admin_org_identifier_value
     */
    public function getAdminOrg($admin_org_identifier_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_admin_org($admin_org_identifier_value);
    }

    /**
     * getAdminOrgList
     * Returns a list of organizations specified by a search value.
     *
     * @param string  $admin_org_field
     * @param string  $admin_org_value
     * @param integer $page
     * @param integer $limit
     */
    public function getAdminOrgList($admin_org_field, $admin_org_value, $page, $limit)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_admin_org_list($admin_org_field, $admin_org_value, $page, $limit);
    }

    /**
     * getCbContent
     * Returns the codebook structure for the specified EFS survey.
     *
     * @param integer $survey_id
     */
    public function getCbContent($survey_id)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_cb_content($survey_id);
    }

    /**
     * getExternalPanelistAuthentication
     * Returns the EFS identifier for a member of an external system. If the member has not been registered in EFS using the save_external_panelist_authentication web service before, this service will throw an exception.
     *
     * @param integer $external_service_id
     * @param string  $external_authentication_value
     * @param string  $panelist_identifier_return_type
     */
    public function getExternalPanelistAuthentication($external_service_id, $external_authentication_value, $panelist_identifier_return_type)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_external_panelist_authentication($external_service_id, $external_authentication_value, $panelist_identifier_return_type);
    }

    /**
     * getExternalSurvey
     * Retrieves the data for an external study.
     *
     * @param string $external_survey_identifier_type
     * @param string $external_survey_identifier_value
     */
    public function getExternalSurvey($external_survey_identifier_type, $external_survey_identifier_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_external_survey($external_survey_identifier_type, $external_survey_identifier_value);
    }

    /**
     * getExternalSurveys
     * Retrieves a list of all external studies.
     *
     * @param string $external_survey_identifier_return_type
     */
    public function getExternalSurveys($external_survey_identifier_return_type)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_external_surveys($external_survey_identifier_return_type);
    }

    /**
     * getFrpCountAllCodes
     * Returns the absolute number of participations for all disposition codes.
     *
     * @param int   $survey_id
     * @param array $restrict
     */
    public function getFrpCountAllCodes($survey_id, $restrict)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_frp_count_all_codes($survey_id, $restrict);
    }

    /**
     * getFrpCountByCode
     * Returns the absolute number of participations by disposition code.
     *
     * @param integer $survey_id
     * @param integer $disposition_code
     * @param array   $restrict
     */
    public function getFrpCountByCode($survey_id, $disposition_code, $restrict)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_frp_count_by_code($survey_id, $disposition_code, $restrict);
    }

    /**
     * getFrpCountByCodeSplit
     * Returns the absolute number of participations by disposition code split by variable.
     *
     * @param integer $survey_id
     * @param integer $disposition_code
     * @param string  $split_var
     * @param array   $restrict
     */
    public function getFrpCountByCodeSplit($survey_id, $disposition_code, $split_var, $restrict)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_frp_count_by_code_split($survey_id, $disposition_code, $split_var, $restrict);
    }

    /**
     * getFrpCountByMulticode
     * Returns the absolute number of participations by disposition code (array).
     *
     * @param integer $survey_id
     * @param array   $disposition_codes
     * @param array   $restrict
     */
    public function getFrpCountByMulticode($survey_id, $disposition_codes, $restrict)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_frp_count_by_multicode($survey_id, $disposition_codes, $restrict);
    }

    /**
     * getFrpCountByMulticodeSplit
     * Returns the absolute number of participations by disposition codes split by variable.
     *
     * @param integer $survey_id
     * @param integer $disposition_code
     * @param string  $split_var
     * @param array   $restrict
     */
    public function getFrpCountByMulticodeSplit($survey_id, $disposition_code, $split_var, $restrict)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_frp_count_by_multicode_split($survey_id, $disposition_code, $split_var, $restrict);
    }

    /**
     * getFrpGraph
     * Returns the png graph which represents the participation.
     *
     * @param integer $survey_id
     * @param integer $lang
     * @param array   $restrict
     */
    public function getFrpGraph($survey_id, $lang, $restrict)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_frp_graph($survey_id, $lang, $restrict);
    }

    /**
     * getFrpGross1
     * Returns the total sample (Gross 1) for the project.
     *
     * @param integer $survey_id
     * @param array   $restrict
     */
    public function getFrpGross1($survey_id, $restrict)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_frp_gross_1($survey_id, $restrict);
    }

    /**
     * getFrpGross1Split
     * Returns the total sample (Gross 1) split by variable.
     *
     * @param integer $survey_id
     * @param string  $split_var
     * @param array   $restrict
     */
    public function getFrpGross1Split($survey_id, $split_var, $restrict)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_frp_gross_1_split($survey_id, $split_var, $restrict);
    }

    /**
     * getFrpGross2
     * Returns the adjusted total sample (Gross 2) for the project.
     *
     * @param integer $survey_id
     * @param array   $restrict
     */
    public function getFrpGross2($survey_id, $restrict)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_frp_gross_2($survey_id, $restrict);
    }

    /**
     * getFrpGross2Split
     * Returns the adjusted total sample (Gross 2) split by variable.
     *
     * @param integer $survey_id
     * @param string  $split_var
     * @param array   $restrict
     */
    public function getFrpGross2Split($survey_id, $split_var, $restrict)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_frp_gross_2_split($survey_id, $split_var, $restrict);
    }

    /**
     * getFrpNet
     * Returns the net participation for the project.
     *
     * @param integer $survey_id
     * @param array   $restrict
     */
    public function getFrpNet($survey_id, $restrict)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_frp_net($survey_id, $restrict);
    }

    /**
     * getFrpNetSplit
     * Returns the net participation split by variable.
     *
     * @param integer $survey_id
     * @param string  $split_var
     * @param array   $restrict
     */
    public function getFrpNetSplit($survey_id, $split_var, $restrict)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_frp_net_split($survey_id, $split_var, $restrict);
    }

    /**
     * getMasterdataVariables
     * Returns the names and labels of all master data variables for the EFS Panel installation. The master data variables are global for the whole installation and not specific for any panelist.
     *
     * @param array $record
     */
    public function getMasterdataVariables($record)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_masterdata_variables($record);
    }

    /**
     * getModifiedExternalSurveys
     * Retrieves all external studies that were modified since a given timestamp.
     *
     * @param string   $external_survey_identifier_return_type
     * @param datetime $timestamp
     */
    public function getModifiedExternalSurveys($external_survey_identifier_return_type, $timestamp)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_modified_external_surveys($external_survey_identifier_return_type, $timestamp);
    }

    /**
     * getModifiedPanelists
     * Returns the identifiers of all panelists whose data (participant data plus master data) has changed since this given timestamp. The return identifier type (email, user_id, panelist code, pseudonym etc.) can be specified.
     *
     * @param string   $panelist_identifier_type
     * @param datetime $timestamp
     * @param integer  $exclude_modifier
     */
    public function getModifiedPanelists($panelist_identifier_type, $timestamp, $exclude_modifier)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_modified_panelists($panelist_identifier_type, $timestamp, $exclude_modifier);
    }

    /**
     * getOspeTimeLogResults
     * Returns a performance data struct for a survey.
     *
     * @param integer $survey_id
     */
    public function getOspeTimeLogResults($survey_id)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_ospe_time_log_results($survey_id);
    }

    /**
     * getPanelComposition
     * Returns the current panel composition (number of panelists by panel status).
     */
    public function getPanelComposition()
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_panel_composition();
    }

    /**
     * getPanelistCompletedSurveys
     * Returns the list of EFS Panel surveys (surveys of type panel survey or master data survey) a panelist has completed so far (complete survey history of the panelist). The panelist can be identified by different criteria (email, user_id, panelist code, pseudonym etc.).
     *
     * @param string $panelist_identifier_type
     * @param string $panelist_identifier_value
     */
    public function getPanelistCompletedSurveys($panelist_identifier_type, $panelist_identifier_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_panelist_completed_surveys($panelist_identifier_type, $panelist_identifier_value);
    }

    /**
     * getPanelistGroupMembership
     * Returns group membership information for a panelist.
     *
     * @param string $panelist_identifier_type
     * @param string $panelist_identifier_value
     */
    public function getPanelistGroupMembership($panelist_identifier_type, $panelist_identifier_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_panelist_group_membership($panelist_identifier_type, $panelist_identifier_value);
    }

    /**
     * getPanelistPassword
     * Returns the panel login password of a panelist.
     *
     * @param string $panelist_identifier_type
     * @param string $panelist_identifier_value
     */
    public function getPanelistPassword($panelist_identifier_type, $panelist_identifier_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_panelist_password($panelist_identifier_type, $panelist_identifier_value);
    }

    /**
     * getPanelistPointsHistory
     * Returns the account balance/bonus points positions for a panelist that happened in the last months. The panelist can be identified by different criteria (user_id, panelist code, pseudonym etc.).
     *
     * @param string  $panelist_identifier_type
     * @param string  $panelist_identifier_value
     * @param integer $months_range
     * @param boolean $skip_empty
     */
    public function getPanelistPointsHistory($panelist_identifier_type, $panelist_identifier_value, $months_range, $skip_empty)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_panelist_points_history($panelist_identifier_type, $panelist_identifier_value, $months_range, $skip_empty);
    }

    /**
     * getPanelistSurveysList
     * Returns the current list of active available EFS Panel surveys (surveys of type panel survey or master data survey) for a panelist. The panelist can be identified by different criteria (email, user_id, panelist code, pseudonym etc.).
     *
     * @param string $panelist_identifier_type
     * @param string $panelist_identifier_value
     */
    public function getPanelistSurveysList($panelist_identifier_type, $panelist_identifier_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_panelist_surveys_list($panelist_identifier_type, $panelist_identifier_value);
    }

    /**
     * getPanelistsOnline
     * Returns the panelists who are online on the website. Panelists are returned as an array of e-mail addresses, pseudonyms, panelist codes etc. together with the language ID of the website they are active on. A panelist might be active in multiple languages at the same time (e.g. by viewing the English version of the website and then changing to the French version). In this case, the panelist will be returned twice in the result array, but with different language IDs each. The interval for counting as being online can be specified in the global configuration of the website in the EFS admin area.
     *
     * @param string $panelist_identifier_return_type
     * @param array  $language_ids
     */
    public function getPanelistsOnline($panelist_identifier_return_type, $language_ids)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_panelists_online($panelist_identifier_return_type, $language_ids);
    }

    /**
     * getPointsStatus
     * Returns the account balance/bonus points positions for all panelists of the installation that happened since the last call to the service. This method can be used to do a regular, incremental replication of the panel's bonus points to another application (where EFS is used as the master system for managing the points). The panelist identifier for the returned balance positions can be user_id, panelist code, pseudonym etc.
     *
     * @param string  $panelist_identifier_type
     * @param boolean $fetch_all
     * @param boolean $log_transfer
     */
    public function getPointsStatus($panelist_identifier_type, $fetch_all, $log_transfer)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_points_status($panelist_identifier_type, $fetch_all, $log_transfer);
    }

    /**
     * getPromotionalStatusForEmailAddress
     * Returns the status of the e-mail address in the panel promotional system (tell-a-friend campaign system). This service can be used to check whether an e-mail address has already been entered into the promotional system and what its status is (invited only or already converted into a panelist).
     *
     * @param string $email
     */
    public function getPromotionalStatusForEmailAddress($email)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_promotional_status_for_email_address($email);
    }

    /**
     * getRandomAztecGod
     * Returns a random aztec god. Unleashes its power every sunday at 2 am when printed on blue paper. Please note that some gods may unleash evil powers!
     */
    public function getRandomAztecGod()
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_random_aztec_god();
    }

    /**
     * getRedemptionsStatus
     * Returns the bonus points redemption positions for all panelists of the installation that happened since the last call to the service. This method can be used to do a regular, incremental replication of the panel's bonus points redemptions to another application (where EFS is used as the master system for managing the points). The panelist identifier for the returned redemptions can be user_id, panelist code, pseudonym etc.
     *
     * @param string  $panelist_identifier_type
     * @param boolean $fetch_all
     * @param boolean $log_transfer
     */
    public function getRedemptionsStatus($panelist_identifier_type, $fetch_all, $log_transfer)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_redemptions_status($panelist_identifier_type, $fetch_all, $log_transfer);
    }

    /**
     * getWsdlVersions
     * Returns the available WSDL versions on the installation.
     */
    public function getWsdlVersions()
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_wsdl_versions();
    }

    /**
     * modifyPanelistPassword
     * Updates the panel login password of a panelist.
     *
     * @param string $panelist_identifier_type
     * @param string $panelist_identifier_value
     * @param string $panelist_password
     */
    public function modifyPanelistPassword($panelist_identifier_type, $panelist_identifier_value, $panelist_password)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->modify_panelist_password($panelist_identifier_type, $panelist_identifier_value, $panelist_password);
    }

    /**
     * modifyPanelistPoints
     * Adds or subtracts bonus points for a panelist's account. The panelist can be identified by different criteria (user_id, panelist code, pseudonym etc.). The transaction will be shown in the panelists' account balance history afterwards.
     *
     * @param string $panelist_identifier_type
     * @param string $panelist_identifier_value
     * @param float  $points_value
     * @param string $points_reason
     */
    public function modifyPanelistPoints($panelist_identifier_type, $panelist_identifier_value, $points_value, $points_reason)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->modify_panelist_points($panelist_identifier_type, $panelist_identifier_value, $points_value, $points_reason);
    }

    /**
     * resetOspeTimeLog
     * Resets all performance data used when accessing the OSPE time log.
     *
     * @param integer $survey_id
     */
    public function resetOspeTimeLog($survey_id)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->reset_ospe_time_log($survey_id);
    }

    /**
     * resetSurveyParticipation
     * Resets a survey participation for one participant. Existing survey data can be kept or deleted.
     *
     * @param string  $identifier_type
     * @param string  $identifier_value
     * @param integer $survey_id
     * @param string  $reset_type
     */
    public function resetSurveyParticipation($identifier_type, $identifier_value, $survey_id, $reset_type)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->reset_survey_participation($identifier_type, $identifier_value, $survey_id, $reset_type);
    }

    /**
     * saveExternalPanelistAuthentication
     * Establishes a mapping between a member of an external system (specified by the external_service_id and the external_service_authentication_value) and an EFS panelist. This can be used to implement seamless logins for EFS panelists coming from external web applications.
     *
     * @param integer $external_service_id
     * @param string  $external_authentication_value
     * @param string  $panelist_identifier_type
     * @param string  $panelist_identifier_value
     */
    public function saveExternalPanelistAuthentication($external_service_id, $external_authentication_value, $panelist_identifier_type, $panelist_identifier_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->save_external_panelist_authentication($external_service_id, $external_authentication_value, $panelist_identifier_type, $panelist_identifier_value);
    }

    /**
     * sendMailSampleId
     * Sends e-mails by a panel sample ID. Participant data and master data wildcards in the mail template will be dynamically replaced for each participant. Optionally you may restrict to specific participants by specifying participant identifiers. An empty array will mail everybody.
     *
     * @param integer $sample_id
     * @param array   $mail_template
     * @param array   $participant_identifiers
     */
    public function sendMailSampleId($sample_id, $mail_template, $participant_identifiers)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->send_mail_sample_id($sample_id, $mail_template, $participant_identifiers);
    }

    /**
     * sendMailSampleIdDispcodes
     * Sends e-mails to participants by sample ID and disposition code. See send_mail_sample_id. Only participants with one of the specified disposition codes will be mailed.
     *
     * @param integer $sample_id
     * @param array   $mail_template
     * @param array   $mail_disposition_codes
     */
    public function sendMailSampleIdDispcodes($sample_id, $mail_template, $mail_disposition_codes)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->send_mail_sample_id_dispcodes($sample_id, $mail_template, $mail_disposition_codes);
    }

    /**
     * sendMailSampleIdIncompletes
     * Convenience function to send a mail to all participants who didn't complete the survey yet (disposition codes 11,12,20,21,22,23). See send_mail_sample_id.
     *
     * @param integer $sample_id
     * @param array   $mail_template
     * @param array   $participant_identifiers
     */
    public function sendMailSampleIdIncompletes($sample_id, $mail_template, $participant_identifiers)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->send_mail_sample_id_incompletes($sample_id, $mail_template, $participant_identifiers);
    }

    /**
     * sendMailSampleIdUnaccessed
     * Convenience function to send a mail to all participants who didn't access the survey yet (disposition codes 11,12). See send_mail_sample_id.
     *
     * @param integer $sample_id
     * @param array   $mail_template
     * @param array   $participant_identifiers
     */
    public function sendMailSampleIdUnaccessed($sample_id, $mail_template, $participant_identifiers)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->send_mail_sample_id_unaccessed($sample_id, $mail_template, $participant_identifiers);
    }

    /**
     * sendMailSurveyIdDispcodes
     * Sends e-mails to users by survey ID and disposition code. See send_mail_survey_id. Only participants with one of the specified disposition codes will be mailed.
     *
     * @param integer $survey_id
     * @param array   $mail_template
     * @param array   $mail_disposition_codes
     */
    public function sendMailSurveyIdDispcodes($survey_id, $mail_template, $mail_disposition_codes)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->send_mail_survey_id_dispcodes($survey_id, $mail_template, $mail_disposition_codes);
    }

    /**
     * sendMailSurveyIdIncompletes
     * Convenience function to send a mail to all participants who didn't complete the survey yet (disposition codes 11,12,20,21,22,23). See send_mail_survey_id.
     *
     * @param integer $survey_id
     * @param array   $mail_template
     * @param array   $participant_identifiers
     */
    public function sendMailSurveyIdIncompletes($survey_id, $mail_template, $participant_identifiers)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->send_mail_survey_id_incompletes($survey_id, $mail_template, $participant_identifiers);
    }

    /**
     * sendMailSurveyIdUnaccessed
     *
     * @param integer $survey_id
     * @param array   $mail_template
     * @param array   $participant_identifiers
     */
    public function sendMailSurveyIdUnaccessed($survey_id, $mail_template, $participant_identifiers)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->send_mail_survey_id_unaccessed($survey_id, $mail_template, $participant_identifiers);
    }

    /**
     * sendMailToAdmin
     * Sends an e-mail to an administrator.
     *
     * @param string $admin_identifier_type
     * @param string $admin_identifier_value
     * @param array  $mail_template
     */
    public function sendMailToAdmin($admin_identifier_type, $admin_identifier_value, $mail_template)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->send_mail_to_admin($admin_identifier_type, $admin_identifier_value, $mail_template);
    }

    /**
     * setAdminGroupAclTemplate
     * Sets a teams rights to a predefined template.
     *
     * @param integer $admin_group_identifier_value
     * @param integer $acl_template_identifier_value
     */
    public function setAdminGroupAclTemplate($admin_group_identifier_value, $acl_template_identifier_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->set_admin_group_acl_template($admin_group_identifier_value, $acl_template_identifier_value);
    }

    /**
     * setAdminOrgValue
     * Sets an organization's value.
     *
     * @param integer $admin_org_identifier_value
     * @param string  $admin_org_field
     * @param string  $admin_org_value
     */
    public function setAdminOrgValue($admin_org_identifier_value, $admin_org_field, $admin_org_value)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->set_admin_org_value($admin_org_identifier_value, $admin_org_field, $admin_org_value);
    }

    /**
     * setUserGroupObjectRight
     * Set read|write privileges for a system object.
     *
     * @param integer $gid
     * @param integer $oid
     * @param integer $right
     */
    public function setUserGroupObjectRight($gid, $oid, $right)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->set_user_group_object_right($gid, $oid, $right);
    }

    /**
     * updateExternalSurvey
     * Updates an existing external study with new attributes. The survey's external ID cannot be changed.
     *
     * @param string  $external_survey_identifier_type
     * @param string  $external_survey_identifier_value
     * @param string  $external_survey_title
     * @param integer $external_survey_type
     * @param array   $external_survey_attributes
     */
    public function updateExternalSurvey($external_survey_identifier_type, $external_survey_identifier_value, $external_survey_title, $external_survey_type, $external_survey_attributes)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->update_external_survey($external_survey_identifier_type, $external_survey_identifier_value, $external_survey_title, $external_survey_type, $external_survey_attributes);
    }

    /**
     * getEfsVersion
     */
    public function getEfsVersion()
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->get_efs_version();
    }
  }