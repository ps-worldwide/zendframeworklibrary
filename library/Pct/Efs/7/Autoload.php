<?php
  /**
   * Pct_Efs_7_Autoload
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoload.php,v 1.9 2013/10/16 09:29:06 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Efs_7_Autoload implements Zend_Loader_Autoloader_Interface
  {
    protected $_efsBasePath = '';
    protected $_dictionary = array();
    protected $_efsVersionExtension = '7_';

    /**
     * __construct
     *
     * @param $efsBasePath
     */
    public function __construct($efsBasePath)
    {
      $this->_efsBasePath = realpath($efsBasePath);

      Zend_Loader::loadFile($this->_efsBasePath . '/wcp/framework/class_efs_autoload.inc.php', null, true);
      $efsLoader         = new ReflectionClass('efs_autoload');
      $properties        = $efsLoader->getDefaultProperties();
      $this->_dictionary = $properties['dictionary'];
    }

    /**
     * Searches for the given class in the EFS autoload dictionary or in one of EFS' modules.
     *
     * @param $class
     *
     * @return bool|mixed|string
     */
    public function autoload($class)
    {
      // Load efs_ stuff from PCT - should be erased sooner (or later).
      if (substr($class, 0, 4) == 'efs_')
      {
        if (substr($class, 0, 11) == 'efs_models_')
        {
          Zend_Loader::loadClass($class);
          return $class;
        }
        elseif (substr($class, 0, 9) == 'efs_apis_')
        {
          // If classname does not already contain certain version of efs-api
          // try to find one instead of the non-versioned
          if (0 == preg_match('/^[1-9]/', substr($class, 9, 1)))
          {

            // The filename for a possible version specific file
            $file = substr($class, 0, 9) . $this->_efsVersionExtension . substr($class, 9);
            $file = str_replace('_', DIRECTORY_SEPARATOR, $file) . '.php';

            // If version file exists load class from there
            if (Zend_Loader::isReadable($file))
              Zend_Loader::loadFile($file);
          }

          // if version file did not specify general class load that now
          if (false == class_exists($class))
            Zend_Loader::loadClass($class);

          return $class;
        }
      }

      // Try in EFS
      if (array_key_exists($class, $this->_dictionary))
      {
        Zend_Loader::loadFile($this->_efsBasePath . '/' . $this->_dictionary[$class]);
        return $class;
      }

      $file = $this->isModuleManager($class);
      if ($file !== false)
      {
        Zend_Loader::loadFile($file);
        return $class;
      }

      // Try EFS legacy
      $file = $this->isEfsLegacy($class);
      if ($file !== false)
      {
        Zend_Loader::loadFile($file);
        return $class;
      }

      return false;
    }

    /**
     * Returns the filename for the given classname if found in
     * EFS module directory
     *
     * @param $class
     *
     * @internal param string $filename
     * @return bool|string
     */
    protected function isModuleManager($class)
    {
      if (strpos($class, "/") !== false)
        return false;

      $classnameLower = strtolower($class);
      $partsLower     = explode("_", $classnameLower);
      $moduleLower    = array_shift($partsLower);

      $filename = $this->_efsBasePath . "/modules/" . $moduleLower . "/components/" . implode("/", $partsLower) . '.inc.php';
      return realpath($filename);
    }

    /**
     * isEfsLegacy
     *
     * @param $class
     *
     * @return boolean|string
     */
    protected function isEfsLegacy($class)
    {
      if (strpos($class, "/") !== false)
        return false;

      $classnameLower = strtolower($class);
      $partsLower     = explode("_", $classnameLower);
      $moduleLower    = array_shift($partsLower);

      $filename = $this->_efsBasePath . '/modules/' . $moduleLower . sprintf('/legacy/class_%s.inc.php', $classnameLower);
      return realpath($filename);
    }
  }
