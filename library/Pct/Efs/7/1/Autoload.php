<?php
  /**
   * Pct_Efs_7_1_Autoload
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoload.php,v 1.4 2013/10/16 09:29:05 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Efs_7_1_Autoload extends Pct_Efs_7_Autoload
  {
    protected $_efsVersionExtension = '7_1_';
  }