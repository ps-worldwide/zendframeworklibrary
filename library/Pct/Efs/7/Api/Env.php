<?php
  /**
   * Pct_Efs_7_Api_Env
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Env.php,v 1.8 2017/03/01 13:28:21 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  abstract class Pct_Efs_7_Api_Env
  {
    // The error-reporting settings
    protected static $errorReporting;

    // EFS environment should be loaded only once
    protected static $isInit = false;

    // The instance of the environment EFS loads
    protected static $efsEnv = null;

    /**
     * Inititates the environment needed to perform API calls
     */
    public static function init($version)
    {
      if (self::$isInit == true)
        return;

      // First we need an autoloader which loads classes from EFS
      //$loader = efs_models_autoload::getInstance();
      Pct_Efs_Autoloader::getInstance($version);

      // avoid silly notices from EFS
      self::setEfsErrorReporting();

      // Load a lots of EFS stuff
      /** @noinspection PhpDynamicAsStaticMethodCallInspection */
      self::$efsEnv = efs_apis_7_env::getInstance();

      // we want to see our own notices
      self::resetEfsErrorReporting();

      // Mark that EFS environment was initilized
      self::$isInit = true;
    }

    /**
     * Adjusts the PHP error-reporting to avoid notices etc reporting by the
     * EFS code
     *
     * @return null
     */
    public static function setEfsErrorReporting()
    {
      $errorReporting = ~E_NOTICE&~E_STRICT;
      if (version_compare(PHP_VERSION, '5.3.0', '>='))
      {
        $errorReporting = $errorReporting&~E_DEPRECATED;
      }
      self::$errorReporting = error_reporting($errorReporting);
    }

    /**
     * Adjusts the PHP error-reporting back to what was set before EFS was called
     *
     * @return null
     */
    public static function resetEfsErrorReporting()
    {
      // we want to see our own notices
      /** @noinspection PhpUndefinedFieldInspection */
      self::$errorReporting = error_reporting(self::$errorReporting);
    }
  }