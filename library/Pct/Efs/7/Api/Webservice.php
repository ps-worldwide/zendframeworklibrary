<?php
  /**
   * Pct_Efs_7_Api_Webservice
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Webservice.php,v 1.6 2013/10/16 09:29:05 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Efs_7_Api_Webservice extends Pct_Efs_6_Api_Webservice
  {
    /**
     * Adds a new panelist
     *
     * @param $data
     * @param $identifier
     */
    public function addNewPanelist(array $data, $identifier = 'pcode')
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->getSoapClient()->add_new_panelist($data, $identifier);
    }
  }