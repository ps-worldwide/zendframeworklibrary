<?php
/**
 * Pct_Efs_20_4_Api_Webservice  #EFSUPGRADE#
 *
 * @package   zendframeworkLibrary
 * @version   $Id: Webservice.php,v 1.1 2021/02/04 10:37:50 fiedler Exp $
 * @copyright (c) QuestBack http://www.questback.com
 * @author    $Author: fiedler $
 */

class Pct_Efs_20_4_Api_Webservice extends Pct_Efs_20_3_Api_Webservice #EFSUPGRADE#
{
}
