<?php
  /**
   * Pct_Efs_19_4_Api_Custom #EFSUPGRADE#
   * place wrappers for custom Service Layer methods here
   * NOTE: custom services need to be symlinked to htdocs/custom/service
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Custom.php,v 1.1 2019/10/17 12:12:44 khalafiniya Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: khalafiniya $
   */

  class Pct_Efs_19_4_Api_Custom extends Pct_Efs_19_3_Api_Servicelayer #EFSUPGRADE#
  {
  }