<?php

/**
 * Pct_Efs_19_5_Autoload #EFSUPGRADE#
 *
 * @package   zendframeworkLibrary
 * @version   $Id: Autoload.php,v 1.1 2020/01/23 15:28:19 fiedler Exp $
 * @copyright (c) QuestBack http://www.questback.com
 * @author    $Author: fiedler $
 */
class Pct_Efs_19_5_Autoload extends Pct_Efs_19_4_Autoload #EFSUPGRADE#
{
    protected $_efsVersionExtension = '19_5_'; #EFSUPGRADE#

    public function __construct($efsBasePath)
    {

        parent::__construct($efsBasePath);

    }

    /**
     * Searches for the given class in the EFS autoload dictionary or in one of EFS' modules.
     *
     * @param $class
     *
     * @return bool|mixed|string
     */
    public function autoload($class)
    {

        $parentClass = parent::autoload($class);

        if ($parentClass == false) {

            $newClass = self::load($class);

            return $newClass;
        }

        return $parentClass;
    }

    /**
     * @param string $classname Classname
     *
     * @return bool|string classname on success
     */
    private static function load($classname)
    {
        $loadedClass = false;

        if (strpos($classname, 'Symfony\\') === 0) {
            $loadedClass = self::loadSymfonyClass($classname);
        }

        if (strpos($classname, 'Elastica\\') === 0) {
            $loadedClass = self::loadElasticaClass($classname);
        }

        if (strpos($classname, 'Elastica\\') === false) {
            $loadedClass = self::loadElasticaClass('Elastica\\Query\\' . $classname);
        }

        return $loadedClass;
    }

    /**
     * @param $classname
     *
     * @return bool|string classname on success
     */
    private static function loadSymfonyClass($classname)
    {
        $symfonyWcpPath = realpath(__DIR__) . '/../../../../../../../../htdocs/vendor/symfony/';

        $filePathParts = explode('\\', $classname);
        $classPath     = implode(DIRECTORY_SEPARATOR, $filePathParts);

        $filePath = $symfonyWcpPath . 'class-loader/' . $classPath . '.php';
        if (!file_exists($filePath)) {
            $filePath = $symfonyWcpPath . 'event-dispatcher/' . $classPath . '.php';
        }

        if (!file_exists($filePath)) {
            return false;
        }

        require_once $filePath;

        return $classname;
    }

    /**
     * @param $classname
     *
     * @return bool|string classname on success
     */
    private static function loadElasticaClass($classname)
    {
        $elasticaPath = realpath(__DIR__) . '/../../../../../../../../htdocs/vendor/ruflin/elastica/lib/' . str_replace(
                '\\',
                '/',
                $classname
            );

        if (file_exists($elasticaPath . '.php')) {
            require_once($elasticaPath . '.php');

            return $classname;
        }

        return false;
    }

}