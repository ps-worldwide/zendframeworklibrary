<?php
  /**
   * Pct_Efs_19_3_Api_Custom #EFSUPGRADE#
   * place wrappers for custom Service Layer methods here
   * NOTE: custom services need to be symlinked to htdocs/custom/service
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Custom.php,v 1.1 2019/06/19 18:34:46 khalafiniya Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: khalafiniya $
   */

  class Pct_Efs_19_3_Api_Custom extends Pct_Efs_19_2_Api_Servicelayer #EFSUPGRADE#
  {
  }