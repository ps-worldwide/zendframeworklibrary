<?php
  /**
   * Pct_Efs_Autoloader
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoloader.php,v 1.6 2013/10/16 09:29:13 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Efs_Autoloader implements Zend_Loader_Autoloader_Interface
  {
    protected static $_instance;
    protected static $_autoload;

    /**
     * The autoload function that can be pushed onto the Zend-Loader autoloader
     * stack. The function itself simply calls the autoload method initiated for
     * the specific EFS version.
     *
     * @param string $class
     *
     * @return mixed
     */
    public function autoload($class)
    {
      /** @noinspection PhpUndefinedMethodInspection */
      return self::$_autoload->autoload($class);
    }

    /**
     * Instantiates the singleton class
     *
     * @param string $efsVersion
     *
     * @throws Zend_Loader_Exception
     * @return \Pct_Efs_Autoloader
     */
    public static function getInstance($efsVersion)
    {
      if ($efsVersion == null)
        throw new Zend_Loader_Exception('EfsVersion not passed to autoloader');

      // Instantiate the autoloader if not done yet
      if (null == self::$_instance)
      {
        // Format the double / float version number
        $efsVersion = str_replace('.', '_', $efsVersion);

        // Instantiate the autoloader
        self::$_instance = new self($efsVersion);
      }

      // Return the instance
      return self::$_instance;
    }

    /**
     * Resets the autoloader instance and object
     */
    public function resetInstance()
    {
      self::$_autoload = null;
      self::$_instance = null;
    }

    /**
     * Singleton constructor
     *
     * @param string $efsVersion
     *
     * @throws Zend_Loader_Exception
     */
    protected function __construct($efsVersion)
    {
      // Determine the autoloader file for the EFS verion
      $autoloadScript = dirname(__FILE__) . DIRECTORY_SEPARATOR . str_replace('_', DIRECTORY_SEPARATOR, $efsVersion) . '/';
      if (false == is_readable(realpath($autoloadScript) . '/Autoload.php'))
        throw new Zend_Loader_Exception($autoloadScript . '/Autoload.php is not readable');

      // Instantiate the autoloader supporting the EFS version
      $className = 'Pct_Efs_' . $efsVersion . '_Autoload';
      Zend_Loader::loadClass($className);
      self::$_autoload = new $className(APPLICATION_EFS_PATH);
    }
  }