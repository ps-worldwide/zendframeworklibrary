<?php

/**
 * Pct_Efs_16_3_Api_Custom
 * place wrappers for custom Service Layer methods here
 * NOTE: custom services need to be symlinked to htdocs/custom/service
 *
 * @package   zendframeworkLibrary
 * @version   $Id: Custom.php,v 1.3 2017/02/16 20:05:50 faust Exp $
 * @copyright (c) QuestBack http://www.questback.com
 * @author    $Author: faust $
 */
class Pct_Efs_16_3_Api_Custom extends Pct_Efs_16_2_Api_Servicelayer
{
    /**
     * @param int $adminId - Admin User ID
     * @return surveyList - list of surveys
     */
    public function customSurveysurveysGetListByAdminId($adminId)
    {
        return $this->makeServiceLayerCall(
            $this->url . "&method=custom.surveysurveys.getListByAdminId",
            array(
                'adminId' => $adminId
            )
        );
    }

}