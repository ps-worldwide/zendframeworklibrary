<?php
  /**
   * Pct_Efs_16_2_Autoload 
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoload.php,v 1.2 2016/09/30 14:57:15 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_16_2_Autoload extends Pct_Efs_11_Autoload 
  {
    protected $_efsVersionExtension = '16_2_'; 
  }