<?php
  /**
   * Pct_Efs_16_2_Api_Custom 
   * place wrappers for custom Service Layer methods here
   * NOTE: custom services need to be symlinked to htdocs/custom/service
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Custom.php,v 1.2 2016/09/30 14:57:15 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_16_2_Api_Custom extends Pct_Efs_11_Api_Servicelayer 
  {
  }