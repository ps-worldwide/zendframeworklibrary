<?php
  /**
   * Pct_Efs_16_4_Api_Custom 
   * place wrappers for custom Service Layer methods here
   * NOTE: custom services need to be symlinked to htdocs/custom/service
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Custom.php,v 1.2 2017/04/04 14:32:50 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_16_4_Api_Custom extends Pct_Efs_16_3_Api_Servicelayer 
  {
  }