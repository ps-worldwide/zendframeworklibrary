<?php

/**
 * Pct_Efs_16_4_Api_Servicelayer 
 *
 * @package   zendframeworkLibrary
 * @version   $Id: Servicelayer.php,v 1.3 2017/04/04 14:04:49 faust Exp $
 * @copyright (c) QuestBack http://www.questback.com
 * @author    $Author: faust $
 */
class Pct_Efs_16_4_Api_Servicelayer extends Pct_Efs_16_4_Api_Custom 
{
    /**
     * efsMailblacklistAdd
     *
     * Description (from WSDL):
     * Add a list of email addresses to blacklist. If surveyId is specified the email will be blacklisted for given survey, otherwise global.
     *
     * @param $emailList
     * @return mixed
     */
    public function efsMailblacklistAdd($emailList)
    {
        return $this->makeServiceLayerCall(
            $this->url . "&method=efs.mailblacklist.add",
            array(
                'emailList' => $emailList,
            )
        );
    }

    /**
     * efsMailblacklistDelete
     *
     * Description (from WSDL):
     * Deletes a list of email addresses from blacklist
     *
     * @param $emailList
     * @return mixed
     */
    public function efsMailblacklistDelete($emailList)
    {
        return $this->makeServiceLayerCall(
            $this->url . "&method=efs.mailblacklist.delete",
            array(
                'emailList' => $emailList,
            )
        );
    }

    /**
     * efsMailblacklistGetList
     *
     * Description (from WSDL):
     * Get a list of blacklisted email addresses (both global and survey related)
     *
     * @return mixed
     */
    public function efsMailblacklistGetList()
    {
        return $this->makeServiceLayerCall(
            $this->url . "&method=efs.mailblacklist.getList",
            array()
        );
    }

    /**
     * efsMailblacklistIsBlacklisted
     *
     * Description (from WSDL):
     * Check whether email address is blacklisted for given survey. If surveyId is not specified checks whether email is in global blacklist.
     *
     * @param $emailAddress
     * @param $surveyId
     * @return mixed
     */
    public function efsMailblacklistIsBlacklisted($emailAddress, $surveyId)
    {
        return $this->makeServiceLayerCall(
            $this->url . "&method=efs.mailblacklist.isBlacklisted",
            array(
                'emailAddress' => $emailAddress,
                'surveyId'     => $surveyId,
            )
        );
    }

}
