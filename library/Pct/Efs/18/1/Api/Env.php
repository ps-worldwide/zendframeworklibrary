<?php
  /**
   * Pct_Efs_18_1_Api_Env #EFSUPGRADE#
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Env.php,v 1.1 2018/05/04 10:50:26 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  abstract class Pct_Efs_18_1_Api_Env extends Pct_Efs_17_4_Api_Env #EFSUPGRADE#
  {
    /**
     * Inititates the environment needed to perform API calls
     */
    public static function init($version)
    {
      if (self::$isInit == true)
        return;

      // First we need an autoloader which loads classes from EFS
      //$loader = efs_models_autoload::getInstance();
      Pct_Efs_Autoloader::getInstance($version);

      // avoid silly notices from EFS
      self::setEfsErrorReporting();

      // Load a lots of EFS stuff
      /** @noinspection PhpDynamicAsStaticMethodCallInspection */
      self::$efsEnv = efs_apis_18_1_env::getInstance(); #EFSUPGRADE#

      // we want to see our own notices
      self::resetEfsErrorReporting();

      // Mark that EFS environment was initilized
      self::$isInit = true;
    }
  }