<?php
  /**
   * Pct_Efs_18_1_Api_Custom #EFSUPGRADE#
   * place wrappers for custom Service Layer methods here
   * NOTE: custom services need to be symlinked to htdocs/custom/service
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Custom.php,v 1.1 2018/05/04 10:50:26 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_18_1_Api_Custom extends Pct_Efs_17_4_Api_Servicelayer #EFSUPGRADE#
  {
  }