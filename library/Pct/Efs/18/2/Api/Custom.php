<?php
  /**
   * Pct_Efs_18_2_Api_Custom #EFSUPGRADE#
   * place wrappers for custom Service Layer methods here
   * NOTE: custom services need to be symlinked to htdocs/custom/service
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Custom.php,v 1.3 2018/10/22 09:03:09 khalafiniya Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: khalafiniya $
   */

  class Pct_Efs_18_2_Api_Custom extends Pct_Efs_18_1_Api_Servicelayer #EFSUPGRADE#
  {
  }
  