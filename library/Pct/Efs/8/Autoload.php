<?php
  /**
   * Pct_Efs_8_Autoload
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoload.php,v 1.4 2013/10/16 09:28:39 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Efs_8_Autoload extends Pct_Efs_7_1_Autoload
  {
    protected $_efsVersionExtension = '8_';
  }