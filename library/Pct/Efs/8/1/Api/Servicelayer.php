<?php
  /**
   * Pct_Efs_8_1_Api_Servicelayer
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Servicelayer.php,v 1.20 2013/10/16 09:28:38 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Efs_8_1_Api_Servicelayer extends Pct_Efs_8_Api_Servicelayer
  {
    /**
     * getPanelistsByFilter
     *
     * Description (from WSDL):
     * returns a list of panelists matched by a specific panel grouping filter
     *
     * @param int   $filterId
     * @param array $returnDataFields
     *
     * @return array|mixed
     */
    public function getPanelistsByFilter($filterId, $returnDataFields)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getListByFilter",
                                         array('filterId'         => (int)$filterId,
                                               'returnDataFields' => $returnDataFields));
    }

    /**
     * surveyChangeDescription
     *
     * @param int    $surveyId
     * @param string $title
     * @param string $description
     * @param string $author
     * @param string $staff
     * @param string $comment
     * @param bool   $isMarked
     *
     * @return array|mixed
     */
    public function surveyChangeDescription($surveyId, $title, $description, $author, $staff, $comment, $isMarked)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.surveys.changeDescription",
                                         array('surveyId'    => $surveyId,
                                               'title'       => $title,
                                               'description' => $description,
                                               'author'      => $author,
                                               'staff'       => $staff,
                                               'comment'     => $comment,
                                               'isMarked'    => $isMarked)
      );
    }

    /**
     * getSampleUsers
     *
     * @param int   $sampleId
     * @param array $returnDataFields
     *
     * @return array|mixed
     */
    public function getSampleUsers($sampleId, array $returnDataFields)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getListBySample",
                                         array('sampleId'         => $sampleId,
                                               'returnDataFields' => $returnDataFields));
    }

    /**
     * getPanelistsByIdentifier
     *
     * Description (from WSDL):
     * returns a list of actual panelists from a list of possible panelist identifiers
     * using this service it can be checked if specific panelists are already contained in the panel or not
     *
     * @param string $identifierType
     * @param array  $identifierList
     * @param array  $returnDataFields
     *
     * @return array|mixed
     */
    public function getPanelistsByIdentifier($identifierType, array $identifierList, array $returnDataFields)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getListByIdentifiers",
                                         array('identifierType'   => $identifierType,
                                               'identifierList'   => $identifierList,
                                               'returnDataFields' => $returnDataFields));
    }

    /**
     * addPanelistToSample
     *
     * Description (from WSDL):
     * Add a panelist to a sample
     *
     * @param string $identifierType
     * @param string $identifierValue
     * @param int    $sampleId
     * @param string $resetParticipation
     *
     * @return array|mixed
     */
    public function addPanelistToSample($identifierType, $identifierValue, $sampleId, $resetParticipation = 'reset_no_change')
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.addToSample",
                                         array('identifierType'     => $identifierType,
                                               'identifierValue'    => $identifierValue,
                                               'sampleId'           => $sampleId,
                                               'resetParticipation' => $resetParticipation));
    }

    /**
     * getQuestionnaireStructure
     *
     * Description (from WSDL):
     * return the questionnaire structure for a survey
     *
     * @param int $surveyId
     *
     * @return array|mixed
     */
    public function getQuestionnaireStructure($surveyId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.questionnaire.getStructure",
                                         array('surveyId' => $surveyId));
    }

    /**
     * efsPrivilegesHasAccessToFeature
     *
     * Description (from WSDL):
     * Tells current user has access to a feature
     *
     * @param string $feature
     * @param string $right
     *
     * @return array|mixed
     */
    public function efsPrivilegesHasAccessToFeature($feature, $right)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.privileges.hasAccessToFeature",
                                         array('feature' => $feature,
                                               'right'   => $right));
    }

    /**
     * efsPrivilegesHasAccessToObject
     *
     * Description (from WSDL):
     * Tells current user has access to an object
     *
     * @param integer $objectId
     * @param string  $right
     *
     * @return array|mixed
     */
    public function efsPrivilegesHasAccessToObject($objectId, $right)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.privileges.hasAccessToObject",
                                         array('objectId' => $objectId,
                                               'right'    => $right));
    }

    /**
     * efsSystemGetVersion
     *
     * Description (from WSDL):
     * determine the version number of the EFS installation
     */
    public function efsSystemGetVersion()
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.system.getVersion", array());
    }

    /**
     * panelPanelistsAddToSurvey
     *
     * Description (from WSDL):
     * Add a panelist to a survey
     *
     * @param string $identifierType
     * @param string $identifierValue
     * @param int    $surveyId
     * @param string $resetParticipation
     *
     * @return array|mixed
     */
    public function panelPanelistsAddToSurvey($identifierType, $identifierValue, $surveyId, $resetParticipation)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.addToSurvey",
                                         array('identifierType'     => $identifierType,
                                               'identifierValue'    => $identifierValue,
                                               'surveyId'           => $surveyId,
                                               'resetParticipation' => $resetParticipation));
    }

    /**
     * panelSurveysGetList
     *
     * Description (from WSDL):
     * get the list of surveys for a panelist
     *
     * @param string $identifierType
     * @param string $identifierValue
     *
     * @return array|mixed
     */
    public function panelSurveysGetList($identifierType, $identifierValue)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.surveys.getList",
                                         array('identifierType'  => $identifierType,
                                               'identifierValue' => $identifierValue));
    }

    /**
     * panelWebsitesGetList
     *
     * Description (from WSDL):
     * returns a list of panel websites
     */
    public function panelWebsitesGetList()
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.websites.getList", array());
    }

    /**
     * surveyListsAddStaticListElement
     *
     * Description (from WSDL):
     * Adds a static list element
     *
     * @param integer $surveyId
     * @param integer $listId
     * @param string  $label
     * @param integer $number
     *
     * @return array|mixed
     */
    public function surveyListsAddStaticListElement($surveyId, $listId, $label, $number)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.lists.addStaticListElement",
                                         array('surveyId' => $surveyId,
                                               'listId'   => $listId,
                                               'label'    => $label,
                                               'number'   => $number));
    }

    /**
     * surveyListsAddStaticListElements
     *
     * Description (from WSDL):
     * Adds a bunch of static list elements
     *
     * @param integer $surveyId
     * @param integer $listId
     * @param array   $items
     *
     * @return array|mixed
     */
    public function surveyListsAddStaticListElements($surveyId, $listId, $items)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.lists.addStaticListElements",
                                         array('surveyId' => $surveyId,
                                               'listId'   => $listId,
                                               'items'    => $items));
    }

    /**
     * surveyListsClearList
     *
     * Description (from WSDL):
     * Clears a list
     *
     * @param integer $surveyId
     * @param integer $listId
     *
     * @return array|mixed
     */
    public function surveyListsClearList($surveyId, $listId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.lists.clearList",
                                         array('surveyId' => $surveyId,
                                               'listId'   => $listId));
    }

    /**
     * surveyListsGetStaticListsList
     *
     * Description (from WSDL):
     * Returns the list of available static lists for a survey
     *
     * @param integer $surveyId
     *
     * @return array|mixed
     */
    public function surveyListsGetStaticListsList($surveyId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.lists.getStaticListsList",
                                         array(
                                              'surveyId' => $surveyId,
                                         ));
    }

    /**
     * surveyListsCreateStaticList
     *
     * Description (from WSDL):
     * Returns the list of available static lists for a survey
     *
     * @param int    $surveyId
     * @param string $label
     * @param string $placeholder
     * @param string $delimiter
     * @param string $lastDelimiter
     *
     * @return array|mixed
     */
    public function surveyListsCreateStaticList($surveyId, $label, $placeholder, $delimiter, $lastDelimiter)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.lists.createStaticList",
                                         array(
                                              'surveyId'      => $surveyId,
                                              'label'         => $label,
                                              'placeholder'   => $placeholder,
                                              'delimiter'     => $delimiter,
                                              'lastDelimiter' => $lastDelimiter,
                                         ));
    }

    /**
     * surveyListsDeleteList
     *
     * Description (from WSDL):
     * Deletes a list
     *
     * @param int $surveyId
     * @param     $listId
     *
     * @return array|mixed
     */
    public function surveyListsDeleteList($surveyId, $listId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.lists.deleteList",
                                         array(
                                              'surveyId' => $surveyId,
                                              'listId'   => $listId,
                                         ));
    }

    /**
     * surveyQuestionnaireCreatePage
     *
     * Description (from WSDL):
     * create a questionnaire page
     *
     * @param $surveyId
     * @param $pageTitle
     * @param $pageType
     * @param $parentPageId
     * @param $previousPageId
     * @param $options
     *
     * @return array|mixed
     */
    public function surveyQuestionnaireCreatePage($surveyId, $pageTitle, $pageType, $parentPageId, $previousPageId, $options)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.questionnaire.createPage",
                                         array(
                                              'surveyId'       => $surveyId,
                                              'pageTitle'      => $pageTitle,
                                              'pageType'       => $pageType,
                                              'parentPageId'   => $parentPageId,
                                              'previousPageId' => $previousPageId,
                                              'options'        => $options,
                                         ));
    }

    /**
     * surveyQuestionnaireDeletePage
     *
     * Description (from WSDL):
     * delete a questionnaire page
     *
     * @param $surveyId
     * @param $pageId
     * @param $force
     *
     * @return array|mixed
     */
    public function surveyQuestionnaireDeletePage($surveyId, $pageId, $force)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.questionnaire.deletePage",
                                         array(
                                              'surveyId' => $surveyId,
                                              'pageId'   => $pageId,
                                              'force'    => $force,
                                         ));
    }

    /**
     * surveyTranslationsGetTranslationList
     *
     * Description (from WSDL):
     * getTranslationList Gets a list of all translations for the specified  language
     *
     * @param $surveyId
     * @param $languageId
     *
     * @return array|mixed
     */
    public function surveyTranslationsGetTranslationList($surveyId, $languageId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.translations.getTranslationList",
                                         array(
                                              'surveyId'   => $surveyId,
                                              'languageId' => $languageId,
                                         ));
    }

    /**
     * surveyTranslationsChangeTranslation
     *
     * Description (from WSDL):
     * changeTranslation - changes a translation
     *
     * @param int $surveyId
     * @param int $languageId
     * @param     $textType
     * @param     $typeId
     * @param     $text
     *
     * @return array|mixed
     */
    public function surveyTranslationsChangeTranslation($surveyId, $languageId, $textType, $typeId, $text)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.translations.changeTranslation",
                                         array(
                                              'surveyId'   => $surveyId,
                                              'languageId' => $languageId,
                                              'textType'   => $textType,
                                              'typeId'     => $typeId,
                                              'text'       => $text,
                                         ));
    }

    /**
     * surveyTranslationsGetLanguagesList
     *
     * Description (from WSDL):
     * getLanguagesList - get a list of all languages in a survey
     *
     * @param int $surveyId
     *
     * @return array|mixed
     */
    public function surveyTranslationsGetLanguagesList($surveyId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.translations.getLanguagesList",
                                         array(
                                              'surveyId' => $surveyId,
                                         ));
    }

    /**
     * surveyTranslationsCreateLanguage
     *
     * Description (from WSDL):
     * addLanguage adds a new language
     *
     * @param int    $surveyId
     * @param string $language
     *
     * @return array|mixed
     */
    public function surveyTranslationsCreateLanguage($surveyId, $language)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.translations.createLanguage",
                                         array(
                                              'surveyId' => $surveyId,
                                              'language' => $language,
                                         ));
    }

    /**
     * surveyTranslationsChangeLanguage
     *
     * Description (from WSDL):
     * _8100000_editLanguage
     *
     * @param int    $surveyId
     * @param int    $languageId
     * @param string $language
     *
     * @return array|mixed
     */
    public function surveyTranslationsChangeLanguage($surveyId, $languageId, $language)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.translations.changeLanguage",
                                         array(
                                              'surveyId'   => $surveyId,
                                              'languageId' => $languageId,
                                              'language'   => $language,
                                         ));
    }

    /**
     * surveyTranslationsDeleteLanguage
     *
     * Description (from WSDL):
     * deleteLanguage deletes a language
     *
     * @param int $surveyId
     * @param int $languageId
     *
     * @return array|mixed
     */
    public function surveyTranslationsDeleteLanguage($surveyId, $languageId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.translations.deleteLanguage",
                                         array(
                                              'surveyId'   => $surveyId,
                                              'languageId' => $languageId,
                                         ));
    }

    /**
     * panelPanelistsGetListByGroup
     *
     * Description (from WSDL):
     * returns a list of panelists in a specific panel group
     *
     * @param int   $groupId
     * @param array $returnDataFields
     *
     * @return array|mixed
     */
    public function panelPanelistsGetListByGroup($groupId, $returnDataFields)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getListByGroup",
                                         array(
                                              'groupId'          => $groupId,
                                              'returnDataFields' => $returnDataFields,
                                         ));
    }
  }
