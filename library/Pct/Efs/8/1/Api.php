<?php
  /**
   * Pct_Efs_8_1_Api
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Api.php,v 1.19 2014/04/07 15:50:05 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_8_1_Api
  {
    // This API connects / supports EFS 8.1
    const EFS_VERSION = 8.1;

    // The API environment used by EFS (we must "boot" EFS)
    protected $efsEnv = null;

    // The EFS-API for survey-actions
    protected $apiSurvey = null;

    // The EFS-API for panel-actions
    protected $apiPanel = null;

    // The EFS-API for hr-actions
    protected $apiHr = null;

    // The EFS-API for webservice-actions
    protected $apiWebservice = null;

    // The EFS-Service Layer
    protected $apiServiceLayer = null;

    // The work directory before the API call
    protected $cwd;

    /**
     * Initialises the APIs for the particular EFS version
     *
     * @param Zend_Config              $conf
     * @param Zend_Db_Adapter_Abstract $db
     * @param array                    $params
     */
    public function init(Zend_Config $conf, Zend_Db_Adapter_Abstract $db = null, $params = array())
    {
      // Survey-API
      if ($conf->enableApi->survey == 1)
      {
        // Load the environment EFS needs
        /** @noinspection PhpDynamicAsStaticMethodCallInspection */
        /** @noinspection PhpVoidFunctionResultUsedInspection */
        $this->efsEnv = Pct_Efs_8_1_Api_Env::init(self::EFS_VERSION);
        $this->prepareCall();
        $this->apiSurvey = new efs_apis_8_1_surveyapi($db);
        $this->finishCall();
      }

      // Panel-API
      if ($conf->enableApi->panel == 1)
      {
        // Load the environment EFS needs
        /** @noinspection PhpDynamicAsStaticMethodCallInspection */
        /** @noinspection PhpVoidFunctionResultUsedInspection */
        $this->efsEnv = Pct_Efs_8_1_Api_Env::init(self::EFS_VERSION);
        $this->prepareCall();
        $this->apiPanel = new efs_apis_8_panelapi($db);
        $this->finishCall();
      }

      // HR-API
      if ($conf->enableApi->hr == 1)
      {
        // Load the environment EFS needs
        /** @noinspection PhpDynamicAsStaticMethodCallInspection */
        /** @noinspection PhpVoidFunctionResultUsedInspection */
        $this->efsEnv = Pct_Efs_8_1_Api_Env::init(self::EFS_VERSION);
        $this->prepareCall();
        $this->apiHr = new efs_apis_8_hrapi($db);
        $this->finishCall();
      }

      // Webservice-API
      if ($conf->enableApi->webservice == 1)
      {
        $baseUrl             = (array_key_exists('externalws', $params)) ? $params['externalws'] : null;
        $clientId            = (array_key_exists('clientid', $params)) ? $params['clientid'] : null;
        $timeout             = (array_key_exists('timeout', $params)) ? (int)$params['timeout'] : 0;
        $this->apiWebservice = new Pct_Efs_8_1_Api_Webservice(null, $conf, $baseUrl, $clientId, true, $timeout);
      }

      // Service Layer
      if ($conf->enableApi->servicelayer == 1)
      {
        // If config (application.ini) has configuration settings merge these into
        // the parameters passed to the API instance
        if (true == isset($conf->servicelayer))
          $params = $this->mergeParamsFromConfig($conf->servicelayer, $params);

        $this->apiServiceLayer = new Pct_Efs_8_1_Api_Servicelayer($conf->get('base_all_url'), $params);
      }
    }

    /**
     * Search the method in one of the available APIs, call it and return
     * the result.
     *
     * @param string $method
     * @param array  $params
     *
     * @throws Exception
     * @return mixed
     */
    public function __call($method, $params)
    {
      // Set when a call resulted in an exception
      $exception = null;

      // Do what is needed before each call
      $this->prepareCall();

      // Do the API call
      $returnValue = null;
      try
      {
        $returnValue = $this->doCall($method, $params);
      } catch (Exception $e)
      {
        $exception = $e;
      }

      // Undo what was done for preparation
      $this->finishCall();

      // Check if we got an excpetion
      if ($exception != null)
      {
        // EFS exceptions have different methods
        if (is_callable(array($exception, 'get_error_message')))
        {
          /** @noinspection PhpUndefinedMethodInspection */
          throw new Exception($exception->get_error_message(), $exception->get_errcode(), $exception);
        }
        else
        {
          throw $exception;
        }
      }

      // No exception then return what the call returned
      return $returnValue;
    }

    /**
     * Do nesessary preparations before the call
     */
    protected function prepareCall()
    {
      // EFS might change the current working directory so back it up
      $this->cwd = getcwd();

      // Adjust the error reporting before the call to avoid EFS notices
      /** @noinspection PhpDynamicAsStaticMethodCallInspection */
      Pct_Efs_8_1_Api_Env::setEfsErrorReporting();

      // EFS might print HTML (or similar) directly so catch that before the call
      ob_start();
    }

    /**
     * Reset preparations
     */
    protected function finishCall()
    {
      // Dump possible output from EFS
      ob_end_clean();

      // Reset the error-reporting to what it was before the API call
      /** @noinspection PhpDynamicAsStaticMethodCallInspection */
      Pct_Efs_8_1_Api_Env::resetEfsErrorReporting();

      // Change back into the previously backed up working directory
      chdir($this->cwd);
    }

    /**
     * Looks for the EFS API for the function wished to call and catches
     * possible EFS exceptions
     */
    protected function doCall($method, $params)
    {
      // Service Layer
      if (method_exists($this->apiServiceLayer, $method))
        return call_user_func_array(array($this->apiServiceLayer, $method), $params);

      // Survey API
      if (method_exists($this->apiSurvey, $method))
        return call_user_func_array(array($this->apiSurvey, $method), $params);

      // Panel API
      if (method_exists($this->apiPanel, $method))
        return call_user_func_array(array($this->apiPanel, $method), $params);

      // Hr API
      if (method_exists($this->apiHr, $method))
        return call_user_func_array(array($this->apiHr, $method), $params);

      // Webservice API
      if (method_exists($this->apiWebservice, $method))
        return call_user_func_array(array($this->apiWebservice, $method), $params);

      // If none of the APIs offers the method throw an exception
      throw new Exception(sprintf('Call to unsupported api-method "%s()"', $method));
    }

    /**
     * Merges provided settings from params with possible default values from
     * config.
     * Also sets authentification parmameter. When session (ses) is provided this
     * is taken. Otherwise it is tried to set the settings from the config
     * (application.ini).
     *
     * @param Zend_Config $conf
     * @param array       $params
     *
     * @return array $params
     */
    protected function mergeParamsFromConfig(Zend_Config $conf, array $params)
    {
      // Default Werte aus der application.ini
      $default = $conf->toArray();

      // Set authentification parameter. When session is set prefer this instead
      // of username and password
      if (false == array_key_exists('ses', $params))
      {
        if (false == array_key_exists('username', $params))
          $params['username'] = $conf->user;
        if (false == array_key_exists('passwd', $params))
          $params['passwd'] = $conf->passwd;
      }

      // Remove passwd and username as these key differ from what the model
      // expects (e.g. username vs user)
      unset($default['user']);
      unset($default['passwd']);
      $params = array_merge($default, $params);
      return $params;
    }
  }