<?php
  /**
   * Pct_Efs_8_1_Autoload
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoload.php,v 1.7 2013/10/16 09:28:39 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Efs_8_1_Autoload extends Pct_Efs_8_Autoload
  {
    protected $_efsVersionExtension = '8_1_';

    /**
     * __construct
     *
     * @param string $efsBasePath
     */
    public function __construct($efsBasePath)
    {
      $this->_efsBasePath = realpath($efsBasePath);

      Zend_Loader::loadFile($this->_efsBasePath . '/modules/efs/components/autoload.inc.php', null, true);
      $efsLoader         = new ReflectionClass('efs_autoload');
      $properties        = $efsLoader->getDefaultProperties();
      $this->_dictionary = $properties['dictionary'];
    }
  }