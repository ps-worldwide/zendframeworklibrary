<?php
  /**
   * Pct_Efs_8_Api_Webservice
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Webservice.php,v 1.12 2013/10/16 09:28:39 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Efs_8_Api_Webservice extends Pct_Efs_7_1_Api_Webservice
  {
    /**
     * getPanelistPartial
     *
     * @param $identifier
     * @param $identifier_value
     * @param $variables
     */
    public function getPanelistPartial($identifier, $identifier_value, array $variables)
    {
      $this->initSoapClient();
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->client->get_panelist_partial($identifier, $identifier_value, $variables);
    }

    /**
     * PanelGetPanelist
     *
     * @param string $identifierType
     * @param string $identifierValue
     *
     * @return array
     * @throws InvalidArgumentException
     */
    public function PanelGetPanelist($identifierType, $identifierValue = null)
    {
      if (!is_null($identifierValue))
      {
        if (!is_string($identifierType))
          throw new InvalidArgumentException('Wrong type for indentifier type, has to be a string of identifier value is passed.');

        /** @noinspection PhpUndefinedMethodInspection */
        return $this->getSoapClient()->get_panelist($identifierType, $identifierValue);
      }
      return array();
    }

    /**
     * Changes the panelists status
     *
     * @param string  $idType
     * @param string  $idValue
     * @param integer $status
     */
    public function PanelModifyPanelistStatus($idType, $idValue, $status)
    {
      $this->initSoapClient();
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->client->modify_panelist_status($idType, $idValue, $status);
    }

    /**
     * Adds the panelist to the given panel group
     *
     * @param string  $idType
     * @param string  $idValue
     * @param integer $groupId
     */
    public function PanelAddPanelistToGroup($idType, $idValue, $groupId)
    {
      $this->initSoapClient();
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->client->add_panelist_to_group($idType, $idValue, $groupId);
    }

    /**
     * Sent the given mail template to the given panelist
     *
     * @param string $idType
     * @param string $idValue
     * @param object $template
     */
    public function PanelSendMailToPanelist($idType, $idValue, $template)
    {
      $this->initSoapClient();
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->client->send_mail_to_panelist($idType, $idValue, $template);
    }
  }
