<?php
  /**
   * Pct_Efs_8_Api_Custom
   * place wrappers for custom Service Layer methods here
   * NOTE: custom services need to be symlinked to htdocs/custom/service
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Custom.php,v 1.6 2018/05/04 10:50:26 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_8_Api_Custom
  {
    /**
     * Custom: checkEfsSession
     *
     * Description (from WSDL):
     * test if panel session is currently active, returns panelist-uid or 0 if session is not active
     *
     * @param string $sesId
     * @param string $sesType
     *
     * @return
     */
    public function checkEfsSession($sesId, $sesType = 'panel')
    {
      /** @noinspection PhpUndefinedFieldInspection */
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->makeServiceLayerCall($this->url . "&method=custom.session.checkEfsSession",
                                         array('sesId'   => $sesId,
                                               'sesType' => $sesType));
    }

    /**
     * Custom: customSurveyparticipantsSendMailByTemplateId
     *
     * Description (from WSDL):
     * Sends mails to participiants in a sample. Will fail on anonymous surveys.
     *
     * @param int   $sampleId
     * @param array $uids
     * @param int   $mailTemplateId
     * @param array $dispcodes
     * @param array $sendOptions
     */
    public function customSurveyparticipantsSendMailByTemplateId($sampleId, array $uids = array(), $mailTemplateId, array $dispcodes = array(), array $sendOptions = array())
    {
      if (count($sendOptions) == 0 || @$sendOptions['sendDate'] == '')
        $sendOptions['sendDate'] = gmdate('Y-m-d\TH:i:s');

      /** @noinspection PhpUndefinedFieldInspection */
      /** @noinspection PhpUndefinedMethodInspection */
      return $this->makeServiceLayerCall($this->url . "&method=custom.surveyparticipants.sendMailByTemplateId",
                                         array('sampleId'       => $sampleId,
                                               'uids'           => $uids,
                                               'mailTemplateId' => $mailTemplateId,
                                               'dispcodes'      => $dispcodes,
                                               'sendOptions'    => $sendOptions));
    }
  }
