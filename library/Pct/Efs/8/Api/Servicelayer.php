<?php
  /**
   * Pct_Efs_8_Api_Servicelayer
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Servicelayer.php,v 1.82 2015/04/20 14:31:49 mueller Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: mueller $
   */

  class Pct_Efs_8_Api_Servicelayer extends Pct_Efs_8_Api_Custom
  {
    /**
     * client
     *
     * @var Zend_Http_Client
     */
    protected $client = null;

    /**
     * url
     *
     * @var string
     */
    protected $url = null;

    /**
     * allowedConfig
     *
     * allows certain parameters to be set for the ServiceLayer
     * standard Zend_Http_Client-params, contained in $params
     *
     * @var array
     */
    protected $allowedConfig = array('maxredirects',
                                     'strictredirects',
                                     'useragent',
                                     'timeout',
                                     'httpversion',
                                     'adapter',
                                     'keepalive');

    /**
     * emptyReturn
     *
     * standard return value from the Service Layer if nothing happened
     * can be returned e.g. in case of empty parameters (which would throw an Exception if passed to the Service Layer)
     *
     * @var array
     */
    protected $emptyReturn = array('success'  => true,
                                   'errors'   => array(),
                                   'warnings' => array(),
                                   'messages' => array(),
                                   'return'   => NULL);

    /**
     * Constructor
     *
     * @param string $baseUrl
     * @param array  $params
     * @param object $client (optional)
     */
    public function __construct($baseUrl, $params, $client = null)
    {
      $this->client = $client;
      $config       = Zend_Registry::get('conf')->toArray();

      // allow for external installations
      if (array_key_exists('externalurl', $params) === false)
      {
        // "efsnoredirect" does not work for Service Layer [BF: 9.11.12]
        if (@$params['https'] === true || @$config['force_ssl'] == 'true')
          $baseUrl = str_replace('http://', 'https://', $baseUrl);
      }
      else
      {
        $baseUrl = $params['externalurl'];

        // can't check for config (see above) as this is a remote installation
        if (@$params['https'] === true)
          $baseUrl = str_replace('http://', 'https://', $baseUrl);
      }

      // setup servicelayer URL
      $this->url = $baseUrl . "service/index.php?handler=php";

      // create client
      if ($this->client == null)
        $this->client = new Zend_Http_Client();

      // If we got a session prefer this one instead of username and password
      if (array_key_exists('ses', $params))
        $this->client->setCookie('ses', $params['ses']);
      else
        $this->client->setAuth(@$params['username'], @$params['passwd']);

      // check for additional config parameters
      foreach ($this->allowedConfig as $paramName)
      {
        if (isset($params[$paramName]))
          $this->client->setConfig(array($paramName => $params[$paramName]));
      }
      $this->client->setMethod(Zend_Http_Client::POST);
    }

    /**
     * getSamples
     *
     * @return array $sampleData
     */
    public function getSamples()
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.samples.getAll", array());
    }

    /**
     * getSample
     *
     * @param int $sid
     *
     * @return array $sampleData
     */
    public function getSample($sid)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.samples.get", array('sampleId' => $sid));
    }

    /**
     * getSurveys
     *
     * @return array $surveyData
     */
    public function getSurveys()
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.surveys.getList", array());
    }

    /**
     * getSurveysByCriteria
     *
     * @param array $params
     *
     * @return array $surveyData
     */
    public function getSurveysByCriteria($params = array())
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.surveys.getListByCriteria", $params);
    }

    /**
     * getSurvey
     *
     * @param int $pid
     *
     * @return array $surveyData
     */
    public function getSurvey($pid)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.surveys.get", array('surveyId' => $pid));
    }

    /**
     * getSurveyPreviewLink
     *
     * @param int $pid
     *
     * @return array $surveyData
     */
    public function getSurveyPreviewLink($pid)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.surveys.getPreviewLink", array('surveyId' => $pid));
    }

    /**
     * copySurvey
     *
     * @param int    $pid
     * @param string $title
     *
     * @return array $surveyData
     */
    public function copySurvey($pid, $title)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.surveys.copy", array('surveyId' => $pid, 'title' => $title));
    }

    /**
     * changeFieldData
     *
     * @param int    $pid
     * @param string $surveyStatus
     * @param string $surveyFieldStartTime
     * @param string $surveyFieldEndTime
     *
     * @internal param array $surveyFieldTime
     * @return array|mixed $result
     */
    public function changeFieldData($pid, $surveyStatus = 'ACTIVE', $surveyFieldStartTime = '2000-01-01T00:00:00+00:00', $surveyFieldEndTime = '2020-01-01T00:00:00+00:00')
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.surveys.changeFieldData",
                                         array('surveyId'        => $pid,
                                               'surveyStatus'    => $surveyStatus,
                                               'surveyFieldTime' => array('startTime' => $surveyFieldStartTime,
                                                                          'endTime'   => $surveyFieldEndTime))
      );
    }

    /**
     * createStandardReport
     *
     * @param int    $pid
     * @param string $title
     * @param string $description
     *
     * @return array|mixed $result
     */
    public function createStandardReport($pid, $title, $description)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.reports.createStandardReport",
                                         array('surveyId'    => $pid,
                                               'title'       => $title,
                                               'description' => $description,)
      );
    }

    /**
     * createReportPublication
     *
     * @param int    $reportId
     * @param string $reportType
     * @param string $fileName
     *
     * @return array|mixed $result
     */
    public function createReportPublication($reportId, $reportType = "html", $fileName = "default.htm")
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.reports.createReportPublication",
                                         array('reportId'   => $reportId,
                                               'reportType' => $reportType,
                                               'fileName'   => $fileName,)
      );
    }

    /**
     * checkReportPublication
     *
     * @param int $reportId
     * @param int $reportPublicationId
     *
     * @return array|mixed $result
     */
    public function checkReportPublication($reportId, $reportPublicationId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.reports.checkReportPublication",
                                         array('reportId'            => $reportId,
                                               'reportPublicationId' => $reportPublicationId)
      );
    }

    /**
     * getReportPublication
     *
     * @param int $reportId
     * @param int $reportPublicationId
     *
     * @return array|mixed $result base64
     */
    public function getReportPublication($reportId, $reportPublicationId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.reports.getReportPublication",
                                         array('reportId'            => $reportId,
                                               'reportPublicationId' => $reportPublicationId)
      );
    }

    /**
     * get PlaceholderList
     *
     * @param int $surveyId
     *
     * @return array|mixed $result
     */
    public function placeholdersGetList($surveyId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.placeholders.getList",
                                         array('surveyId' => $surveyId)
      );
    }

    /**
     * get PlaceholderList for a language id
     *
     * @param int $surveyId
     * @param int $languageId
     *
     * @return array|mixed $result
     */
    public function placeholdersGetListTranslated($surveyId, $languageId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.placeholders.getListTranslated",
                                         array('surveyId'   => $surveyId,
                                               'languageId' => $languageId)
      );
    }

    /**
     * change Placeholder
     *
     * @param int    $surveyId
     * @param int    $id
     * @param string $text
     *
     * @return array|mixed $result
     */
    public function placeholdersChange($surveyId, $id, $text)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.placeholders.change",
                                         array('surveyId' => $surveyId,
                                               'id'       => $id,
                                               'text'     => $text)
      );
    }

    /**
     * surveyPlaceholdersChangeTranslated
     *
     * Description (from WSDL):
     * change an existings survey placeholder's text or handle (identifier) in a certain survey language
     *
     * @param int    $surveyId
     * @param int    $languageId
     * @param int    $id
     * @param string $text
     *
     * @return array|mixed
     */
    public function surveyPlaceholdersChangeTranslated($surveyId, $languageId, $id, $text)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.placeholders.changeTranslated",
                                         array('surveyId'   => $surveyId,
                                               'languageId' => $languageId,
                                               'id'         => $id,
                                               'text'       => $text));
    }

    /**
     * getDescriptionList
     *
     * @param int    $surveyId
     * @param string $varTypes
     *
     * @return array $items
     */
    public function getDescriptionList($surveyId, $varTypes)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.variables.getDescriptionList",
                                         array('surveyId'            => $surveyId,
                                               'surveyVariableTypes' => (array)$varTypes));
    }

    /**
     * getAdminUserId
     *
     * @param string $sesId
     *
     * @return int $userId
     */
    public function getAdminUserId($sesId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.session.getAdminUserId", array('sessionId' => $sesId));
    }

    /**
     * getAdminUser
     *
     * @return array|mixed $user
     */
    public function getAdminUser()
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.staff.getAdminUser", array());
    }

    /**
     * getSampleUsers
     *
     * @param int   $sampleId
     * @param array $returnDataFields (only first element will be taken, array needs to be given to make this work under 8.1)
     *
     * @return array|mixed
     */
    public function getSampleUsers($sampleId, array $returnDataFields)
    {
      $return = $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getListBySample",
                                            array('sampleId'             => $sampleId,
                                                  'returnIdentifierType' => $returnDataFields[0]));
      if (@!$return["success"])
        return $return;

      // make result compatible to 8.1
      $newReturn = array();
      foreach ($return['return'] as $value)
        $newReturn[] = array($value);

      $return['return'] = $newReturn;
      return $return;
    }

    /**
     * addPanelistsCsv
     *
     * Note: Data must contain column headers in line 1
     *
     * @param string $identifier
     * @param string $csvData
     * @param string $mappingColumn (optional)
     *
     * @return array|mixed
     */
    public function addPanelistsCsv($identifier, $csvData, $mappingColumn = null)
    {
      $params = array('returnIdentifierType' => $identifier,
                      'csvData'              => $csvData);
      if ($mappingColumn != null)
        $params['mappingColumn'] = $mappingColumn;

      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.addCSV", $params);
    }

    /**
     * panelPanelistsAdd
     * adds a panelist to the panel
     *
     * @param string $returnIdentifierType
     * @param array  $panelistData
     *
     * @return array|mixed
     */
    public function panelPanelistsAdd($returnIdentifierType, $panelistData)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.add",
                                         array('returnIdentifierType' => $returnIdentifierType,
                                               'panelistData'         => $panelistData));
    }

    /**
     * updatePanelistsCsv
     *
     * Note: Data must contain column headers in line 1
     *
     * @param string $identifier
     * @param string $csvData
     *
     * @return array|mixed
     */
    public function updatePanelistsCsv($identifier, $csvData)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.changeCSV",
                                         array('identifierType' => $identifier,
                                               'csvData'        => $csvData));
    }

    /**
     * changePanelist
     *
     * change an existing panelist's data
     * Note: this is identical to "updatePanelist" in the webservice but uses the Service Layer
     *       the Service Layer is BR0KSEN if u_gender != 1 && u_gender != 2 !! (should be fixed by now, BF 25.10.11)
     *
     * @param string $identifierType
     * @param string $identifierValue
     * @param array  $panelistData
     *
     * @return array|mixed
     */
    public function changePanelist($identifierType, $identifierValue, $panelistData)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.change",
                                         array('identifierType'  => $identifierType,
                                               'identifierValue' => $identifierValue,
                                               'panelistData'    => $panelistData));
    }

    /**
     * getPanelistsByIdentifier
     *
     * Description (from WSDL):
     * returns a list of actual panelists from a list of possible panelist identifiers
     * using this service it can be checked if specific panelists are already contained in the panel or not
     *
     * @param string $identifierType
     * @param array  $identifierList
     * @param array  $returnDataFields (will be ignored, but needs to be given to remain compatible after an upgrade to 8.1)
     *
     * @return array|mixed
     */
    public function getPanelistsByIdentifier($identifierType, array $identifierList, /** @noinspection PhpUnusedParameterInspection */
                                             array $returnDataFields)
    {
      $return = $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getListByIdentifiers",
                                            array('identifierType' => $identifierType,
                                                  'identifierList' => $identifierList));
      if (@!$return["success"])
        return $return;

      // make result compatible to 8.1
      $newReturn = array();
      foreach ($return['return'] as $value)
        $newReturn[] = array($value);

      $return['return'] = $newReturn;
      return $return;
    }

    /**
     * getPanelist
     *
     * Description (from WSDL):
     * get information about a panelist
     *
     * @param string $identifierType
     * @param string $identifierValue
     *
     * @return array|mixed
     */
    public function getPanelist($identifierType, $identifierValue)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.get",
                                         array('identifierType'  => $identifierType,
                                               'identifierValue' => $identifierValue));
    }

    /**
     * surveySamplesGetList
     *
     * Description (from WSDL):
     * return the list of samples for a survey
     *
     * @param int $surveyId
     *
     * @return array|mixed
     */
    public function surveySamplesGetList($surveyId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.samples.getList",
                                         array('surveyId' => (int)$surveyId));
    }

    /**
     * sampleGetUsers
     *
     * Description (from WSDL):
     * returns a list of participants
     *
     * @param int $sampleId
     *
     * @return array|mixed
     */
    public function sampleGetUsers($sampleId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.participants.getList",
                                         array('sampleId' => (int)$sampleId));
    }

    /**
     * surveyParticipantsSendMail
     *
     * Description (from WSDL):
     * notify survey participants via email/sms
     *
     * @param int            $sampleId
     * @param array          $uids
     * @param array          $templateArray
     * @param bool|\datetime $sendDate
     *
     * @return array|mixed
     */
    public function surveyParticipantsSendMail($sampleId, $uids, $templateArray, $sendDate = false)
    {
      $sendDate = (!$sendDate) ? gmdate("c") : $sendDate;
      return $this->makeServiceLayerCall($this->url . "&method=survey.participants.sendMail",
                                         array('sampleId'     => $sampleId,
                                               'uids'         => $uids,
                                               'mailTemplate' => $templateArray,
                                               'sendOptions'  => array('sendDate' => $sendDate)));
    }

    /**
     * surveyParticipantsImportCsv
     *
     * Description (from WSDL):
     * add multiple participants from a CSV file to a survey or update existing participants using data from a CSV file
     *
     * @param int    $sampleId
     * @param array  $csvData
     * @param string $returnIdentifierType, enum { 'u_email', 'code', 'uid' }
     * @param bool   $allowDuplicateEmails
     * @param string $mappingColumn       (seems to be optional)
     *
     * @return array|mixed
     */
    public function surveyParticipantsImportCsv($sampleId, $csvData, $returnIdentifierType = 'uid', $allowDuplicateEmails = false, $mappingColumn = null)
    {
      $params = array('sampleId'             => $sampleId,
                      'csvData'              => $csvData,
                      'returnIdentifierType' => $returnIdentifierType,
                      'allowDuplicateEmails' => $allowDuplicateEmails);
      if ($mappingColumn != null)
        $params['mappingColumn'] = $mappingColumn;

      return $this->makeServiceLayerCall($this->url . "&method=survey.participants.importCSV", $params);
    }

    /**
     * addParticipantsToSurvey
     *
     * Description (from WSDL):
     * add multiple participants to a survey
     *
     * @param int    $sampleId
     * @param string $returnIdentifierType
     * @param array  $participantList
     *
     * @return array|mixed
     */
    public function addParticipantsToSurvey($sampleId, $returnIdentifierType, $participantList)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.participants.addList",
                                         array('sampleId'             => (int)$sampleId,
                                               'returnIdentifierType' => $returnIdentifierType,
                                               'participantList'      => $participantList));
    }

    /**
     * getMasterdataVars
     *
     * Description (from WSDL):
     * returns the panel's master data variables along with their predefined answer codes
     *
     * @return array|mixed
     */
    public function getMasterdataVars()
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.masterdata.getDescriptionList", array());
    }

    /**
     * getParticipantsByCriteria
     *
     * Description (from WSDL):
     * returns a list of participants according to the specified sample. the operation is allowed for personalized surveys only.
     * NOTE: does NOT return uid!
     *
     * @param $sampleId
     * @param $condition
     *
     * @internal param array $params
     * @return array $surveyData
     */
    public function getParticipantsByCriteria($sampleId, $condition)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.participants.getListByCriteria",
                                         array('sampleId'         => $sampleId,
                                               'logicalCondition' => $condition));
    }

    /**
     * getSurveyResultsRawdataCsv
     *
     * Description (from WSDL):
     * return the survey results raw data in CSV format
     *
     * @param int   $surveyId
     * @param array $exportTypes, enum { 'QUESTIONNAIRE', 'PARTICIPANT' }
     * @param array $includeVariables
     *
     * @return array|mixed
     */
    public function getSurveyRawDataCsv($surveyId, $exportTypes = array('QUESTIONNAIRE', 'PARTICIPANT'), $includeVariables = null)
    {
      $params = array('surveyId'    => $surveyId,
                      'exportTypes' => $exportTypes);
      if ($includeVariables != null)
        $params['includeVariables'] = $includeVariables;

      return $this->makeServiceLayerCall($this->url . "&method=survey.results.getRawdataCSV", $params);
    }

    /**
     * emptySurvey
     *
     * Description (from WSDL):
     * delete all participants from a sample, including their already collected survey results. the operation is allowed for personalized and anonymous surveys only.
     *
     * @param int $sampleId
     *
     * @return array|mixed
     */
    public function emptySurvey($sampleId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.participants.deleteAll", array('sampleId' => $sampleId));
    }

    /**
     * deleteParticipantFromSurvey
     *
     * Description (from WSDL):
     * delete a participant from a survey sample, along with any already collected survey results for the participant
     *
     * @param int $sampleId
     * @param int $userId
     *
     * @return array|mixed
     */
    public function deleteParticipantFromSurvey($sampleId, $userId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.participants.delete",
                                         array('sampleId' => $sampleId,
                                               'userId'   => $userId));
    }

    /**
     * surveyMailtemplatesGetList
     *
     * Description (from WSDL):
     * get a list of mail templates the user has access to
     *
     * @param array $mailTemplateTypes
     *
     * @return array|mixed
     */
    public function surveyMailtemplatesGetList($mailTemplateTypes)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.mailtemplates.getList",
                                         array('mailTemplateTypes' => $mailTemplateTypes));
    }

    /**
     * efsJobsGetStatus
     *
     * Description (from WSDL):
     * query the status of a single EFS job
     *
     * @param string $jobId
     *
     * @return array|mixed
     */
    public function efsJobsGetStatus($jobId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.jobs.getStatus",
                                         array('jobId' => $jobId));
    }

    /**
     * efsStaffAddAdminUser
     *
     * Description (from WSDL):
     * Create a new user for the EFS admin interface
     *
     * @param array $adminData
     *
     * @return array|mixed
     */
    public function efsStaffAddAdminUser($adminData)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.staff.addAdminUser",
                                         array('adminData' => $adminData));
    }

    /**
     * efsSystemGetLoad
     *
     * Description (from WSDL):
     * query the current system's 1-minute load average
     */
    public function efsSystemGetLoad()
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.system.getLoad", array());
    }

    /**
     * panelVariablesGetDescriptionList
     *
     * Description (from WSDL):
     * returns the panel's variables along with their predefined answer codes (if any)
     */
    public function panelVariablesGetDescriptionList()
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.variables.getDescriptionList", array());
    }

    /**
     * qtoolResultsGetSurveyQuotes
     *
     * Description (from WSDL):
     * return the completed surveys results for specific variables in a structured format
     *
     * @param integer $surveyId
     * @param array   $codedVariables
     * @param array   $quoteVariables
     * @param integer $count
     *
     * @return array|mixed
     */
    public function qtoolResultsGetSurveyQuotes($surveyId, $codedVariables, $quoteVariables, $count)
    {
      return $this->makeServiceLayerCall($this->url . "&method=qtool.results.getSurveyQuotes",
                                         array('surveyId'       => $surveyId,
                                               'codedVariables' => $codedVariables,
                                               'quoteVariables' => $quoteVariables,
                                               'count'          => $count));
    }

    /**
     * surveyLayoutsApply
     *
     * Description (from WSDL):
     * apply a survey layout in a certain survey
     *
     * @param integer $surveyId
     * @param integer $layoutId
     *
     * @return array|mixed
     */
    public function surveyLayoutsApply($surveyId, $layoutId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.layouts.apply",
                                         array('surveyId' => $surveyId,
                                               'layoutId' => $layoutId));
    }

    /**
     * surveyLayoutsGetList
     *
     * Description (from WSDL):
     * returns a list of all survey layouts the user has access to
     */
    public function surveyLayoutsGetList()
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.layouts.getList", array());
    }

    /**
     * surveyLayoutsGetListByCriteria
     *
     * Description (from WSDL):
     * returns a list of survey layouts the user has access to, based on search criteria
     *
     * @param array $logicalCondition
     *
     * @return array|mixed
     */
    public function surveyLayoutsGetListByCriteria($logicalCondition)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.layouts.getListByCriteria",
                                         array('logicalCondition' => $logicalCondition));
    }

    /**
     * surveyLogosChange
     *
     * Description (from WSDL):
     * change a logo in a survey
     *
     * @param integer $surveyId
     * @param string  $logoType
     * @param         $image
     *
     * @return array|mixed
     */
    public function surveyLogosChange($surveyId, $logoType, $image)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.logos.change",
                                         array('surveyId' => $surveyId,
                                               'logoType' => $logoType,
                                               'image'    => $image));
    }

    /**
     * surveyReportsDeleteReport
     *
     * Description (from WSDL):
     * delete a report for a survey
     *
     * @param integer $reportId
     *
     * @return array|mixed
     */
    public function surveyReportsDeleteReport($reportId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.reports.deleteReport",
                                         array('reportId' => $reportId));
    }

    /**
     * surveyResultsGetParticipations
     *
     * Description (from WSDL):
     * return frequencies for all disposition codes achieved in a survey, thus basic information about the survey's participation statuses
     *
     * @param integer $surveyId
     *
     * @return array|mixed
     */
    public function surveyResultsGetParticipations($surveyId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.results.getParticipations",
                                         array('surveyId' => $surveyId));
    }

    /**
     * surveyResultsGetSimpleStatistics
     *
     * Description (from WSDL):
     * return frequency statistics for all closed questions of a survey
     *
     * @param integer $surveyId
     *
     * @return array|mixed
     */
    public function surveyResultsGetSimpleStatistics($surveyId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.results.getSimpleStatistics",
                                         array('surveyId' => $surveyId));
    }

    /**
     * surveySurveysChangeType
     *
     * Description (from WSDL):
     * Change the type of a survey
     *
     * @param integer $surveyId
     * @param string  $surveyType
     *
     * @return array|mixed
     */
    public function surveySurveysChangeType($surveyId, $surveyType)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.surveys.changeType",
                                         array('surveyId'   => $surveyId,
                                               'surveyType' => $surveyType));
    }

    /**
     * surveySurveysDelete
     *
     * Description (from WSDL):
     * deletes a survey
     *
     * @param integer $surveyId
     *
     * @return array|mixed
     */
    public function surveySurveysDelete($surveyId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.surveys.delete",
                                         array('surveyId' => $surveyId));
    }

    /**
     * surveySurveysGetActiveList
     *
     * Description (from WSDL):
     * get a list of active surveys the user has access to
     */
    public function surveySurveysGetActiveList()
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.surveys.getActiveList", array());
    }

    /**
     * surveySurveysGetQuestionnaireStructure
     *
     * Description (from WSDL):
     * return the questionnaire structure for a survey
     *
     * @param integer $surveyId
     *
     * @return array|mixed
     */
    public function surveySurveysGetQuestionnaireStructure($surveyId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.surveys.getQuestionnaireStructure",
                                         array('surveyId' => $surveyId));
    }

    /**
     * callCustomService
     * use this to call custom Service Layer methods that are only used in a few projects
     * use with the name of the service (e.g. "custom.surveyparticipants.sendMailByTemplateId") and the parameters as required (see WSDL)
     *
     * @param string $serviceName
     * @param array  $params
     *
     * @return array|mixed
     */
    public function callCustomService($serviceName, $params)
    {
      return $this->makeServiceLayerCall($this->url . "&method=" . $serviceName, $params);
    }

    /**
     * makeServiceLayerCall
     *
     * @param string $callUrl
     * @param array  $params
     *
     * @return array|mixed
     */
    protected function makeServiceLayerCall($callUrl, $params)
    {
      // configure call
      $this->client->setUri($callUrl);
      $this->client->setRawData(serialize($params));

      // execute call and get results
      $response = $this->client->request();
      $return   = $response->getBody();

      if ($response->isError())
      {
        $return = unserialize($return);
        return array('success' => false,
                     'errors'  => array(@$return['error']['message'] . ' (Code: ' . @$return['error']['code'] . ', Status: ' . $response->getStatus() . ')'));
      }

      return unserialize($return);
    }
  }