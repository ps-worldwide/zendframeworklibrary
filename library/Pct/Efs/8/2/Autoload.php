<?php
  /**
   * Pct_Efs_8_2_Autoload
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoload.php,v 1.3 2013/10/16 09:28:38 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Efs_8_2_Autoload extends Pct_Efs_8_1_Autoload
  {
    protected $_efsVersionExtension = '8_2_';
  }