<?php
  /**
   * Pct_Efs_8_2_Api_Servicelayer
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Servicelayer.php,v 1.52 2013/10/16 09:28:38 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Efs_8_2_Api_Servicelayer extends Pct_Efs_8_1_Api_Servicelayer
  {
    /**
     * getPanelGroups
     *
     * @param int $categoryId
     *
     * @return array $sampleData
     */
    public function getPanelGroups($categoryId = null)
    {
      $param = array();
      if ($categoryId !== null)
        $param = array('restrictCategoryId' => $categoryId);

      return $this->makeServiceLayerCall($this->url . "&method=panel.groups.getList", $param);
    }

    /**
     * addPanelGroup
     *
     * @param string $name
     * @param string $description
     * @param int    $categoryId
     *
     * @return array $result
     */
    public function addPanelGroup($name, $description, $categoryId = 1)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.groups.add", array('name'        => $name,
                                                                                        'description' => $description,
                                                                                        'categoryId'  => $categoryId));
    }

    /**
     * getPanelistsByGroup
     *
     * Description (from WSDL):
     * returns a list of panelists in a specific panel group
     *
     * @param int   $groupId
     * @param array $returnDataFields
     *
     * @return array|mixed
     */
    public function getPanelistsByGroup($groupId, $returnDataFields)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getListByGroup",
                                         array('groupId'          => $groupId,
                                               'returnDataFields' => $returnDataFields));
    }

    /**
     * deleteGratification
     *
     * Description (from WSDL)
     * Delete Gratification, includes deletion of prizes and winners
     *
     * @param int $gratId
     *
     * @return array|mixed
     */
    public function deleteGratification($gratId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.lotteries.delete",
                                         array('id' => $gratId));
    }

    /**
     * deletePanelGroup
     *
     * Description (from WSDL)
     * delete a panel group along with any panelist-group membership records plus any update rules for group.
     * Panelist data of group members will not be deleted.
     *
     * @param int $groupId
     *
     * @return array|mixed
     */
    public function deletePanelGroup($groupId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.groups.delete",
                                         array('id' => $groupId));
    }

    /**
     * addPanelistsToGroup
     *
     * Description (from WSDL)
     * add panelists to an existing panel group
     *
     * @param int    $groupId
     * @param string $identifierType
     * @param array  $identifiers
     *
     * @return array|mixed
     */
    public function addPanelistsToGroup($groupId, $identifierType, $identifiers)
    {
      // NOTE: this check can be removed (if necessary) as a fix in the Service Layer
      // will now return a proper error msg, if no identifiers are given
      if (count($identifiers) == 0)
        return $this->emptyReturn;

      return $this->makeServiceLayerCall($this->url . "&method=panel.groups.addPanelists",
                                         array('id'               => $groupId,
                                               'identifierType'   => $identifierType,
                                               'identifierValues' => $identifiers));
    }

    /**
     * createGratification
     *
     * Description (from WSDL)
     * Create lottery
     *
     * @param string $title
     * @param string $description
     * @param int    $status
     * @param int    $groupId
     *
     * @return array|mixed
     */
    public function createGratification($title, $description, $status, $groupId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.lotteries.add",
                                         array('title'       => $title,
                                               'description' => $description,
                                               'status'      => $status,
                                               'gid'         => $groupId));
    }

    /**
     * getGratification
     *
     * Description (from WSDL)
     * Get Lottery by Lottery id * Provides lottery object to access gratification and other parameters (??)
     *
     * @param int $gratId
     *
     * @return array|mixed
     */
    public function getGratification($gratId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.lotteries.get",
                                         array('id' => $gratId));
    }

    /**
     * emptyPanelGroup
     *
     * @param int $groupId
     *
     * @return array|mixed
     */
    public function emptyPanelGroup($groupId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.groups.empty",
                                         array('id' => $groupId));
    }

    /**
     * setGratificationStatus
     *
     * Description (from WSDL)
     * Update status of lottery with given Lottery id
     *
     * @param int $gratId
     * @param int $status
     *
     * @return array|mixed
     */
    public function setGratificationStatus($gratId, $status)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.lotteries.change",
                                         array('id'     => $gratId,
                                               'status' => $status));
    }

    /**
     * deleteGratificationWinners
     *
     * Description (from WSDL)
     * Delete winner
     *
     * @param int $gratId
     *
     * @return array|mixed
     */
    public function deleteGratificationWinners($gratId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.lotteries.deleteWinners",
                                         array('id' => $gratId));
    }

    /**
     * getGratificationWinners
     *
     * Description (from WSDL)
     * Get winners by gratification id
     *
     * @param int $gratId
     *
     * @return array|mixed
     */
    public function getGratificationWinners($gratId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.lotteries.getWinners",
                                         array('id' => $gratId));
    }

    /**
     * resetBonusPoints
     *
     * Description (from WSDL)
     * resets the bonus points of one or multiple panelists to zero.
     * this action will also add a note to each panelists' bonus points balance so panelists get informed about the nullification
     * if a psnelist already has a balance of zero points, no additional note will be added to his/her points log
     *
     * @param string $identifierType
     * @param array  $identifierList
     * @param string $description
     *
     * @return array|mixed
     */
    public function resetBonusPoints($identifierType, $identifierList, $description)
    {
      if (count($identifierList) == 0)
        return $this->emptyReturn;

      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.resetBonusPoints",
                                         array('identifierType' => $identifierType,
                                               'identifierList' => $identifierList,
                                               'description'    => $description));
    }

    /**
     * panelistIncrementValue
     *
     * Description (from WSDL)
     * increment/decrement a master data variable's value for one or multiple panelists
     *
     * @param string $identifierType
     * @param array  $identifierList
     * @param string $variableName
     * @param float  $step
     *
     * @return array|mixed
     */
    public function panelistIncrementValue($identifierType, $identifierList, $variableName, $step)
    {
      if (count($identifierList) == 0)
        return $this->emptyReturn;

      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.incrementValue",
                                         array('identifierType' => $identifierType,
                                               'identifierList' => $identifierList,
                                               'variableName'   => $variableName,
                                               'step'           => $step));
    }

    /**
     * getRawDataCSVCompleted
     *
     * Description (from WSDL)
     * return the survey results raw data in CSV format. The export is restricted to completed interviews and to a specifiable date range
     *
     * @param $surveyId
     * @param $exportTypes
     * @param $dateRange
     * @param survey:variableList $includeVariables - optional list of variables to include
     *
     * @return array|mixed $result - raw survey results data in CSV format
     */
    public function getSurveyRawDataCsvCompleted($surveyId, $exportTypes, $dateRange, $includeVariables = null)
    {
      $params = array('surveyId'    => $surveyId,
                      'exportTypes' => $exportTypes,
                      'dateRange'   => $dateRange);
      if ($includeVariables != null)
        $params['includeVariables'] = $includeVariables;

      return $this->makeServiceLayerCall($this->url . "&method=survey.results.getRawdataCSVCompleted", $params);
    }

    /**
     * getSurveyRawDataCsvAll
     *
     * Description (from WSDL)
     * return the survey results raw data in CSV format. The export is restricted to a specifiable date range
     *
     * @param      $surveyId         - id of survey
     * @param      $exportTypes      - types of values (scope) to export
     * @param      $dateRange        - date range the export is restricted to
     * @param null $includeVariables - optional list of variables to include
     * @param null $sort             - optional list of columns to sort by
     *
     * @return array|mixed $result - raw survey results data in CSV format
     */
    public function getSurveyRawDataCsvAll($surveyId, $exportTypes, $dateRange, $includeVariables = null, $sort = null)
    {
      $params = array('surveyId'    => $surveyId,
                      'exportTypes' => $exportTypes,
                      'dateRange'   => $dateRange);
      if ($includeVariables != null)
        $params['includeVariables'] = $includeVariables;
      if ($sort != null)
        $params['sort'] = $sort;

      return $this->makeServiceLayerCall($this->url . "&method=survey.results.getRawdataCSVAll", $params);
    }

    /**
     * getEfsBuildName
     *
     * Description (from WSDL)
     * get buildname of the EFS installation
     */
    public function getEfsBuildName()
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.system.getBuildName", array());
    }

    /**
     * addMasterdataVariable
     *
     * @param string  $name
     * @param string  $label
     * @param string  $type
     * @param int     $categoryId
     * @param boolean $optional
     *
     * @return array|mixed $result
     */
    public function addMasterdataVariable($name, $label, $type, $categoryId, $optional)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.masterdata.add",
                                         array('name'     => $name,
                                               'label'    => $label,
                                               'type'     => $type,
                                               'category' => $categoryId,
                                               'optional' => $optional));
    }

    /**
     * addSampleToSurvey
     *
     * @param int    $pid
     * @param string $title
     * @param string $description
     *
     * @return array|mixed $result
     */
    public function addSampleToSurvey($pid, $title, $description)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.samples.add",
                                         array('pid'         => $pid,
                                               'title'       => $title,
                                               'description' => $description));
    }

    /**
     * grantBonusPoints
     *
     * Description (from WSDL)
     * Grants Bonuspoints to a list of panelists
     *
     * @param string $identifierType
     * @param array  $identifierList
     * @param int    $surveyId
     * @param int    $points
     * @param string $description
     *
     * @return array|mixed $result
     */
    public function grantBonusPointsForSurvey($identifierType, $identifierList, $surveyId, $points, $description)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.grantBonusPointsForSurveyParticipation",
                                         array('identifierType' => $identifierType,
                                               'identifierList' => $identifierList,
                                               'surveyId'       => $surveyId,
                                               'points'         => $points,
                                               'description'    => $description));
    }

    /**
     * getResponseCategoryByVarname
     *
     * Description (from WSDL)
     * get response-categories by variable name and survey id
     *
     * @param int    $surveyId
     * @param string $variableName
     *
     * @return array|mixed $result
     */
    public function getResponseCategoryByVarname($surveyId, $variableName)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.questions.getResponseCategoriesByVarname",
                                         array('surveyId' => $surveyId,
                                               'varname'  => $variableName));
    }

    /**
     * panelExternalsurveysDelete
     *
     * Description (from WSDL):
     * Deletes an external study with all its participation data.
     *
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
    public function panelExternalsurveysDelete($identifierType, $identifierValue)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.externalsurveys.delete",
                                         array(
                                              'identifierType'  => $identifierType,
                                              'identifierValue' => $identifierValue,
                                         ));
    }

    /**
     * panelExternalsurveysGet
     *
     * Description (from WSDL):
     * Retrieves the data of the external survey
     *
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
    public function panelExternalsurveysGet($identifierType, $identifierValue)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.externalsurveys.get",
                                         array(
                                              'identifierType'  => $identifierType,
                                              'identifierValue' => $identifierValue,
                                         ));
    }

    /**
     * panelExternalsurveysGetList
     *
     * Description (from WSDL):
     * Retrieves a list of all external surveys.
     *
     */
    public function panelExternalsurveysGetList()
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.externalsurveys.getList",
                                         array());
    }

    /**
     * panelExternalsurveysGetListModified
     *
     * Description (from WSDL):
     * Retrieves a list of external surveys that were modified since a given timestamp
     *
     * @param $timestamp
     *
     * @return array|mixed
     */
    public function panelExternalsurveysGetListModified($timestamp)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.externalsurveys.getListModified",
                                         array(
                                              'timestamp' => $timestamp,
                                         ));
    }

    /**
     * panelGroupsEmptyGroup
     *
     * Description (from WSDL):
     * empty a panel group by deleting all members from it
     *
     * @param $id
     *
     * @return array|mixed
     */
    public function panelGroupsEmptyGroup($id)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.groups.emptyGroup",
                                         array(
                                              'id' => $id,
                                         ));
    }

    /**
     * panelGroupsGetGroupMembers
     *
     * Description (from WSDL):
     * Supplies a list of panelists which are members in a given group
     *
     * @param $returnIdentifierType
     * @param $id
     *
     * @return array|mixed
     */
    public function panelGroupsGetGroupMembers($returnIdentifierType, $id)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.groups.getGroupMembers",
                                         array(
                                              'returnIdentifierType' => $returnIdentifierType,
                                              'id'                   => $id,
                                         ));
    }

    /**
     * panelGroupsRemovePanelists
     *
     * Description (from WSDL):
     * Removes panelist from group
     *
     * @param $id
     * @param $identifierType
     * @param $identifierValues
     *
     * @return array|mixed
     */
    public function panelGroupsRemovePanelists($id, $identifierType, $identifierValues)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.groups.removePanelists",
                                         array(
                                              'id'               => $id,
                                              'identifierType'   => $identifierType,
                                              'identifierValues' => $identifierValues,
                                         ));
    }

    /**
     * panelPanelistsAddV1
     *
     * Description (from WSDL):
     * Adds a panelist to the panel.
     *
     * @param $returnIdentifierType
     * @param $panelistData
     *
     * @return array|mixed
     */
    public function panelPanelistsAddV1($returnIdentifierType, $panelistData)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.addV1",
                                         array(
                                              'returnIdentifierType' => $returnIdentifierType,
                                              'panelistData'         => $panelistData,
                                         ));
    }

    /**
     * panelPanelistsChangeV1
     *
     * Description (from WSDL):
     * Change an existing panelist's data
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $panelistData
     *
     * @return array|mixed
     */
    public function panelPanelistsChangeV1($identifierType, $identifierValue, $panelistData)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.changeV1",
                                         array(
                                              'identifierType'  => $identifierType,
                                              'identifierValue' => $identifierValue,
                                              'panelistData'    => $panelistData,
                                         ));
    }

    /**
     * panelPanelistsDelete
     *
     * Description (from WSDL):
     * Delete panelist
     *
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
    public function panelPanelistsDelete($identifierType, $identifierValue)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.delete",
                                         array(
                                              'identifierType'  => $identifierType,
                                              'identifierValue' => $identifierValue,
                                         ));
    }

    /**
     * panelPanelistsDeleteExternalAuthentication
     *
     * Description (from WSDL):
     * Deletes an external authentication mapping from EFS. Please mind: Only the mapping will be deleted. The panelist will not be removed from EFS.
     *
     * @param $externalServiceId
     * @param $externalAuthenticationValue
     *
     * @return array|mixed
     */
    public function panelPanelistsDeleteExternalAuthentication($externalServiceId, $externalAuthenticationValue)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.deleteExternalAuthentication",
                                         array(
                                              'externalServiceId'           => $externalServiceId,
                                              'externalAuthenticationValue' => $externalAuthenticationValue,
                                         ));
    }

    /**
     * panelPanelistsGetBonusPoints
     *
     * Description (from WSDL):
     * Returns the bonus points of a given panelist.
     *
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
    public function panelPanelistsGetBonusPoints($identifierType, $identifierValue)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getBonusPoints",
                                         array(
                                              'identifierType'  => $identifierType,
                                              'identifierValue' => $identifierValue,
                                         ));
    }

    /**
     * panelPanelistsGetExternalAuthentication
     *
     * Description (from WSDL):
     * Returns the EFS identifier for a member of an external system. If the member has not been registered in EFS using the save_external_panelist_authentication web service before, this service will throw an exception.
     *
     * @param $externalServiceId
     * @param $externalAuthenticationValue
     * @param $identifierReturnType
     *
     * @return array|mixed
     */
    public function panelPanelistsGetExternalAuthentication($externalServiceId, $externalAuthenticationValue, $identifierReturnType)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getExternalAuthentication",
                                         array(
                                              'externalServiceId'           => $externalServiceId,
                                              'externalAuthenticationValue' => $externalAuthenticationValue,
                                              'identifierReturnType'        => $identifierReturnType,
                                         ));
    }

    /**
     * panelPanelistsGetGroupMembership
     *
     * Description (from WSDL):
     * Returns a list of panel groups in which the panelist is a member
     *
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
    public function panelPanelistsGetGroupMembership($identifierType, $identifierValue)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getGroupMembership",
                                         array(
                                              'identifierType'  => $identifierType,
                                              'identifierValue' => $identifierValue,
                                         ));
    }

    /**
     * panelPanelistsGetListOfModified
     *
     * Description (from WSDL):
     * Returns the identifiers of all panelists whose data (participant data plus master data) has changed since this given timestamp.
     *
     * @param $identifierType
     * @param $modificationDate
     * @param $excludeModifier
     *
     * @return array|mixed
     */
    public function panelPanelistsGetListOfModified($identifierType, $modificationDate, $excludeModifier)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getListOfModified",
                                         array(
                                              'identifierType'   => $identifierType,
                                              'modificationDate' => $modificationDate,
                                              'excludeModifier'  => $excludeModifier,
                                         ));
    }

    /**
     * panelPanelistsGetOnline
     *
     * Description (from WSDL):
     * Returns the panelists who are online on the website. Panelists are returned as an array of e-mail addresses, pseudonyms, panelist codes etc. together with the language ID of the website they are active on. A panelist might be active in multiple languages at the same time (e.g. by viewing the English version of the website and then changing to the French version). In this case, the panelist will be returned twice in the result array, but with different language IDs each. The interval for counting as being online can be specified in the global configuration of the website in the EFS admin area.
     *
     * @param $identifierReturnType
     * @param $languageIds
     *
     * @return array|mixed
     */
    public function panelPanelistsGetOnline($identifierReturnType, $languageIds)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getOnline",
                                         array(
                                              'identifierReturnType' => $identifierReturnType,
                                              'languageIds'          => $languageIds,
                                         ));
    }

    /**
     * panelPanelistsGetPointsHistory
     *
     * Description (from WSDL):
     * Returns the account balance/bonus points positions for a panelist that happened in the last months. The panelist can be identified by different criteria (user_id, panelist code, pseudonym etc.).
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $monthsRange
     * @param $skipEmpty
     *
     * @return array|mixed
     */
    public function panelPanelistsGetPointsHistory($identifierType, $identifierValue, $monthsRange, $skipEmpty)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getPointsHistory",
                                         array(
                                              'identifierType'  => $identifierType,
                                              'identifierValue' => $identifierValue,
                                              'monthsRange'     => $monthsRange,
                                              'skipEmpty'       => $skipEmpty,
                                         ));
    }

    /**
     * panelPanelistsGetPointsStatus
     *
     * Description (from WSDL):
     * Returns the account balance/bonus points positions for all panelists of the installation that happened since the last call to the service. This method can be used to do a regular, incremental replication of the panel's bonus points to another application (where EFS is used as the master system for managing the points). The panelist identifier for the returned balance positions can be user_id, panelist code, pseudonym etc.
     *
     * @param $returnIdentifierType
     * @param $fetchAll
     * @param $logTransfer
     *
     * @return array|mixed
     */
    public function panelPanelistsGetPointsStatus($returnIdentifierType, $fetchAll, $logTransfer)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getPointsStatus",
                                         array(
                                              'returnIdentifierType' => $returnIdentifierType,
                                              'fetchAll'             => $fetchAll,
                                              'logTransfer'          => $logTransfer,
                                         ));
    }

    /**
     * panelPanelistsGetRedemptionStatus
     *
     * Description (from WSDL):
     * Returns the bonus points redemption positions for all panelists of the installation that happened since the last call to the service. This method can be used to do a regular, incremental replication of the panel's bonus points redemption to another application (where EFS is used as the master system for managing the points). The panelist identifier for the returned redemption can be user_id, panelist code, pseudonym etc.
     *
     * @param $identifierType
     * @param $fetchAll
     * @param $logTransfer
     *
     * @return array|mixed
     */
    public function panelPanelistsGetRedemptionStatus($identifierType, $fetchAll, $logTransfer)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getRedemptionStatus",
                                         array(
                                              'identifierType' => $identifierType,
                                              'fetchAll'       => $fetchAll,
                                              'logTransfer'    => $logTransfer,
                                         ));
    }

    /**
     * panelPanelistsGetStatus
     *
     * Description (from WSDL):
     * Returns the current panel status of a panelist and accepts single uid only if given.
     *
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
    public function panelPanelistsGetStatus($identifierType, $identifierValue)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getStatus",
                                         array(
                                              'identifierType'  => $identifierType,
                                              'identifierValue' => $identifierValue,
                                         ));
    }

    /**
     * panelPanelistsGetV1
     *
     * Description (from WSDL):
     * get information about a panelist
     *
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
    public function panelPanelistsGetV1($identifierType, $identifierValue)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getV1",
                                         array(
                                              'identifierType'  => $identifierType,
                                              'identifierValue' => $identifierValue,
                                         ));
    }

    /**
     * panelPanelistsGetValue
     *
     * Description (from WSDL):
     * Get a master data variable's value for one or multiple panelists
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $variableName
     *
     * @return array|mixed
     */
    public function panelPanelistsGetValue($identifierType, $identifierValue, $variableName)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.getValue",
                                         array(
                                              'identifierType'  => $identifierType,
                                              'identifierValue' => $identifierValue,
                                              'variableName'    => $variableName,
                                         ));
    }

    /**
     * panelPanelistsIsValidLogin
     *
     * Description (from WSDL):
     * Checks whether a panelist can login to the panel website with the login data specified.
     *
     * @param $loginType
     * @param $account
     * @param $password
     *
     * @return array|mixed
     */
    public function panelPanelistsIsValidLogin($loginType, $account, $password)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.isValidLogin",
                                         array(
                                              'loginType' => $loginType,
                                              'account'   => $account,
                                              'password'  => $password,
                                         ));
    }

    /**
     * panelPanelistsModifyPoints
     *
     * Description (from WSDL):
     * Adds or subtracts bonus points for a panelist's account. The panelist can be identified by different criteria (user_id, panelist code, pseudonym etc.). The transaction will be shown in the panelists' account balance history afterwards.
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $pointsValue
     * @param $pointsReason
     *
     * @return array|mixed
     */
    public function panelPanelistsModifyPoints($identifierType, $identifierValue, $pointsValue, $pointsReason)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.modifyPoints",
                                         array(
                                              'identifierType'  => $identifierType,
                                              'identifierValue' => $identifierValue,
                                              'pointsValue'     => $pointsValue,
                                              'pointsReason'    => $pointsReason,
                                         ));
    }

    /**
     * panelPanelistsSaveExternalAuthentication
     *
     * Description (from WSDL):
     * Establishes a mapping between a member of an external system (specified by the external_service_id and the external_service_authentication_value) and an EFS panelist. This can be used to implement seamless logins for EFS panelists coming from external web applications.
     *
     * @param $externalServiceId
     * @param $externalAuthenticationValue
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
    public function panelPanelistsSaveExternalAuthentication($externalServiceId, $externalAuthenticationValue, $identifierType, $identifierValue)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.saveExternalAuthentication",
                                         array(
                                              'externalServiceId'           => $externalServiceId,
                                              'externalAuthenticationValue' => $externalAuthenticationValue,
                                              'identifierType'              => $identifierType,
                                              'identifierValue'             => $identifierValue,
                                         ));
    }

    /**
     * panelPanelistsSendMail
     *
     * Description (from WSDL):
     * Sends mails to every panelist in the sample
     *
     * @param $identifierType
     * @param $identifierValues
     * @param $mailTemplate
     * @param $dispcodes
     * @param $sendOptions
     *
     * @return array|mixed
     */
    public function panelPanelistsSendMail($identifierType, $identifierValues, $mailTemplate, $dispcodes, $sendOptions)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.sendMail",
                                         array(
                                              'identifierType'   => $identifierType,
                                              'identifierValues' => $identifierValues,
                                              'mailTemplate'     => $mailTemplate,
                                              'dispcodes'        => $dispcodes,
                                              'sendOptions'      => $sendOptions,
                                         ));
    }

    /**
     * panelPanelistsUpdateStatus
     *
     * Description (from WSDL):
     * Updates the panel status of a panelist.
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $status
     *
     * @return array|mixed
     */
    public function panelPanelistsUpdateStatus($identifierType, $identifierValue, $status)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.panelists.updateStatus",
                                         array(
                                              'identifierType'  => $identifierType,
                                              'identifierValue' => $identifierValue,
                                              'status'          => $status,
                                         ));
    }

    /**
     * panelPromotionsGetStatus
     *
     * Description (from WSDL):
     * This service can be used to check whether an e-mail address has already been entered into the promotional system and what its status is. Invited only or already converted into a panelist.
     *
     * @param $recipientEmail
     *
     * @return array|mixed
     */
    public function panelPromotionsGetStatus($recipientEmail)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.promotions.getStatus",
                                         array(
                                              'recipientEmail' => $recipientEmail,
                                         ));
    }

    /**
     * panelPromotionsTellafriend
     *
     * Description (from WSDL):
     * Sends an invitation as e-mail to a friend.
     *
     * @param $promotionId
     * @param $recipientEmail
     * @param $header
     * @param $footer
     * @param $panelistUid
     *
     * @return array|mixed
     */
    public function panelPromotionsTellafriend($promotionId, $recipientEmail, $header, $footer, $panelistUid)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.promotions.tellafriend",
                                         array(
                                              'promotionId'    => $promotionId,
                                              'recipientEmail' => $recipientEmail,
                                              'header'         => $header,
                                              'footer'         => $footer,
                                              'panelistUid'    => $panelistUid,
                                         ));
    }

    /**
     * panelSamplesSendMail
     *
     * Description (from WSDL):
     * Sends mails to every panelist in the sample
     *
     * @param $sampleId
     * @param $mailTemplate
     * @param $dispcodes
     * @param $sendOptions
     *
     * @return array|mixed
     */
    public function panelSamplesSendMail($sampleId, $mailTemplate, $dispcodes, $sendOptions)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.samples.sendMail",
                                         array(
                                              'sampleId'     => $sampleId,
                                              'mailTemplate' => $mailTemplate,
                                              'dispcodes'    => $dispcodes,
                                              'sendOptions'  => $sendOptions,
                                         ));
    }

    /**
     * panelStatisticsGetStatus
     *
     * Description (from WSDL):
     * Panel status distribution
     *
     */
    public function panelStatisticsGetStatus()
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.statistics.getStatus",
                                         array());
    }

    /**
     * panelStatisticsGetVariables
     *
     * Description (from WSDL):
     * Dynamic panel status distribution
     *
     * @param $panelStatisticsFilter
     *
     * @return array|mixed
     */
    public function panelStatisticsGetVariables($panelStatisticsFilter)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.statistics.getVariables",
                                         array(
                                              'panelStatisticsFilter' => $panelStatisticsFilter,
                                         ));
    }

    /**
     * panelSurveysGetListCompleted
     *
     * Description (from WSDL):
     * Returns the list of EFS Panel surveys (surveys of type panel survey or master data survey) a panelist has completed so far
     *
     * @param $identifierType
     * @param $identifierValue
     *
     * @return array|mixed
     */
    public function panelSurveysGetListCompleted($identifierType, $identifierValue)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.surveys.getListCompleted",
                                         array(
                                              'identifierType'  => $identifierType,
                                              'identifierValue' => $identifierValue,
                                         ));
    }

    /**
     * surveyLanguagesAdd
     *
     * Description (from WSDL):
     * adds a new language
     *
     * @param $surveyId
     * @param $language
     *
     * @return array|mixed
     */
    public function surveyLanguagesAdd($surveyId, $language)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.languages.add",
                                         array(
                                              'surveyId' => $surveyId,
                                              'language' => $language,
                                         ));
    }

    /**
     * surveyLanguagesChange
     *
     * Description (from WSDL):
     * change language data
     *
     * @param $surveyId
     * @param $language
     *
     * @return array|mixed
     */
    public function surveyLanguagesChange($surveyId, $language)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.languages.change",
                                         array(
                                              'surveyId' => $surveyId,
                                              'language' => $language,
                                         ));
    }

    /**
     * surveyLanguagesDelete
     *
     * Description (from WSDL):
     * deletes a language
     *
     * @param $surveyId
     * @param $languageId
     *
     * @return array|mixed
     */
    public function surveyLanguagesDelete($surveyId, $languageId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.languages.delete",
                                         array(
                                              'surveyId'   => $surveyId,
                                              'languageId' => $languageId,
                                         ));
    }

    /**
     * surveyLanguagesGet
     *
     * Description (from WSDL):
     * get a language
     *
     * @param $surveyId
     * @param $languageId
     *
     * @return array|mixed
     */
    public function surveyLanguagesGet($surveyId, $languageId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.languages.get",
                                         array(
                                              'surveyId'   => $surveyId,
                                              'languageId' => $languageId,
                                         ));
    }

    /**
     * surveyLanguagesGetList
     *
     * Description (from WSDL):
     * get a list of all languages in a survey
     *
     * @param $surveyId
     *
     * @return array|mixed
     */
    public function surveyLanguagesGetList($surveyId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.languages.getList",
                                         array(
                                              'surveyId' => $surveyId,
                                         ));
    }

    /**
     * surveyParticipantsReset
     *
     * Description (from WSDL):
     * Resets a survey participation for participant. Existing survey data can be kept or deleted. The operation is allowed for personalized surveys only.
     *
     * @param $participantIdentifierType
     * @param $participantIdentifierValue
     * @param $surveyId
     * @param $resetType
     *
     * @return array|mixed
     */
    public function surveyParticipantsReset($participantIdentifierType, $participantIdentifierValue, $surveyId, $resetType)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.participants.reset",
                                         array(
                                              'participantIdentifierType'  => $participantIdentifierType,
                                              'participantIdentifierValue' => $participantIdentifierValue,
                                              'surveyId'                   => $surveyId,
                                              'resetType'                  => $resetType,
                                         ));
    }

    /**
     * surveyParticipantsSendMailV1
     *
     * Description (from WSDL):
     * Sends mails to every participiant in a survey. Will fail on anonymous surveys.
     *
     * @param $surveyId
     * @param $mailTemplate
     * @param $dispcodes
     * @param $sendOptions
     *
     * @return array|mixed
     */
    public function surveyParticipantsSendMailV1($surveyId, $mailTemplate, $dispcodes, $sendOptions)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.participants.sendMailV1",
                                         array(
                                              'surveyId'     => $surveyId,
                                              'mailTemplate' => $mailTemplate,
                                              'dispcodes'    => $dispcodes,
                                              'sendOptions'  => $sendOptions,
                                         ));
    }

    /**
     * surveyResultsGetSimpleStatisticsByPanelist
     *
     * Description (from WSDL):
     * getSimpleStatisticsByPanelist
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $surveyId
     * @param $includeVariables
     *
     * @return array|mixed
     */
    public function surveyResultsGetSimpleStatisticsByPanelist($identifierType, $identifierValue, $surveyId, $includeVariables)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.results.getSimpleStatisticsByPanelist",
                                         array(
                                              'identifierType'   => $identifierType,
                                              'identifierValue'  => $identifierValue,
                                              'surveyId'         => $surveyId,
                                              'includeVariables' => $includeVariables,
                                         ));
    }

    /**
     * surveyStatisticsGetAllCodesCount
     *
     * Description (from WSDL):
     * Returns the absolute number of participation for all disposition codes.
     *
     * @param $surveyId
     * @param $restriction
     *
     * @return array|mixed
     */
    public function surveyStatisticsGetAllCodesCount($surveyId, $restriction)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.statistics.getAllCodesCount",
                                         array(
                                              'surveyId'    => $surveyId,
                                              'restriction' => $restriction,
                                         ));
    }

    /**
     * surveyStatisticsGetCountBy
     *
     * Description (from WSDL):
     * Returns the absolute number of participation by disposition code/s and/or split-variable.
     *
     * @param $surveyId
     * @param $criteria
     * @param $restriction
     *
     * @return array|mixed
     */
    public function surveyStatisticsGetCountBy($surveyId, $criteria, $restriction)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.statistics.getCountBy",
                                         array(
                                              'surveyId'    => $surveyId,
                                              'criteria'    => $criteria,
                                              'restriction' => $restriction,
                                         ));
    }

    /**
     * surveyStatisticsGetGraph
     *
     * Description (from WSDL):
     * Returns the png graph which represents the participation.
     *
     * @param $surveyId
     * @param $lang
     * @param $restriction
     *
     * @return array|mixed
     */
    public function surveyStatisticsGetGraph($surveyId, $lang, $restriction)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.statistics.getGraph",
                                         array(
                                              'surveyId'    => $surveyId,
                                              'lang'        => $lang,
                                              'restriction' => $restriction,
                                         ));
    }

    /**
     * surveyStatisticsGetGross1
     *
     * Description (from WSDL):
     * Returns the total sample (Gross 1) for the project.
     *
     * @param $surveyId
     * @param $restriction
     * @param $splitVariable
     *
     * @return array|mixed
     */
    public function surveyStatisticsGetGross1($surveyId, $restriction, $splitVariable)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.statistics.getGross1",
                                         array(
                                              'surveyId'      => $surveyId,
                                              'restriction'   => $restriction,
                                              'splitVariable' => $splitVariable,
                                         ));
    }

    /**
     * surveyStatisticsGetGross2
     *
     * Description (from WSDL):
     * Returns the adjusted total sample (Gross 2) for the project.
     *
     * @param $surveyId
     * @param $restriction
     * @param $splitVariable
     *
     * @return array|mixed
     */
    public function surveyStatisticsGetGross2($surveyId, $restriction, $splitVariable)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.statistics.getGross2",
                                         array(
                                              'surveyId'      => $surveyId,
                                              'restriction'   => $restriction,
                                              'splitVariable' => $splitVariable,
                                         ));
    }

    /**
     * surveyStatisticsGetListOfCountsBy
     *
     * Description (from WSDL):
     * Returns a list of the absolute number/s of participation by disposition code/s and/or split-variable.
     *
     * @param $surveyId
     * @param $criteria
     * @param $restriction
     *
     * @return array|mixed
     */
    public function surveyStatisticsGetListOfCountsBy($surveyId, $criteria, $restriction)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.statistics.getListOfCountsBy",
                                         array(
                                              'surveyId'    => $surveyId,
                                              'criteria'    => $criteria,
                                              'restriction' => $restriction,
                                         ));
    }

    /**
     * surveyStatisticsGetNet
     *
     * Description (from WSDL):
     * Returns the net-participation for the project.
     *
     * @param $surveyId
     * @param $restriction
     * @param $splitVariable
     *
     * @return array|mixed
     */
    public function surveyStatisticsGetNet($surveyId, $restriction, $splitVariable)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.statistics.getNet",
                                         array(
                                              'surveyId'      => $surveyId,
                                              'restriction'   => $restriction,
                                              'splitVariable' => $splitVariable,
                                         ));
    }

    /**
     * surveySurveysAdd
     *
     * Description (from WSDL):
     * creates a new survey
     *
     * @param $surveyData
     *
     * @return array|mixed
     */
    public function surveySurveysAdd($surveyData)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.surveys.add",
                                         array(
                                              'surveyData' => $surveyData,
                                         ));
    }

    /**
     * surveySurveysCompile
     *
     * Description (from WSDL):
     * Compiles the survey. Creates all missing variable names and resets the participants if requested. You need the write ACL right to perform this action.
     *
     * @param $surveyId
     * @param $surveyCompilationType
     *
     * @return array|mixed
     */
    public function surveySurveysCompile($surveyId, $surveyCompilationType)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.surveys.compile",
                                         array(
                                              'surveyId'              => $surveyId,
                                              'surveyCompilationType' => $surveyCompilationType,
                                         ));
    }

    /**
     * surveySurveysGetExport
     *
     * Description (from WSDL):
     * Exports survey results according to the given configuration
     *
     * @param $exportConfig
     *
     * @return array|mixed
     */
    public function surveySurveysGetExport($exportConfig)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.surveys.getExport",
                                         array(
                                              'exportConfig' => $exportConfig,
                                         ));
    }

    /**
     * surveyTimelogGetList
     *
     * Description (from WSDL):
     * Returns a performance data structure for a survey.
     *
     * @param $surveyId
     *
     * @return array|mixed
     */
    public function surveyTimelogGetList($surveyId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.timelog.getList",
                                         array(
                                              'surveyId' => $surveyId,
                                         ));
    }

    /**
     * surveyTimelogReset
     *
     * Description (from WSDL):
     * Resets all performance data used when accessing the OSPE time log.
     *
     * @param $surveyId
     *
     * @return array|mixed
     */
    public function surveyTimelogReset($surveyId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=survey.timelog.reset",
                                         array(
                                              'surveyId' => $surveyId,
                                         ));
    }

    /**
     * efsMailtemplatesAdd
     *
     * Description (from WSDL):
     * Add mail template
     *
     * @param $mailtemplate
     *
     * @return array|mixed
     */
    public function efsMailtemplatesAdd($mailtemplate)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.mailtemplates.add",
                                         array(
                                              'mailtemplate' => $mailtemplate,
                                         ));
    }

    /**
     * efsMailtemplatesChange
     *
     * Description (from WSDL):
     * Change mail template data
     *
     * @param $mailtemplate
     * @param $id
     *
     * @return array|mixed
     */
    public function efsMailtemplatesChange($mailtemplate, $id)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.mailtemplates.change",
                                         array(
                                              'mailtemplate' => $mailtemplate,
                                              'id'           => $id,
                                         ));
    }

    /**
     * efsMailtemplatesDelete
     *
     * Description (from WSDL):
     * Delete mail template by id
     *
     * @param $id
     *
     * @return array|mixed
     */
    public function efsMailtemplatesDelete($id)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.mailtemplates.delete",
                                         array(
                                              'id' => $id,
                                         ));
    }

    /**
     * efsMailtemplatesGet
     *
     * Description (from WSDL):
     * Get mail template by id
     *
     * @param $id
     *
     * @return array|mixed
     */
    public function efsMailtemplatesGet($id)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.mailtemplates.get",
                                         array(
                                              'id' => $id,
                                         ));
    }

    /**
     * efsMailtemplatesGetList
     *
     * Description (from WSDL):
     * Get a list of mailtemplates
     *
     * @param $mailTemplateTypes
     *
     * @return array|mixed
     */
    public function efsMailtemplatesGetList($mailTemplateTypes)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.mailtemplates.getList",
                                         array(
                                              'mailTemplateTypes' => $mailTemplateTypes,
                                         ));
    }

    /**
     * efsOrganizationsAdd
     *
     * Description (from WSDL):
     * Adds an organization.
     *
     * @param $adminOrgRecord
     * @param $adminOrgIdentifierReturnType
     *
     * @return array|mixed
     */
    public function efsOrganizationsAdd($adminOrgRecord, $adminOrgIdentifierReturnType)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.organizations.add",
                                         array(
                                              'adminOrgRecord'               => $adminOrgRecord,
                                              'adminOrgIdentifierReturnType' => $adminOrgIdentifierReturnType,
                                         ));
    }

    /**
     * efsOrganizationsAddUser
     *
     * Description (from WSDL):
     * Can add and update an existing staff member to an organization.
     *
     * @param $adminUserIdentifierType
     * @param $adminUserIdentifierValue
     * @param $adminOrgIdentifierType
     * @param $adminOrgIdentifierValue
     *
     * @return array|mixed
     */
    public function efsOrganizationsAddUser($adminUserIdentifierType, $adminUserIdentifierValue, $adminOrgIdentifierType, $adminOrgIdentifierValue)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.organizations.addUser",
                                         array(
                                              'adminUserIdentifierType'  => $adminUserIdentifierType,
                                              'adminUserIdentifierValue' => $adminUserIdentifierValue,
                                              'adminOrgIdentifierType'   => $adminOrgIdentifierType,
                                              'adminOrgIdentifierValue'  => $adminOrgIdentifierValue,
                                         ));
    }

    /**
     * efsOrganizationsChange
     *
     * Description (from WSDL):
     * Sets an organization`s value.
     *
     * @param $adminOrgIdentifierType
     * @param $adminOrgIdentifierValue
     * @param $adminOrgField
     * @param $adminOrgValue
     *
     * @return array|mixed
     */
    public function efsOrganizationsChange($adminOrgIdentifierType, $adminOrgIdentifierValue, $adminOrgField, $adminOrgValue)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.organizations.change",
                                         array(
                                              'adminOrgIdentifierType'  => $adminOrgIdentifierType,
                                              'adminOrgIdentifierValue' => $adminOrgIdentifierValue,
                                              'adminOrgField'           => $adminOrgField,
                                              'adminOrgValue'           => $adminOrgValue,
                                         ));
    }

    /**
     * efsOrganizationsDelete
     *
     * Description (from WSDL):
     * Deletes an organization.
     *
     * @param $adminOrgIdentifierType
     * @param $adminOrgIdentifierValue
     *
     * @return array|mixed
     */
    public function efsOrganizationsDelete($adminOrgIdentifierType, $adminOrgIdentifierValue)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.organizations.delete",
                                         array(
                                              'adminOrgIdentifierType'  => $adminOrgIdentifierType,
                                              'adminOrgIdentifierValue' => $adminOrgIdentifierValue,
                                         ));
    }

    /**
     * efsOrganizationsGet
     *
     * Description (from WSDL):
     * Returns an organization.
     *
     * @param $adminOrgIdentifierType
     * @param $adminOrgIdentifierValue
     *
     * @return array|mixed
     */
    public function efsOrganizationsGet($adminOrgIdentifierType, $adminOrgIdentifierValue)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.organizations.get",
                                         array(
                                              'adminOrgIdentifierType'  => $adminOrgIdentifierType,
                                              'adminOrgIdentifierValue' => $adminOrgIdentifierValue,
                                         ));
    }

    /**
     * efsOrganizationsGetList
     *
     * Description (from WSDL):
     * Returns a list of organizations specified by a search value.
     *
     * @param $adminOrgField
     * @param $adminOrgValue
     * @param $pageFrom
     * @param $pageLimit
     *
     * @return array|mixed
     */
    public function efsOrganizationsGetList($adminOrgField, $adminOrgValue, $pageFrom, $pageLimit)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.organizations.getList",
                                         array(
                                              'adminOrgField' => $adminOrgField,
                                              'adminOrgValue' => $adminOrgValue,
                                              'pageFrom'      => $pageFrom,
                                              'pageLimit'     => $pageLimit,
                                         ));
    }

    /**
     * efsPrivilegesChangeRight
     *
     * Description (from WSDL):
     * Sets a right on an object for given team.
     *
     * @param $objectId
     * @param $groupId
     * @param $right
     *
     * @return array|mixed
     */
    public function efsPrivilegesChangeRight($objectId, $groupId, $right)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.privileges.changeRight",
                                         array(
                                              'objectId' => $objectId,
                                              'groupId'  => $groupId,
                                              'right'    => $right,
                                         ));
    }

    /**
     * efsStaffAdd
     *
     * Description (from WSDL):
     * Create a new staff member for the EFS admin interface To set the staff members organization you need to have the appropriate right. If no organization is set the staff member will inherit the creating staff members organization.
     *
     * @param $staffMember
     *
     * @return array|mixed
     */
    public function efsStaffAdd($staffMember)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.staff.add",
                                         array(
                                              'staffMember' => $staffMember,
                                         ));
    }

    /**
     * efsStaffChange
     *
     * Description (from WSDL):
     * Changes a staff members data
     *
     * @param $identifierType
     * @param $identifier
     * @param $changes
     *
     * @return array|mixed
     */
    public function efsStaffChange($identifierType, $identifier, $changes)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.staff.change",
                                         array(
                                              'identifierType' => $identifierType,
                                              'identifier'     => $identifier,
                                              'changes'        => $changes,
                                         ));
    }

    /**
     * efsStaffChangeRole
     *
     * Description (from WSDL):
     * Alters a staff members role within a given team
     *
     * @param $userId
     * @param $teamId
     * @param $newRole
     * @param $newOwnerUserId
     *
     * @return array|mixed
     */
    public function efsStaffChangeRole($userId, $teamId, $newRole, $newOwnerUserId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.staff.changeRole",
                                         array(
                                              'userId'         => $userId,
                                              'teamId'         => $teamId,
                                              'newRole'        => $newRole,
                                              'newOwnerUserId' => $newOwnerUserId,
                                         ));
    }

    /**
     * efsStaffDelete
     *
     * Description (from WSDL):
     * Deletes a staff member
     *
     * @param $identifierType
     * @param $identifier
     *
     * @return array|mixed
     */
    public function efsStaffDelete($identifierType, $identifier)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.staff.delete",
                                         array(
                                              'identifierType' => $identifierType,
                                              'identifier'     => $identifier,
                                         ));
    }

    /**
     * efsStaffGet
     *
     * Description (from WSDL):
     * Return information about a staff member
     *
     * @param $identifierType
     * @param $identifier
     *
     * @return array|mixed
     */
    public function efsStaffGet($identifierType, $identifier)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.staff.get",
                                         array(
                                              'identifierType' => $identifierType,
                                              'identifier'     => $identifier,
                                         ));
    }

    /**
     * efsStaffGetList
     *
     * Description (from WSDL):
     * Returns a list of EFS staff members
     *
     */
    public function efsStaffGetList()
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.staff.getList",
                                         array());
    }

    /**
     * efsStaffSendMail
     *
     * Description (from WSDL):
     * Sends e-mail to an administrator
     *
     * @param string $identifierType
     * @param        $identifierValue
     * @param        $mailTemplateData
     *
     * @return array|mixed
     */
    public function efsStaffSendMail($identifierType, $identifierValue, $mailTemplateData)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.staff.sendMail",
                                         array(
                                              'identifierType'   => $identifierType,
                                              'identifierValue'  => $identifierValue,
                                              'mailTemplateData' => $mailTemplateData,
                                         ));
    }

    /**
     * efsSystemGetRandomAztecGod
     *
     * Description (from WSDL):
     * Returns a random aztec god. Unleashes its power every sunday at 2 am when printed on blue paper. Please note that some gods may unleash evil powers!
     *
     */
    public function efsSystemGetRandomAztecGod()
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.system.getRandomAztecGod",
                                         array());
    }

    /**
     * efsSystemGetTime
     *
     * Description (from WSDL):
     * Returns the current server time
     *
     * @param $format
     *
     * @return array|mixed
     */
    public function efsSystemGetTime($format)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.system.getTime",
                                         array(
                                              'format' => $format,
                                         ));
    }

    /**
     * efsTeamsAdd
     *
     * Description (from WSDL):
     * Adds a new EFS team
     *
     * @param $team
     *
     * @return array|mixed
     */
    public function efsTeamsAdd($team)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.teams.add",
                                         array(
                                              'team' => $team,
                                         ));
    }

    /**
     * efsTeamsChange
     *
     * Description (from WSDL):
     * Changes the EFS team details
     *
     * @param $teamId
     * @param $team
     *
     * @return array|mixed
     */
    public function efsTeamsChange($teamId, $team)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.teams.change",
                                         array(
                                              'teamId' => $teamId,
                                              'team'   => $team,
                                         ));
    }

    /**
     * efsTeamsDelete
     *
     * Description (from WSDL):
     * Deletes an EFS team
     *
     * @param $teamId
     *
     * @return array|mixed
     */
    public function efsTeamsDelete($teamId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.teams.delete",
                                         array(
                                              'teamId' => $teamId,
                                         ));
    }

    /**
     * efsTeamsGet
     *
     * Description (from WSDL):
     * Retrieves the data of an EFS team
     *
     * @param $teamId
     *
     * @return array|mixed
     */
    public function efsTeamsGet($teamId)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.teams.get",
                                         array(
                                              'teamId' => $teamId,
                                         ));
    }

    /**
     * efsTeamsGetList
     *
     * Description (from WSDL):
     * Retrieves a list with the data of an EFS teams
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $page
     * @param $limit
     *
     * @return array|mixed
     */
    public function efsTeamsGetList($identifierType, $identifierValue, $page, $limit)
    {
      return $this->makeServiceLayerCall($this->url . "&method=efs.teams.getList",
                                         array(
                                              'identifierType'  => $identifierType,
                                              'identifierValue' => $identifierValue,
                                              'page'            => $page,
                                              'limit'           => $limit,
                                         ));
    }

    /**
     * panelExternalsurveysAdd
     *
     * Description (from WSDL):
     * Adds a new external survey
     *
     * @param $id
     * @param $title
     * @param $type
     * @param $attributes
     *
     * @return array|mixed
     */
    public function panelExternalsurveysAdd($id, $title, $type, $attributes)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.externalsurveys.add",
                                         array(
                                              'id'         => $id,
                                              'title'      => $title,
                                              'type'       => $type,
                                              'attributes' => $attributes,
                                         ));
    }

    /**
     * panelExternalsurveysAddParticipants
     *
     * Description (from WSDL):
     * Adds or updates the participation status of multiple participants in an external survey
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $panelistIdentifierType
     * @param $surveyParticipations
     *
     * @return array|mixed
     */
    public function panelExternalsurveysAddParticipants($identifierType, $identifierValue, $panelistIdentifierType, $surveyParticipations)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.externalsurveys.addParticipants",
                                         array(
                                              'identifierType'         => $identifierType,
                                              'identifierValue'        => $identifierValue,
                                              'panelistIdentifierType' => $panelistIdentifierType,
                                              'surveyParticipations'   => $surveyParticipations,
                                         ));
    }

    /**
     * panelExternalsurveysChange
     *
     * Description (from WSDL):
     * Updates an existing external survey with new attributes. The survey's external ID cannot be changed.
     *
     * @param $identifierType
     * @param $identifierValue
     * @param $title
     * @param $type
     * @param $attributes
     *
     * @return array|mixed
     */
    public function panelExternalsurveysChange($identifierType, $identifierValue, $title, $type, $attributes)
    {
      return $this->makeServiceLayerCall($this->url . "&method=panel.externalsurveys.change",
                                         array(
                                              'identifierType'  => $identifierType,
                                              'identifierValue' => $identifierValue,
                                              'title'           => $title,
                                              'type'            => $type,
                                              'attributes'      => $attributes,
                                         ));
    }
  }
