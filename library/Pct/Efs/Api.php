<?php
  /**
   * Pct_Efs_Api
   * The central object to call EFS API methods
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Api.php,v 1.22 2018/05/04 10:50:26 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_Api extends Pct_Efs_ApiHelper
  {
    // Singleton instances
    protected static $instance = array();

    // The EFS class for the particular version
    protected $efsApi = null;

    protected $version = '';

    /**
     * Creates the singleton instance
     *
     * @param                        $version
     * @param Zend_Db_Adapter_Mysqli $db
     * @param Zend_Config            $conf
     * @param array                  $params
     *
     * @return array|\Pct_Efs_Api
     */
    public static function getInstance($version, Zend_Db_Adapter_Mysqli $db = null, Zend_Config $conf, $params = array())
    {
      if (array_key_exists('externalurl', $params) !== false || array_key_exists('externalws', $params) !== false)
        return new self($version, $db, $conf, $params);

      if (self::$instance == null)
        self::$instance = new self($version, $db, $conf, $params);

      return self::$instance;
    }

    /**
     * Call the EFS API for the initiated version. Simply passes the function call the the API instance for the EFS version.
     *
     * @param string $method
     * @param array  $params
     *
     * @return mixed
     */
    public function __call($method, $params)
    {
      return call_user_func_array(array($this->efsApi, $method), $params);
    }

    /**
     * Singleton constructor. Initiates the an API instance for the given EFS
     * version which can then be used be the __call method.
     */
    protected function __construct($version, $db, $conf, $params = array())
    {
      // Check that API file exists
      $classFile = $this->getFileNameForEfsVersion($version);
      if (false == Zend_Loader::isReadable($classFile))
        throw new Exception('EFS API for version ' . $version . ' not available');

      $className     = $this->getClassNameForEfsVersion($version);
      $this->efsApi  = new $className();
      $this->version = $version;
      /** @noinspection PhpUndefinedMethodInspection */
      $this->efsApi->init($conf, $db, $params);
    }

    /**
     * Returns the filename of the API for the given EFS version.
     *
     * @param float $version
     *
     * @return string filename
     */
    protected function getFileNameForEfsVersion($version)
    {
      return str_replace('_', DIRECTORY_SEPARATOR, $this->getClassNameForEfsVersion($version)) . '.php';
    }

    /**
     * Return the name of the class that holds the API methods for the given EFS version
     *
     * @param string $version
     *
     * @return string
     */
    protected function getClassNameForEfsVersion($version)
    {
      $versionString = str_replace('.', '_', $version);
      return (string)'Pct_Efs_' . $versionString . '_Api';
    }
  }