<?php
  /**
   * Pct_Efs_11_Autoload 
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Autoload.php,v 1.2 2016/06/22 13:51:49 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Efs_11_Autoload extends Pct_Efs_10_9_Autoload 
  {
    protected $_efsVersionExtension = '11_'; 
  }