<?php
  /**
   * Pct_Maintenance_Logfile
   *
   * @package   zendframeworkLibrary
   * @version   $Id: Logfile.php,v 1.11 2016/07/27 08:05:05 faust Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: faust $
   */

  class Pct_Maintenance_Logfile
  {
    const DATE_FORMAT_ATOM  = 'c';
    const DATE_PATTERN_ATOM = '/^(([\d]{4})-([\d]{2}))-([\d]{2})([T]{1})(([\d]{2}):([\d]{2}):([\d]{2}))([\+]{1})(([\d]{2}:[\d]{2}))/';
    const DATE_FORMAT_EFS   = 'Y-m-d H:i:s';
    const DATE_PATTERN_EFS  = '/^(([\d]{4})-([\d]{2}))-([\d]{2}) (([\d]{2}):([\d]{2}):([\d]{2}))/';
    /**
     * Logger
     *
     * @var Zend_Log
     */
    protected $log = null;

    /**
     * __construct
     *
     */
    public function __construct()
    {
      $this->log = new Zend_Log(new Zend_Log_Writer_Null);
    }

    /**
     * logRotation
     * moves all entries in logfile prior to this month to another file and zips it (if $compress == true)
     * also works if $this->log writes to the same file that is to be rotated (usually logs/application.log)
     *
     * only works with a date-format of 'Y-m-d H:i:s' for now, so this is recommended:
     * either add to getLog()-method: $log->setTimestampFormat('Y-m-d H:i:s');
     * or     add to application.ini: resources.log.timestampFormat = "Y-m-d H:i:s"
     *
     * @param string $logFile
     * @param bool   $compress
     *
     * @return void
     */
    public function logRotation($logFile, $compress = true)
    {
      // date-format used by Zend_Log, make sure $fullFormat < gmdate() works and $formatCheck matches $fullFormat!
      $fullFormat     = self::DATE_FORMAT_ATOM;
      $formatCheck    = self::DATE_PATTERN_ATOM;
      $fullFormatEfs  = self::DATE_FORMAT_EFS;
      $formatCheckEfs = self::DATE_PATTERN_EFS;
      $dateFormat  = 'Y-m';

      // removed to allow for rotating any file while $this->log doesn't meet $fullFormat
      /*
      if ($this->log->getTimestampFormat() != $fullFormat)
      {
        $this->log->err('logRotation: log->getTimestampFormat() "' . $this->log->getTimestampFormat() . '" does not match fullFormat "' . $fullFormat . '" in logRotation, exiting.');
        return;
      }
      */

      $this->log->debug('Executing logRotation on "' . basename($logFile) . '".');

      $fInput = efs_models_filefunx::fopen($logFile, 'r');
      if ($fInput)
      {
        // dissect filename
        $fileParts  = pathinfo($logFile);
        $rotateFile = $fileParts['filename'] . '_%s.' . $fileParts['extension'];

        // find first valid date in file
        $firstDate = '';
        $row       = '';
        while (!efs_models_filefunx::feof($fInput))
        {
          $row         = efs_models_filefunx::fgets($fInput);
          $fullDate    = substr($row, 0, strlen(gmdate($fullFormat)));
          $fullDateEfs = substr($row, 0, strlen(gmdate($fullFormatEfs)));

          // check DATE_FORMAT_ATOM (new)
          if (preg_match($formatCheck, $fullDate))
          {
            $firstDate = $fullDate;
            break;
          }

          // check DATE_FORMAT_EFS (old)
          if (preg_match($formatCheckEfs, $fullDateEfs))
          {
            $firstDate = $fullDateEfs;
            break;
          }

        }
        efs_models_filefunx::fclose($fInput);

        // account for old PHP-versions, NOTE: this works for $dateFormat = 'Y-m' but may not if day and/or time is included!
        if (gmdate('Y', strtotime('FIRST DAY OF THIS MONTH')) == '1970')
          $checkDate = gmdate($dateFormat);
        else
          $checkDate = gmdate($dateFormat, strtotime('FIRST DAY OF THIS MONTH'));

        // check if there's something to do
        if ($firstDate == '')
        {
          $this->log->debug('No valid date found in logfile, exiting.');
          return;
        }
        if (substr($row, 0, strlen(gmdate($dateFormat))) >= $checkDate)
        {
          $this->log->debug('Logfile starts at: ' . $firstDate . ', no need to rotate.');
          return;
        }
        $this->log->debug('Logfile starts at: ' . $firstDate . ', starting rotation.');

        // open outfile
        $outFile = $fileParts['dirname'] . '/' . sprintf($rotateFile, substr($firstDate, 0, strlen(gmdate($dateFormat))));
        $fOutput = efs_models_filefunx::fopen($outFile, 'w');
        if (!$fOutput)
        {
          $this->log->err('Could not open "' . $outFile . '".');
          return;
        }

        // open input file
        $fInput = efs_models_filefunx::fopen($logFile, 'r');
        $count  = 0;
        while (!efs_models_filefunx::feof($fInput))
        {
          $row = efs_models_filefunx::fgets($fInput);

          // valid date in row?
          $fullDate = substr($row, 0, strlen(gmdate($fullFormat)));
          if (preg_match($formatCheck, $fullDate))
          {
            // entry from current month?
            $rowDate = substr($row, 0, strlen(gmdate($dateFormat)));
            if ($rowDate >= $checkDate)
              break;
          }

          if (preg_match($formatCheckEfs, $fullDate))
          {
            // entry from current month?
            $rowDate = substr($row, 0, strlen(gmdate($dateFormat)));
            if ($rowDate >= $checkDate)
              break;
          }

          efs_models_filefunx::fwrite($fOutput, $row);
          $count++;
        }
        efs_models_filefunx::fclose($fInput);

        // close archive file and zip it (if required)
        efs_models_filefunx::fclose($fOutput);
        if ($compress)
        {
          file_put_contents("compress.zlib://" . $outFile . ".gz", file_get_contents($outFile));
          efs_models_filefunx::removeFile($outFile);
          $outFile .= ".gz";
        }

        $this->log->debug('Rotation finished, file created: "' . basename($outFile) . '", containing ' . $count . ' lines.');

        // cut $count lines from beginning of original logfile
        $newFile = new SplFileObject($logFile . '_new', 'w');
        foreach (new LimitIterator(new SplFileObject($logFile), $count) as $line)
          $newFile->fwrite($line);

        efs_models_filefunx::removeFile($logFile);
        efs_models_filefunx::renameFile($logFile . '_new', $logFile);
        efs_models_filefunx::setRights($logFile);
      }
      else
      {
        $this->log->err('Could not open "' . $logFile . '".');
      }
    }

    /**
     * Sets logger
     *
     * @param Zend_Log $log logger
     */
    public function setLog(Zend_Log $log)
    {
      $this->log = $log;
    }
  }
