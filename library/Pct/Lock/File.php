<?php
  /**
   * Handle lock files.
   * Quick howto:
   * $lockInstance = new Pct_Lock_File();
   * if($lockInstance->lock('ModuleControllerAction') == 0) {
   *   // function did not return expiration timestamp but 0 so lock file was not created
   *   throw new Expcetion('Feature with lock ModuleControllerAction is still locked');
   * }
   * // Lock was aquired so do cool stuff and don't forget to unlock afterwards
   * $lockInstance->unlock('ProjectModuleControllerAction')
   *
   * @author umschlag
   */
  class Pct_Lock_File
  {
    /**
     * How long is the lock file valid?
     *
     * @var integer
     */
    public $timeout = 1800;

    /**
     * What directory is used for the lock file
     *
     * @var string null
     */
    public $diretory = null;

    /**
     * List of lockfiles.
     *
     * @var array null
     */
    public $lockfile = null;

    /**
     *
     * @return the integer
     */
    public function getTimeout()
    {
      return $this->timeout;
    }

    /**
     *
     * @param $timeout
     *
     * @return $this
     */
    public function setTimeout($timeout)
    {
      $this->timeout = $timeout;
      return $this;
    }

    /**
     * The directory where the lock file is created
     *
     * @return the string
     */
    public function getDiretory()
    {
      if ($this->diretory == null)
      {
        $this->setDiretory(constant('APPLICATION_PATH'));
      }
      return $this->diretory;
    }

    /**
     *
     * @param string $diretory
     *
     * @return $this
     */
    public function setDiretory($diretory)
    {
      $this->diretory = $diretory;
      return $this;
    }

    /**
     * Creates and return the name of the lockfile
     *
     * @param string $id
     *
     * @return string
     */
    public function getLockfile($id)
    {
      if (null == $this->lockfile[$id])
      {
        $this->setLockfile($this->getDiretory() . '/' . $id . '.lock', $id);
      }
      return $this->lockfile[$id];
    }

    /**
     * Sets the name of the lock file
     *
     * @param string $lockfile
     * @param string $id
     *
     * @return $this
     */
    public function setLockfile($lockfile, $id = 'default')
    {
      $this->lockfile[$id] = $lockfile;
      return $this;
    }

    /**
     * Removes all non characters or number
     *
     * @param mixed $id
     *
     * @return mixed
     */
    public function idToFilename($id)
    {
      return preg_replace('/[^\w\d]/', '', $id);
    }

    /**
     * Creates the lockfile
     *
     * @param string $id
     *
     * @return int expire
     */
    public function lock($id)
    {
      $id = $this->idToFilename($id);
      if ($this->isLocked($id))
      {
        return 0;
      }

      $expire = (int)gmdate('U') + (int)$this->getTimeout();
      file_put_contents($this->getLockfile($id), $expire);
      return $expire;
    }

    /**
     * Checks if the lock file exists and if the timestamp is still higher than
     * current time + timeout
     *
     * @param string $id
     *
     * @return boolean
     */
    public function isLocked($id)
    {
      $id = $this->idToFilename($id);
      if (false == file_exists($this->getLockfile($id)))
        return false;
      $timeStamp = file_get_contents($this->getLockfile($id));
      if ((int)$timeStamp < gmdate('U'))
      {
        $this->unlock($id);
        return false;
      }
      return true;
    }

    /**
     * Unlinks the lockfile for the given ID
     *
     * @param string $id
     *
     * @return boolean|NULL
     */
    public function unlock($id)
    {
      $id = $this->idToFilename($id);
      if (file_exists($this->getLockfile($id)))
      {
        return unlink($this->getLockfile($id));
      }
      return null;
    }
  }