<?php
  /**
   * Pct_Ip_Network
   *
   * @package   zendframeworkLibrary
   *
   * @version   $Id: Network.php,v 1.12 2013/10/16 09:28:22 funke Exp $
   * @copyright (c) QuestBack http://www.questback.com
   * @author    $Author: funke $
   */

  class Pct_Ip_Network
  {
    const GATE3 = '87.79.30.2';

    protected $localIps = null;

    /**
     * isGlobalparkOfficeGermany
     *
     * @param string $ip
     *
     * @return boolean
     */
    public function isGlobalparkOfficeGermany($ip)
    {
      if ($ip == self::GATE3)
      {
        return true;
      }
      return false;
    }

    /**
     * Checks if the given Ip is within 127.0.0.1 and 127.255.255.254
     *
     * @param string $ip
     *
     * @return bool
     */
    public function isLocalhost($ip)
    {
      // The max ranges for the localhost IPs
      $ipMin = ip2long('127.0.0.1');
      $ipMax = ip2long('127.255.255.254');

      // The IP to check
      $ipLong = ip2long($ip);

      // IP must be bigger than smallest
      if ($ipLong < $ipMin)
        return false;

      // IP must be smaller than max
      if ($ipLong > $ipMax)
        return false;

      // Within range
      return true;
    }

    /**
     * Scans all IPs from the currect machine
     *
     * @param string $ip
     *
     * @return bool $isLocalIp
     */
    public function isLocalIp($ip)
    {
      if ($this->localIps == null)
      {
        $stringIps = shell_exec('/sbin/ip -o -f inet addr');
        preg_match_all(
          '/ inet (\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/',
          $stringIps, $matches);
        $this->localIps = $matches[1];
      }

      foreach ($this->localIps as $localIp)
      {
        if ($ip == $localIp)
          return true;
      }
      return false;
    }

    /**
     * Checks if the given IP is one from the local machine or from the QuestBack
     * office. Usually only those are allowed to call actions for e.g. cronjobs.
     *
     * @param string $ip
     *
     * @return bool
     */
    public function isCronAllowed($ip)
    {
      return (self::isGlobalparkOfficeGermany($ip) || self::isLocalIp($ip));
    }
  }
